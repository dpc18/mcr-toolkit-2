// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

    // References

    // 1) "3D forward and back-projection for X-ray CT using separable footprints."
    //    Long, Yong, Jeffrey A. Fessler, and James M. Balter.
    //    IEEE Transactions on Medical Imaging 29.11 (2010): 1839-1850.

    // 2) Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
    //    Cone-beam reprojection using projection-matrices.
    //    IEEE transactions on medical imaging, 22(10), 1202-1214.

// Backprojection kernels for flat panel detectors
// Supports arbitrary source, detector offsets and detector rotation

#ifndef DD_v25_BACKPROJECT_FLAT
#define DD_v25_BACKPROJECT_FLAT

// ***                  ***
// *** Function Headers ***
// ***                  ***

    // Flat panel, cy_detector = 0
    __global__ void DD_backproject_flat(const float *proj, float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                        const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                        int use_affine, const unsigned short* mask_header, const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset,
                                        float dv, float vc, float limit_angular_range, int use_norm_vol, float *norm_vol, float proj_weight);
    
    // Flat panel after rebinning, cy_detector = 3
    __global__ void DD_backproject_flatb(const float *proj, float *vol, const int3 n3xyz, int nz0, const float3 d3xyz, const int nu, const int nv,
                                         const float *aff, const float* pm_, const float rect_rect_factor, const float3 src,
                                         const float du, const float uc, const float vc, const float dsd,
                                         int use_affine, const unsigned short* mask_header, const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset,
                                         float dv, float limit_angular_range, int use_norm_vol, float *norm_vol, float3 puvs, float proj_weight);

// ***                            ***
// *** DEVICE - MAIN - Flat Panel ***
// ***    cy_detector = 0         ***

__global__ void DD_backproject_flat(const float *proj, float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                    const float *aff, const float* pm_, float rect_rect_factor, float3 src,
                                    int use_affine, const unsigned short* mask_header, const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset,
                                    float dv, float vc, float limit_angular_range, int use_norm_vol, float *norm_vol, float proj_weight)
{

    // Setup
    
        int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
        int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
        
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
        
        int iz;
        int i, j;
        float gamma[1];
        float us[4] = {0.0,0.0,0.0,0.0};
        float vs[2] = {0.0,0.0}; // ,0.0,0.0};
        float x,y,z,x2,y2,z2,nx,ny,nz,w;
        float min_u, max_u, min_v, max_v; //, v0, v1;
        float r, s1, s2, f1, f2;
        int idxu, idxv;
        float C;
        float weight2 = 1; // helical weighting factor

        nx = n3xyz.x; ny = n3xyz.y; nz = nz0; // n3xyz.z; Don't use the buffer size for geo calculations!
        nx = nx/2.0f; ny = ny/2.0f; nz = nz/2.0f;

        float weight, signy1, signy2, signx1, signx2, signz1, signz2; // sign

        unsigned int nuv = nu*nv;
        
        size_t idx, idx0;
    
    // Check that we are in the implied mask
        
        // All voxels along z will also be outside of the mask, so return.
        // Setting x, y only once assumes no affine.
        x = ix; y = iy;
        
        if (implicit_mask == 1) {
            
            if (sqrtf((x - nx)*(x - nx) + (y - ny)*(y - ny)) > fminf(nx,ny)) return;
            
        }

    // Precompute reused quantities
        
        idx0 = (iy*n3xyz.x) + ix;
        
        float pmv2, pmv3;
        float u1, u2, u3, u4, u5, u6, u7, u8;
        if (use_affine == 0) {
         
            pmv2 = pm_[4]*ix + pm_[5]*iy + pm_[7];
            pmv3 = pm_[8]*ix + pm_[9]*iy + pm_[11];
            
            signy1 = iy - 0.5f;
            signy2 = iy + 0.5f;
            signx1 = ix - 0.5f;
            signx2 = ix + 0.5f;

            u1 = pm_[0]*signx1 + pm_[1]*signy1 + pm_[3];
            u2 = pm_[8]*signx1 + pm_[9]*signy1 + pm_[11];

            u3 = pm_[0]*signx2 + pm_[1]*signy1 + pm_[3];
            u4 = pm_[8]*signx2 + pm_[9]*signy1 + pm_[11];

            u5 = pm_[0]*signx1 + pm_[1]*signy2 + pm_[3];
            u6 = pm_[8]*signx1 + pm_[9]*signy2 + pm_[11];

            u7 = pm_[0]*signx2 + pm_[1]*signy2 + pm_[3];
            u8 = pm_[8]*signx2 + pm_[9]*signy2 + pm_[11];
            
        }
        else { // use_affine == 1
         
            x2 = ix - nx + 0.5f;
            y2 = iy - ny + 0.5f;
            
            u1 = x2*aff[0] + y2*aff[3] + aff[9];
            u2 = x2*aff[1] + y2*aff[4] + aff[10];
            u3 = x2*aff[2] + y2*aff[5] + aff[11];
            
        }
        
    // Use ILP to reduce overhead
    // for (iz = zp; iz < zp+ZSTEP; iz++) {
    for (iz = zstart; iz < zend; iz++) {
        
        // Reset area integral and update z, reset x, y
        
                r = 0;
                x = ix; y = iy; z = iz;
            
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
                
            idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
            
        // Apply affine transform, compute spatial weighting function
        // ANTs (WIN v2.1.0) compatible affine transform

            if (use_affine == 1) {
            
                // ANTs (WIN v2.1.0) compatible affine transform
                // x2 = x - nx + 0.5f;
                // y2 = y - ny + 0.5f;
                z2 = z - nz;

                x = u1 + z2*aff[6];
                y = u2 + z2*aff[7];
                z = u3 + z2*aff[8];

                x = x + nx - 0.5f;
                y = y + ny - 0.5f;
                z = z + nz;

                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = pm_[4]*x + pm_[5]*y + pm_[6] *signz1 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz1 + pm_[11];
                vs[0] = vs[0]/w;

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = pm_[4]*x + pm_[5]*y + pm_[6] *signz2 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz2 + pm_[11];
                vs[1] = vs[1]/w;
            
            }
            else {
             
                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = ( pmv2 + pm_[6] *signz1 ) / ( pmv3 + pm_[10]*signz1 );

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = ( pmv2 + pm_[6] *signz2 ) / ( pmv3 + pm_[10]*signz2 );
                
            }
        
            max_v = ceilf(vs[1] - 0.5f);
            if ( max_v < 0 ) continue;
            
        // continue if we are outside of the 3D reconstruction mask, explicit_mask = 2 only
        // skip the z range indicators in the first two slices
            
            if (explicit_mask == 2) {
                
                // if (mask[idx + 2*n3xyz.x*n3xyz.y] == 0) continue;
                
                // Now use a relative index. Also, the header is no longer included.
                if (mask[idx] == 0) continue;
                
            }
            
        // Limit angular range
        // Ignore the contribution of rays based on their divergence from the central ray along the z axis at the detector.
            
            // weight2 = limit_z(vs, nv, dv, vc, limit_angular_range);
            weight2 = limit_z( d3xyz.z*(z - nz)-src.z, nv, dv, vc, limit_angular_range);
            
            if ( weight2 < 0 ) {
             
                continue;
                
            }

        // Compute spatial weight

            // lsd^2 included in rect factor
            weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            // weight *= weight;
            
        // Trapezoidal approximation along u
        
            // NOTE: It is possible to reduce the computation / memory use by only computing the used points based on the rotation.
            // However, using the same axis for all rays (i.e. ignoring the fan angles), does not allow the use of different axes
            // within the same projection, creating small errors.
            
            if (use_affine == 1) {
            
                signy1 = y - 0.5f;
                signy2 = y + 0.5f;
                signx1 = x - 0.5f;
                signx2 = x + 0.5f;

                us[0] = pm_[0]*signx1 + pm_[1]*signy1 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx1 + pm_[9]*signy1 + pm_[10]*z + pm_[11];
                us[0] = us[0]/w;

                us[1] = pm_[0]*signx2 + pm_[1]*signy1 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx2 + pm_[9]*signy1 + pm_[10]*z + pm_[11];
                us[1] = us[1]/w;

                us[2] = pm_[0]*signx1 + pm_[1]*signy2 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx1 + pm_[9]*signy2 + pm_[10]*z + pm_[11];
                us[2] = us[2]/w;

                us[3] = pm_[0]*signx2 + pm_[1]*signy2 + pm_[2]*z + pm_[3];
                w =     pm_[8]*signx2 + pm_[9]*signy2 + pm_[10]*z + pm_[11];
                us[3] = us[3]/w;
            
            }
            else {

                us[0] = ( u1 + pm_[2]*z ) / ( u2 + pm_[10]*z );

                us[1] = ( u3 + pm_[2]*z ) / ( u4 + pm_[10]*z );

                us[2] = ( u5 + pm_[2]*z ) / ( u6 + pm_[10]*z );

                us[3] = ( u7 + pm_[2]*z ) / ( u8 + pm_[10]*z );
                
            }

            sort4(us,us+1,us+2,us+3);

            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);
            
            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) continue;
        
        // Normalization

            // Normalize relative to magnified rect area
            // C = weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );

            // Normalize relative to magnified trap area
            // C = weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));
            
            // Normalize relative to magnified trap - rect area
            C = weight2 * weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            

        // Area integration
            
            for (i = 0; i < max_u - min_u + 1; i++) {

                idxu = min_u + i;

                // rect
                // s1 = min_u + i + du/2;
                // s2 = min_u + i - du/2;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);

                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                // f1 = gamma[0];
                f1 = gamma[0];

                for (j = 0; j < max_v - min_v + 1; j++) {

                    idxv = min_v + j;

                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);

                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = f1*gamma[0];

                    idxv = idxv*nu + idxu;

                    // check to make sure we have not gone out of bounds before updating the projection
                    if (idxv < nuv && idxv >= 0) { // && f1 == f1

                        r += proj[idxv]*f2; //*f2; //*C;

                    }

                }

            }
            
        // Add to voxel
        
            r *= C;

            // replace this with an atomic operation
            // Why does this seem to make it faster???
            // Does it have something to do with atomic operations not going through cache?
            // vol[idx] += r;
            if (r == r) { // mitigate occasional divide by zero issues (this should only be an issue when the wrong version of the operator, e.g. xz vs. yz, is called)

                // vol[idx] += r;
                atomicAdd(&vol[idx],r);
                
                if ( use_norm_vol == 1 ) {
                 
                    // atomicAdd(&norm_vol[idx],1);
                    atomicAdd(&norm_vol[idx],weight2*proj_weight);
                    
                }

            }
    
    }

}

// ***                               ***
// *** DEVICE - MAIN - Rebinned Flat ***
// *** cy_detector = 3               ***

__global__ void DD_backproject_flatb(const float *proj, float *vol, const int3 n3xyz, int nz0, const float3 d3xyz, const int nu, const int nv,
                                     const float *aff, const float* pm_, const float rect_rect_factor, const float3 src,
                                     const float du, const float uc, const float vc, const float dsd,
                                     int use_affine, const unsigned short* mask_header, const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset,
                                     float dv, float limit_angular_range, int use_norm_vol, float *norm_vol, float3 puvs, float proj_weight)
{

    // Setup
    
        int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
        int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
    
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
    int iz;
    int i, j;
    float gamma[1];
    float us[4] = {0.0,0.0,0.0,0.0};
    float vs[2] = {0.0,0.0}; // ,0.0,0.0};
    float x,y,z,x2,y2,z2,nx,ny,nz,w;
    float min_u, max_u, min_v, max_v; //, v0, v1;
    float r, s1, s2, f1, f2;
    int idxu, idxv;
    float C;
    float weight2 = 1; // helical weighting factor
    
    nx = n3xyz.x; ny = n3xyz.y; nz = nz0; // n3xyz.z; Don't use the buffer size for geo calculations!
    nx = nx/2; ny = ny/2; nz = nz/2;
    
    float signy1, signy2, signx1, signx2, signz1, signz2; //, ang; // , ang2; // sign, weight, 
    
    unsigned int nuv = nu*nv;
    
    size_t idx, idx0;
    
    // check that we are in the mask
    // All voxels along z will also be outside of the mask, so return.
    // Setting x, y only once assumes no affine.
    
        x = ix; y = iy;
    
        if (implicit_mask == 1) {
            
            if (sqrtf((x - nx)*(x - nx) + (y - ny)*(y - ny)) > fminf(nx,ny)) return;
            
        }
    
    // Precompute reused quantities
        
        idx0 = (iy*n3xyz.x) + ix;
        
        float pmv1,pmv2,pmv3;
        float u1, u2, u3, u4;
        if (use_affine == 0) {
            
            pmv1 = pm_[0]*ix + pm_[1]*iy + pm_[3];
            pmv2 = pm_[4]*ix + pm_[5]*iy + pm_[7];
            pmv3 = pm_[8]*ix + pm_[9]*iy + pm_[11];
            
            signy1 = iy - 0.5f;
            signy2 = iy + 0.5f;
            signx1 = ix - 0.5f;
            signx2 = ix + 0.5f;
            
            u1 = d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y;
            u2 = d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y;
            u3 = d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y;
            u4 = d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y;
            
        }
        else { // use_affine == 1
         
            x2 = ix - nx + 0.5f;
            y2 = iy - ny + 0.5f;
            
            u1 = x2*aff[0] + y2*aff[3] + aff[9];
            u2 = x2*aff[1] + y2*aff[4] + aff[10];
            u3 = x2*aff[2] + y2*aff[5] + aff[11];
            
        }
        
    // convert puvs to a unit vector for parallel beam calculations
        
        float puvs_mag = sqrtf( puvs.x * puvs.x + puvs.y * puvs.y + puvs.z * puvs.z );
        // float r_mag;
        // float v_prime;
    
    // Use ILP to reduce overhead
    // for (iz = zp; iz < zp+ZSTEP; iz++) {
    for (iz = zstart; iz < zend; iz++) {
        
        // Reset area integral and update z, reset x, y
        
            r = 0;
            x = ix; y = iy; z = iz;
            
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
            
            idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
        
        // Apply affine transform, compute spatial weighting function
        // ANTs (WIN v2.1.0) compatible affine transform
            
            if (use_affine == 1) {
            
                // ANTs (WIN v2.1.0) compatible affine transform
                // x2 = x - nx + 0.5f;
                // y2 = y - ny + 0.5f;
                z2 = z - nz;

                x = u1 + z2*aff[6];
                y = u2 + z2*aff[7];
                z = u3 + z2*aff[8];

                x = x + nx - 0.5f;
                y = y + ny - 0.5f;
                z = z + nz;

                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = pm_[4]*x + pm_[5]*y + pm_[6] *signz1 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz1 + pm_[11];
                vs[0] = vs[0]/w;

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = pm_[4]*x + pm_[5]*y + pm_[6] *signz2 + pm_[7];
                w =     pm_[8]*x + pm_[9]*y + pm_[10]*signz2 + pm_[11];
                vs[1] = vs[1]/w;
            
            }
            else {
             
                // Compute v first to make sure we are inside the projection along z
                signz1 = (z-0.5f);
                signz2 = (z+0.5f);

                vs[0] = ( pmv2 + pm_[6] *signz1 ) / ( pmv3 + pm_[10]*signz1 );

                min_v = ceilf(vs[0] - 0.5f);
                if ( min_v >= nv ) return;

                vs[1] = ( pmv2 + pm_[6] *signz2 ) / ( pmv3 + pm_[10]*signz2 );
                
            }
            
            max_v = ceilf(vs[1] - 0.5f);
            if ( max_v < 0 ) continue;
            
        // continue if we are outside of the 3D reconstruction mask, explicit_mask = 2 only
        // skip the z range indicators in the first two slices
            
            if (explicit_mask == 2) {
                
                // if (mask[idx + 2*n3xyz.x*n3xyz.y] == 0) continue;
                
                // Now use a relative index. Also, the header is no longer included.
                if (mask[idx] == 0) continue;
                
            }
            
        // Limit angular range
        // Ignore the contribution of rays based on their divergence from the central ray along the z axis at the detector.
            
            // weight2 = limit_z(vs, nv, dv, vc, limit_angular_range);
            weight2 = limit_z( d3xyz.z*(z - nz)-src.z, nv, dv, vc, limit_angular_range);
            
            if ( weight2 < 0 ) {
             
                continue;
                
            }
            
        // Compute spatial weight

            // lso included in rect factor
            // weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            // weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*((vs[0]+vs[1])/2.0f - vc)-src.z)*(d3xyz.z*((vs[0]+vs[1])/2.0f - vc)-src.z) );
            // weight *= weight;
 
        // Trapezoidal approximation along u
            
            // NOTE: It is possible to reduce the computation / memory use by only computing the used points based on the rotation.
            // However, using the same axis for all rays (i.e. ignoring the fan angles), does not allow the use of different axes
            // within the same projection, creating small errors.
            
            if (use_affine == 0) {
                
                us[0] = ( u1 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[0] = ( us[0] / du + uc );

                us[1] = ( u2 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[1] = ( us[1] / du + uc );

                us[2] = ( u3 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[2] = ( us[2] / du + uc );

                us[3] = ( u4 + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[3] = ( us[3] / du + uc );
                
            }
            else {
             
                signy1 = y - 0.5f;
                signy2 = y + 0.5f;
                signx1 = x - 0.5f;
                signx2 = x + 0.5f;

                us[0] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[0] = ( us[0] / du + uc );

                us[1] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[1] = ( us[1] / du + uc );

                us[2] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[2] = ( us[2] / du + uc );

                us[3] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
                us[3] = ( us[3] / du + uc );
                
            }

            sort4(us,us+1,us+2,us+3);
        
            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);
            
            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) continue;
            
            // v_prime = 700 * (d3xyz.z*(z - nz)-src.z) / ( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) );
            
            // weight = 700 * rsqrtf( 700*700 + ( du*(us[0] - uc) ) * ( du*(us[0] - uc) ) + v_prime * v_prime );
            
        // Weight and area normalization factor

            // Normalize relative to magnified rect area
            // C = weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );

            // Normalize relative to magnified trap area
            // C = weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));

            // Normalize relative to magnified trap-rect area
            // C = weight2 * weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            C = weight2 * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            
        // Compute 2D area integral w/ weights
        // loop over u first since v is much less expensive to compute 
            
            for (i = 0; i < max_u - min_u + 1; i++) {
                
                idxu = min_u + i;

                // rect
                // s1 = min_u + i + 0.5f;
                // s2 = min_u + i - 0.5f;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);

                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                // f1 = gamma[0];
                f1 = gamma[0];
                
                for (j = 0; j < max_v - min_v + 1; j++) {
                 
                    idxv = min_v + j;

                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);
                    
                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = gamma[0];
                    
                    idxv = idxv*nu + idxu;
                    
                    // check to make sure we have not gone out of bounds before updating the projection
                    if (idxv < nuv && idxv >= 0) { // && f1 == f1

                        r += proj[idxv]*f2; //*f2; //*C;

                    }
                    
                }
                
            }
            
        // Add integrated intensity to volume
        
            r *= C;

            // replace this with an atomic operation
            // Why does this seem to make it faster???
            // Does it have something to do with atomic operations not going through cache?
            // vol[idx] += r;
            if (r == r) { // mitigate occasional divide by zero issues (this should only be an issue when the wrong version of the operator, e.g. xz vs. yz, is called)

                atomicAdd(&vol[idx],r);
                // atomicAdd(&norm_vol[idx],1);
                
                if ( use_norm_vol == 1 ) {
                 
                    // atomicAdd(&norm_vol[idx],1);
                    atomicAdd(&norm_vol[idx],weight2*proj_weight);
                    
                }

            }
    
    }

}

#endif // DD_v25_BACKPROJECT_FLAT