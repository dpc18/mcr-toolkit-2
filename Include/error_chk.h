// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
#ifndef ERROR_CHK

    #define ERROR_CHK

    #include <stdio.h>
    #include <stdlib.h>
    #include "cuda_runtime.h"

    #ifdef MEX_COMPILE_FLAG
    #define ERR( x ) mexErrMsgTxt( x );
    #else
    #define ERR( x ) printf("Error: %s", x); // exit(-1);
    #endif

    #ifdef MEX_COMPILE_FLAG
    #define MSG( x ) mexPrintf( x );
    #else
    #define MSG( x ) printf("Error: %s", x); // exit(-1);
    #endif

    // Make this static to avoid multiple definition errors
    static void check( int marker ) // cudaError_t error, int marker)
    {

        // Error Checking
        cudaError_t error = cudaGetLastError();
        if (error != cudaSuccess) {

            // mexPrintf("Error reported at marker %i\n",marker);
            
            // mexPrintf("Please clear any outstanding memory allocations and reset the device(s) in use.\n");
            
            // mexErrMsgTxt("CUDA Failed\n");
            
            char outstring[250];
            
            sprintf(outstring, "Error reported at marker %i, %s\n", marker, cudaGetErrorString(error));
            
            MSG( outstring );
            
            MSG("Please clear any outstanding memory allocations and reset the device(s) in use.\n");
            
            cudaDeviceReset();
            
            ERR("CUDA Failed\n");

        }

    }
    
#endif