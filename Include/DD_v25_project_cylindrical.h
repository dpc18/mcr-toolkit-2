// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

    // References

    // 1) "3D forward and back-projection for X-ray CT using separable footprints."
    //    Long, Yong, Jeffrey A. Fessler, and James M. Balter.
    //    IEEE Transactions on Medical Imaging 29.11 (2010): 1839-1850.

    // 2) Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    //    Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    //    Physics in Medicine & Biology, 49(11), 2209.

// Projection kernels for 3rd generation CT scanners with cylindrical detectors
// Supports arbitrary source offsets.
// Assumes the v (projection) and z (global and object coordinate system) axes are parallel for efficiency.

#ifndef DD_PROJECT_v25_CYLINDRICAL
#define DD_PROJECT_v25_CYLINDRICAL

// ***           ***
// *** FUNCTIONS ***
// ***           ***
    
    // NOTE: Cylindrical operators use rect approximation along v and the trap approximation along u.
    
    // cy_detector = 1) cylindrical

        __global__ void DD_project_cyl(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                       float rect_rect_factor, float3 src, float du, float uc, float vc,
                                       float dsd, float dso, float db, const unsigned short* mask_header,
                                       const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset, float dv,
                                       float theta, float det_z );

    // cy_detector = 2) rebinned cylindrical
        
        __global__ void DD_project_cylb(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                        float rect_rect_factor, float3 src, float du, float uc, float vc,
                                        float dsd, float dso, float db, const unsigned short* mask_header,
                                        const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset, float dv,
                                        float theta, float det_z, float3 puvs);
    
// ***                             ***
// *** DEVICE - MAIN - Cylindrical ***
// *** cy_detector = 1             ***

__global__ void DD_project_cyl(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                               float rect_rect_factor, float3 src, float du, float uc, float vc,
                               float dsd, float dso, float db, const unsigned short* mask_header,
                               const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset, float dv,
                               float theta, float det_z )
{

    // A) Obtain x, y, and z coordinates for the current voxel and declare other variables
	int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
	int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
    
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
        
        int iz;
        int i, j;
        float gamma[1];
        float us[4] = {0.0,0.0,0.0,0.0};
        float vs[2] = {0.0,0.0}; // ,0.0,0.0};
        float x,y,z,nx,ny,nz;
        float min_u, max_u, min_v, max_v; //, v0, v1;
        float s1, s2, f1, f2; // r
        int idxu, idxv;
        float C;

        // nx = n3xyz.x; ny = n3xyz.y; nz = nz0; // n3xyz.z; Don't use the buffer size for geo calculations!
        // nx = nx/2; ny = ny/2; nz = nz/2;

        // Why was this set to e.g. nx / 2 instead of (nx - 1) / 2?
        nx = (n3xyz.x - 1.0f) / 2.0f;
        ny = (n3xyz.y - 1.0f) / 2.0f; 
        nz = (nz0 - 1.0f) / 2.0f;

        float weight; // , signy1, signy2, signx1, signx2, signz1, signz2, ang; // sign

        unsigned int nuv = nu*nv;

        size_t idx, idx0;
        
        idx0 = (iy*n3xyz.x) + ix;
    
    // check that we are in the mask
    // All voxels along z will also be outside of the mask, so return.
    // Setting x, y only once assumes no affine.
        
        x = ix; y = iy;
    
        if (implicit_mask == 1) {
            
            // Use nx/2 instead of (nx-1)/2 to match Matlab masks
            if (sqrtf( sq(x - n3xyz.x/2.0f) + sq(y - n3xyz.y/2.0f) ) > fminf(nx,ny)) return;
            
        }
        
    // Precompute reused quantities
        
        float theta_r = theta * pi_val / 180.0f;
        float cos_th = cosf( theta_r );
        float sin_th = sinf( theta_r );

        // Since we will counter-rotate the object and source, treat theta as zero here
        // float q2x = (src.x + dso * cos_th ) / dsd;
        // float q2y = (src.y + dso * sin_th ) / dsd;
        
        float sx =  src.x * cos_th + src.y * sin_th;
        float sy = -src.x * sin_th + src.y * cos_th;
        
        float q2x = (sx + dso) / dsd;
        float q2y = sy / dsd;

        float cq = sq( q2x ) + sq( q2y ) - 1.0f;
        
    // Precompute u detector indices
        
        float q1x, q1y, aq, bq, scale, Dx, Dy, Dz;

        // Solve for the u indices
        // Counter-rotate the object coordinates instead of rotating the detector coordinates 
        // to avoid messy conditional trig formulas for arcsine and arccosine.
        
            float xm5 = d3xyz.x * ( x - nx - 0.5f );
            float xp5 = d3xyz.x * ( x - nx + 0.5f );
            float ym5 = d3xyz.y * ( y - ny - 0.5f );
            float yp5 = d3xyz.y * ( y - ny + 0.5f );
            
            // us[0]: x - 0.5f, y - 0.5f
            q1x   =  xm5 * cos_th + ym5 * sin_th; q1x = (q1x - sx) / dsd;
            q1y   = -xm5 * sin_th + ym5 * cos_th; q1y = (q1y - sy) / dsd;
            aq    = sq( q1x ) + sq( q1y );
            bq    = 2.0f * ( q1x*q2x + q1y*q2y );
            scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
            us[0] = ( asinf( scale * q1y + q2y ) ) / db + uc; // - theta_r
            
            // us[1]: x + 0.5f, y - 0.5f
            q1x =  xp5 * cos_th + ym5 * sin_th; q1x = (q1x - sx) / dsd;
            q1y = -xp5 * sin_th + ym5 * cos_th; q1y = (q1y - sy) / dsd;
            aq    = sq( q1x ) + sq( q1y );
            bq    = 2.0f * ( q1x*q2x + q1y*q2y );
            scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
            us[1] = ( asinf( scale * q1y + q2y ) ) / db + uc; // - theta_r
            
            // us[2]: x - 0.5f, y + 0.5f
            q1x   =  xm5 * cos_th + yp5 * sin_th; q1x = (q1x - sx) / dsd;
            q1y   = -xm5 * sin_th + yp5 * cos_th; q1y = (q1y - sy) / dsd;
            aq    = sq( q1x ) + sq( q1y );
            bq    = 2.0f * ( q1x*q2x + q1y*q2y );
            scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
            us[2] = ( asinf( scale * q1y + q2y ) ) / db + uc; // - theta_r
            
            // us[3]: x + 0.5f, y + 0.5f
            q1x   =  xp5 * cos_th + yp5 * sin_th; q1x = (q1x - sx) / dsd;
            q1y   = -xp5 * sin_th + yp5 * cos_th; q1y = (q1y - sy) / dsd;
            aq    = sq( q1x ) + sq( q1y );
            bq    = 2.0f * ( q1x*q2x + q1y*q2y );
            scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
            us[3] = ( asinf( scale * q1y + q2y ) ) / db + uc; // - theta_r
            
            sort4(us,us+1,us+2,us+3);
        
            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);
            
            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) return;
            
        // Precompute centered values in x and y for use in computing the z, v coordinate
            
            Dx = d3xyz.x * ( x - nx );
            Dy = d3xyz.y * ( y - ny );
            
            q1x =  Dx * cos_th + Dy * sin_th; q1x = (q1x - sx) / dsd;
            q1y = -Dx * sin_th + Dy * cos_th; q1y = (q1y - sy) / dsd;
            
            aq = sq( q1x ) + sq( q1y );
            bq = 2.0f * ( q1x*q2x + q1y*q2y );
            
            scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
        
    // Use ILP to reduce overhead
    // for (iz = zp; iz < zp+ZSTEP; iz++) {
    for (iz = zstart; iz < zend; iz++) {
        
        // update z, reset x, y
        
            x = ix; y = iy; z = iz;
            
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
            
            idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
            
        // Project onto the v, z axis
        // Quit early if we are off of the detector
            
            Dz    = d3xyz.z * ( z - nz - 0.5f );
            vs[0] = ( scale * Dz + (1.0f - scale) * src.z - det_z) / dv + vc;
            
            min_v = ceilf(vs[0] - 0.5f);
            if ( min_v >= nv ) return;
            
            Dz    = d3xyz.z * ( z - nz + 0.5f );
            vs[1] = ( scale * Dz + (1.0f - scale) * src.z - det_z) / dv + vc;
            
            max_v = ceilf(vs[1] - 0.5f);
            if ( max_v < 0 ) continue;

        // v24: Use the mask explicitly rather than pre-applying to the volume, since we may
        // only be access a small portion of the data on the GPU.
            
            if (explicit_mask == 2) {
                
                if (mask[idx] == 0) continue;
                
            }
            
        // No point in projecting zero values
            
            C = vol[idx];
            if ( (C == 0) ) continue;
            
        // Compute spatial weight
 
            // lsd included in rect factor
            weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            weight *= weight;
            
        // Weight and area normalization factor

            // Normalize relative to magnified rect area
            // C *= weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );
        
            // Normalize relative to magnified trap area
            // C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));

            // Normalize relative to magnified trap-rect area
            C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            
        // Use weights to perform projection
            
            for (i = 0; i < max_u - min_u + 1; i++) {
                
                idxu = min_u + i;

                // rect
                // s1 = min_u + i + 0.5f;
                // s2 = min_u + i - 0.5f;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);

                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                // f1 = gamma[0];
                f1 = gamma[0];
                
                for (j = 0; j < max_v - min_v + 1; j++) {
                 
                    idxv = min_v + j;

                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = C*f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);
                    
                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = gamma[0];
                    
                    idxv = idxv*nu + idxu;
                    
                    // check to make sure we have not gone out of bounds before updating the projection
                    // if (idxu < BUFFER*nuv && idxu >= 0) {
                    if (idxv < nuv && idxv >= 0 && f2 == f2) {

                        // proj[idxv] += C*F1[i]*F2[j]; // will fail due to simultaneous read/write operations between threads
                        // atomicAdd(proj+idxv,f2); // *C); // atomic add addresses this problem
                        
                        // Add to double precision projection data to avoid random rounding
                        
                        #if __CUDA_ARCH__ < 600
                        
                            atomicAdd_old(proj+idxv,f2);
                        
                        #else
                        
                            atomicAdd(proj+idxv,f2);
                        
                        #endif
                        

                    }

                }
                
            }
    
    }

}


// ***                                      ***
// *** DEVICE - MAIN - Rebinned Cylindrical ***
// *** cy_detector = 2                      ***

__global__ void DD_project_cylb(double *proj, const float *vol, int3 n3xyz, int nz0, float3 d3xyz, int nu, int nv,
                                float rect_rect_factor, float3 src, float du, float uc, float vc,
                                float dsd, float dso, float db, const unsigned short* mask_header,
                                const unsigned short* mask, int implicit_mask, int explicit_mask, int z_offset, float dv,
                                float theta, float det_z, float3 puvs)
{

    // Setup

        int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
        int iy = (blockIdx.y * blockDim.y) + threadIdx.y;
    
    // Check the z range if we are using an explicit mask
    // when zstart = zend, the kernel does nothing

        // Absolute indices
        int zstart = z_offset;
        int zend   = z_offset + n3xyz.z; // one slice past the end slice

        if (explicit_mask > 0) {

            // restrict the operation to the intersection of our volume chunk and what is inside of the mask
            zstart = max( zstart, mask_header[iy*n3xyz.x + ix]);
            zend   = min( zend, mask_header[n3xyz.x*n3xyz.y + iy*n3xyz.x + ix]);

            // return early if there is no work to do inside of the mask
            if ( zend <= zstart ) return;

        }
    
    // Declare variables
        
        int iz;
        int i, j;
        float gamma[1];
        float us[4] = {0.0,0.0,0.0,0.0};
        float vs[2] = {0.0,0.0}; // ,0.0,0.0};
        float x,y,z,nx,ny,nz;
        float min_u, max_u, min_v, max_v; //, v0, v1;
        float s1, s2, f1, f2; // r
        int idxu, idxv;
        float C;

        // nx = n3xyz.x; ny = n3xyz.y; nz = nz0; // n3xyz.z; Don't use the buffer size for geo calculations!
        // nx = nx/2; ny = ny/2; nz = nz/2;

        // Why was this set to e.g. nx / 2 instead of (nx - 1) / 2?
        nx = (n3xyz.x - 1.0f) / 2.0f;
        ny = (n3xyz.y - 1.0f) / 2.0f; 
        nz = (nz0 - 1.0f) / 2.0f;

        float weight; // , signy1, signy2, signx1, signx2, signz1, signz2, ang; // sign

        unsigned int nuv = nu*nv;

        size_t idx, idx0;
        
        idx0 = (iy*n3xyz.x) + ix;
    
    // check that we are in the mask
    // All voxels along z will also be outside of the mask, so return.
    // Setting x, y only once assumes no affine.

        x = ix; y = iy;

        if (implicit_mask == 1) {

            if (sqrtf((x - nx)*(x - nx) + (y - ny)*(y - ny)) > fminf(nx,ny)) return;

        }
        
    // Precompute reused quantities
        
        float theta_r = theta * pi_val / 180.0f;
        float cos_th = cosf( theta_r );
        float sin_th = sinf( theta_r );
        
        float sx =  src.x * cos_th + src.y * sin_th;
        float sy = -src.x * sin_th + src.y * cos_th;
        
        float q2x = (sx + dso) / dsd;
        float q2y = sy / dsd;

        float cq = sq( q2x ) + sq( q2y ) - 1.0f;
        
    // Precompute variables for u

        // convert puvs to a unit vector for parallel beam calculations
        float puvs_mag = sqrtf( puvs.x * puvs.x + puvs.y * puvs.y + puvs.z * puvs.z );

        float signy1 = y - 0.5f;
        float signy2 = y + 0.5f;
        float signx1 = x - 0.5f;
        float signx2 = x + 0.5f;
            
    // Precompute centered values in x and y for use in computing the z, v coordinate
        
        float Dx = d3xyz.x * ( x - nx );
        float Dy = d3xyz.y * ( y - ny );
        float Dz;

        float q1x =  Dx * cos_th + Dy * sin_th; q1x = (q1x - sx) / dsd;
        float q1y = -Dx * sin_th + Dy * cos_th; q1y = (q1y - sy) / dsd;

        float aq = sq( q1x ) + sq( q1y );
        float bq = 2.0f * ( q1x*q2x + q1y*q2y );

        float scale = ( -bq + sqrtf( sq( bq ) - 4.0f * aq * cq ) ) / ( 2.0f * aq );
        
    // Use ILP to reduce overhead
    for (iz = zstart; iz < zend; iz++) {
        
        // update z, reset x, y
        
            x = ix; y = iy; z = iz;
            
        // Use the relative z index to do volume lookups, since we are working with a volume buffer
        // NOTE: This may result in illegal indexing if the iz bounds checks are not performed.
            
            idx = ( (size_t) (iz - z_offset) )*( (size_t) n3xyz.x*n3xyz.y ) + idx0;
            
        // Compute u coordinates (parallel beam)

            us[0] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
            us[0] = ( us[0] / du + uc );

            us[1] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy1 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
            us[1] = ( us[1] / du + uc );

            us[2] = ( d3xyz.x*(signx1 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
            us[2] = ( us[2] / du + uc );

            us[3] = ( d3xyz.x*(signx2 - nx) * puvs.x + d3xyz.y*(signy2 - ny) * puvs.y + d3xyz.z*(z - nz) * puvs.z ) / puvs_mag;
            us[3] = ( us[3] / du + uc );

            sort4(us,us+1,us+2,us+3);

            min_u = ceilf(us[0] - 0.5f);
            max_u = ceilf(us[3] - 0.5f);

            // continue if we go out of bounds
            if ( ( max_u < 0 ) || ( min_u >= nu ) ) continue;
            
        // Project onto the v, z axis
        // Quit early if we are off of the detector
            
            Dz    = d3xyz.z * ( z - nz - 0.5f );
            vs[0] = ( scale * Dz + (1.0f - scale) * src.z - det_z) / dv + vc;
            
            min_v = ceilf(vs[0] - 0.5f);
            if ( min_v >= nv ) return;
            
            Dz    = d3xyz.z * ( z - nz + 0.5f );
            vs[1] = ( scale * Dz + (1.0f - scale) * src.z - det_z) / dv + vc;
            
            max_v = ceilf(vs[1] - 0.5f);
            if ( max_v < 0 ) continue;

        // v24: Use the mask explicitly rather than pre-applying to the volume, since we may
        // only be access a small portion of the data on the GPU.
            
            if (explicit_mask == 2) {
                
                if (mask[idx] == 0) continue;
                
            }
            
        // No point in projecting zero values
            
            C = vol[idx];
            if ( (C == 0) ) continue;
            
        // Compute spatial weight
 
            // lsd included in rect factor
            weight = rsqrtf( (d3xyz.x*(x - nx)-src.x)*(d3xyz.x*(x - nx)-src.x) + (d3xyz.y*(y - ny)-src.y)*(d3xyz.y*(y - ny)-src.y) + (d3xyz.z*(z - nz)-src.z)*(d3xyz.z*(z - nz)-src.z) );
            // weight *= weight;
            // weight = 1.0;
            
        // Weight and area normalization factor

            // Normalize relative to magnified rect area
            // C *= weight * rect_rect_factor * (1 / ( ( (us[3]-us[0])) * (vs[3]-vs[0]) ) );
        
            // Normalize relative to magnified trap area
            // C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (2 / ( ((vs[3]-vs[0])+(vs[2]-vs[1])) ));

            // Normalize relative to magnified trap-rect area
            C *= weight * rect_rect_factor * (2 / ( ((us[3]-us[0])+(us[2]-us[1])) )) * (1 / ( vs[1] - vs[0] ) );
            
        // Use weights to perform projection
            
            for (i = 0; i < max_u - min_u + 1; i++) {
                
                idxu = min_u + i;

                // rect
                // s1 = min_u + i + 0.5f;
                // s2 = min_u + i - 0.5f;
                // f1 = fmaxf(fminf(s1,us[1]) - fmaxf(s2,us[0]),0);
                // f1 = fmaxf(fminf(s1,us[3]) - fmaxf(s2,us[0]),0);

                // trap
                s1 = min_u + i + 0.5f;
                s2 = min_u + i - 0.5f;
                gamma[0] = 0.0f;
                gamma_trap(us, s2, s1, gamma);
                // f1 = gamma[0];
                f1 = gamma[0];
                
                for (j = 0; j < max_v - min_v + 1; j++) {
                 
                    idxv = min_v + j;

                    // rect
                    s1 = min_v + j + 0.5f;
                    s2 = min_v + j - 0.5f;
                    f2 = C*f1*fmaxf(fminf(s1,vs[1]) - fmaxf(s2,vs[0]),0);
                    
                    // trap
                    // s1 = min_v + j + 0.5f;
                    // s2 = min_v + j - 0.5f;
                    // gamma[0] = 0.0f;
                    // gamma_trap(vs, s2, s1, gamma);
                    // f2 = gamma[0];
                    
                    idxv = idxv*nu + idxu;
                    
                    // check to make sure we have not gone out of bounds before updating the projection
                    // if (idxu < BUFFER*nuv && idxu >= 0) {
                    if (idxv < nuv && idxv >= 0 && f2 == f2) {

                        // proj[idxv] += C*F1[i]*F2[j]; // will fail due to simultaneous read/write operations between threads
                        // atomicAdd(proj+idxv,f2); // *C); // atomic add addresses this problem
                        
                        // Add to double precision projection data to avoid random rounding
                        
                        #if __CUDA_ARCH__ < 600
                        
                            atomicAdd_old(proj+idxv,f2);
                        
                        #else
                        
                            atomicAdd(proj+idxv,f2);
                        
                        #endif
                        

                    }

                }
                
            }
    
    }

}

#endif // DD_PROJECT_v25_CYLINDRICAL
