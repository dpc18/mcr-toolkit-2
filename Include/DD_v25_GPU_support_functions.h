// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Authors: Sam Johnston, PhD; Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// References

    // 1) "3D forward and back-projection for X-ray CT using separable footprints."
    //    Long, Yong, Jeffrey A. Fessler, and James M. Balter.
    //    IEEE Transactions on Medical Imaging 29.11 (2010): 1839-1850.

    // 2) Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    //    Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    //    Physics in Medicine & Biology, 49(11), 2209.

#ifndef GPU_SUPPORT

    #define GPU_SUPPORT
    
        #include "cufft.h"

        #if __CUDA_ARCH__ < 600

            __device__ double atomicAdd_old(double* address, double val);

        #endif

        __global__ void  apply_mask(float *vol, const unsigned short *mask, const int3 n3xyz);
        __global__ void  copy_dp_to_sp(float *proj_sp, const double *proj, int nu, int nv);
        __global__ void  add_dp_to_sp(float *proj_sp, const double *proj, int nu, int nv);
        __device__ void  sort2(float* a, float* b);
        __device__ void  sort4(float* a, float* b, float* c, float* d);
        __device__ void  gamma_trap(float* us, float s1, float s2, float* gamma);
        __device__ float limit_z(float zoffset, int nv, float dv, float vc,  float limit_angular_range);

        __global__ void realtocomplex(const float *proj, cufftComplex *cproj, const int nu, const int nv, const int nu_fft);
        __global__ void complextoreal(const cufftComplex *cproj, float *proj, const int nu, const int nv, const int nu_fft);
        __global__ void filterprojection(cufftComplex *fftproj, const float *filt, const int nu_fft, const int nv);
        
        __global__ void volume_normalization(float *vol_out, const float *norm_vol, const int3 n3xyz, const int factor);
        
        __global__ void imscale(float *proj, float val, int nu, int nv);

        // device functions

        // double precision atomic add function for older GPUs which do not support the native Pascal/CUDA 8.0+ version.
        // This is somewhat slow compared to the native operator in Pascal/CUDA 8.0+.
        #if __CUDA_ARCH__ < 600

            __device__ double atomicAdd_old(double* address, double val)
            {

                unsigned long long int* address_as_ull = (unsigned long long int*)address; 
                unsigned long long int old = *address_as_ull, assumed; 
                do { 

                    assumed = old; 
                    old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed))); 

                } while (assumed != old); // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)

                return __longlong_as_double(old);

            }

        #endif

        // v24: No longer used to avoid copying all volume and mask data to the GPU when using managed memory.
        // Set values outside of the mask to zero to make the (DD) projection operations more efficient.
        // i.e. only check the volume within the projection kernel vs. checking the volume and the mask
        // Also used to make sure the TV graidient is always zero outside of the explicit mask.
        // Works with explicit_mask = 2.
        __global__ void apply_mask(float *vol, const unsigned short *mask, const int3 n3xyz) {

            int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
            int iy = (blockIdx.y * blockDim.y) + threadIdx.y;

            if (ix >= n3xyz.x) return;

            unsigned int idx0 = (iy*n3xyz.x) + ix;
            unsigned int idx1;

            for (int iz = 0; iz < n3xyz.z; iz++) {

                idx1 = iz*n3xyz.x*n3xyz.y + idx0;

                // if ( mask[idx1 + 2*n3xyz.x*n3xyz.y] == 0 ) {
                if ( mask[idx1] == 0 ) { // in newer versions of the code with preallocation, we have already removed the header...

                    vol[idx1] = 0;

                }

            }

        }

        // Copy operation with precision reduction.
        // v24: Input and output order flipped to match cudaMemcpy semantics.
        __global__ void copy_dp_to_sp(float *proj_sp, const double *proj, int nu, int nv)
        {

            int iu = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( iu >= nu ) return; // v25: return if index is outside of the projection
            
            int iv = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( iv >= nv ) return; // v25: return if index is outside of the projection

            unsigned int idx = iv*nu + iu;

            proj_sp[idx] = proj[idx];

        }

        // += operation with precision reduction.
        // v24: Input and output order flipped to match cudaMemcpy semantics.
        __global__ void add_dp_to_sp(float *proj_sp, const double *proj, int nu, int nv)
        {

            int iu = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( iu >= nu ) return; // v25: return if index is outside of the projection
            
            int iv = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( iv >= nv ) return; // v25: return if index is outside of the projection

            unsigned int idx = iv*nu + iu;

            proj_sp[idx] += proj[idx];

        }

        //     __device__ float interp_1D(const float *table, const float idx, const int max) {
        //         
        //         int idx1 = floorf(idx);
        //         int idx2 = ceilf(idx);
        //         
        //         if ( idx2 > max-1 ) {
        //             
        //             return table[max-1];
        //             
        //         }
        //         
        //         if ( idx1 < 0 ) {
        //             
        //             return table[0];
        //             
        //         }
        //         
        //         return (idx-idx1)*table[idx1] + (idx2-idx)*table[idx2];
        //         
        //     }
        //     
        //     __device__ float atan2_table(const float val, const float uc, const float du, const float dsd)
        //     {
        //      
        //         return atan2f( du*( val - uc ), dsd);
        //         
        //     }

        __device__ void sort2(float* a, float* b)
        {
            if (*a > *b)
            {
                float tmp = *b;
                *b = *a;
                *a = tmp;
            }
        }

        __device__ void sort4(float* a, float* b, float* c, float* d)
        {

            sort2(a,b);
            sort2(c,d);
            sort2(a,c);
            sort2(b,d);
            sort2(b,c);

            // sort3
            // sort2(b, c);
            // sort2(a, b);
            // sort2(b, c);

        }

        __device__ void gamma_trap(float* us, float s1, float s2, float* gamma)
        {

            // Gamma 1
            //     gamma1 = @(b1,b2) (b2 > b1)*((1/(2*(tau1-tau0))) * ((b2 - tau0)^2 - (b1 - tau0)^2));
            //     gamma1 = gamma1(max(s1,tau0),min(s2,tau1)); gamma1(isnan(gamma1)) = 0;
            float b1, b2, gamma1;
            b1 = fmaxf(s1,us[0]);
            b2 = fminf(s2,us[1]);

            if (b2 > b1) {

                gamma1 = (1/(2*(us[1]-us[0]))) * ((b2 - us[0])*(b2 - us[0]) - (b1 - us[0])*(b1 - us[0]));

            }
            else {

                gamma1 = 0;

            }

            // Check for nan
            if (gamma1 == gamma1) { 

                gamma[0] += gamma1;

            }

            // Gamma 2
            //     gamma2 = @(b1,b2) (b2 > b1)*(b2 - b1);
            //     gamma2 = gamma2(max(s1,tau1),min(s2,tau2)); gamma2(isnan(gamma2)) = 0;
            b1 = fmaxf(s1,us[1]);
            b2 = fminf(s2,us[2]);

            if (b2 > b1) {

                gamma1 = b2 - b1;

            }
            else {

                gamma1 = 0;

            }

            // Check for nan
            if (gamma1 == gamma1) { 

                gamma[0] += gamma1;

            }

            // Gamma 3
            //     gamma3 = @(b1,b2) (b2 > b1)*((1/(2*(tau3-tau2))) * ((b1 - tau3)^2 - (b2 - tau3)^2));
            //     gamma3 = gamma3(max(s1,tau2),min(s2,tau3)); gamma3(isnan(gamma3)) = 0;
            b1 = fmaxf(s1,us[2]);
            b2 = fminf(s2,us[3]);

            if (b2 > b1) {

                gamma1 = (1/(2*(us[3]-us[2]))) * ((b1 - us[3])*(b1 - us[3]) - (b2 - us[3])*(b2 - us[3]));

            }
            else {

                gamma1 = 0;

            }

            // Check for nan
            if (gamma1 == gamma1) { 

                gamma[0] += gamma1;

            }

        }
        
        // Helper function for limiting the z range of contributing projection data
        __device__ float limit_z(float zoffset, int nv, float dv, float vc,  float limit_angular_range) {

            float weight2 = 1.0; // use this weight if limit_angular_range == 0

            if ( (limit_angular_range > 0) && (limit_angular_range < 1.00001) ) { // Q

                // Case: compromise between dose efficiency (using all measurements) and z resolution

                // "q"
                // weight2 = fabsf( ( (vs[0]+vs[1])/2.0f - vc ) / ( ( (float) nv ) / 2.0f ) );
                weight2 = fabsf( zoffset / ( dv * ( (float) nv ) / 2.0f ) );

                if ( weight2 < limit_angular_range ) {

                    weight2 = 1.0f;

                }
                else if ( weight2 < 1 ) {

                    weight2 = cosf( ( pi_val / 2.0f ) * ( weight2 - limit_angular_range ) / ( 1.0f - limit_angular_range ) );

                    weight2 *= weight2;

                }
                else {

                    weight2 = -1; // continue flag

                }

            }
            else if (limit_angular_range > 1.00001) { // Case: e.g. effectively limit to ~180 degrees

                // applicable functions are those that sum to one everywhere (e.g. cos^2 over +/- 180 degrees), otherwise there will be angle dependent artifacts
                // and z inconsistencies (should work up to a pitch of 1.0)

                weight2 = fabsf( zoffset );
                
                if ( weight2 <= limit_angular_range ) {
                    
                    weight2 = 1.0f;
                    
                }
                else { // exclusion
                    
                    weight2 = -1; // continue flag
                    
                }

                // if ( weight2 < limit_angular_range ) { // full weight region
                // 
                //     // weight2 = cosf( ( pi_val / 2.0f ) * ( weight2 ) / ( limit_angular_range ) );
                //     // 
                //     // weight2 *= weight2;
                // 
                //     weight2 = 1.0;
                // 
                // }
                // else { // exclusion
                // 
                //     weight2 = -1; // continue flag
                // 
                // }

            }

            return weight2;

        }

        __global__ void realtocomplex(const float *proj, cufftComplex *cproj, const int nu, const int nv, const int nu_fft)
        {
            // thread index
            int u = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( u >= nu_fft ) return; // v25: return if index is outside of the projection

            int v = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( v >= nv ) return; // v25: return if index is outside of the projection

            unsigned int uv = u + (v*nu);
            unsigned int uv2 = u + (v*nu_fft);

            if (u < nu) {

                cproj[uv2].x = proj[uv];
                cproj[uv2].y = 0.0;

            }
            else {

                cproj[uv2].x = 0.0;
                cproj[uv2].y = 0.0;

            }

        }

        __global__ void complextoreal(const cufftComplex *cproj, float *proj, const int nu, const int nv, const int nu_fft)
        {
            // thread index
            int u = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( u >= nu ) return; // v25: return if index is outside of the projection

            int v = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( v >= nv ) return; // v25: return if index is outside of the projection

            unsigned int uv = u + (v*nu);
            unsigned int uv2 = u + (v*nu_fft);

            proj[uv] = cproj[uv2].x;

        }

        __global__ void filterprojection(cufftComplex *fftproj, const float *filt, const int nu_fft, const int nv)
        {
            // thread index
            int u = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( u >= nu_fft ) return; // v25: return if index is outside of the projection

            int v = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( v >= nv ) return; // v25: return if index is outside of the projection

            int uv = u + (v*nu_fft);

            fftproj[uv].x *= filt[uv];
            fftproj[uv].y *= filt[uv];

        }

        __global__ void volume_normalization(float *vol_out, const float *norm_vol, const int3 n3xyz, const int factor)
        {

            int ix = (blockIdx.x * blockDim.x) + threadIdx.x;
            int iy = (blockIdx.y * blockDim.y) + threadIdx.y;

            float norm_val;
            unsigned int idx;

            for (int iz = 0; iz < n3xyz.z; iz++)
            {

                idx = (iz*n3xyz.x*n3xyz.y) + (iy*n3xyz.x) + ix;

                norm_val = norm_vol[idx];

                if (norm_val > 0)
                {

                    // expected number of projections / actual number of projections which contributed to this voxel
                    // vol_out[idx] *= factor / norm_val;
                    vol_out[idx] *= 1.0f / norm_val;

                }

            }

        }

        __global__ void imscale(float *proj, float val, int nu, int nv)
        {
            // thread index
            int u = (blockIdx.x * blockDim.x) + threadIdx.x;
            if ( u >= nu ) return; // v25: return if index is outside of the projection
            
            int v = (blockIdx.y * blockDim.y) + threadIdx.y;
            if ( v >= nv ) return; // v25: return if index is outside of the projection
            
            int uv = u + (v*nu);

            proj[uv] *= val;

        }
        
#endif // GPU_SUPPORT