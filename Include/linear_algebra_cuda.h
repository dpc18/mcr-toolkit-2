#ifndef LINEAR_ALGEBRA_CUDA
#define LINEAR_ALGEBRA_CUDA

#define TBP_x2 2048

dim3 vec_grid;
dim3 vec_block = dim3(TBP_x2/2,1,1);
// unsigned int shared_mem_sz = TBP_x2*sizeof(float);
unsigned int shared_mem_sz = TBP_x2*sizeof(double);

#include "dp_inner_product.h"

dim3 vec_grid_y;

// double *ip_result;

// void initialize_linear_algebra() {

//    cudaMalloc( (void **) &ip_result, sizeof(double) );

// }

// scalar operations

__global__ void set_as(float *a, float b) // as = bs
{

    a[0] = b;

}

__global__ void scalar_sqrt(float *a, float *b) // bs = sqrt(as)
{

    b[0] = sqrtf(a[0]);

}

__global__ void scalar_equals(float *a, float *b) // a = b
{

    a[0] = b[0];

}

__global__ void scalar_divide(float *a, float *b, float *c) // (scalar 1, scalar 2, output)
{

    c[0] = a[0] / b[0];

}

__global__ void scalar_multiply(float *a, float *b, float *c) // (scalar 1, scalar 2, output)
{

    c[0] = a[0] * b[0];

}

// vector - scalar multiply operations

__global__ void av_dot_bs(float *a, float b, float*c) {

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    c[bid]              = a[bid] * b;
    c[bid + blockDim.x] = a[bid + blockDim.x] * b;

}

// vector - scalar reduction operations

// __global__ void do_squared_sum(float *a, double *b) // bs = sum(av.^2)
// {
//
//     // Index within the thread block
//     unsigned int tid = threadIdx.x; // 0-1023
//
//     // Index offset to current block within a and b
//     unsigned int bid = blockIdx.x*2*blockDim.x;
//
//     // Assign a variable name to dynamically allocated shared memory
//     extern __shared__ double smem[];
//
//     // Coalesced global memory reads into shared memory
//     // Non-overlapping indices.
//
//     smem[tid]              = a[bid + tid]*a[bid + tid];
//     smem[tid + blockDim.x] = a[bid + tid + blockDim.x]*a[bid + tid + blockDim.x];
//
//     // Do first reduction operation which uses the same thread
//     // No __syncthreads() needed!
//
//     smem[tid] += smem[tid + blockDim.x];
//
//     // Do not continue computing until the shared memory is filled!
//     __syncthreads();
//
//     // Binary reduction operation
//     // - - - - - - - - 8
//     // - - - - 4
//     // - - 2
//     // - 1
//     for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads
//
//             if (tid < i) {
//
//                 smem[tid] += smem[tid + i];
//
//             }
//
//             __syncthreads();
//
//     }
//
//     // Add result to global sum without parallel conflicts.
//     if (tid == 0) {
//
//         atomicAdd(b, smem[0]+smem[1] );
//
//     }
//
// }
//
// __global__ void do_inner_prod(float *a, float *b, double *c) // (av, bv, out var)
// {
//
//     // Index within the thread block
//     unsigned int tid = threadIdx.x; // 0-1023
//
//     // Index offset to current block within a and b
//     unsigned int bid = blockIdx.x*2*blockDim.x;
//
//     // Assign a variable name to dynamically allocated shared memory
//     extern __shared__ double smem[];
//
//     // Coalesced global memory reads into shared memory
//     // Non-overlapping indices.
//
//     smem[tid] = a[bid + tid]*b[bid + tid];
//     smem[tid + blockDim.x] = a[bid + tid + blockDim.x]*b[bid + tid + blockDim.x];
//
//     // Do first reduction operation which uses the same thread
//     // No __syncthreads() needed!
//
//     smem[tid] += smem[tid + blockDim.x];
//
//     // Do not continue computing until the shared memory is filled!
//     __syncthreads();
//
//     // Binary reduction operation
//     // - - - - - - - - 8
//     // - - - - 4
//     // - - 2
//     // - 1
//     for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads
//
//             if (tid < i) {
//
//                 smem[tid] += smem[tid + i];
//
//             }
//
//             __syncthreads();
//
//     }
//
//     // Add result to global sum without parallel conflicts.
//     if (tid == 0) {
//
//         atomicAdd(c,smem[0]+smem[1]);
//
//     }
//
// }
//
// __global__ void set_as_dp_sp(double *a, float b) {
//
//     a[0] = b;
//
// }
//
// __global__ void set_as_sp_dp(float *a, double *b) {
//
//     a[0] = b[0];
//
// }
//
// void squared_sum(float *a, float *b) {
//
//     set_as_dp_sp<<<dim3(1,1,1),dim3(1,1,1)>>>(ip_result, 0.0f);
//
//     do_squared_sum<<<vec_grid,vec_block,shared_mem_sz>>>(a, ip_result);
//
//     set_as_sp_dp<<<dim3(1,1,1),dim3(1,1,1)>>>(b, ip_result);
//
// }
//
// void inner_prod(float *a, float *b, float* c) {
//
//     set_as_dp_sp<<<dim3(1,1,1),dim3(1,1,1)>>>(ip_result, 0.0f);
//
//     do_inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(a, b, ip_result);
//
//     set_as_sp_dp<<<dim3(1,1,1),dim3(1,1,1)>>>(c, ip_result);
//
// }

// vector - vector operations

// be careful making a divide version of this given zero padding...
__global__ void av_dot_bv(float *a, float *b, float *c) // (av, bv, output var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    c[bid] = a[bid] * b[bid];
    c[bid + blockDim.x] = a[bid + blockDim.x] * b[bid + blockDim.x];

}

__global__ void av_equals_bv(float *a, float *b) // av = bv
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    a[bid] = b[bid];
    a[bid + blockDim.x] = b[bid + blockDim.x];

}

__global__ void av_sub_bv(float *a, float *b, float *c) // (av, bv, output var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    c[bid] =              a[bid]              - b[bid];
    c[bid + blockDim.x] = a[bid + blockDim.x] - b[bid + blockDim.x];

}

__global__ void av_plus_bs_dot_cv(float *a, float *b, float *c, float *d)  // (av, bs, cv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    float b0 = b[0];

    d[bid] =              a[bid]              + b0 * c[bid];
    d[bid + blockDim.x] = a[bid + blockDim.x] + b0 * c[bid + blockDim.x];

}

__global__ void av_minus_bs_dot_cv(float *a, float *b, float *c, float *d) // (av, bs, cv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    float b0 = b[0];

    d[bid] =              a[bid]              - b0 * c[bid];
    d[bid + blockDim.x] = a[bid + blockDim.x] - b0 * c[bid + blockDim.x];

}

// vector - vector operations w/ scalar multipliers

__global__ void av_minus_bs_dot_cv_sc(float *a, float b, float *c, float *d) // (av, bs, cv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    d[bid] =              a[bid]              - b * c[bid];
    d[bid + blockDim.x] = a[bid + blockDim.x] - b * c[bid + blockDim.x];

}

__global__ void as_dot_bv_plus_cs_dot_dv(float a, float *b, float c, float *d, float *e)  // (as, bv, cs, dv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    e[bid] =              a*b[bid]              + c*d[bid];
    e[bid + blockDim.x] = a*b[bid + blockDim.x] + c*d[bid + blockDim.x];

}

__global__ void as_dot_bv_plus_cs_dot_dv2(float *a, float *b, float *c, float *d, float *e)  // (as, bv, cs, dv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    e[bid] =              a[0]*b[bid]              + c[0]*d[bid];
    e[bid + blockDim.x] = a[0]*b[bid + blockDim.x] + c[0]*d[bid + blockDim.x];

}

__global__ void as_dot_bv_plus_cs_dot_dv_dot_dv(float a, float *b, float c, float *d, float *e)  // (as, bv, cs, dv, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    e[bid] =              a*b[bid]              + c*d[bid]*d[bid];
    e[bid + blockDim.x] = a*b[bid + blockDim.x] + c*d[bid + blockDim.x]*d[bid + blockDim.x];

}

// X = X - eta * m ./ ( s + eps );
__global__ void av_s_bs_dot_cs_dd__dv_p_es(float *a, float b, float *c, float *d, float e, float *f)  // (av, bs, cv, dv, es, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    float dval =  d[bid];
    float dval2 = d[bid + blockDim.x];

    float temp, temp2;

    // is rsqrtf better for handling near zero values?
    // avoid "denormals"?
    // use ftz=true compile option

    if ( dval >= e) {
        temp =  a[bid]              - b*c[bid]              / (dval + e);
        // temp =  a[bid]              - b*c[bid]              * rsqrtf(dval*dval + e);
    }
    else {
        temp = 0;
    }

    if ( dval2 >= e) {
        temp2 = a[bid + blockDim.x] - b*c[bid + blockDim.x] / (dval2 + e);
        // temp2 = a[bid + blockDim.x] - b*c[bid + blockDim.x] * rsqrtf(dval2*dval2 + e);
    }
    else {
        temp2 = 0;
    }

    f[bid] = temp;
    f[bid + blockDim.x] = temp2;

}

// X = X - eta * m ./ sqrt(s + eps)
__global__ void av_s_bs_dot_cs_dd__sq_dv_p_es(float *a, float b, float *c, float *d, float e, float *f)  // (av, bs, cv, dv, es, out var)
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    float dval =  d[bid];
    float dval2 = d[bid + blockDim.x];

    float temp, temp2;

    if ( dval >= e) {
        temp =  a[bid]              - b*c[bid]              * rsqrtf(dval + e);
    }
    else {
        temp = 0;
    }

    if ( dval2 >= e) {
        temp2 = a[bid + blockDim.x] - b*c[bid + blockDim.x] * rsqrtf(dval2 + e);
    }
    else {
        temp2 = 0;
    }

    f[bid] = temp;
    f[bid + blockDim.x] = temp2;

}

 // conditional operations

 // dv = max( as*bv, cv)
__global__ void max_as_dot_bv_or_abs_cv_equals_dv(float a, float *b, float *c, float *d)  // (as, bv, cv, dv )
{

    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023

    // Index offset to current block within b and c
    unsigned int bid = blockIdx.x*2*blockDim.x + tid;

    float bval  = a * b[bid];
    float bval2 = a * b[bid + blockDim.x];

    float cval  = fabsf(c[bid]);
    float cval2 = fabsf(c[bid + blockDim.x]);

    if ( bval > cval ) {

        d[bid] = bval;

    }
    else {

        d[bid] = cval;

    }

    if ( bval2 > cval2 ) {

        d[bid + blockDim.x] = bval2;

    }
    else {

        d[bid + blockDim.x] = cval2;

    }

}

 // if scalar a < scalar b, cv = dv
__global__ void scalar_a_less_b_vector_equals(float *a, float *b, float *c, float *d)  // (as, bs, cv, dv )
{

    if (a[0] < b[0]) {

        // Index within the thread block
        unsigned int tid = threadIdx.x; // 0-1023

        // Index offset to current block within a and b
        unsigned int bid = blockIdx.x*2*blockDim.x + tid;

        c[bid] =              d[bid];
        c[bid + blockDim.x] = d[bid + blockDim.x];

    }

}

// if scalar a < scalar b, scalar c = scalar d
__global__ void scalar_a_less_b_scalar_equals(float *a, float *b, float *c, float *d)  // (as, bs, cs, ds )
{

    if (a[0] < b[0]) {

        c[0] = d[0];

    }

}

#endif // LINEAR_ALGEBRA_CUDA
