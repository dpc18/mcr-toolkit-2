// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DP_INNER_PRODUCT
#define DP_INNER_PRODUCT

double *ip_buffer_; // double pre
// double *dp_gpu_scalar_;

// ***        ***
// *** DEVICE ***
// ***        ***

__global__ void do_inner_prod(float *a, float *b, double* c) // (av, bv, out var)
{
 
    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023
    
    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x;
    
    // Assign a variable name to dynamically allocated shared memory
    extern __shared__ double smem[];
    
    // Coalesced global memory reads into shared memory
    // Non-overlapping indices.
    
    // smem[tid] = (double) (a[bid + tid]*b[bid + tid]);
    // smem[tid + blockDim.x] = (double) (a[bid + tid + blockDim.x]*b[bid + tid + blockDim.x]);
    
    smem[tid] = a[bid + tid]*b[bid + tid];
    smem[tid + blockDim.x] = a[bid + tid + blockDim.x]*b[bid + tid + blockDim.x];
    
    // Do first reduction operation which uses the same thread
    // No __syncthreads() needed!
    
    smem[tid] += smem[tid + blockDim.x];
    
    // Do not continue computing until the shared memory is filled!
    __syncthreads();
    
    // Binary reduction operation
    // - - - - - - - - 8
    // - - - - 4
    // - - 2
    // - 1
    for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads

            if (tid < i) {

                smem[tid] += smem[tid + i];

            }

            __syncthreads();

    }

    // Add result to global sum without parallel conflicts.
    if (tid == 0) {

        // add to double precision scalar to minimize variability due to order of operations
        // optimization algorithms appear to be very sensitive to order of operations rounding variability with floats
        // atomicAdd( c , smem[0]+smem[1]);
        
        // avoid atomicAdd entirely using a buffer
        c[ blockIdx.x ] = smem[0] + smem[1];

    }
    
}

__global__ void do_squared_sum(float *a, double *b) // bs = sum(av.^2)
{
    
    // Index within the thread block
    unsigned int tid = threadIdx.x; // 0-1023
    
    // Index offset to current block within a and b
    unsigned int bid = blockIdx.x*2*blockDim.x;
    
    // Assign a variable name to dynamically allocated shared memory
    extern __shared__ double smem[];
    
    // Coalesced global memory reads into shared memory
    // Non-overlapping indices.
    
    smem[tid]              = a[bid + tid]*a[bid + tid];
    smem[tid + blockDim.x] = a[bid + tid + blockDim.x]*a[bid + tid + blockDim.x];
    
    // Do first reduction operation which uses the same thread
    // No __syncthreads() needed!
    
    smem[tid] += smem[tid + blockDim.x];
    
    // Do not continue computing until the shared memory is filled!
    __syncthreads();
    
    // Binary reduction operation
    // - - - - - - - - 8
    // - - - - 4
    // - - 2
    // - 1
    for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads

            if (tid < i) {

                smem[tid] += smem[tid + i];

            }

            __syncthreads();

    }
    
    // Add result to global sum without parallel conflicts.
    if (tid == 0) {

        // add to double precision scalar to minimize variability due to order of operations
        // optimization algorithms appear to be very sensitive to order of operations rounding variability with floats
        // atomicAdd(b, smem[0]+smem[1] );
        
        // avoid atomicAdd entirely using a buffer
        b[ blockIdx.x ] = smem[0] + smem[1];

    }
    
}

__global__ void reduce_ip_buffer( double *buffer_, unsigned int nxyz ) {
    
    int tid = threadIdx.x;
    
    extern __shared__ double smem[];
    
    // For each block
    for (int k = 0; k < ((nxyz+TBP_x2-1)/TBP_x2); k += 2*blockDim.x)
    {

        // Don't copy in new data until we are 100% done with the old data.
        __syncthreads();
        
        smem[tid]              = buffer_[tid + k];
        smem[tid + blockDim.x] = buffer_[tid + k + blockDim.x];
        
        smem[tid] += smem[tid + blockDim.x];
    
        // Do not continue computing until the shared memory is filled!
        __syncthreads();
     
        // Reduce block
        for (unsigned int i = blockDim.x/2; i > 1; i>>=1) { // save the last operation for the output to do one less syncthreads

            if (tid < i) {

                smem[tid] += smem[tid + i];

            }

            __syncthreads();
            
        }
        
        if (tid == 0) {
             
            // Set first element to zero during first iteration of outer loop only.
            // This is ok, since we have already copied the first element to shared memory
            if (k == 0) {
                 
                buffer_[0] = 0;
                    
            }
                
            buffer_[0] += smem[0] + smem[1];
                
        }
        
    }
    
}

__global__ void set_as_sp_dp(float *a, double *b)
{
    
    a[0] = b[0];
    
}

// ***      ***
// *** HOST ***
// ***      ***

// initialize a double precision buffer on the GPU
// Use of this buffer avoids atomicAdd operations which return different sums based on execution order and rounding
// ASSUMES: Inner product computations between volumes (vs. projections).
void dp_inner_product_init(unsigned int nxyz) {
    
    size_t vol_p_blocks = (nxyz+TBP_x2-1)/TBP_x2;
    
    // cudaMalloc( (void **) &dp_gpu_scalar_, sizeof(double) );
    cudaMalloc( (void **) &ip_buffer_, vol_p_blocks*sizeof(double) );
    
}

// helper function to manage single-double conversion
void inner_prod(float *a, float *b, float *c, unsigned int nxyz) {
    
    // set temporary double buffer to zero
    cudaMemset(ip_buffer_, 0, ( (nxyz+TBP_x2-1)/TBP_x2 )*sizeof(double));
    
    // compute double precision inner product
    // vec_grid = dim3(vol_p/TBP_x2,1,1);
    do_inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(a, b, ip_buffer_);
    
    // reduce inner product buffer
    reduce_ip_buffer<<<dim3(1,1,1),dim3(TBP_x2/2,1,1),shared_mem_sz>>>(ip_buffer_, nxyz);
    
    // copy result back to single precision scalar
    set_as_sp_dp<<<dim3(1,1,1),dim3(1,1,1)>>>(c, ip_buffer_);
    
}

void squared_sum(float *a, float *b, unsigned int nxyz) {
    
    // set temporary double buffer to zero
    cudaMemset(ip_buffer_, 0, ( (nxyz+TBP_x2-1)/TBP_x2 )*sizeof(double));
 
    // compute double precision squared sum
    do_squared_sum<<<vec_grid,vec_block,shared_mem_sz>>>(a, ip_buffer_);
    
    // reduce squared sum buffer
    reduce_ip_buffer<<<dim3(1,1,1),dim3(TBP_x2/2,1,1),shared_mem_sz>>>(ip_buffer_, nxyz);
    
    // copy result back to single precision scalar
    set_as_sp_dp<<<dim3(1,1,1),dim3(1,1,1)>>>(b, ip_buffer_);
    
}

#endif // DP_INNER_PRODUCT