# (M)ulti-(C)hannel CT (R)econstruction Toolkit 2

An open-source toolkit for advanced applications of preclinical and clinical x-ray CT.

Copyright (C) 2022-2024, [Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/), Duke University

NOTE: MCR Toolkit v2.0 is still under development. Not all listed features are available in the current release.

## Index

1. [About](#about)
2. [Citation](#citation)
3. [Gallery](#gallery)
4. [System Requirements](#system-requirements)
5. [Installation Instructions](#installation-instructions)
6. [Denoising and Reconstruction Demos](#denoising-and-reconstruction-demos)
7. [Funding and Support](#funding-and-support)
8. [References](#references)
9. [Licensing](#licensing)

## About

The MCR Toolkit has evolved over nearly a decade, first to address unique challenges associated with *in vivo* preclinical micro-CT, and now to support translation of advances in CT reconstruction between the preclinical and clinical domains. As the name suggests, the Toolkit specializes in solving "multi-channel" reconstruction problems: multi-energy (ME) CT (dual-energy, photon-counting), dynamic time-resolved (TR) CT (cardiac, perfusion), and combined multi-energy and time-resolved (METR) CT. The Toolkit strives to balance the computational demands of multi-channel reconstruction problems against the need for numerically robust denoising and reconstruction operators required to solve real-world problems.

The initial version of the Toolkit ([v1.0](https://gitlab.oit.duke.edu/dpc18/mcr-toolkit-public)) relies on MATLAB and its MEX interface for orchestrating CT reconstruction pipelines; however, heavy reliance on MATLAB comes with licensing restrictions and limited support for deep learning augmentation of reconstruction pipelines. In concert with our conference submission (below) and presentation at [SPIE Medical Imaging 2024](https://spie.org/conferences-and-exhibitions/medical-imaging#_=_), we are releasing the first beta version of the MCR Toolkit v2.0, an open-source Python port of the Toolkit and its reconstruction tools.

Conference submission: Paper 12925-84 – “Multi-Channel Reconstruction (MCR) Toolkit v2.0: open-source Python-based tools for X-ray CT reconstruction”

### Contact Information:

The initial public release of the Toolkit was developed by:

Darin P. Clark, PhD\
Asst. Professor in Radiology\
Duke University Medical Center\
[dpc18@duke.edu](mailto:dpc18@duke.edu)\
[Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/)\
[Center for Virtual Imaging Trials](https://cvit.duke.edu/)

If you are interested in contributing to the Toolkit's development, please do not hesitate to contact me.

### Contributions:

We acknowledge Samuel Johnston, PhD, for his work on early versions of the MCR Toolkit.

Alex Allphin, a PhD student in the QIAL, has contributed to the development of a graphical user interface and sample reconstruction scripts written as Jupyter notebooks.

Rohan Nadkarni, MS, a PhD student in the QIAL, has contributed to the development of a deep learning interface for the Toolkit.

### Toolkit Features:

* Rapid prototyping of reconstruction pipelines through Python
* Interoperability with deep learning packages
* Unified iterative reconstruction functions for single energy (SE), ME, TR, and METR reconstruction problems.
* Matched projection and backprojection operators supporting flat and cylindrical CT detectors and multiple GPUs
* Data adaptation: semi-automatic and automatic adjustment of regularization parameters
* GPU-based denoising and regularization algorithms:
  * Rank-Sparse Kernel Regression (RSKR): iterative application of BF to weighted singular vectors to approximately homogenize noise variance across ME CT data sets.
  * Patch-based Singular Value Thresholding (pSVT): singular value thresholding applied to patches extracted along the time dimension to improve denoising performance.
  * Dictionary sparse coding: patch-wise coding using a learned dictionary of atoms; a precursor to convolutional sparse coding and convolutional neural networks

## Citation

__Continued development of the MCR Toolkit relies on research grants. If you use the MCR Toolkit in your work, please cite the following publication:__

__Clark DP, Badea CT. MCR toolkit: A GPU-based toolkit for multi-channel reconstruction of preclinical and clinical x-ray CT data. Medical Physics. 2023; 50: 4775–4796. https://doi.org/10.1002/mp.16532__

## Gallery

![Sarcoma tumor imaging with ME CT.](./Images/ME_CT_Sarcoma_Tumor_Imaging.png)\
*Photon-counting CT imaging of a sarcoma tumor on the flank of a mouse, three days after the injection of iodine liposomes. Maximum intensity projections are shown in axial (left) and sagittal (right) orientations. Post-reconstruction material decomposition was applied to decompose the resulting multi-energy CT data into water (gray), iodine (red), and calcium (green) maps. Collaborators: [David Kirsch, MD, PhD](https://scholars.duke.edu/person/david.kirsch); [Chang-Lung Lee, PhD](https://scholars.duke.edu/person/chang-lung.lee); [Yvonne M. Mowery, MD, PhD](https://scholars.duke.edu/person/yvonne.mowery).*
\
\
![METR Cardiac CT](./Images/METR_Cardiac_CT_180417_5_v2.gif)
![METR Cardiac CT](./Images/METR_Cardiac_CT_180417_6_v2.gif)
![METR Cardiac CT](./Images/METR_Cardiac_CT_180417_8_v2.gif)
![METR Cardiac CT](./Images/METR_Cardiac_CT_180417_9_v2.gif)\
*Photon-counting CT imaging of the heart in a mouse model of atherosclerosis (ApoE knockout). Maximum intensity projections are shown through the heart following time and energy resolved reconstruction and post-reconstruction material decomposition: water (gray), iodine (red), calcium and gold (green). Collaborators: [Chang-Lung Lee, PhD](https://scholars.duke.edu/person/chang-lung.lee); [Matthew Holbrook, PhD](https://scholar.google.com/citations?user=fDVtc_0AAAAJ&hl=en).*
\
\
![Perfusion Imaging](./Images/METR_Perfusion_Imaging_HN_Cancer.gif)\
*Photon-counting CT perfusion imaging of a mouse model of head and neck cancer. A 2D coronal slice is shown after time and energy resolved reconstruction and post-reconstruction material decomposition: water (gray), iodine (red), calcium (green). Collaborators: [Yvonne M. Mowery, MD, PhD](https://scholars.duke.edu/person/yvonne.mowery); [Alex Allphin](https://scholars.duke.edu/person/alex.allphin).*

## System Requirements

* At least one NVIDIA GPU with 16 GB of GPU memory and support for compute capability 6.1 (Pascal) or higher. When using multiple GPUs, the Toolkit currently expects all GPUs used to be similar. 
* NVIDIA CUDA Toolkit 9.0 or newer (https://developer.nvidia.com/cuda-toolkit)
* Linux operating system with NVIDIA driver support (highly recommended)\
  -OR-\
  Windows operating system
* 128 GB of system RAM (TR and METR iterative reconstruction)
* Python package dependencies: NumPy, NiBabel, Matplotlib, Scipy


## Installation Instructions

### Python Interface (stand alone)

* NOTE: Python support and deep learning integration are currently under development. Python support has not been tested under Windows and version and system requirements are still undergoing evaluation.
1. Edit the Makefile in the *Operators* subdirectory of the Toolkit.
   * CC: absolute path to C compiler.
   * CUDA_LIBS2: '-L' + absolute path to 'targets/x86_64-linux/lib/' CUDA subdirectory    
   * CUDA_I: '-I' + absolute path to 'targets/x86_64-linux/include/' CUDA subdirectory
2. Run 'make --always-make' from within the *Operators* subdirectory of the Toolkit.
3. Confirm that the "DD_operators_vXX.so" library has been built successfully in the *Operators* subdirectory of the Toolkit.
4. Consult the 'Demo_Scripts/Recon_Scripts/Python_Interface/Reconstruction_Pipeline_v1.py' demo for an example of using the Python interface for reconstruction.
   * Before running, update the *toolkit_path*, the *outpath*, and the *dpath_base* variables to point to the appropriate directories.

### Python Interface (DukeSim)

NOTE: These instructions are specific to reconstruction parameter files output by the [DukeSim CT simulation software package](https://cvit.duke.edu/resources/). The "DD_operators_vXX.so" library must be built (steps 1-3 above) before reconstructing DukeSim data.

Sample DukeSim reconstruction parameter file (plain text fields):

header format: "# (Header)" <br>
parameter format: "(Parameter): (Value)"

| Parameter                   | Value                                               |
|-----------------------------|-----------------------------------------------------|
| **# Paths:**                |                                                     |
| projection_file             | (string).xcat, (string).nii, or (string).nii.gz     |
| outpath                     | (path to reconstruction output directory)           |
| **# Geometry:**             |                                                     |
| rot_time                    | (grantry rotation time in ms)                       |
| pitch                       | (z translation per rotation / detector collimation) |
| source_to_detector_distance | (mm)                                                |
| source_to_object_distance   | (mm)                                                |
| proj_per_rotation           | (integer)                                           |
| offset                      | (central ray column offset in detector elements, 0 = no offset) |
| anode_angle                 | (degrees) |
| row_pixel_size              | (mm) |
| column_pixel_size           | (mm) |
| table_direction             | 1 or -1 |
| rotation_direction          | 1 or -1 |
| initial_angle               | (gantry angle for projection 1, degrees) |
| **# ZFFS:**                 | |
| even_offset                 | 0 (z flying focal spot, not yet implemented) |
| odd_offset:                 | 0 (z flying focal spot, not yet implemented)  |
| **# Projections:**          |  |
| n_rows                      | (integer number of detector rows) |
| n_columns                   | (integer number of detector columns) |
| n_energy_bins               | (# of DukeSim keV values, not used for reconstruction)|
| rescaling_factor            | (multiple projection data by this value before reconstruction) |
| mu_eff                      | (water attenution value used for HU conversion, 1/cm)  |
| **# Reconstruction**        | |
| recon_type | "single_source_WFBP" (analytical reconstruction) <br> "single_source_SART" (algebraic reconstruction) <br> "single_source_iterative" (iterative reconstruction with noise reduction) |
| filter | "ram-lak" (standard ramp filter) <br> "shepp-logan" <br> "cosine" <br> "hamming" <br> "hann" <br> "smooth" (window monotonic, decreasing) <br> "sharp" (window rect + monotonic, decreasing) <br> "enhancing" (window exceeds 1.0) |
| GPU_index                   | (GPU(s) to use for reconstruction; indexing starts from 0) |
| z_offset                    | (z axis offset of the center of the reconstruction from the center of the scan, mm) |
| z_range                     | (total z range of the reconstruction in mm) <br> (value < 0 determines the total z range automatically ) <br> (for algebraic and iterative reconstruction, z_range should ideally be larger than the simulation scan range) |
| matsize                     | (# of x,y voxels spanning the reconstruction field of view, integer) |
| recon_FoV                   | (reconstruction field of view, mm; axial voxel size = recon_FoV / matsize ) |
| slice_thickness             | (reconstruction slice thickness, mm) |
| f50                         | (50% modulation frequency for FBP kernels; cycles / mm) |
| run_fbp                     | (deprecated in MCR Toolkit v2.0+; see recon_type ) |
| run_iterative               | (deprecated in MCR Toolkit v2.0+; see recon_type ) |
| **# Cardiac data**          | |
| heart_rate                  | (heart rate in bpm; cardiac recon. not yet implemented in MCR Toolkit v2.0) |
| n_cardiac_phases            | (number of cardiac phases to reconstruct) |
| cardiac_subphases           | (number of subphases to use when quantizing cardiac phase for projection weighting) |

Sample Python code for reconstructing DukeSim projection data:

~~~
from Python.Recon_Operators import DukeSim_Recon

recon_lib_path   = '(Path_to_Toolkit)/Operators/DD_operators_v25.so'
recon_param_file = '(Path_to_Param_File)/recon_parameters.txt'

# Parse reconstruction parameters.
DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )

# Load projection data.
DukeSim_Op.load_projections()

# Set up projections and GPU allocations for reconstruction.
DukeSim_Op.operator_setup()

# Perform reconstruction and save results.
DukeSim_Op.reconstruction()
~~~


## Denoising and Reconstruction Demos

NOTE: This section still refers to [v1.0](https://gitlab.oit.duke.edu/dpc18/mcr-toolkit-public) of the MCR Toolkit.

* Supplemental data required to run the MCR Toolkit demo scripts can be found here: https://doi.org/10.7924/r45m6cs1f.
* For convenience, demo scripts can be run from the *run_demos.m* script in the Toolkit root directory after completing a [MATLAB installation](#matlab-interface). Note that some demos are commented out due to restrictions on sharing supplemental data; however, complete scripts are still provided in the *Demo_Scripts* subdirectory of the Toolkit.
  * *Demo_Scripts/Denoising_Scripts*: Preclinical and clinical examples of denoising METR CT data with RSKR and pSVT. The clinical example also performs post-reconstruction material decomposition.
  * *Demo_Scripts/DukeSim_Recon_Scripts*: Cardiac CT reconstruction of XCAT phantom projection data simulated using DukeSim.
  * *Demo_Scripts/Recon_Scripts/MOBY_Phantom_METR*: SE, ME, TR, and METR reconstruction of MOBY phantom projection data with analytical and iterative reconstruction.
  * *Demo_Scripts/Recon_Scripts/Preclinical_CT_METR*: A complete reconstruction pipeline for *in vivo*, cardiac, photon-counting x-ray CT data acquired in a mouse model of atherosclerosis. The photon-counting detector was a SANTIS 0804 ME prototype detector on loan from DECTRIS, AG. The detector has a 1 mm thick CdTe sensor, 150-μm isotropic pixels (515x257), and four independent and fully adjustable energy thresholds (set to 25, 34, 40, and 55 keV).
  * *Demo_Scripts/Recon_Scripts/Siemens_Flash_DS_DE*: Dual-source, dual-energy CT reconstruction of [Multi-Energy CT Phantom](https://www.sunnuclear.com/products/multi-energy-ct-phantom) projection data acquired with a Siemens Flash scanner.


## Funding and Support

Funding for this work was provided by the National Institutes of Health: National Institute on Aging (RF1AG070149, supplement RF1AG070149-01S1), National Cancer Institute (U24CA220245), and the National Institute of Biomedical Imaging and Bioengineering (R01EB001838, P41EB028744).

The MCR Toolkit has been developed to support collaborative research projects with Siemens Healthineers, AG, and DECTRIS, AG; however, neither company has provided direct support for the Toolkit’s development.

## References

  Matched projection and backprojection operators\
  Long, Y., Fessler, J. A., & Balter, J. M. (2010).
  3D forward and back-projection for X-ray CT using separable footprints.
  IEEE transactions on medical imaging, 29(11), 1839-1850.
  
  Approximate, analytical reconstruction of helical CT data\
  Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
  Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
  Physics in Medicine & Biology, 49(11), 2209.
  
  Projection matrices for efficient geometry calculations with flat detectors\
  Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
  Cone-beam reprojection using projection-matrices.
  IEEE transactions on medical imaging, 22(10), 1202-1214.
  
  BiCGSTAB(l) linear solver (implemented in the MCR Toolkit)\
  Sleijpen, G. L., & Van Gijzen, M. B. (2010).
  Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
  SIAM journal on scientific computing, 32(5), 2687-2709.
  Algorithm 3.1
  
  Denoising with patch-based singular value thresholding (pSVT, implemented in the MCR Toolkit)\
  Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
  Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
  IEEE transactions on medical imaging, 34(3), 748-760.
  
  Denoising with rank-sparse kernel regression (RSKR, implemented in the MCR Toolkit)\
  Clark, D. P., & Badea, C. T. (2017).
  Hybrid spectral CT reconstruction.
  PloS one, 12(7), e0180324.

  Denoising with dictionary sparse coding (implemented in the MCR Toolkit)\
  Rubinstein, R., Zibulevsky, M., & Elad, M. (2008). Efficient implementation of the K-SVD algorithm using batch orthogonal matching pursuit (No. CS Technion report CS-2008-08). Computer Science Department, Technion.
  
  [DukeSim CT Simulator](https://cvit.duke.edu/resources/)\
  Abadi, E., Harrawood, B., Sharma, S., Kapadia, A., Segars, W. P., & Samei, E. (2018).
  DukeSim: a realistic, rapid, and scanner-specific simulation framework in computed tomography.
  IEEE transactions on medical imaging, 38(6), 1457-1465.
  
  [MOBY mouse phantom](https://cvit.duke.edu/resources/)\
  Segars, W. P., Tsui, B. M., Frey, E. C., Johnson, G. A., & Berr, S. S. (2004).
  Development of a 4-D digital mouse phantom for molecular imaging research.
  Molecular Imaging & Biology, 6(3), 149-159.
  
  [XCAT human phantom](https://cvit.duke.edu/resource/xcat-phantom-program/)\
  W. Segars, G. Sturgeon, S. Mendonca, J. Grimes, and B. M. Tsui. (2010)
  4D XCAT phantom for multimodality imaging research.
  Medical Physics 37(9), 4902-4915.


## Licensing

Copyright (C) 2022-2024, [Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/), Duke University 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
