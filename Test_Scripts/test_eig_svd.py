# Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Test an eigen decomposition alternative to np.linalg.svd() that yields higher precision
# when the input data in float32 and limited system memory is available.
# NOTE: This script compares the result with a result produced in double precision, so this
#       test script is not memory efficient.

import numpy as np # (v1.21.5)
import nibabel as nib # (5.1.0)
import time

from Python.Recon_Utilities import eig_SVD, load_nii

if True:

# Load a large, multi-channel test data set (~30GB, single precision)

    test_file = '/data/Temp/Y2.nii'
    X = load_nii( test_file )

# Reshape

    # ne,np,nv,nu => ne,np*nv*nu
    X = np.reshape( X, [X.shape[0],-1] )

# Compute the reference SVD (very memory intensive)

    Xd = X.astype('float64')
    NXd = np.linalg.norm( Xd.flatten() )

    # Numpy's svd crashes for an 4xN matrix, so work with the transpose.
    Xd = np.transpose(Xd,[1,0])

    print('\nReference SVD in double precision...\n')

    t0 = time.time()

    U_ref, S_ref, Vt_ref = np.linalg.svd( Xd, full_matrices=False )

    error = np.linalg.norm( ( ( U_ref * S_ref[...,None,:] ) @ Vt_ref - Xd ).flatten() ) / NXd

    # Reference % error: 7.326693880222834e-12 ( 115.00089383125305 seconds)
    print('\nReference % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Single precision error: This seems to be the wrost case for memory usage. Likely np.linalg.svd makes
#                         a double copy internally.

    print('\nReference SVD in single precision...\n')

    t0 = time.time()

    U_s, S_s, Vt_s = np.linalg.svd( np.transpose(X,[1,0]), full_matrices=False )

    error = np.linalg.norm( ( ( U_s * S_s[...,None,:] ) @ Vt_s - Xd ).flatten() ) / NXd

    # Single precision SVD % error: 6.821319215545063e-06 ( 133.01713681221008 seconds)
    print('\nSingle precision SVD % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Reverse the transpose.

    Xd = np.transpose(Xd,[1,0])

# Compute the limited memory SVD

    print('\nLimited memory SVD...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( X )

    # Limited memory % error: 7.986800998678458e-06 ( 78.8614935874939 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - Xd ).flatten() ) / NXd

    print('\nLimited memory % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Make sure we get the same answer when we pass in X.T

    print('\nLimited memory SVD (transpose)...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( np.transpose( X, [1,0] ) )

    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - np.swapaxes( Xd, -2, -1 ) ).flatten() ) / NXd

    # Limited memory (transpose) % error: 7.462619320661482e-06 ( 124.25054287910461 seconds)
    print('\nLimited memory (transpose) % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Make sure we get the same answer when we reproduce the double precision SVD with eig_SVD

    print('\nDouble precision SVD with eig_SVD...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( X, memory_efficient=False )

    # Double precision SVD with eig_SVD % error: 2.6285907362779333e-14 ( 39.933688163757324 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - Xd ).flatten() ) / NXd

    print('\nDouble precision SVD with eig_SVD % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Make sure we get the same answer when we reproduce the double precision SVD with eig_SVD, transposed

    print('\nDouble precision SVD with eig_SVD (transpose)...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( np.transpose( X, [1,0] ), memory_efficient=False )

    # Double precision SVD with eig_SVD % error (transpose): 5.640096863209787e-14 ( 90.01465678215027 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - np.swapaxes( Xd, -2, -1 ) ).flatten() ) / NXd

    print('\nDouble precision SVD with eig_SVD % error (transpose): ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# clear memory

    del X, Xd, U_test, S_test, Vt_test

# Test batched SVD (reference)

    print('\nReference batched SVD...\n')

    # Defaults to float64
    Xd = np.random.normal( size=(10,100000,10) )

    NXd = np.linalg.norm( Xd.flatten() )

    t0 = time.time()

    U_ref, S_ref, Vt_ref = np.linalg.svd( Xd, full_matrices=False )

    # Reference SVD % error: 1.6016815567359285e-13 ( 0.17753148078918457 seconds)
    error = np.linalg.norm( ( ( U_ref * S_ref[...,None,:] ) @ Vt_ref - Xd ).flatten() ) / NXd

    print('\nReference SVD % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Test batched SVD.

    print('\nTesting batched SVD...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( Xd.astype('float32') )

    # Batched SVD % error: 1.001778232494153e-05 ( 0.028142690658569336 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - Xd ).flatten() ) / NXd

    print('\nBatched SVD % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Test batched SVD (transpose)

    print('\nTesting batched SVD (transpose)...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( np.transpose( Xd.astype('float32'), [0,2,1] ) )

    # Batched SVD (transpose) % error: 1.0383129426337722e-05 ( 0.555978536605835 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - np.transpose( Xd, [0,2,1] ) ).flatten() ) / NXd

    print('\nBatched SVD (transpose) % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

# Test batched SVD (double)

    print('\nTesting batched SVD (double)...\n')

    t0 = time.time()

    U_test, S_test, Vt_test = eig_SVD( Xd, memory_efficient=False )

    # Batched SVD (double) % error: 1.0395919706343656e-11 ( 0.31793642044067383 seconds)
    error = np.linalg.norm( ( ( U_test * S_test[...,None,:] ) @ Vt_test - Xd ).flatten() ) / NXd

    print('\nBatched SVD (double) % error: ' + str(100 * error) + ' ( ' + str(time.time() - t0) + ' seconds)\n')

