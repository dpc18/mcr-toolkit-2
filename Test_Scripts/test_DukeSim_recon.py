
from Python.Recon_Operators import DukeSim_Recon
from Python.Recon_Utilities import custom_recon_windows_v2, get_half_ramp, load_nii, save_nii
import numpy as np
import matplotlib.pyplot as plt

recon_lib_path   = '/home/dpc/Documents/MCR_Toolkit_2/Operators/DD_operators_v25.so'

## Reconstruct projections created by DukeSim.
# XCAT phantom modeling emphysema and lung nodules.
if True:

    # recon_param_file = '/data/DukeSim_XCAT_Phantom/recon_params_v5_Python.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom/recon_params_v5_Python_sharp_07.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/recon_params_v5_Python_pitch0.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/recon_params.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Iterative_Reconstruction/recon_params_iterative.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Iterative_Reconstruction_Axial/recon_params.txt'
    # recon_param_file = '/data/SPIE_2025_Recon_Param_Opt/140_kVp_100_mAs/recon_parameters.txt'
    recon_param_file = '/data/SPIE_2025_Recon_Param_Opt/140_kVp_150_mAs/recon_parameters.txt'

    DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )

    DukeSim_Op.load_projections()

    DukeSim_Op.operator_setup()

    DukeSim_Op.reconstruction()


## Reconstruct projections created by DukeSim.
# XCAT phantom modeling emphysema and lung nodules.
# Use iterative reconstruction?
# if True:
#
#     from Python.Recon_Operators import DukeSim_SS_SE_initialization_ops, DukeSim_SS_SE_fidelity_ops, SplitBregman_IterRecon
#     from Python.Denoising import RSKR
#
#     recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Iterative_Reconstruction/recon_params_iterative.txt'
#
#     debug_outpath = '/data/DukeSim_XCAT_Phantom_Axial/Iterative_Reconstruction/debug/'
#
#     DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )
#
#     DukeSim_Op.load_projections()
#
#     DukeSim_Op.operator_setup()
#
#     attn_water = np.array( [DukeSim_Op._mu_eff], np.float32 ) # Attenuation values for water per energy threshold
#     At         = [1.0] # Temporal resampling for 4D BF
#     e          = 1
#     p          = 1
#     gpu_list   = DukeSim_Op._gpu_list
#
#     denoise_op = RSKR( recon_lib_path, At=At, nvols=e, ntimes=p, mask=True, time_mode=1, weights=attn_water, gpu_idx=gpu_list[0] )
#
#     # Time, energy initialization operators (WFBP)
#     analytical_init = DukeSim_SS_SE_initialization_ops( DukeSim_Op )
#
#     # Time, energy data fidelity update operators (BiCGSTAB(l) with unfiltered backprojection)
#     fidelity_ops = DukeSim_SS_SE_fidelity_ops( DukeSim_Op )
#
#     X_iter = SplitBregman_IterRecon( initialization_ops=analytical_init, fidelity_updates=fidelity_ops, regularizer=denoise_op, times=p, energies=e,
#                                      attn_water=attn_water, debug_outpath=debug_outpath )( DukeSim_Op._Y )
#
#     save_nii( X_iter, os.path.join(DukeSim_Op._outpath,'X_iter.nii') )


## Reconstruct projections of the ACR phantom acquired with a Siemens Flash scanner.
# Simulated quarter-dose projections used in the Mayo Clinic Low Dose CT Grand Challenge.
# Box link to data: https://aapm.app.box.com/s/eaw4jddb53keg1bptavvvd1sf4x3pe9h
# TO DO: The projections were post-processed from DICOMs to NIFTI files. Add more direct support for reconstructing these
#        data sets.

# Reprojections to resolve gain issues in the original projection data => artifacts in reconstructions
# X = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom/MCR_Recon_1/Recon_WFBP_ram-lak.nii')
# X = ( 0.1921 * ( X / 1000 + 1 ) ) / ( 10.0 / self._dx )
# X_min, resvec, _ = bicgstabl_os_v2( RC=self, Y=np.squeeze(self._Y), x=X, maxit=2, W=self._W, ell=2)
# print('Algebraic reconstruction residuals: ' + str(resvec))
# recon_name = os.path.join( self._outpath, 'Recon_SART.nii' )
# save_nii( X_min, recon_name, vox_size=[self._dz,self._dy,self._dx] )
# Y_out = self.R( X_min.astype('float32'), self._W )
# recon_name = os.path.join( self._outpath, 'Y_reproj.nii' )
# save_nii( Y_out, recon_name, vox_size=[1,self._dv,self._du] )

# Filter rebinned projections on the CPU using scipy to look for the source of high frequency artifacts
# from scipy.fft import irfft, rfft
# Yb_pad = np.pad( Yb, ((0,0),(0,0),(0,1024-736)) )
# Yb_out = irfft( rfft( Yb_pad, n=1024, axis=-1 ) * filter, n=1024, axis=-1 )[:,:,:736]
# save_nii( Yb_out, '/data/DukeSim_XCAT_Phantom/Temp/Yb_filtered.nii' )

# if True:
#
#     recon_lib_path   = '/home/dpc/Documents/MCR_Toolkit_2/Operators/DD_operators_v25.so'
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/recon_params_v5_Python.txt'
#
#     # Focal spot position 1.
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom/ACR_Phantom_focal_position_1.txt'
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/ACR_Phantom_focal_position_1.txt'
#
#     # Focal spot position 2.
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom/ACR_Phantom_focal_position_2.txt'
#     recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/ACR_Phantom_focal_position_2.txt'
#
#     DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )
#
#     DukeSim_Op.load_projections()
#
#     DukeSim_Op.operator_setup()
#
#     DukeSim_Op.reconstruction()


# ## Plot kernel prototypes used to reconstruct the XCAT phantom data.
# if True:
#
#     # detector pixel size after rebinning
#     du = 1.0 / (1050.0 / 575.0)
#
#     # Padded filter length (nu = 900 => next power of 2, 1024)
#     lenu = 1024
#
#     # 1 / mm
#     feval = ( 1.0 / du ) * np.linspace( 0, (lenu//2) / (lenu + 1), lenu//2+1 )
#     # freq = np.linspace( 0, 0.85, 100 )
#     fmax = (1.0/du) * (lenu//2) / (lenu + 1)
#
#     # Kernel parameter
#     # The window modulates the MTF by 50% at this frequency (1/mm)
#
#
#     ramp = get_half_ramp( lenu, du )
#
#     fc    = 0.7
#     modt  = [ 1.0, 1.0      , 1.0     , 0.5     , 0.0 ]
#     omega = [ 0.0, 0.25 * fc, 0.6 * fc, 1.0 * fc, fmax]
#
#     sharp_window = custom_recon_windows( omega, modt, feval, ftype='sharp' )
#
#     fc    = 0.5
#     modt  = [ 1.0, 0.5, 0.0  ]
#     omega = [ 0.0, fc , fmax ]
#
#     smooth_window = custom_recon_windows( omega, modt, feval, ftype='smooth' )
#
#     fc    = 0.5
#     modt  = [1.0, 1.05    , 1.25     , 1.0     , 0.5, 0.0 ]
#     omega = [0.0, 0.1 * fc, 0.3 * fc, 0.8 * fc, fc , fmax]
#
#     enhancing_kernel = custom_recon_windows( omega, modt, feval, ftype='enhance' )
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     ax.plot(feval, ramp            , color='tab:gray' , label='Ramp')
#     ax.plot(feval, sharp_window    , color='tab:orange', label='Sharp')
#     ax.plot(feval, smooth_window   , color='tab:blue' , label='Smooth')
#     ax.plot(feval, enhancing_kernel, color='tab:red'  , label='Enhancing')
#     ax.set_title('Reconstruction Kernel Windows')
#     ax.legend()
#     plt.xlabel('1/mm')
#     plt.ylabel('Modulation')


## Plot kernel prototypes used to reconstruct the XCAT phantom data.
# if True:
#
#     # detector pixel size after rebinning
#     du = 1.0 / (1050.0 / 575.0)
#
#     # Padded filter length (nu = 900 => next power of 2, 1024)
#     lenu = 1024
#
#     # 1 / mm
#     feval = ( 1.0 / du ) * np.linspace( 0, (lenu//2) / (lenu + 1), lenu//2+1 )
#     # freq = np.linspace( 0, 0.85, 100 )
#     fmax = (1.0/du) * (lenu//2) / (lenu + 1)
#
#     # Kernel parameter
#     # The window modulates the MTF by 50% at this frequency (1/mm)
#     ramp = get_half_ramp( lenu, du )
#
#     params     = [0.5,0.2] # rect function end position, FWHM / 2 of Gaussian
#     amplitudes = []
#     sharp_window = custom_recon_windows_v2( params, amplitudes, feval, ftype='sharp' )
#
#     params     = [0.4] # FWHM / 2 of Gaussian
#     amplitudes = []
#     smooth_window = custom_recon_windows_v2( params, amplitudes, feval, ftype='smooth' )
#
#     params     = [0.1,0.2,0.23,0.68] # FWHM / 2.0 of first and second Gaussians, means of the first and second Gaussians
#     amplitudes = [0.4]     # Enhancing peak height above 1.0
#     enhancing_window = custom_recon_windows_v2( params, amplitudes, feval, ftype='enhancing' )
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     ax.plot(feval, ramp            , color='tab:gray' , label='Ramp Filter')
#     ax.plot(feval, sharp_window    , color='tab:orange', label='Sharp Window')
#     ax.plot(feval, smooth_window   , color='tab:blue' , label='Smooth Window')
#     ax.plot(feval, enhancing_window, color='tab:red'  , label='Enhancing Window')
#     ax.set_title('Reconstruction Kernel Windows')
#     ax.legend()
#     plt.xlabel('1/mm')
#     plt.ylabel('Modulation')


## Match the spatial resolution (noise power?) in the ACR phantom (quarter dose, B30 kernel)
# if True:
#
#     # detector pixel size after rebinning
#     du = 1.285839334751052 / (1085.6 / 595.0)
#
#     # Padded filter length (nu = 736 => next power of 2, 1024)
#     lenu = 1024
#
#     # 1 / mm
#     feval = ( 1.0 / du ) * np.linspace( 0, (lenu//2) / (lenu + 1), lenu//2+1 )
#     # freq = np.linspace( 0, 0.85, 100 )
#     fmax = (1.0/du) * (lenu//2) / (lenu + 1)
#
#     # MCR Toolkit - Ramp filter:
#     # average of two focal spot positions, measured on slice 136
#     mean_ramp = np.array( [1042.95146650, 1012.91901530, 1025.04247303, 1007.12386215, 1027.96476336], np.float32 )
#     min_ramp  = np.array( [428.45904541 , 508.60614014 , 686.85357666 , 647.55126953 , 864.43249512 ], np.float32 )
#     max_ramp  = np.array( [1649.36621094, 1476.81701660, 1351.00231934, 1240.92675781, 1184.03222656], np.float32 )
#
#     # [0.5984839 , 0.47461316, 0.3255631 , 0.29087034, 0.15666653]
#     MT_ramp = np.abs( max_ramp - min_ramp ) / 2040
#
#     mean_B30 = np.array( [1092.75382653, 1063.69491525, 1078.48717949, 1058.53738910, 1073.41256158], np.float32 )
#     min_B30  = np.array( [566          , 758          , 901          , 893          , 989          ], np.float32 )
#     max_B30  = np.array( [1625         , 1349         , 1268         , 1158         , 1165         ], np.float32 )
#
#     # [0.51911765, 0.28970587, 0.17990196, 0.12990196, 0.08627451]
#     MT_B30 = np.abs( max_B30 - min_B30 ) / 2040
#
#     MT_ratio = MT_B30 / MT_ramp
#     lp_mm = np.array( [4,5,6,7,8], np.float32 ) / 10
#
#     fc = 0.50 # (MT_ratio[2] - 0.5) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[2] + (0.5 - MT_ratio[3]) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[3]
#
#     # params     = [fc] # FWHM / 2 of Gaussian
#     # amplitudes = []
#     # smooth_window_fit = custom_recon_windows_v2( params, amplitudes, feval, ftype='smooth' )
#
#     params = [ fc - 0.15, 0.15 ]
#     modt   = []
#     sharp_window_fit = custom_recon_windows_v2( params, modt, feval, ftype='sharp' )
#
#     # modt  = [ 1.0, 0.5, 0.0  ]
#     # omega = [ 0.0, fc , fmax ]
#     #
#     # smooth_window_fit = custom_recon_windows( omega, modt, feval, ftype='smooth' )
#
#     ramp = get_half_ramp( lenu, du )
#
#     # fc    = 0.55 # (MT_ratio[2] - 0.5) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[2] + (0.5 - MT_ratio[3]) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[3]
#     # modt  = [ 1.0, 1.0      , 1.0     , 0.5     , 0.0 ]
#     # omega = [ 0.0, 0.25 * fc, 0.6 * fc, 1.0 * fc, fmax ]
#
#     # sharp_window_fit = custom_recon_windows( omega, modt, feval, ftype='sharp' )
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     ax.scatter( lp_mm, MT_ratio, color='tab:red' )
#     ax.plot(feval, ramp             , color='tab:gray' , label='Ramp')
#     # ax.plot(feval, smooth_window_fit, color='tab:blue' , label='Smooth')
#     ax.plot(feval, sharp_window_fit, color='tab:blue' , label='Sharp')
#     ax.set_title('Reconstruction Kernel Windows')
#     ax.legend()
#     plt.xlabel('1/mm')
#     plt.ylabel('Modulation')


## Match the spatial resolution (noise power?) in the ACR phantom (quarter dose, B30 kernel)
# Now using full dose data
# if True:
#
#     # detector pixel size after rebinning
#     du = 1.285839334751052 / (1085.6 / 595.0)
#
#     # Padded filter length (nu = 736 => next power of 2, 1024)
#     lenu = 1024
#
#     # 1 / mm
#     feval = ( 1.0 / du ) * np.linspace( 0, (lenu//2) / (lenu + 1), lenu//2+1 )
#     # freq = np.linspace( 0, 0.85, 100 )
#     fmax = (1.0/du) * (lenu//2) / (lenu + 1)
#
#     # MCR Toolkit - Ramp filter:
#     # average of two focal spot positions, measured on slice 136
#     mean_ramp = np.array( [1042.15249494,1010.90008039,1028.42232974,1010.35084917,1026.93969884], np.float32 ) # ,1033.09100153
#     min_ramp  = np.array( [475.26727295 ,545.14953613 ,749.01452637 ,720.80078125 ,889.16491699 ], np.float32 ) # ,945.27478027
#     max_ramp  = np.array( [1629.64111328,1444.67773438,1291.60742188,1185.16210938,1140.61975098], np.float32 ) # ,1111.75585938
#
#     # [0.5658695 , 0.4409452 , 0.2659769 , 0.2276281 , 0.12326217]
#     MT_ramp = np.abs( max_ramp - min_ramp ) / 2040
#
#     mean_B30 = np.array( [1098.11834320,1063.96330275,1078.79277108,1061.28274968,1078.69665810], np.float32 ) # ,1080.82102273
#     min_B30  = np.array( [603          ,784          ,941          ,908          ,1013         ], np.float32 ) # ,1028
#     max_B30  = np.array( [1583         ,1335         ,1223         ,1149         ,1153         ], np.float32 ) # ,1162
#
#     # [0.48039216, 0.27009803, 0.1382353 , 0.11813726, 0.06862745]
#     MT_B30 = np.abs( max_B30 - min_B30 ) / 2040
#
#     MT_ratio = MT_B30 / MT_ramp
#     lp_mm = np.array( [4,5,6,7,8], np.float32 ) / 10
#
#     fc = 0.5 # (MT_ratio[2] - 0.5) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[2] + (0.5 - MT_ratio[3]) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[3]
#
#     # params     = [fc] # FWHM / 2 of Gaussian
#     # amplitudes = []
#     # smooth_window_fit = custom_recon_windows_v2( params, amplitudes, feval, ftype='smooth' )
#
#     params = [ fc - 0.20, 0.27 ]
#     modt   = []
#     sharp_window_fit = custom_recon_windows_v2( params, modt, feval, ftype='sharp' )
#
#     # modt  = [ 1.0, 0.5, 0.0  ]
#     # omega = [ 0.0, fc , fmax ]
#     #
#     # smooth_window_fit = custom_recon_windows( omega, modt, feval, ftype='smooth' )
#
#     ramp = get_half_ramp( lenu, du )
#
#     # fc    = 0.55 # (MT_ratio[2] - 0.5) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[2] + (0.5 - MT_ratio[3]) / ( MT_ratio[2] - MT_ratio[3] ) * lp_mm[3]
#     # modt  = [ 1.0, 1.0      , 1.0     , 0.5     , 0.0 ]
#     # omega = [ 0.0, 0.25 * fc, 0.6 * fc, 1.0 * fc, fmax ]
#
#     # sharp_window_fit = custom_recon_windows( omega, modt, feval, ftype='sharp' )
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     ax.scatter( lp_mm, MT_ratio, color='tab:red' )
#     ax.plot(feval, ramp             , color='tab:gray' , label='Ramp')
#     # ax.plot(feval, smooth_window_fit, color='tab:blue' , label='Smooth')
#     ax.plot(feval, sharp_window_fit, color='tab:blue' , label='Sharp')
#     ax.set_title('Reconstruction Kernel Windows')
#     ax.legend()
#     plt.xlabel('1/mm')
#     plt.ylabel('Modulation')


##

    # custom_recon_windows
    # mean_fit = np.array( [1036.86849586,1006.06937716,1023.15952000,1004.63905897,1024.33044946], np.float32 )
    # min_fit  = np.array( [612.73352051,713.95794678,843.14019775,764.48101807,878.80511475], np.float32 )
    # max_fit  = np.array( [1460.00244141,1277.19494629,1189.94409180,1135.09399414,1115.90979004], np.float32 )
    #
    # # [0.4153279 , 0.27609658, 0.17000191, 0.18167302, 0.11622778]
    # MT_fit = np.abs( max_fit - min_fit ) / 2040

    # custom_recon_windows_v2; smooth, 0.65
    # mean_fit = np.array( [1040.57439952,1012.15543348,1028.62388705,1009.62443560,1028.25649214], np.float32 )
    # min_fit  = np.array( [543.31274414 ,648.19610596 ,833.85247803 ,772.95196533 ,869.80065918], np.float32 )
    # max_fit  = np.array( [1495.72924805,1326.01049805,1219.54724121,1158.59875488,1150.33276367], np.float32 )
    #
    # # [0.4153279 , 0.27609658, 0.17000191, 0.18167302, 0.11622778]
    # MT_fit = np.abs( max_fit - min_fit ) / 2040

## Reconstruct B30 fit for ACR.
# if True:
#
#     recon_lib_path   = '/home/dpc/Documents/MCR_Toolkit_2/Operators/DD_operators_v25.so'
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/recon_params_v5_Python.txt'
#
#     # Focal spot position 1.
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom/ACR_Phantom_focal_position_1_B30.txt'
#     recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/ACR_Phantom_focal_position_1_B30.txt'
#
#     # Focal spot position 2.
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom/ACR_Phantom_focal_position_2_B30.txt'
#     # recon_param_file = '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/ACR_Phantom_focal_position_2_B30.txt'
#
#     DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )
#
#     DukeSim_Op.load_projections()
#
#     DukeSim_Op.operator_setup()
#
#     DukeSim_Op.reconstruction()

# Projection mask

    # sigma = 40 / ( 2 * np.sqrt( 2 * np.log(2) ) );
    # G     = np.exp( -np.linspace( 0, 735, 736 )**2 / ( 2 * sigma**2 ) )
    # mask  = 1.0 - G - G[::-1]
    # self._Y *= mask

    # sigma = 40 / ( 2 * np.sqrt( 2 * np.log(2) ) );
    # G     = np.exp( -np.linspace( 0, 715, 716 )**2 / ( 2 * sigma**2 ) )
    # mask  = 1.0 - G - G[::-1]; mask = np.pad( mask, (10,10) )
    # self._Y *= mask


## Noise power
# if True:
#
#     X_ramp = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom/B30_Fit/MSE_Toolkit_Ramp_slice153.nii')
#     X_B30  = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom/B30_Fit/Mayo_B30_slice120.nii')
#     # X_fit  = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom/B30_Fit/MSE_Toolkit_B30_Fit_slice153.nii')
#     X_fit  = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom/B30_Fit/MSE_Toolkit_B30_Fit_v2_slice153.nii')
#
#     dx = 0.4883
#     X_ramp_F = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_ramp[0,0,128:-128,128:-128].astype('float64') ) ) )**2
#     X_B30_F  = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_B30 [0,0,128:-128,128:-128].astype('float64') ) ) )**2
#     X_fit_F  = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_fit [0,0,128:-128,128:-128].astype('float64') ) ) )**2
#
#     X_ramp_F_NP = np.sum( X_ramp_F )
#     X_B30_F_NP  = np.sum( X_B30_F )
#     X_fit_F_NP  = np.sum( X_fit_F )
#
#     plt.imshow( np.log( np.concatenate( [X_ramp_F,X_B30_F,X_fit_F], axis=1 ) + 1 ), cmap='gray' )
#
#     save_nii( np.concatenate( [X_ramp_F,X_B30_F,X_fit_F], axis=1 ), '/data/DukeSim_XCAT_Phantom/ACR_Phantom/NPS.nii' )


## Noise power
# Full dose ACR phantom
# if True:
#
#     from scipy.fft import rfft2, irfft2, fftshift, ifftshift
#
#     X_ramp = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/MCR_Toolkit_Ram-Lak_slice173.nii')[0,0,:,:]
#     X_B30  = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/Mayo_B30_slice120.nii')[0,0,:,:]
#     X_fit  = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/MCR_Toolkit_B30_Fit_slice173.nii')[0,0,:,:]
#     X_fit2 = load_nii('/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/MCR_Toolkit_B30_Fit_slice200.nii')[0,0,:,:]
#
#     # Multi-slice rebinning uses data from rows which were filtered independently
#     # Does this cause the spurious high spatial frequencies when performing analytical reconstruction with a filter?
#     # Enforce the cutoff frequency here in the image domain...
#     dx = 0.4883
#
#     xi, yi = np.meshgrid( (1.0/dx) * np.linspace( 0, 256//2, 257 ) / ( 256 + 1 ), np.linspace( -(512//2)+1, 512//2, 512 ) / ( 256 ) )
#     cutoff = np.linspace( 0, 256//2, 257 ) / (256 + 1)
#     mask = ifftshift( ( np.sqrt( xi**2 + yi**2 ) < ( 1.0 / ( 2.0 * 0.7 ) ) ).astype('float32'), axes=0 )
#     X_fit_out = irfft2( mask * rfft2(X_fit) )
#     X_fit2_out = irfft2( mask * rfft2(X_fit2) )
#
#     X_ramp_F  = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_ramp[128:-128,128:-128].astype('float64') ) ) )**2
#     X_B30_F   = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_B30 [128:-128,128:-128].astype('float64') ) ) )**2
#     X_fit_F   = ( dx / 256 )**2 * np.abs( np.fft.fftshift( np.fft.fft2( X_fit_out[128:-128,128:-128].astype('float64') ) ) )**2
#
#
#     X_ramp_F_NP = np.sum( X_ramp_F )  # 3930718 HU^2 * mm^2
#     X_B30_F_NP  = np.sum( X_B30_F )   # 1219337 HU^2 * mm^2
#     X_fit_F_NP  = np.sum( X_fit_F )   # 1994082 HU^2 * mm^2
#
#     plt.imshow( np.log( np.concatenate( [X_ramp_F,X_B30_F,X_fit_F], axis=1 ) + 1 ), cmap='gray' )
#
#     save_nii( np.concatenate( [X_ramp_F,X_B30_F,X_fit_F], axis=1 ), '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/NPS.nii' )
#     save_nii( X_fit2_out, '/data/DukeSim_XCAT_Phantom/ACR_Phantom_FD/B30_Fit/MCR_Toolkit_B30_Fit_slice200_windowed.nii' )

## MT values after full dose fitting with a sharp window
# mean_Fit = np.array( [1041.37335857,1010.97107937,1029.36758634,1012.31150388,1028.91318811], np.float32 ) # ,1033.37806013
# min_Fit  = np.array( [524.92297363 ,727.53637695 ,863.52404785 ,835.93951416 ,935.38391113 ], np.float32 ) #  ,983.73547363
# max_Fit  = np.array( [1551.35595703,1301.77441406,1150.19335938,1096.17358398,1079.60815430], np.float32 ) # ,1086.68078613
# MT_Fit = np.abs( max_Fit - min_Fit ) / 2040
