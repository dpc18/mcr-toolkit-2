
import os, glob
import pydicom
import numpy as np
from Python.Recon_Utilities import save_nii

matsize = 512

## Functions
def import_DICOMs( directory, matsize=matsize ):
# Assumes files are saved as 000...(integer).dcm

    # Find the dcm files in the provided directory
    files  = glob.glob('*.dcm',root_dir=directory)

    X = np.zeros( [len(files),matsize,matsize], np.float32 )

    for i in range(len(files)):

        idx = int(files[i][:-4])
        dcf = pydicom.dcmread(os.path.join(directory,files[i]))
        assert dcf.Columns == matsize
        X[idx-1,...] = dcf.RescaleSlope * dcf.pixel_array.astype('float32') + dcf.RescaleIntercept

    return X


## Import real reference data (threshold 1, threshold 2, VMI)
if True:

    X_T1  = import_DICOMs( '/data/Count1_DukeSim_Recon/real_ACR_images_ReconCT/real_5_0.4mm_Br40/Abdomen_Multi_Phase_[factory](Adult)_Standard_-_PNR_20240315_172300/ProtocolModel_WFBP_T1_Br40f(3)_0.4_(0.4)_[A,1]/' )
    X_T2  = import_DICOMs( '/data/Count1_DukeSim_Recon/real_ACR_images_ReconCT/real_5_0.4mm_Br40/Abdomen_Multi_Phase_[factory](Adult)_Standard_-_PNR_20240315_172300/ProtocolModel_WFBP_T2_Br40f(3)_0.4_(0.4)_[A,1]/' )
    X_VMI = import_DICOMs( '/data/Count1_DukeSim_Recon/real_ACR_images_ReconCT/real_5_0.4mm_Br40/Abdomen_Multi_Phase_[factory](Adult)_Standard_-_PNR_20240315_172300/ProtocolModel_Mono_70keV_Br40f(3)_0.4_(0.4)_[A,1]/' )

## Solve for the linear mapping
if True:

    # Average similar ACR slices together to reduce noise
    X_VMI_avg = np.mean( X_VMI[320:400,...], axis=0 )
    X_T1_avg  = np.mean(  X_T1[320:400,...], axis=0 )
    X_T2_avg  = np.mean(  X_T2[320:400,...], axis=0 )

    coeffs = np.reshape( X_VMI_avg, [1,-1] ) @ np.linalg.pinv( np.stack( [X_T1_avg.flatten(),X_T2_avg.flatten()], 0 ) )

## Test the linear mapping in the source data
if True:

    per_error = 100 * np.linalg.norm( coeffs @ np.stack( [X_T1.flatten(),X_T2.flatten()], 0 ) - X_VMI.flatten() ) / np.linalg.norm( X_VMI.flatten() )

    print('Real VMI fit, percent error: %0.2f' % (per_error) + '%')

    # save_nii( X_VMI, '/data/Temp/X_VMI.nii' )
    # save_nii( np.reshape( coeffs @ np.stack( [X_T1.flatten(),X_T2.flatten()], 0 ), X_T1.shape ) , '/data/Temp/X_VMI_est.nii' )

## Test the linear mapping on DukeSim simulated data
if True:

    Xd_T1  = import_DICOMs( '/data/Count1_DukeSim_Recon/simulation_ACR_images_ReconCT/sim_5_0.4mm_Br40/Unknown_Standard_-_PNR_20240506_174448/Unknown_WFBP_T1_Br40f(3)_0.4_(0.4)_[A,1]/' )
    Xd_T2  = import_DICOMs( '/data/Count1_DukeSim_Recon/simulation_ACR_images_ReconCT/sim_5_0.4mm_Br40/Unknown_Standard_-_PNR_20240506_174448/Unknown_WFBP_T2_Br40f(3)_0.4_(0.4)_[A,1]/' )
    Xd_VMI = import_DICOMs( '/data/Count1_DukeSim_Recon/simulation_ACR_images_ReconCT/sim_5_0.4mm_Br40/Unknown_Standard_-_PNR_20240506_174448/Unknown_Mono_70keV_Br40f(3)_0.4_(0.4)_[A,1]/' )

    per_error = 100 * np.linalg.norm( coeffs @ np.stack( [Xd_T1.flatten(),Xd_T2.flatten()], 0 ) - Xd_VMI.flatten() ) / np.linalg.norm( Xd_VMI.flatten() )

    print('DukeSim VMI fit, percent error: %0.2f' % (per_error) + '%')

    # save_nii( Xd_VMI, '/data/Temp/Xd_VMI.nii' )
    # save_nii( np.reshape( coeffs @ np.stack( [Xd_T1.flatten(),Xd_T2.flatten()], 0 ), Xd_T1.shape ) , '/data/Temp/Xd_VMI_est.nii' )
