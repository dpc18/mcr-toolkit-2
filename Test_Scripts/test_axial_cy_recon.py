
from Python.Recon_Operators import DukeSim_Recon
from Python.Recon_Utilities import custom_recon_windows_v2, get_half_ramp, load_nii, save_nii
import numpy as np
import matplotlib.pyplot as plt

import os.path

if True:

# Test to see if the axial reconstruction artifact is caused by the Toolkit or a mismatch with DukeSim
# by using the DukeSim geometry but creating and reconstructing the projections with the Toolkit.

    X = np.squeeze( load_nii('/data/DukeSim_XCAT_Phantom_Axial/test_phantom.nii') )
    # X = X[ 80-5:80+4 , ... ] # 2.0 mm slice thickness
    X = X[ 80-9:80+9, ... ] # 1.0 mm slice thickness
    X = np.pad( X, [[0,0],[56,56],[56,56]] )

    recon_lib_path   = '/home/dpc/Documents/MCR_Toolkit_2/Operators/DD_operators_v25.so'

    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/recon_params_v5_Python_pitch0_test_phantom_SART.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/recon_params_SART.txt'
    recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/XCAT_Files/recon_params_SART.txt'

    DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )

    DukeSim_Op.load_projections()

    # We want to create projections that are not rebinned.
    DukeSim_Op._recon_type = 'single_source_SART'

    DukeSim_Op.operator_setup()

    Y_test = DukeSim_Op.R( X, DukeSim_Op._W )

    recon_name = os.path.join( DukeSim_Op._outpath, 'Y_test.nii' )
    save_nii( Y_test, recon_name, vox_size=[DukeSim_Op._np,DukeSim_Op._dv,DukeSim_Op._du] )


    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/recon_params_WFBP.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/recon_params_SART.txt'
    recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/XCAT_Files/recon_params_SART.txt'
    # recon_param_file = '/data/DukeSim_XCAT_Phantom_Axial/Sample/XCAT_Files/recon_params_WFBP.txt'

    DukeSim_Op = DukeSim_Recon( recon_lib_path, recon_param_file )

    DukeSim_Op.load_projections()

    DukeSim_Op.operator_setup()

    DukeSim_Op.reconstruction()





