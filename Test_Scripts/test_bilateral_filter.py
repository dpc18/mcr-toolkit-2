#     Copyright (C) 2024, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
#     Original Author: Darin Clark, PhD
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References

    # Tomasi, C., & Manduchi, R. (1998, January).
    # Bilateral filtering for gray and color images.
    # In Sixth international conference on computer vision (IEEE Cat. No. 98CH36271) (pp. 839-846). IEEE.
    #
    # Takeda, H., Farsiu, S., & Milanfar, P. (2007).
    # Kernel regression for image processing and reconstruction.
    # IEEE Transactions on image processing, 16(2), 349-366.
    #
    # Clark, D. P., Ghaghada, K., Moding, E. J., Kirsch, D. G., & Badea, C. T. (2013).
    # In vivo characterization of tumor vasculature using iodine and gold nanoparticles and dual energy micro-CT.
    # Physics in Medicine & Biology, 58(6), 1683.
    #
    # Clark, D. P., & Badea, C. T. (2017).
    # Hybrid spectral CT reconstruction.
    # PloS one, 12(7), e0180324.

'''
Imports
'''

import ctypes
from Python.Denoising import BilateralFilter, RSKR
from Python.Recon_Utilities import imc, load_nii

import numpy as np # (v1.21.5)

import matplotlib # (v3.5.1)
import matplotlib.pyplot as plt

import nibabel as nib # (5.1.0)

from scipy.signal import convolve

import os

'''
Paths
'''

if True:

    toolkit_path = '/home/dpc/Documents/MCR_Toolkit_2'

    recon_lib_path = os.path.join(toolkit_path,'Operators/DD_operators_v25.so')
    test_data      = os.path.join(toolkit_path,'Test_Data')

'''
Test Data
'''

if True:

    X1 = np.zeros( (256,256,256), np.float32 )
    X2 = np.zeros( (256,256,256), np.float32 )

    X1[:,128:,:] = 0.1
    X2[:,128:,:] = 0.5

    # A Gaussian noise with a standard deviation of 0.1
    sigma = 0.10
    X1 += sigma * np.random.randn( 256, 256, 256 )
    X2 += sigma * np.random.randn( 256, 256, 256 )


'''
Test 3D BF on a single, low contrast volume (X1).
Perform kernel resampling on the GPU.
Check the accuracy of local noise estimation.
'''

if True:

    # Filter parameters
    nvols            = 1  # (int32)   number of volumes to jointly filter
    ntimes           = 1  # (int32)   odd number of time points along which to extend the filtration domain
    gpu_idx          = 0  # (int32)   hardware index pointing to the GPU to be used for BF (defaults to the system default)

    BF = BilateralFilter( recon_lib_path, nvols, ntimes, gpu_idx=gpu_idx, return_noise_map=True )

    # Apply filter
    X1f, noise_map = BF( X1 )

    # View results
    imc( img_list=[X1[128,:,:],X1f[128,:,:]], shape=[256,256], title='3D BF Results (low contrast volume): Before vs. After', fig_num=1 )

    print('Expected noise coefficient: ' + str(sigma) + ' vs. measured noise coefficient: ' + str(np.mean(noise_map)))

'''
Test 3D BF on a single, low contrast volume (X1).
Pass in a kernel resampled volume.
Expected results: 'GPU resampling vs. CPU resampling (% error): 6.6228e-05'
'''

if True:

# Perform CPU resampling
# Note: The 3D resampling kernel is approximated as the sum of 3 separable kernels for computational efficiency.

    A  = BF.resampling_kernel( w2=BF.init_params['resampling'], verbose=False )
    A1 = A[0,:]
    A2 = A[1,:]
    A3 = A[2,:]

    d = X1.copy()
    d = np.pad( d, BF.init_params['radius'], 'reflect' )

    d_in = d.copy()
    d    =     convolve( convolve( convolve( d_in, A1[np.newaxis,np.newaxis,:], mode='valid' ),
                                                   A1[np.newaxis,:,np.newaxis], mode='valid' ),
                                                   A1[:,np.newaxis,np.newaxis], mode='valid' )
    d    = d + convolve( convolve( convolve( d_in, A2[np.newaxis,np.newaxis,:], mode='valid' ),
                                                   A2[np.newaxis,:,np.newaxis], mode='valid' ),
                                                   A2[:,np.newaxis,np.newaxis], mode='valid' )
    d    = d + convolve( convolve( convolve( d_in, A3[np.newaxis,np.newaxis,:], mode='valid' ),
                                                   A3[np.newaxis,:,np.newaxis], mode='valid' ),
                                                   A3[:,np.newaxis,np.newaxis], mode='valid' )

    del d_in

# Pass resampled volume to the filter

    # Apply filter
    X1_rf, _ = BF( X1, d.astype(np.float32) )

    error = 100 * np.linalg.norm( X1f - X1_rf ) / np.linalg.norm( X1_rf )

    print( 'GPU resampling vs. CPU resampling (% error): ' + str( error ) )

    imc( img_list=[X1f[128,:,:],X1_rf[128,:,:]], shape=[256,256], title='3D BF Results (low contrast volume): GPU vs. CPU Resampling', fig_num=2 )

'''
Demonstrate adaptive noise estimation in BF v5
NOTE: For efficiency, adaptation is hard-wired to use 32x32 pixel patches with 16 pixel overlap in x and y.
'''

if True:

    Xp  = load_nii( os.path.join( toolkit_path, 'Test_Data/contrast_res_phantom.nii.gz' ) ).astype(np.float32)
    Xp /= np.amax(Xp)
    N1  = 0.0 * np.random.randn( Xp.shape[0], Xp.shape[1], Xp.shape[2] ).astype(np.float32)
    N2  = 0.3 * np.random.randn( Xp.shape[0], Xp.shape[1], Xp.shape[2] ).astype(np.float32)

    # Blend noise maps to generate a noise standard deviation gradient
    weights = np.linspace( 0, 1.0, Xp.shape[1] ).astype(np.float32)
    weights = np.reshape(weights,[1,-1,1])
    N       = weights * N1 + (1.0-weights) * N2

    Xpn = Xp + N

    Xf_v5, noise_v5 = BF( Xpn )

    print('Expected average noise coefficient: ' + str(0.15) + ' vs. measured average noise coefficient ' + str(np.mean(noise_v5)) )

    imc( img_list=[Xp[85,:,:],Xpn[85,:,:],Xf_v5[85,:,:]], shape=[440,440], skip=1, stddevs=[2,3],
         title='3D BF Results: Expected, Input, Output BFv5, Resid BFv5', fig_num=3 )

    imc( img_list=[0.3*(1.0-weights[0,...])*np.ones_like(noise_v5[85,:,:]),noise_v5[85,:,:]], skip=1, shape=[440,440], fig_num=4,
         title='Noise Mapping (Standard Deviation): Expected Noise Map, Measured Noise Map' )

'''
Test Joint, 3D BF (3D + Energy).
Filtration between a low contrast volume (X1) and a high contrast volume (X2) with the same noise level.
Constructing a joint kernel will transfer image structure from the high contrast volume to the low contrast volume.
'''

if True:

# Single channel filtration

    X1f, _ = BF( X1 )

# Multi-channel filtration

    # Create a new, joint filter
    BF_joint = BilateralFilter( recon_lib_path, nvols=2, gpu_idx=gpu_idx, return_noise_map=False )

    X_DE = np.stack( [X1,X2], axis=0 )
    Xjf, _ =  BF_joint( X_DE )

    imc( img_list=[ X1[128,...], X1f[128,...], Xjf[0,0,128,...] ], skip=2, shape=[256,256], fig_num=5, stddevs=[1,1],
         title='3D Joint BF Results: Orig. (low cont.) - 3D BF - Joint BF' )

'''
Test Joint, 3D BF (3D + Energy) w/ Rank-Sparse Kernel Regression (RSKR).
Filtration between a low contrast volume (X1) and a high contrast volume (x2) with the same noise level.
Performing RSKR allows stronger filtration of the difference between the two input images.
'''

if True:

    # Additional RSKR() Inputs:
    #   pSVT_operator    : (None) not used
    #                      (pSVT object) calibrated to denoise X; applied in sequence with the BF_operator
    #   mask             : (False) do not use a mask
    #                      (True)  use the implicit reconstruction mask (see Python.Recon_Utilities.make_default_rmask() )
    #                              improves accuracy when estimating the average global noise standard deviation
    #                      ([z,y,x] np.ndarray) binary mask; 1.0 for voxels inside of the reconstruction mask
    #   weights          : (None)            do not use weighting
    #                      ([e,] np.ndarray) multiplicative weights applied per energy prior to the SVD, [e]
    #                                        typically weights[e] = 1 / ( relative_sigma[e] * attn_water[e] ), where relative_sigma[e]
    #                                        is the average noise standard deviation scaled to 1.0 for the energy with the lowest noise
    #   stride           : (1)       denoise within a BF domain of 13**3 voxels
    #                      (odd, >1) dilate the domain size by this factor to affect a larger receptive field, address lower frequency noise
    #   exponent         : exponential factor which scales denoising strength as a function of each singular vector's significance (E value)
    #                      less significant singular vectors (smaller E values) are denoised more
    #                      BF multiplier = lambda0 * ( E[0,0] / diag(E) ) ** exponent

    joint_BF_w_RSKR = RSKR( recon_lib_path, nvols=2, mask=False )

    X_RSKR = joint_BF_w_RSKR( X_DE[:,np.newaxis,...] )

    imc( img_list=[ X1[128,...], Xjf[0,0,128,...], X_RSKR[0,0,128,...] ], skip=3, shape=[256,256], fig_num=6, stddevs=[1,1],
         title='3D Joint BF Results: Orig. (low cont.) - Joint BF - RSKR' )


'''
Test 4D BF (3D + Time)
Extend the filtration domain by 1 phase before and after the phase to be filtered.
Demonstrate that spatio-temporal resampling correctly averages along the temporal dimension.
'''
if True:

    sigma = 0.05
    Xt1   = 0.2 * np.ones( (256,256,256), np.float32 ) + sigma * np.random.randn( 256, 256, 256 ).astype(np.float32)
    Xt2   = 1.0 * np.ones( (256,256,256), np.float32 ) + sigma * np.random.randn( 256, 256, 256 ).astype(np.float32)
    Xt3   = 0.7 * np.ones( (256,256,256), np.float32 ) + sigma * np.random.randn( 256, 256, 256 ).astype(np.float32)
    Xt_in = np.stack( (Xt1,Xt2,Xt3), axis=0 )[np.newaxis,...]

    # Kernel resampling is applied to each time point.
    # The resampled value for the central time point is then this weighted average of the resampled intensity values
    # of each invidual time point.
    # Must sum to 1.
    At = [0.2, 0.6, 0.2]

    multiplier = 2.5  # (single*) scales filtration strength as this multiple of the estimated noise standard deviation; vector or broadcasts over nvols
    time_mode  = 0    # (int)     time mode (ntimes > 1): (0) Valid - 3 input volumes, 1 output volume

    BF_time = BilateralFilter( recon_lib_path, nvols=1, ntimes=3, At=At, gpu_idx=gpu_idx, return_noise_map=True, time_mode=time_mode, multiplier=multiplier )

    Xt_out, noise = BF_time( Xt_in )

    imc( img_list=[ Xt_in[0,0,128,...], Xt_in[0,1,128,...], Xt_out[0,0,128,...], Xt_in[0,2,128,...] ], skip=3, shape=[256,256], fig_num=7, stddevs=[1,1],
         title='4D BF Results: Time Point (TP) 1 - TP 2 - TP 2 Filtered - TP 3' )

    print('Expected noise coefficient, time point 2: ' + str( sigma ))
    print('vs. measured noise coefficient, time point 2: ' + str( np.mean( noise ) ))

    # Compute expected result...

    # weighted average from kernel resampling
    a = np.sum( np.array( [0.2, 1, 0.7] ) * np.array( [0.2, 0.6, 0.2] ) )

    # Range kernel weights per time point
    R1 = np.exp( -(Xt1-a)**2 / (2*multiplier**2*sigma**3) )
    R2 = np.exp( -(Xt2-a)**2 / (2*multiplier**2*sigma**3) )
    R3 = np.exp( -(Xt3-a)**2 / (2*multiplier**2*sigma**3) )

    expected_val = np.sum( Xt1 * R1 + Xt2 * R2 + Xt3 * R3 ) / np.sum( R1 + R2 + R3 )

    print( 'Expected value in the filtered volume (approx.): ' + str(expected_val) )
    print( 'Measured value in the filtered volume: ' + str( np.mean( Xt_out ) ) )


'''
Test 4D, joint BF. (3D + time + energy)
This is the most general filtration case for combined time and energy data.
All other filtration cases are special cases of 4D, joint BF.
4D: extending the filtration domain along the time dimension
joint: multiplying range kernels computed at different energies
'''

if True:

    # Photon-counting cardiac micro-CT
    # ApoE knockout mouse
    # WFBP reconstruction
    # See data set details in the following reference:
    # Clark, D. P., Holbrook, M., Lee, C. L., & Badea, C. T.
    # Photon-counting cine-cardiac CT in the mouse.
    # PLOS ONE, 2019. 14(9), e0218417.
    # e,t,z,y,x => (4,10,32,256,256)
    X = load_nii( os.path.join( test_data, 'Mouse_Cardiac_PCCT.nii.gz' ) )

    # Kernel resampling is applied to each time point.
    # The resampled value for the central time point is then this weighted average of the resampled intensity values
    # of each invidual time point.
    # Must sum to 1.
    At = [0.2,0.6,0.2]

    time_mode = 1  # (int) time mode (1) Wrap - cardiac filtration domains wrap around

    # Attenuation values for water per energy threshold
    RSKR_weights = np.array( [0.002767,0.002592,0.002496,0.002415], np.float32 )

    RSKR_4D_joint = RSKR( recon_lib_path, nvols=X.shape[0], ntimes=X.shape[1], At=At, mask=False, time_mode=time_mode, weights=RSKR_weights, gpu_idx=gpu_idx )

    X_out = RSKR_4D_joint( X )

    imc( img_list=[ X[:,0,16,...], X_out[:,0,16,...] ], shape=[4*256,256], fig_num=8, stddevs=[1.5,2.5], abs_res=True,
         title='4D, Joint BF Results (All Energies, Phase 1): Input (Row 1) - Output (Row 2) - Residuals (Row 3)' )

    imc( img_list=[ X[2,:5,16,...], X_out[2,:5,16,...] ], shape=[5*256,256], fig_num=9, stddevs=[1.5,2.5], abs_res=False,
         title='4D, Joint BF Results (Energy 3, Phases 1-5): Input (Row 1) - Output (Row 2) - Residuals (Row 3)' )


'''
Test 4D, joint BF (3D + time + energy) with a stride > 1.
A stride greater than 1 effectively increases the size of the filtration domain.
'''

if True:

    stride_in = 3 # Average the results of BF with a second BF operation applied after resampling the volume at this scale.

    RSKR_4D_joint_strided = RSKR( recon_lib_path, nvols=X.shape[0], ntimes=X.shape[1], At=At, mask=False, time_mode=time_mode, weights=RSKR_weights, gpu_idx=gpu_idx,
                                  stride = stride_in )

    X_out = RSKR_4D_joint_strided( X )

    imc( img_list=[ X[:,0,16,...], X_out[:,0,16,...] ], shape=[4*256,256], fig_num=10, stddevs=[1.5,2.5], abs_res=True,
         title='4D, Joint BF Results (All Energies, Phase 1): Input (Row 1) - Output (Row 2) - Residuals (Row 3)' )

    imc( img_list=[ X[2,:5,16,...], X_out[2,:5,16,...] ], shape=[5*256,256], fig_num=11, stddevs=[1.5,2.5], abs_res=False,
         title='4D, Joint BF Results (Energy 3, Phases 1-5): Input (Row 1) - Output (Row 2) - Residuals (Row 3)' )




















