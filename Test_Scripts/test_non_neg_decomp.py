#     Copyright (C) 2024, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
#     Original Author: Darin Clark, PhD
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

if True:

    import numpy as np

    from Python.Recon_Utilities import load_nii, save_nii, mat_decomp_LS, mat_decomp_LS_non_neg

    import matplotlib.pyplot as plt

    import os

    outpath = '/data/Python_Recon/'

    X = load_nii('/data/Python_Recon/X_RSKR.nii')
    X = X[:,:1,...]

    M = np.array( [ [0.0027611 , 0.00257994, 0.00249895, 0.00240828], # water
                    [0.00329706, 0.00298967, 0.00284651, 0.00266453], # 5 mg/mL gold
                    [0.00497604, 0.00468759, 0.00427131, 0.00364161]  # 12 mg/mL iodine
                  ], np.float32 )

    # Subtract water from mixtures
    M[1:,:] = M[1:,:] - M[:1,:]

    # Rescale to treat all materials equally during material decomposition
    scale = np.sqrt( np.sum( M**2, axis=1, keepdims=True ) )

    # Normalized condition number: 1.7244412/0.0623512 = 27.66
    M = M / scale

    C_LS = mat_decomp_LS( X, M )

    save_nii(C_LS,os.path.join( outpath, 'C_LS.nii' ))

    C_noneg = mat_decomp_LS_non_neg( X, M )

    save_nii(C_noneg,os.path.join( outpath, 'C_noneg.nii' ))

