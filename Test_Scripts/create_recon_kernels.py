# Copyright (C) 2024, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import numpy as np # (v1.21.5)
import time
import sys
import os.path
import math
# import csv
import matplotlib # (v3.5.1)
import matplotlib.pyplot as plt

import nibabel as nib # (5.1.0)

from Python.Recon_Utilities import get_half_ramp

# Test function

if True:

    def recon_window( omega, modt, freq, ftype ):
    # omega : spatial frequencies of control points
    # modt  : modulation of control points
    # freq  : spatial frequencies sampled by the discrete Fourier transform
    # ftype : 'sharp'
    #           mt0 = mt1 = mtf2
    #           mt3 >= mt4
    #           mt4 >= 0
    #         'smooth'
    #           mt0 = 1.0
    #           mt1 < 1.0
    #           mt2 = 0.0
    #         'enhancing'
    #           mt0 = mt2
    #           mt1 >= 1
    #

        window = np.zeros( (len(freq),), np.float32 )

        if ftype == 'sharp' or ftype == 'Sharp':

            assert len(omega) == 5
            assert len(modt)  == 5

            assert modt[0] == 1.0 and modt[1] == 1.0 and modt[2] == 1.0
            assert modt[3] >= modt[4]

            # flat part
            window[freq <= omega[2]] = 1.0
            window[freq >  omega[4]]  = 0.0

            # curve part - 3rd order
            A = np.array( [ [ omega[2]**3   , omega[2]**2 , omega[2]**1, 1 ],
                            [ omega[3]**3   , omega[3]**2 , omega[3]**1, 1 ],
                            [ omega[4]**3   , omega[4]**2 , omega[4]**1, 1 ],
                            [3 * omega[2]**2, 2 * omega[2], 1          , 0 ]] )
            b = np.array( [[1.0],[modt[3]],[modt[4]],[0.0]] )

            # curve part - 4th order
            # A = np.array( [ [omega[2]**4    , omega[2]**3    , omega[2]**2 , omega[2]**1, 1 ],
            #                 [omega[3]**4    , omega[3]**3    , omega[3]**2 , omega[3]**1, 1 ],
            #                 [omega[4]**4    , omega[4]**3    , omega[4]**2 , omega[4]**1, 1 ],
            #                 [4 * omega[4]**3, 3 * omega[4]**2, 2 * omega[4],           1, 0 ],
            #                 [4 * omega[2]**3, 3 * omega[2]**2, 2 * omega[2],           1, 0 ]] )
            #                 # [omega[3]*6 ,       2    ,           0,       0 ]] )
            # b = np.array( [[1.0],[modt[3]],[modt[4]],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            interp = x[0]*freq**3 + x[1]*freq**2 + x[2]*freq**1 + x[3]
            # interp = x[0]*freq**4 +x[1]*freq**3 + x[2]*freq**2 + x[3]*freq**1 + x[4]

            idxs = np.logical_and(freq > omega[2],freq <= omega[4])
            window[idxs] = interp[idxs]

            window[window < 0  ] = 0.0
            window[window > 1.0] = 1.0

        elif ftype == 'smooth' or ftype == 'Smooth':

            assert len(omega) == 3
            assert len(modt)  == 3

            assert modt[0] == 1.0
            assert modt[1] < modt[0] and modt[1] >= 0.0
            assert modt[2] == 0.0

            # curve part - 4th order
            A = np.array( [ [omega[0]**4    , omega[0]**3    , omega[0]**2 , omega[0]**1, 1 ],
                            [omega[1]**4    , omega[1]**3    , omega[1]**2 , omega[1]**1, 1 ],
                            [omega[2]**4    , omega[2]**3    , omega[2]**2 , omega[2]**1, 1 ],
                            [4 * omega[0]**3, 3 * omega[0]**2, 2 * omega[0],           1, 0 ],
                            [4 * omega[2]**3, 3 * omega[2]**2, 2 * omega[2],           1, 0 ]] )
                            # [omega[3]*6 ,       2    ,           0,       0 ]] )
            b = np.array( [[1.0],[modt[1]],[0.0],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            window = x[0]*freq**4 +x[1]*freq**3 + x[2]*freq**2 + x[3]*freq**1 + x[4]

            window[window < 0  ] = 0.0
            window[window > 1.0] = 1.0

        elif ftype == 'enhance' or ftype == 'Enhance':

            assert len(omega) == 6
            assert len(modt)  == 6

            assert modt[0] == 1.0
            assert modt[1] >= 1.0
            assert modt[2] >= 1.0
            assert modt[3] == 1.0
            assert modt[4] < modt[3]
            assert modt[5] < modt[4]

            dofs = 8

            A = np.array( [ [ omega[0]**i for i in range(dofs) ],
                            [ omega[1]**i for i in range(dofs) ],
                            [ omega[2]**i for i in range(dofs) ],
                            [ omega[3]**i for i in range(dofs) ],
                            [ omega[4]**i for i in range(dofs) ],
                            [ omega[5]**i for i in range(dofs) ],
                            [0] + [ i * omega[0]**(i-1) for i in range(1,dofs) ],
                            [0] + [ i * omega[5]**(i-1) for i in range(1,dofs) ] ] )

            b = np.array( [[1.0],[modt[1]],[modt[2]],[1.0],[modt[4]],[modt[5]],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            window = [ x[i]*freq**i for i in range(dofs) ]
            window = np.sum( np.stack(window,axis=0), axis=0)

            # idxs = np.logical_and(freq >= omega[0], freq <= omega[1])
            # window[idxs] = 1.0
            # idxs = np.logical_and(freq >= omega[0], freq <= omega[3])
            # window[idxs] = np.maximum(window[idxs],1.0)
            window[window < 0] = 0.0

        return window


# Test problem

if True:

    # Number of detector elements per row
    nu = 1024

    # Detector pixel width at isocenter (mm)
    du = 0.6

    # 1 / mm
    freq = ( 1.0 / du ) * np.linspace( 0, (nu//2) / (nu + 1), nu//2+1 )
    # freq = np.linspace( 0, 0.85, 100 )
    fmax = (1.0/du) * (nu//2) / (nu + 1)

    # Ramp filter        : highest possible noise, spatial resolution
    ramp = get_half_ramp( nu, du )

    # Windows
    # "Sharp" filters    : modulation is greater than 0 at the cutoff frequency
    # "Cutoff" filters   : modulation goes to 0 at the cutoff frequency
    # "Smooth" filters   : modulation goes to 0 before the cutoff frequency
    # "Enhancing" filters: peak modulation is greater than 0

# Sharp kernels

    modt  = [1.0,1.0,1.0,1.0 ,0.5 ]
    omega = [0.0,0.2,0.3,0.67,fmax]
    sharp_window0 = recon_window( omega, modt, freq, ftype='sharp' )

    modt  = [1.0,1.0,1.0,0.5 ,0.1 ]
    omega = [0.0,0.2,0.3,0.67,fmax]
    sharp_window1 = recon_window( omega, modt, freq, ftype='sharp' )

    modt  = [1.0,1.0,1.0,0.8 ,0.4 ]
    omega = [0.0,0.2,0.3,0.67,fmax]
    sharp_window2 = recon_window( omega, modt, freq, ftype='sharp' )

    modt  = [1.0,1.0,1.0,0.2 ,0.0 ]
    omega = [0.0,0.2,0.3,0.67,fmax]
    sharp_window3 = recon_window( omega, modt, freq, ftype='sharp' )

    modt  = [1.0,1.0,1.0,0.05,0.0 ]
    omega = [0.0,0.2,0.3,0.67,fmax]
    sharp_window4 = recon_window( omega, modt, freq, ftype='sharp' )

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(freq, sharp_window0, color='tab:gray')
    ax.plot(freq, sharp_window1, color='tab:blue')
    ax.plot(freq, sharp_window2, color='tab:red')
    ax.plot(freq, sharp_window3, color='tab:green')
    ax.plot(freq, sharp_window4, color='tab:cyan')
    ax.set_title('Sharp Windows')

# Smooth kernels

    modt  = [1.0,0.8,0.0 ]
    omega = [0.0,0.4,fmax]
    smooth_window0 = recon_window( omega, modt, freq, ftype='smooth' )

    modt  = [1.0,0.6,0.0 ]
    omega = [0.0,0.4,fmax]
    smooth_window1 = recon_window( omega, modt, freq, ftype='smooth' )

    modt  = [1.0,0.5,0.0 ]
    omega = [0.0,0.4,fmax]
    smooth_window2 = recon_window( omega, modt, freq, ftype='smooth' )

    modt  = [1.0,0.4,0.0 ]
    omega = [0.0,0.4,fmax]
    smooth_window3 = recon_window( omega, modt, freq, ftype='smooth' )

    modt  = [1.0,0.2,0.0 ]
    omega = [0.0,0.4,fmax]
    smooth_window4 = recon_window( omega, modt, freq, ftype='smooth' )

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(freq, smooth_window0, color='tab:gray')
    ax.plot(freq, smooth_window1, color='tab:blue')
    ax.plot(freq, smooth_window2, color='tab:red')
    ax.plot(freq, smooth_window3, color='tab:green')
    ax.plot(freq, smooth_window4, color='tab:cyan')
    ax.set_title('Smooth Windows')

# Enhancement kernels

    modt  = [1.0,1.00,1.1,1.0,0.5,0.0 ]
    omega = [0.0,0.1 ,0.3,0.6,0.7,fmax]
    enhance_window0 = recon_window( omega, modt, freq, ftype='enhance' )

    modt  = [1.0,1.05,1.2,1.0,0.5,0.0 ]
    omega = [0.0,0.1 ,0.3,0.6,0.7,fmax]
    enhance_window1 = recon_window( omega, modt, freq, ftype='enhance' )

    modt  = [1.0,1.10,1.4,1.0,0.5,0.0 ]
    omega = [0.0,0.1 ,0.3,0.6,0.7,fmax]
    enhance_window2 = recon_window( omega, modt, freq, ftype='enhance' )

    modt  = [1.0,1.00,1.1,1.0,0.1,0.0 ]
    omega = [0.0,0.1 ,0.3,0.5,0.7,fmax]
    enhance_window3 = recon_window( omega, modt, freq, ftype='enhance' )

    modt  = [1.0,1.10,1.4,1.0,0.5,0.0 ]
    omega = [0.0,0.1 ,0.3,0.6,0.7,fmax]
    enhance_window4 = recon_window( omega, modt, freq, ftype='enhance' )

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(freq, enhance_window0, color='tab:gray')
    ax.plot(freq, enhance_window1, color='tab:blue')
    ax.plot(freq, enhance_window2, color='tab:red')
    ax.plot(freq, enhance_window3, color='tab:green')
    ax.plot(freq, enhance_window4, color='tab:cyan')
    ax.set_title('Enhancing Windows')