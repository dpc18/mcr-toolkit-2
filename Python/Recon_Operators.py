
# Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References

# Long, Y., Fessler, J. A., & Balter, J. M. (2010).
# 3D forward and back-projection for X-ray CT using separable footprints.
# IEEE transactions on medical imaging, 29(11), 1839-1850.
#
# Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
# Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
# Physics in Medicine & Biology, 49(11), 2209.
#
# Van der Vorst, H. A. (1992).
# Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
# SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
#
# Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
# Cone-beam reprojection using projection-matrices.
# IEEE transactions on medical imaging, 22(10), 1202-1214.
#
# Sleijpen, G. L., & Van Gijzen, M. B. (2010).
# Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
# SIAM journal on scientific computing, 32(5), 2687-2709.
# Algorithm 3.1


# Notes:
# => See geometry and data parameter definitions in Recon().__init__(...) below.

# TO DO:
# * Add save / load functionality for Recon objects
# * Revisit the uc_A and AM parameters for cone-parallel projection rebinning and WFBP reconstruction.
# * Allow parameter changes for initialized reconstruction objects (i.e. after calling DD_init())
# * Re-enable other forms of projection rebinning? (flat detector, temporal)

if True:

    import ctypes
    import numpy as np # (v1.21.5)
    import time
    import sys
    import os.path
    import math
    # import csv
    import matplotlib # (v3.5.1)
    import matplotlib.pyplot as plt

    from copy import deepcopy

    # from PIL import Image
    import scipy.signal

    from Python.Recon_Utilities import *
    from Python.Denoising import masked_noise_measurement_3D, RSKR

    from functools import partial

    # Global variables
    PI             = 3.14159265358979
    CRITICAL_PITCH = 0.8 # pitch >= to this value triggers short-scan reconstruction for single-source data

'''
Reconstruction
'''

if True:


    class Recon():


        def __init__(self, recon_lib_path):

            self._recon_lib_path = recon_lib_path
            self._recon_lib      = ctypes.CDLL(self._recon_lib_path)

        # Flags to make sure all required parameters are set before calling operators.

            # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list ]
            self._flags = np.zeros(  (7,), dtype=np.int32 )

            self._op_flag = 0

        # "Private" _variables which are designed to be set through setter functions to
        # allow error checking. Should be treated as read-only by the end-user.

            # self._int_params = [ nu, nv, np, nx, ny, nz, filtration, use_affine, [implicit_mask], [explicit_mask], [cy_detector], [use_norm_vol] ]
            # nu: number of projection columns
            # nv: number of projection rows
            # np: number of projection angles
            # nx, ny, nz: reconstructed volume dimensions
            # filtration: (1) filtered backprojection (FBP), (0) simple backprojection
            # use_affine: (0) affine transform is not used (even if one is provided), (1) affine transform is used
            # [implicit_mask]: (0) do not use implicit, cylindrical reconstruction mask, (1) use implicit reconstruction mask
            # [explicit_mask]: (0) do not use explicit reconstruction mask, (1) use explicit reconstruction mask, (2) use volumetric reconstruction mask
            # [cy_detector]: (0) detector is flat
            #                (1) detector is cylindrical
            #                (2) cylindrical, after cone-parallel rebinning
            #                (3) flat panel, after cone-parallel rebinning
            #                (4) array of parallel beams (synchrotron geometry)
            # [use_norm_vol]: (0) do not allocate / use count normalization volume during backprojection
            #                 (1) allocate / use count normalization volume during backprojection (wFBP)
            self._int_params = np.zeros( (12,), dtype=np.int32 )

            # self._double_params = [ du, dv, dx, dy, dz, zo, ao, lsd, lso, uc, vc, eta, sig, phi, [scale], [db], [limit_angular_range] ]
            # du: detector pixel column width (mm)
            # dv: detector pixel row width (mm)
            # dx, dy, dz: reconstructed volume voxel size (mm)
            # zo: reconstructed volume offset from the central ray along the z axis (mm)
            # ao: reconstructed volume angular offset (radians)
            # lsd: source-detector distance (mm)
            # lso: source-object distance (mm)
            # uc: detector column at which the central ray intersects the detector (unitless index)
            # vc: detector row at which the central ray intersects the detector (unitless index)
            # eta: detector rotation (y-z plane)
            # sig: detector rotation (x-z plane)
            # phi: detector rotation (x-y plane)
            # [scale]: multiplicative scaling factor applied to reconstructed attenuation values
            # [db]: constant angular increment between detector elements (cylindrical detector, radians)
            # [limit_angular_range]: ignore or reweight off-axis rays to improve z spatial resolution
            #                        (0) weight all backprojected rays equally
            #                        (>0, <=1.0) cos^2 weighting from Stierstorfer et al. 2004 (unitless ratio based on the detector collimation)
            #                        (>1.0) ignore rays which diverge by more than this distance from the central ray along the z axis at the isocenter (mm)
            self._double_params = np.zeros( (17,), dtype=np.double )

            # FBP filter file
            self._filt_name_original = ''
            self._filt_name          = ''
            self._filt_type          = ''
            self._filt_f50        = None

            # Text file with angle-by-angle geometry information
            self._geofile = ''
            self._proj_weights = None
            self._z_pos        = None
            self._angles       = None


            # Image-domain affine transform relative to the specified geometry
            self._Rt_aff = np.zeros( (12,), dtype=np.float32 )

            # Explicit reconstruction mask
            self._rmask = np.zeros( (1,), dtype=np.uint16 )

            # GPUs to use for reconstruction
            self._gpu_list = np.zeros( (1,), dtype=np.int32 )

        # Private variables which should never be directly modified.

            # Static pre-allocation of resources required for projection and backprojection
            self._recon_alloc = np.zeros( (1,), dtype=np.uint64 )

            # The number of int and double params specified. Needed because the number of inputs is variable.
            self._int_params_len    = 0
            self._double_params_len = 0

            # Gantry rotation direction (+/- 1)
            self._rot_dir = 1

            # This is already implied to be zero in int_params if not specified.
            # (0) detector is flat
            # (1) detector is cylindrical
            # (2) cylindrical, after cone-parallel rebinning
            # (3) flat, after cone-parallel rebinning
            # (4) array of parallel beams (synchrotron geometry)
            # self._detector_type = 0


        def set_int_params(self, int_params_in, rot_dir):

        # Error checking - int_params_in

            if ( self._op_flag == 1 ):

                print( 'Cannot change int_params associated with a current recon allocation.' )
                return -1

            if ( type(int_params_in) != np.ndarray ):

                print( 'Numpy ndarray is expected.' )
                return -1

            if ( len( int_params_in.shape ) > 1 ):

                print( 'One dimensional array expected.' )
                return -1

            if ( int_params_in.dtype != np.int32 ):

                print( 'np.int32 dtype expected.' )
                return -1

            if ( int_params_in.size < 8 ):

                print( 'Too few int_params specified.' )
                return -1

            if ( int_params_in.size > 12 ):

                print( 'Too many int_params specified.' )
                return -1

            # Check if we are reconstructing cylindrical, cone-parallel data ( int_params_in[10] = 2 ).
            if ( int_params_in.size >= 11 and int_params_in[10] == 2 ):

                if ( int_params_in.size != 12 ):

                    print( 'All 12 int_params are required for cylindrical, cone-parallel data.' )
                    return -1

        # Error checking - rot_dir

            if ( not isinstance(rot_dir, int) ):

                 print( 'Rotation direction must be an integer.' )
                 return -1

            if ( rot_dir != -1 and rot_dir != 1 ):

                 print( 'Invalid rotation direction: possible values {1, -1}.' )
                 return -1

        # Set the internal int_params variable.

            self._int_params[ 0:int_params_in.size ] = int_params_in

            self._int_params_len = int_params_in.size
            self._rot_dir = rot_dir

            self._flags[0] = 1

            return 0


        def set_double_params(self, double_params_in):

            if ( self._op_flag == 1 ):

                print( 'Cannot change double_params associated with a current recon allocation.' )
                return -1

            if ( self._flags[0] == 0 ):

                print( 'Please set integer parameters before setting double parameters.' )
                return -1

        # Error checking

            if ( type(double_params_in) != np.ndarray ):

                print( 'Numpy ndarray is expected.' )
                return -1

            if ( len( double_params_in.shape ) > 1 ):

                print( 'One dimensional array expected.' )
                return -1

            if ( double_params_in.dtype != np.double ):

                print( 'np.double dtype expected.' )
                return -1

            if ( double_params_in.size < 14 ):

                print( 'Too few double_params specified.' )
                return -1

            if ( double_params_in.size > 17 ):

                print( 'Too many double_params specified.' )
                return -1

            if ( ( self._int_params[10] == 1 or self._int_params[10] == 2 ) and double_params_in.size != 17 ):

                print( 'All 17 double parameters are required cylindrical detector data.' )
                return -1


        # Set the internal double_params variable.

            self._double_params[ 0:double_params_in.size ] = double_params_in

            self._double_params_len = double_params_in.size

            self._flags[1] = 1

            return 0


        # def make_and_set_filter(self, original_out_name, filter_type='ram-lak', cutoff=1.0, rebinned=False ):
        def make_and_set_filter(self, original_out_name, filter_type='ram-lak', f50=1.0, rebinned=False ):

        # Error checking

            if ( self._op_flag == 1 ):

                print( 'Cannot change filter associated with a current recon allocation.' )
                return -1

            if ( self._flags[0] == 0 ):

                print( 'Integer parameters must be specified before creating a filter file.' )
                return -1

            if ( self._flags[1] == 0 ):

                print( 'Double parameters must be specified before creating a filter file.' )
                return -1

            if ( self._int_params[10] == 2 and self._flags[8] == 0 ):

                print( 'For WFBP objects, rebinning must be performed before creating a filter file.' )
                return -1

            # if ( ( cutoff > 1.0 ) | ( cutoff <= 0.0 ) ) :
            #
            #     print( 'Relative cutoff frequency should be in range (0,1].' )
            #     return -1

        # Parse parameters

            # If we are creating a filter for use with rebinned data, use the rebinned parameters.
            if rebinned:

                nu = self._int_params_rebinned[0]
                du = self._double_params_rebinned[0]

            else:

                nu = self._int_params[0]
                du = self._double_params[0]

            lenu = ( 2 ** ( np.ceil( np.log2( nu ) ) ) ).astype(np.int32)

        # Make filter

            out_name = original_out_name % str(lenu)

            # make_FBP_filter( out_name, du, lenu, filter_type, cutoff )
            make_FBP_filter_v2( out_name, du, lenu, filter_type, f50 )

        # Make sure we made the output file...

            if ( os.path.isfile(out_name) == False ):

                 sys.exit("Failed to produce filter file.")

        # Set filter and filter flag

            self._filt_name_original = original_out_name
            self._filt_name          = out_name
            self._filt_type          = filter_type
            self._filt_f50           = f50

            self._flags[2] = 1

            return 0


        def make_and_set_geofile(self, geofile, angles, z_pos, proj_weights, rebinned=False ):

        # Error checking - flags

            if ( self._op_flag == 1 ):

                print( 'Cannot change geo file associated with a current recon allocation.' )
                return -1

            if ( self._flags[0] == 0 ):

                print( 'Integer parameters must be specified before creating a geometry file.' )
                return -1

            if ( self._flags[1] == 0 ):

                print( 'Double parameters must be specified before creating a geometry file.' )
                return -1

            if ( self._int_params[10] == 2 and self._flags[8] == 0 ):

                print( 'For WFBP objects, rebinning must be performed before creating a geometry file.' )
                return -1

        # Error checking - geofile

            if not isinstance(geofile, str):

                print( 'Geometry file output directory must be a string (geofile).' )
                return -1

        # Error checking - angles

            if ( type(angles) != np.ndarray ):

                print( 'Numpy ndarray is expected (angles).' )
                return -1

            if ( len( angles.shape ) > 1 ):

                print( 'One dimensional array expected (angles).' )
                return -1

            if ( angles.dtype != np.double ):

                print( 'np.double dtype expected (angles).' )
                return -1

            np_ = self._int_params[2]

            if ( angles.size != np_ ):

                print( 'Angles vector must have np elements (' + str(np_) + '). np was specified in int_params.' )
                return -1

        # Error checking - z position

            if ( type(z_pos) != np.ndarray ):

                print( 'Numpy ndarray is expected (z_pos).' )
                return -1

            if ( len( z_pos.shape ) > 1 ):

                print( 'One dimensional array expected (z_pos).' )
                return -1

            if ( z_pos.dtype != np.double ):

                print( 'np.double dtype expected (z_pos).' )
                return -1

            if ( z_pos.size != 1 and z_pos.size != np_ ):

                print( 'Z_pos vector must have 1 or np elements (' + str(np_) + '). np was specified in int_params.' )
                return -1

        # Error checking - projection weights

            if ( type(proj_weights) != np.ndarray ):

                print( 'Numpy ndarray is expected (proj_weights).' )
                return -1

            if ( len( proj_weights.shape ) != 1 and len( proj_weights.shape ) != 2 ):

                print( 'One dimensional array expected (proj_weights).' )
                return -1

            if ( proj_weights.dtype != np.double ):

                print( 'np.double dtype expected (proj_weights).' )
                return -1

            if ( proj_weights.size != np_ ):

                print( 'Proj_weights vector must have np elements (' + str(np_) + '). np was specified in int_params.' )
                return -1

        # prevent 45 degree angle increments which cause numerical instabilities

            temp = angles % 45 == 0
            angles[temp] = angles[temp] + 1e-4

        # Produce geometry file

            if rebinned:

                geolines = make_geo_file( geofile, self._int_params_rebinned, self._double_params_rebinned, self._rot_dir, angles, proj_weights, z_pos,
                                          flip_z=self._flip_z, AMu=self._double_params_b[11], AMv=0.0, db=self._double_params[15] )

            else:

                if hasattr( self, '_flip_z'):
                    flip_in = self._flip_z
                else:
                    flip_in = 1.0

                if hasattr( self, '_AMu'):
                    AMu_in = self._AMu
                else:
                    AMu_in = 0.0

                if hasattr( self, '_db'):
                    db_in = self._db
                else:
                    db_in = 0.0

                make_geo_file( geofile, self._int_params, self._double_params, self._rot_dir, angles, proj_weights, z_pos,
                               flip_z=flip_in, AMu=AMu_in, AMv=0.0, db=db_in )

        # Make sure we made the output file...

            if ( os.path.isfile(geofile) == False ):

                 print("Failed to produce geometry file.")
                 return -1

        # Set the geofile and geofile flag

            self._proj_weights = proj_weights
            self._z_pos        = z_pos
            self._angles       = angles
            self._geofile      = geofile

            self._flags[3] = 1

            return 0


        def set_affine(self, aff_in):

        # Error checking - flags

            if ( self._op_flag == 1 ):

                print( 'Cannot change affine associated with a current recon allocation.' )
                return -1

            if ( self._flags[0] == 0 ):

                print( 'Integer parameters must be specified before creating a affine transform.' )
                return -1

            use_affine = self._int_params[7]

            if ( use_affine == 0 ):

                # print('use_affine = 0. No affine transform will be specified.')

                self._Rt_aff = np.array( [1,0,0,0,1,0,0,0,1,0,0,0], dtype=np.float32 )

                self._flags[4] = 1

                return 0

            if ( type(aff_in) != np.ndarray ):

                print( 'Numpy ndarray is expected (aff_in).' )
                return -1

            # if ( len( aff_in.shape ) > 2 ):
            if ( len( aff_in.shape ) > 1 ):

                print( 'One dimensional array expected (aff_in).' )
                return -1

            if ( aff_in.dtype != np.float32 ):

                print( 'np.float32 dtype expected (aff_in).' )
                return -1

            if ( aff_in.size != 12 ):

                print( 'Incorrect affine transform size. (9-parameter 3D rotation / shear / scale; 3-parameter xyz translation.)' )
                return -1

        # Set the affine transform and the affine transform flag.

            self._flags[4] = 1

            self._Rt_aff = aff_in

            return 0


        def set_rmask(self, rmask_in):

        # Error checking - flags

            if ( self._op_flag == 1 ):

                print( 'Cannot change rmask associated with a current recon allocation.' )
                return -1

            if ( self._flags[0] == 0 ):

                print( 'Integer parameters must be specified before creating a mask.' )
                return -1

        # Check for the base case

            if ( self._int_params_len < 10 ):

                # print('Explicit mask type not specified. Proceeding without an explicit mask.')

                self._flags[5] = 1

                # Set this again, in case it is set and then changed.
                self._rmask   = np.zeros( (1,), dtype=np.uint16 )
                self._rmask_in = None

                return 0

            nx            = self._int_params[3]
            ny            = self._int_params[4]
            nz            = self._int_params[5]
            explicit_mask = self._int_params[9]

            if ( explicit_mask == 0 ):

                # print('Explicit mask type is 0. Proceeding without an explicit mask.')

                self._flags[5] = 1

                # Set this again, in case it is set and then changed.
                self._rmask   = np.zeros( (1,), dtype=np.uint16 )
                self._rmask_in = None

                return 0

        # Error checking - rmask_in

            if ( explicit_mask < 0 | explicit_mask > 2 ):

                print('Unknown explicit mask type in int_params vector ( index 9 ). Valid values: 0 (no explicit mask), \
                       1 (z projected explicit mask), 2 (z projected explicit mask and volumetric explicit mask).')

                return -1

            if ( type(rmask_in) != np.ndarray ):

                print( 'Numpy ndarray is expected (rmask).' )
                return -1

            if ( rmask_in.dtype != np.uint16 ):

                print( 'np.uint16 dtype expected (rmask).' )
                return -1

            # if ( ( explicit_mask == 1 | explicit_mask == 2 ) and ( rmask_in.shape != (nx,ny,nz) ) ):
            if ( ( explicit_mask == 1 | explicit_mask == 2 ) and ( rmask_in.shape != (nz,ny,nx) ) ):

                # print('Explicit mask type is 1 or 2, but the input rmask shape is not (nx,ny,nz). \
                #        Consult the make_default_rmask(...) function for an example.')
                print('Explicit mask type is 1 or 2, but the input rmask shape is not (nz,ny,nx). \
                       Consult the make_default_rmask(...) function for an example.')
                return -1

        # Decide which type of mask to make

            if ( explicit_mask == 1 ):

                rmask = mask_zrange( rmask_in )

                self._flags[5] = 1

                self._rmask   = rmask
                self._rmask_in = np.copy(rmask_in)

                return 0

            elif ( explicit_mask == 2 ):

                # rmask_in = np.asfortranarray( rmask_in )

                rmask = mask_zrange( rmask_in )

                # rmask = np.concatenate( (rmask, rmask_in), axis=2 )
                rmask = np.concatenate( (rmask, rmask_in), axis=0 )

                self._flags[5] = 1

                self._rmask   = rmask
                self._rmask_in = np.copy(rmask_in)

                return 0

            else:

                print('Something went wrong. How did we get here?')
                return -1


        def set_GPU_list(self, gpu_list_in):

        # Error checking - flags

            if ( self._op_flag == 1 ):

                print( 'Cannot change GPU list associated with a current recon allocation.' )
                return -1

            if ( type(gpu_list_in) != np.ndarray ):

                print( 'Numpy ndarray is expected (gpu_list_in).' )
                return -1

            # if ( len( gpu_list_in.shape ) > 2 ):
            if ( len( gpu_list_in.shape ) > 1 ):

                print( 'One dimensional array expected (gpu_list_in).' )
                return -1

            if ( gpu_list_in.dtype != np.int32 ):

                print( 'np.int32 dtype expected (gpu_list_in).' )
                return -1

            if ( ( gpu_list_in < 0 ).any() ):

                print( 'Valid GPU indices start from 0 (gpu_list_in).' )
                return -1

            # if ( np.amin( gpu_list_in ) > 0 ):
            #
            #     print( 'GPU index 0 not included. Is this intentional? This is not an error, but be aware that GPU indexing starts from 0.' )

        # Set gpu_indices
        # NOTE: Additional GPU validation checks are performed during initialization.

            self._gpu_list = gpu_list_in

            self._flags[6] = 1

            return 0


        def DD_init( self, rebinned=False ):

        # Make sure all parameters are initialized

            if ( ( self._flags != 1 ).any() ):

                print('Cannot initialize reconstruction. One or more parameters are unset.')
                return -1

            if ( self._op_flag == 1 ):

                print('Current reconstruction initialization / allocations must be cleared ( reset_allocation() ) prior to performing a new initialization.')
                return -1

        # Statically initialize a new recon_parameters structure on the host and GPU(s).

            # [ int *int_params, int int_params_len, double *double_params, int double_params_len, char *filt_name_in, char *geo_name_in, float32 *aff_in,
            #   unsigned short* rmask_in, int *GPU_indices_in, int GPU_counter ]

            DD_init = self._recon_lib.DD_init

            DD_init.argtypes = [ np.ctypeslib.ndpointer(dtype=np.int32),  ctypes.c_int32,
                                 np.ctypeslib.ndpointer(dtype=np.double), ctypes.c_int32,
                                 ctypes.c_char_p, ctypes.c_char_p, np.ctypeslib.ndpointer(dtype=np.float32),
                                 np.ctypeslib.ndpointer(dtype=np.uint16), np.ctypeslib.ndpointer(dtype=np.int32), ctypes.c_int32 ]

            DD_init.restype = ctypes.c_uint64

            # create byte objects from the strings
            filt_p = self._filt_name.encode('utf-8')
            geo_p  = self._geofile.encode('utf-8')

            if rebinned:

                self._recon_alloc[0] = DD_init( self._int_params_rebinned   , self._int_params_len,
                                                self._double_params_rebinned, self._double_params_len,
                                                filt_p, geo_p, self._Rt_aff, self._rmask, self._gpu_list, len(self._gpu_list) )

            else:

                self._recon_alloc[0] = DD_init( self._int_params,    self._int_params_len,
                                                self._double_params, self._double_params_len,
                                                filt_p, geo_p, self._Rt_aff, self._rmask, self._gpu_list, len(self._gpu_list) )

            # print( 'Return value: ' + str( self._recon_alloc[0] ) )

        # Set a flag saying it is now safe to call the operators

            self._op_flag = 1

            return 0


        def check_recon_initialization(self):
        # Check if this reconstruction object is fully initialized.

            if ( self._flags == 1 ).all() and self._op_flag == 1:

                return True

            else:

                return False


        def get_recon_params(self):
        # Create and return a complete list of parameters which can be used to save / reload this reconstruction object.

            if not self.check_recon_initialization():

                print('A complete set of reconstruction parameters can only be returned after hardware initialization of the Recon object; call DD_init().')

                return -1

            # Return only the int / double parameters which were explicitly specified.
            int_params    = self._int_params[ 0:self._int_params_len ]
            double_params = self._double_params[ 0:self._double_params_len ]

            recon_params = { 'recon_lib'    : self._recon_lib_path,
                             'int_params'   : int_params,
                             'rot_dir'      : self._rot_dir,
                             'double_params': double_params,
                             'filt_name'    : self._filt_name_original,
                             'filt_type'    : self._filt_type,
                             'filt_f50'     : self._filt_f50,
                             'proj_weights' : self._proj_weights,
                             'z_pos'        : self._z_pos,
                             'angles'       : self._angles,
                             'geofile'      : self._geofile,
                             'Rt_aff'       : self._Rt_aff,
                             'rmask_in'     : self._rmask_in,
                             'gpu_list_in'  : self._gpu_list
                           }

            return recon_params


        def R(self, X, W):
        # X: volume to be projected
        # W: projection weights

        # Make sure all parameters and the GPU/Host are initialized

            if ( ( self._flags != 1 ).any() ):

                print('Cannot perform projection. One or more parameters are unset.')
                return -1

            if ( self._op_flag != 1 ):

                print('Cannot perform projection. Reconstruction parameters have not been initialized (call DD_init()).')
                return -1

        # Check to make sure the inputs are valid

            nu  = self._int_params[0]
            nv  = self._int_params[1]
            np_ = self._int_params[2]
            nx  = self._int_params[3]
            ny  = self._int_params[4]
            nz  = self._int_params[5]

            if ( type(X) != np.ndarray ):

                print( 'Numpy ndarray is expected (X).' )
                return -1

            # if ( X.size != nx*ny*nz ):
            if ( ( X.shape != (nz*ny*nx,) ) and ( X.shape != (nz,ny,nx) ) ):

                # print( 'Input array size must be nx*ny*nz (X).' )
                print( 'Input array (X) shape must be (nz*ny*nx,) or (nz,ny,nx).' )
                return -1

            # If we pass in a flattened vector, return a flattened vector.
            # Otherwise, return a volume.
            if X.shape == (nz*ny*nx,): return_vector = True
            else:                      return_vector = False

            if ( X.dtype != np.float32 ):

                print( 'np.float32 dtype expected (X).' )
                return -1

            if ( type(W) != np.ndarray ):

                print( 'Numpy ndarray is expected (W).' )
                return -1

            if ( W.size != np_ ):

                print( 'Projection weights (W) size must equal the number of projections (np).' )
                return -1

            if ( W.dtype != np.double ):

                print( 'np.double dtype expected (W).' )
                return -1

        # Perform projection

            DD_project = self._recon_lib.DD_project

            # float32 *X, float32 *Y, uint64_t rp_in, double *weights_in
            DD_project.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                    ctypes.c_uint64, np.ctypeslib.ndpointer(dtype=np.double) ]

            if return_vector:
                Y = np.zeros( (np_*nv*nu,), dtype=np.float32)
            else:
                Y = np.zeros( (np_,nv,nu), dtype=np.float32)

            # Result is written directly into Y.
            DD_project( X, Y, self._recon_alloc[0], W )

            return Y


        def Rt(self, Y, W):
        # Y: input projection data
        # W: projection weights

        # Make sure all parameters and the GPU/Host are initialized

            if ( ( self._flags != 1 ).any() ):

                print('Cannot perform backprojection. One or more parameters are unset.')
                return -1

            if ( self._op_flag != 1 ):

                print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
                return -1

        # Check to make sure the inputs are valid

            nu  = self._int_params[0]
            nv  = self._int_params[1]
            np_ = self._int_params[2]
            nx  = self._int_params[3]
            ny  = self._int_params[4]
            nz  = self._int_params[5]

            if ( type(Y) != np.ndarray ):

                print( 'Numpy ndarray is expected (Y).' )
                return -1

            # if ( Y.size != nu*nv*np_ ):
            if ( Y.shape != (np_*nv*nu,) and Y.shape != (np_,nv,nu) ):

                # print( 'Input array size must be nu*nv*np (Y).' )
                print( 'Input array (Y) shape must be (np_*nv*nu,) or (np_,nv,nu).' )
                return -1

            # If we pass in a flattened vector, return a flattened vector.
            # Otherwise, return a volume.
            if Y.shape == (np_*nv*nu,): return_vector = True
            else:                       return_vector = False

            if ( Y.dtype != np.float32 ):

                print( 'np.float32 dtype expected (Y).' )
                return -1

            if ( type(W) != np.ndarray ):

                print( 'Numpy ndarray is expected (W).' )
                return -1

            if ( W.size != np_ ):

                print( 'Projection weights (W) size must equal the number of projections (np).' )
                return -1

            if ( W.dtype != np.double ):

                print( 'np.double dtype expected (W).' )
                return -1

        # Perform unfiltered backprojection

            DD_backproject = self._recon_lib.DD_backproject

            # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in
            DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                        ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

            if return_vector:
                X = np.zeros( (nz*ny*nx,), dtype=np.float32)
            else:
                X = np.zeros( (nz,ny,nx), dtype=np.float32)

            # Result is written directly into X.
            DD_backproject( Y, X, self._recon_alloc[0], int(0), W )

            return X


        def Rtf(self, Y, W):
        # Y: input projection data
        # W: projection weights

        # Make sure all parameters and the GPU/Host are initialized

            if ( ( self._flags != 1 ).any() ):

                print('Cannot perform backprojection. One or more parameters are unset.')
                return -1

            if ( self._op_flag != 1 ):

                print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
                return -1

        # Check to make sure the inputs are valid

            nu         = self._int_params[0]
            nv         = self._int_params[1]
            np_        = self._int_params[2]
            nx         = self._int_params[3]
            ny         = self._int_params[4]
            nz         = self._int_params[5]
            filtration = self._int_params[6]

            if ( type(Y) != np.ndarray ):

                print( 'Numpy ndarray is expected (Y).' )
                return -1

            # if ( Y.size != nu*nv*np_ ):
            if ( Y.shape != (np_*nv*nu,) and Y.shape != (np_,nv,nu) ):

                # print( 'Input array size must be nu*nv*np (Y).' )
                print( 'Input array (Y) shape must be (np_*nv*nu,) or (np_,nv,nu).' )
                return -1

            # If we pass in a flattened vector, return a flattened vector.
            # Otherwise, return a volume.
            if Y.shape == (np_*nv*nu,): return_vector = True
            else:                       return_vector = False

            if ( Y.dtype != np.float32 ):

                print( 'np.float32 dtype expected (Y).' )
                return -1

            if ( type(W) != np.ndarray ):

                print( 'Numpy ndarray is expected (W).' )
                return -1

            if ( W.size != np_ ):

                print( 'Input array size must be np (W).' )
                return -1

            if ( W.dtype != np.double ):

                print( 'np.double dtype expected (W).' )
                return -1

            if ( filtration == 0 ):

                print( 'Cannot perform filtered backprojection because a filter was not specified or prepared during initialization ( int_params[6] = 0 ).' )
                return -1

        # Perform filtered backprojection

            DD_backproject = self._recon_lib.DD_backproject

            # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in
            DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                        ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

            if return_vector:
                X = np.zeros( (nz*ny*nx,), dtype=np.float32)
            else:
                X = np.zeros( (nz,ny,nx), dtype=np.float32)

            # Result is written directly into X.
            DD_backproject( Y, X, self._recon_alloc[0], int(1), W )

            return X


        def _reset_flags(self):
        # Flags to make sure all parameters are set before calling the operators again.

            # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list ]
            # self._flags = np.zeros(  (7,), dtype=np.int32 )
            self._flags = self._flags * 0

            self._op_flag = 0

            return 0


        def reset_all_GPUs(self):
        # Allows recovery when GPUs are in an error state.
        # Currently, this resets >ALL< visible GPUs and will invalidate allocations associated
        # with reconstruction operators as well as programs such as Tensorflow.

            # We can only clear resources if we have allocated them...
            # Check this before resetting the GPU since host allocations are also involved.
            # Will this work when the GPU is in an error state?
            if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

                clear = self._recon_lib.clear

                clear()

            self._reset_flags()

            reset_devices = self._recon_lib.reset_devices

            reset_devices()

            self._op_flag = 0

            return 0


        def reset_allocation(self, reset_flags=True):
        # Keeps the object, but resets the GPU/Host allocations.

            # We can only clear resources if we have allocated them...
            if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

                clear = self._recon_lib.clear

                clear()

            if reset_flags: self._reset_flags()

            self._op_flag = 0

            return 0


        def __del__(self):
        # Imply deallocation of resources when the last reference to this object is cleared (i.e. during garbage collection).

            # We can only clear resources if we have allocated them...
            if ( ( self._flags != 0 ).all() & self._op_flag == 1 ):

                clear = self._recon_lib.clear

                clear()

            return 0


    # Approximate, analytical reconstruction of helical data acquired with a cylindrical detector.
    # "Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch"
    # Karl Stierstorfer, Annabella Rauscher, Jan Boese, Herbert Bruder, Stefan Schaller and Thomas Flohr
    # Phys. Med. Biol. 49 (2004) 2209–2218
    # "Image reconstruction and image quality evaluation for a dual source CT scanner"
    # T. G. Flohr, H. Bruder, K. Stierstorfer, M. Petersilka, B. Schmidt, C. H. McCollough
    # Medical Physics, 35(12), 5882-5897.
    # ASSUMES / IMPLIES
    #   1) The angular increment between projections is equal between chains and before and after rebinning.
    #   2) The alignment in chain B is the same as the alignment in chain A, following rebinning of chain B.
    #      alignment: detector element offset of the central ray
    class WFBP( Recon ):


        def __init__(self, recon_lib_path ):

        # Recon object initialization

            super(WFBP, self).__init__(recon_lib_path)

        # Set these after calling the Recon __init__ function to override values.

            # Add two extra flags for setting the rebinning parameter vector and performing rebinning.
            # [ int_params, double_params, filt_name, geofile, Rt_aff, rmask, gpu_list, params_b, is_rebinned ]
            self._flags = np.zeros(  (9,), dtype=np.int32 )

            # self._int_params_b = np.zeros( (7,), dtype=np.int32 )
            # self._double_params_b = np.zeros( (9,), dtype=np.double )

            # int32([ nu_A, nu_B, nv, np_, r_os, rotdir ]);
            # nu_A: number of detector columns, chain A data
            # nu_B: (0) no chain B data
            #       (>0) number of detector columns, chain B data
            # nv: number of detector rows
            # np_: total number of projections
            # r_os: row oversampling factor
            # rotdir: gantry rotation direction (+/- 1)
            self._int_params_b = np.zeros( (6,), dtype=np.int32 )

            # [ Rf, zrot, delta_theta, delta_beta, dv, uc_A, uc_B, du_out, theta_B, scale_A_B, mag, AM ]
            # Rf: object to detector distance (mm)
            # zrot: grantry z travel in mm / degree
            # delta_theta: degrees / projection
            #              assumes original angular increment = rebinned angular increment
            # delta_beta: degrees / detector element
            # dv: detector row height (mm)
            # uc_A: detector element index where the central ray hits the detector, chain A
            #       generally, (nu_A - 1)/2
            #       currently, excludes the offset AM
            # uc_B: detector element index where the central ray hits the detector, chain B
            # du_out: detector column width after rebinning (mm)
            # theta_B: rotation offset between chains A and B (degrees)
            # scale_A_B: linear rescaling factor applied when using chain A data to complete chain B data
            # mag: system geometric magnification (lsd/lso)
            # AM: detector index column offset where the central ray intersects the detector
            self._double_params_b = np.zeros( (12,), dtype=np.double )

        # Placeholders for arrays that are filled in with modified values after projection rebinning

            self._int_params_rebinned    = None
            self._double_params_rebinned = None

        # For some clinical geometries, we need to flip the z axis.

            self._flip_z = 1.0


        def set_rebin_params(self, zrot, delta_theta, uc_A, nu_A, nu_B=0, uc_B=0.0, theta_B=0.0, scale_A_B=1.0, row_oversampling_factor=1):
        # NOTE: Assigning nu_B > 0 implies that this WFBP object is associated with "chain B" and that
        # the chain B projection data must be completed with chain A.

            if ( self._int_params[10] != 2 ):

                print( 'Rebinning parameters are for cylindrical data to be rebinned only ( cy_detector, int_params[10] = 2 ).' )
                return -1

            if ( self._flags[0] == 0 or self._flags[1] == 0 ):

                print( 'Please set integer and double parameters before setting rebin parameters.' )
                return -1

        # Oversampling factor to use for rebinning

            if ( not isinstance( row_oversampling_factor, int ) ):

                print( 'Integer data type expected for row_oversampling_factor.' )
                return -1

            if ( row_oversampling_factor < 1 ):

                print( 'row_oversampling_factor must be at least 1.' )
                return -1

        # Errors / Warnings - nu_B

            if ( not isinstance( nu_B, int ) ):

                print( 'Integer data type expected for nu_B (detector elements per row, chain B).' )
                return -1

            if ( nu_B == 0 ):

                 print( 'nu_B = 0: chain B will not be used.' )

            if ( nu_B < 0 ):

                print( 'nu_B (detector elements per row, chain B) must be >= 0.' )
                return -1

            if ( ( nu_B > 0 and uc_B != self._double_params[9] ) or ( nu_B == 0 and uc_A != self._double_params[9] )  ):

                print( 'Specified uc_A (chain A) or uc_B (chain B) parameter does not match previously specified uc value ( double_params[9] ).' )
                return -1

            if ( nu_B == 0 and nu_A != self._int_params[0] ):

                print( 'Specified nu_A parameter does not match previously specified value ( int_params[0] ).' )
                return -1

        # Save the inputs for saving / reloading reconstruction objects.

            self._rebin_inputs = { 'zrot': zrot,
                                   'delta_theta': delta_theta,
                                   'uc_A': uc_A,
                                   'nu_A': nu_A,
                                   'nu_B': nu_B,
                                   'uc_B': uc_B,
                                   'theta_B': theta_B,
                                   'scale_A_B': scale_A_B,
                                   'row_oversampling_factor': row_oversampling_factor }

        # Set rebinning parameter vectors

            os_ = row_oversampling_factor
            mag = self._double_params[7] / self._double_params[8]

            # int_params_b: [ nu_A, nu_B, nv, np, r_os, rotdir ]
            self._int_params_b[0] = nu_A
            self._int_params_b[1] = nu_B
            self._int_params_b[2] = self._int_params[1]           # nv
            self._int_params_b[3] = self._int_params[2]           # np
            self._int_params_b[4] = row_oversampling_factor
            self._int_params_b[5] = self._rot_dir                 # rotation direction

            # double_params_b: [ lsd, zrot, delta_theta, delta_beta, flip_z * dv, uc_A, uc_B, du_out, theta_B, scale_A_B, mag, AMu ]
            self._double_params_b[0]  = self._double_params[7]                # Rf
            self._double_params_b[1]  = zrot                                  # zrot (z translation in mm / degree)
            self._double_params_b[2]  = delta_theta                           # delta_theta, angular increment in degrees => assumed output angular increment
            self._double_params_b[3]  = self._double_params[15] * 180.0 / PI  # delta_beta (radians => degrees), fan angle increment between detector elements
            self._double_params_b[4]  = self._flip_z * self._double_params[1] # dv
            self._double_params_b[5]  = uc_A
            self._double_params_b[6]  = uc_B
            # self._double_params_b[7] = self._double_params[2] / os_       # du / os_ => dx (output)
            self._double_params_b[7]  = self._double_params[0] / os_ / mag    # du_out = du / r_os / mag
            self._double_params_b[8]  = theta_B                               # angular offset from chain A to chain B
            self._double_params_b[9]  = scale_A_B                             # mu_water_B / mu_water_A or similar A to B attenuation scaling factor
            self._double_params_b[10] = mag                                   # geometric magnification
            self._double_params_b[11] = self._AMu                             # detector alignment along rows
                                                                              # detector index offset from where central ray hits the detector

        # Flag that the rebinning parameters have been successfully set.

            self._flags[7] = 1

            return 0


        def rebin(self, Y_A, Y_B, GPU_idx ):
        # Y_A: input chain A projection data.
        # Y_B: input chain B projection data.
        # GPU_idx: index of the GPU to use to perform rebinning. (indexing starts from zero)

        # NOTE: The cyrebin C function includes rebinning for flat, cone-beam data (det_type = 0)
        #       and beta code for temporal rebinning (det_type = 2).
        #       These options cannot currently be accessed through the Python interface.
        # NOTE: Even if the full set of int parameters is not specified, this index exists and equals 0.

        # We only rebin when the cy_detector parameter equals 2.

            if ( self._int_params[10] != 2 ):

                print( 'Rebinning is only compatible with cy_detector = 2 (cylindrical detector data to be rebinned).' )
                return -1

        # Make sure the rebinning parameters have been specified

            if ( self._flags[0] == 0 or self._flags[1] == 0 or self._flags[7] == 0 ):

                print('int_params, double_params, and rebin_params must be set before projection rebinning.')
                return -1

        # Check to make sure the inputs are valid

            nu_A   = self._int_params_b[0]
            nu_B   = self._int_params_b[1]
            nu_out = nu_A * self._int_params_b[4]
            nv     = self._int_params[1]
            np_    = self._int_params[2]

            if ( type(Y_A) != np.ndarray ):

                print( 'Numpy ndarray is expected (Y_A).' )
                return -1

            if ( Y_A.size != np_*nv*nu_A ):

                print( 'Input array size must be np_*nv*nu_A (Y_A).' )
                return -1

            # If we pass in a flattened vector, return a flattened vector.
            # Otherwise, return a volume.
            if Y_A.shape == (nu_A*nv*np_,): return_vector = True
            else:                           return_vector = False

            if ( Y_A.dtype != np.float32 ):

                print( 'np.float32 dtype expected (Y_A).' )
                return -1

            if ( self._int_params_b[1] > 0 ):

                if ( type(Y_B) != np.ndarray ):

                    print( 'Numpy ndarray is expected (Y_B).' )
                    return -1

                if ( Y_B.size != np_*nv*nu_B ):

                    print( 'Input array size must be np_*nv*nu_B (Y_B).' )
                    return -1

                if ( Y_B.dtype != np.float32 ):

                    print( 'np.float32 dtype expected (Y_B).' )
                    return -1

            if ( not isinstance( GPU_idx, int ) ):

                print( 'Integer data type expected for GPU_idx.' )
                return -1

            if ( GPU_idx < 0 ):

                print( 'GPU_idx expected to be >= 0.' )
                return -1

        # Perform rebinning

            rebin = self._recon_lib.rebin

            # void rebin( const float *Y_A, const float *Y_B, float *Y_b,
            #             int *int_params, double *double_params, float *proj_time,
            #             float *proj_time_out, int GPU_idx, int det_type )
            rebin.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                               np.ctypeslib.ndpointer(dtype=np.int32),   np.ctypeslib.ndpointer(dtype=np.double) , np.ctypeslib.ndpointer(dtype=np.float32),
                               np.ctypeslib.ndpointer(dtype=np.float32), ctypes.c_int32, ctypes.c_int32 ]

            if return_vector:
                Y_b = np.zeros( (np_*nv*nu_out,), dtype=np.float32 )
            else:
                Y_b = np.zeros( (np_,nv,nu_out), dtype=np.float32 )

            # Wrap the Y_b np.ndarray into a custom subclass which includes an additional
            # attribute to identify rebinned data ( Projections.rebinned bool ).
            Y_b = Projections( Y_b, rebinned=True )

            # dummy arguments for unused features
            if ( self._int_params_b[1] <= 0 ): Y_B = np.zeros( (1,), dtype=np.float32 )
            proj_time     = np.zeros( (1,), dtype=np.float32 )
            proj_time_out = np.zeros( (1,), dtype=np.float32 )
            det_type = 1

            rebin( Y_A, Y_B, Y_b,
                   self._int_params_b, self._double_params_b, proj_time,
                   proj_time_out, GPU_idx, det_type )

        # Once the projections are rebinned, we need to update the int_params and double_params
        # Set a flag to indicate that the 'rebinned' versions of these parameters have been specified.

            self._int_params_rebinned    = np.copy( self._int_params )
            self._double_params_rebinned = np.copy( self._double_params )

            os_ = self._int_params_b[4]
            mag = self._double_params[7] / self._double_params[8]

            # nu
            self._int_params_rebinned[0] *= os_

            # uc
            self._double_params_rebinned[9] *= os_

            # du
            self._double_params_rebinned[0] = self._double_params[0] / os_ / mag

            # scale
            # self._double_params_rebinned[14] = os_ * mag

            self._flags[8] = 1

            return Y_b


        # def R(self, X, W):
        # # X: volume to be projected
        # # W: projection weights
        #
        #     print('It is possble to reproject in a cone-parallel geometry, but it is not currently implemented from the Python interface.')
        #
        #     return -1
        #
        #
        # def Rt(self, Y, W):
        # # Y: input projection data
        # # W: projection weights
        #
        #     print('It is possble to do unfiltered backprojection in a cone-parallel geometry, but it is not currently implemented from the Python interface.')
        #
        #     return -1


        def Rtf(self, Y, W):
        # Y: input projection data
        # W: projection weights

            print('Call WFBP.Rtfb(Y_b, W) to perform filtered backprojection using rebinned projections.')

            return -1


        def Rtfb(self, Y_b, W):
        # WFBP reconstruction of cylindrical, helical, cone-parallel projection data
        # Y_b: input, rebinned projection data
        # X: output backprojected volume
        # W: projection weights

        # Check that the input projection data has, in fact, been rebinned.

            assert isinstance( Y_b, Projections ) and Y_b.rebinned

        # Check to make sure the inputs are valid

            nu         = self._int_params_rebinned[0]
            nv         = self._int_params_rebinned[1]
            np_        = self._int_params_rebinned[2]
            nx         = self._int_params_rebinned[3]
            ny         = self._int_params_rebinned[4]
            nz         = self._int_params_rebinned[5]
            filtration = self._int_params_rebinned[6]

            detector_type = self._int_params_rebinned[10]

        # Make sure all parameters and the GPU/Host are initialized

            if ( ( self._flags != 1 ).any() ):

                print('Cannot perform WFBP (Rtfb). One or more parameters are unset.')
                return -1

            if ( self._op_flag != 1 ):

                print('Cannot perform backprojection. Reconstruction parameters have not been initialized (call DD_init()).')
                return -1

            if detector_type != 2:

                print('Rebinned, cone-parallel projection data required for WFBP (Rtfb).')
                return -1

            # Replaced with the more specific Projections class check above.
            # if ( type(Y_b) != np.ndarray ):
            #
            #     print( 'Numpy ndarray is expected (Y_b).' )
            #     return -1

            if ( Y_b.shape != (np_*nv*nu,) and Y_b.shape != (np_,nv,nu) ):

                # print( 'Input array size must be nu*nv*np (Y).' )
                print( 'Input array (Y_b) shape must be (np_*nv*nu,) or (np_,nv,nu).' )
                return -1

            # If we pass in a flattened vector, return a flattened vector.
            # Otherwise, return a volume.
            if Y_b.shape == (np_*nv*nu,): return_vector = True
            else:                         return_vector = False

            if ( Y_b.dtype != np.float32 ):

                print( 'np.float32 dtype expected (Y_b).' )
                return -1

            if ( type(W) != np.ndarray ):

                print( 'Numpy ndarray is expected (W).' )
                return -1

            if ( W.size != np_ ):

                print( 'Input array size must be np (W).' )
                return -1

            if ( W.dtype != np.double ):

                print( 'np.double dtype expected (W).' )
                return -1

            if ( filtration == 0 ):

                print( 'Cannot perform WFBP because a filter was not specified or prepared during initialization ( int_params_rebinned[6] = 0 ).' )
                return -1


        # Perform filtered backprojection using rebinned, cone-parallel projection data.

            DD_backproject = self._recon_lib.DD_backproject

            # float32 *Y, float32 *X, uint64_t rp_in, int perform_filtration_in, double *weights_in
            DD_backproject.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                        ctypes.c_uint64, ctypes.c_int32, np.ctypeslib.ndpointer(dtype=np.double) ]

            if return_vector:
                X = np.zeros( (nz*ny*nx,), dtype=np.float32)
            else:
                X = np.zeros( (nz,ny,nx), dtype=np.float32)

            # Result is written directly into X.
            DD_backproject( Y_b, X, self._recon_alloc[0], int(1), W )

            return X


        def get_recon_params(self):

            base_recon_params = super().get_recon_params()

            rebin_recon_params = self._rebin_inputs

            return dict(list(base_recon_params.items()) + list(rebin_recon_params.items()))


    # recon_type = 'single_source_WFBP': Approximate, analytical reconstruction of helical data acquired with a cylindrical detector.
    # recon_type = 'single_source_SART': Algebraic reconstruction with unfiltered backprojection and the BiCGSTAB(l) solver.
    # Includes utility functions for importing DukeSim projection data and for creating reconstruction objects
    # from DukeSim reconstruction parameter files.
    class DukeSim_Recon( WFBP ):


        def __init__( self, recon_lib_path, recon_param_file ):

        # Recon object initialization

            super(DukeSim_Recon, self).__init__(recon_lib_path)

        # Import parameters from the recon_param_file

            # Comments start with "#" and are ignored.
            # Empty lines and lines with a new line character are ignored.
            # Otherwise a line format of "field: value\n" is expected.
            recon_params = self.parse_recon_file( recon_param_file )

        # Parse the recon_params dictionary to variables consistent with the MCR Toolkit nomenclature

            # Paths
            self._projection_file = recon_params['projection_file']
            self._outpath         = recon_params['outpath']

            # Geometry
            self._rotation_time = float(recon_params['rot_time']) / 1000             # ms => sec
            self._pitch         = float(recon_params['pitch'])                       # ( z travel / rotation ) / detector collimation
            self._lsd           = float(recon_params['source_to_detector_distance']) # source-detector distance in mm
            self._lso           = float(recon_params['source_to_object_distance'])   # source-object distance in mm
            self._np_rot        = int(recon_params['proj_per_rotation'])             # projections / rotation
            self._AMu           = float(recon_params['offset'])                      # detector element offset along rows
            self._AMv           = 0                                                  # detector element offset along columns (is this ever non-zero?)
            self._anode_angle   = float(recon_params['anode_angle'])                 # degrees
            self._dv            = float(recon_params['row_pixel_size'])              # detector pixel height, mm
            self._du            = float(recon_params['column_pixel_size'])           # detector pixel width, mm
            self._table_feed    = int(recon_params['table_direction'])               # (1) head first?, (-1) head last?
            self._rotdir        = int(recon_params['rotation_direction'])            # +/- 1
            self._theta0        = float(recon_params['initial_angle'])               # degrees

            # Should be equal to dv / magnification?; this is an over-ride to get the correct z translation / slice
            # slicewidth0   = str2num(struct.slice_width);                 % override of 0.6 here seems to give the correct z translation
            self._slicewidth0 = self._dv / (self._lsd / self._lso)

            # ZFFS => placeholder for z flying focal spot parameters; not currently supported
            self._even_offset = float(recon_params['even_offset'])
            self._odd_offset  = float(recon_params['odd_offset'])

            # Projections
            self._nv             = int(recon_params['n_rows'])             # Number of detector rows
            self._nu             = int(recon_params['n_columns'])          # Number of detector columns
            self._kVp            = int(recon_params['n_energy_bins'])      # Modeled source kVp steps (not currently used for reconstruction)
            self._rescale_factor = float(recon_params['rescaling_factor']) # Rescale projection data by this factor before use
            self._mu_eff         = float(recon_params['mu_eff'])           # effective attenuation of water, convert from 1/cm to HU

            # Reconstruction
            self._recon_type   = recon_params['recon_type']                               # Reconstruction protocol. Must be 'single_source_WFBP' or 'single_source_SART'
            # r_os        = str2num(struct.oversampling_factor)                           # Row oversampling factor to use when rebinning projection data; currently implied to be 1
            self._filter_types = recon_params['filter'].split(',')                        # Filter preset to use for analytical reconstruction
            self._gpu_list     = [ int(x) for x in recon_params['GPU_index'].split(',') ] # GPU(s) to use for reconstruction (indexing starts from 0)
            # dx          = str2num(struct.recon_voxel_size)                              # Voxel size, x
            # dy          = dx                                                            # Voxel size, y
            # dz          = dx                                                            # Voxel size, z => reconstruct thicker slices directly?
            # nx          = str2num(struct.vol_x)                                         # number of voxels to reconstruction along the x axis
            # ny          = str2num(struct.vol_y)                                         # number of voxels to reconstruction along the y axis
            # nz          = str2num(struct.vol_z)                                         # number of voxels to reconstruction along the z axis

            # Revision 1
            self._nx         = int(recon_params['matsize'])  # number of voxels to reconstruction along the x axis
            self._ny         = self._nx                      # number of voxels to reconstruction along the y axis
            # scan_range  = str2num(struct.scan_range);      # scan range to reconstruct (mm) starting from the z offset (determine automatically if scan_range < 0)
            self._z_offset   = int(recon_params['z_offset']) # offset the center of the reconstruction relative to the center of the scan

            # Revision 2
            self._recon_FoV = float(recon_params['recon_FoV']) # FoV to reconstruct (mm)
            # dz        = str2num(struct.recon_slice_width);   # reconstructed slice thickness (mm)
            self._dx        = self._recon_FoV / self._nx       # Voxel size, x; implied from the FoV rather than being specified directly

            # Revision 3
            # ker_smooth = strcmp('yes',struct.ker_smooth);      % "Smooth" reconstruction,  50% cutoff
            # ker_medium = strcmp('yes',struct.ker_medium);      % "Medium" reconstruction,  75% cutoff
            # ker_sharp  = strcmp('yes',struct.ker_sharp);       % "Sharp" reconstruction , 100% cutoff

            # Revision 4
            # Fix this value for now, so the frequency filter definition is handled appropriately.
            self._r_os = 1

            # Revision 5
            # Reconstruction filters and frequency cutoffs specified
            # cutoff_frequency = str2num(struct.cutoff_frequency);
            self._run_fbp       = recon_params['run_fbp'] == 'yes'
            self._run_iterative = recon_params['run_iterative'] == 'yes'
            self._z_range       = float(recon_params['z_range'])         # Replaces "scan_range", (mm) starting from the z offset (determine automatically if < 0)
            self._dz            = float(recon_params['slice_thickness']) # Replaces recon_slice_width

            # v2: changed 'cutoff_frequency' field (cutoff frequency relative to Nyquist, fraction)
            #     to 'f50' (50% modulation frequency in FBP filter; cycles / mm)
            self._f50s = [ float(x) for x in recon_params['f50'].split(',') ]

            # Issue a warning if the voxel size may be smaller than necessary.
            # It may be feasible to reconstruct a smaller in-plane voxel size when using a flying focal spot...
            if self._dx < ( self._du / ( self._lsd / self._lso ) ):

                print('\nReconstruction FoV ' + str(self._recon_FoV) + ' (mm) and matrix size ' + str(self._nx) + ' imply a voxel size of ' + str(self._dx) +
                      ' mm which is smaller than the nominal size of ' + str(self._du / ( self._lsd / self._lso )) + ' mm.')

            if self._dz < self._slicewidth0:

                print('\nReconstruction slice thickness of ' + str(self._dz) + ' (mm) is smaller than the nominal slice width of ' + str(self._slicewidth0) + ' mm.')

            self._dy = self._dx


        # Check to make sure the recon_type is valid.

            assert self._recon_type == 'single_source_WFBP'      or \
                   self._recon_type == 'single_source_SART'      or \
                   self._recon_type == 'single_source_iterative'

        # Precomputed and implied values.

            self._gpu_list = np.array( self._gpu_list, np.int32 )

            # geometric magnification at isocenter
            self._mag = self._lsd / self._lso

            # fan angle increment
            self._db = np.arctan2( self._du, self._lsd ) # radians

            # angular increment between projections (degrees)
            self._ap = 360.0 / self._np_rot

            # total fan angle (degrees)
            self._fan_angle = self._db * ( 180.0 / PI ) * self._nu

            # Detector center index (unitless)
            self._uc = ( self._nu - 1.0 ) / 2.0
            self._vc = ( self._nv - 1.0 ) / 2.0

            # Detector rotations - currently, rotating a cylindrical detector will likely lead to calculation errors
            # (radians?)
            self._eta = 0.0
            self._sig = 0.0
            self._phi = 0.0

            # z travel per projection
            self._detector_collimation = self._slicewidth0 * self._nv
            self._mm_per_projection    = self._table_feed * self._pitch * ( 1.0 / self._np_rot ) * self._detector_collimation

            # Short-scan reconstruction
            if self._pitch >= CRITICAL_PITCH: # "high" pitch, use short-scan reconstruction => better resolution, more noise

                # Minimum necessary projection data after rebinning divided by 2
                self._limit_angular_range = self._detector_collimation * self._pitch * ( 180.0 / 360.0 ) / 2.0

            else: # "low" pitch, use full-scan reconstruction => lower resolution, lower noise

                self._limit_angular_range = self._detector_collimation * self._pitch / 2.0


            # Expand the minimum data range when performing algebraic reconstruction
            # 0 means do not limit the data range
            self._limit_angular_range2 = 0 # detector_collimation * pitch * (180 + fan_angle)/360 / 2

        # The +z axis in DukeSim and the +z axis in the Toolkit are in opposite directions.
        # For compatibility with pre-existing functions, it is easiest to flip the detector axis in the geometry file.
        # Note: As currently implemented, this will only work for an ideal cylindrical detector with no v/z offset at the detector.

            self._flip_z = -1
            # self._dv *= self._flip_z

        # Placeholders

            # Projection data
            self._Y      = None # Projection data array (np,nv,nu)
            self._Yb     = None # Rebinned projection data array
            self._np     = None # Number of projections
            self._angles = None # projection angles (degrees)
            self._W      = None # projection weights (np,)
            self._z_pos  = None # z position of the gantry per projection (mm)
            self._zo     = None # z position at the center of the reconstructed volume (mm)


        # Outpath for saving intermediate files and reconstructions

            # Create the output folder, if it doesn't already exist
            if not os.path.isdir(self._outpath): os.mkdir(self._outpath)


        def load_projections( self ):
        # Import projection data from a '*.xcat' file, a '*.nii' file, or a '*.nii.gz' file
        # TO DO: Handle multiple energies in the same projection file? => Only makes sense if they have the same geometry.
        # TO DO: Add in code to suppress zingers and prevent artifacts?

            extension = self._projection_file.split('.')[-1]

            if extension == 'xcat':

                # Read binary file
                self._Y = np.fromfile(self._projection_file, dtype=np.float32)

                # Make sure the pre-specified projection dimensions are valid
                assert self._Y.shape[0] == ( self._nv * self._nu ) * ( self._Y.shape[0] // ( self._nv * self._nu ) )

                self._np = self._Y.shape[0] // ( self._nv * self._nu )

                # nu*nv*nv*ne => nu,nv,np => np,nv,nu
                self._Y = np.reshape( self._Y, (self._nu, self._nv, self._np ) )
                self._Y = np.transpose( self._Y, np.arange(3)[::-1] )

            elif extension == 'nii' or extension == 'gz':

                # Load nifti file
                # Note: The load_nii function inverts the array dimensions by default.
                # np,nv,nu
                self._Y = load_nii( self._projection_file )

                # Make sure the projections have the expected precision.
                self._Y = self._Y.astype('float32')

                # Mask sure the projections we have loaded match the expected dimensions
                assert self._Y.shape[-1] == self._nu
                assert self._Y.shape[-2] == self._nv

                self._np = self._Y.shape[-3]

            else:

                print('Unrecognized projection file format (*.xcat binary file or *.nii / *.nii.gz nifti file expected).')

                return -1

            # Apply the rescale factor, if necessary.
            self._Y *= self._rescale_factor

        # Make sure the projection data is free of NaN values which would cause artifacts.

            assert np.sum( np.isnan( self._Y ).astype('float32').flatten() ) == 0

        # Compute additional parameters which can depend on the number of projections.

            # Compute angles for all projections (degrees)
            # np.arange() excludes the final position
            self._angles = np.arange( self._theta0, self._theta0 + self._np * self._ap, self._ap, np.float64 )

            # Calculate the relative z position of the gantry for each projection and decide how many slices to reconstruct
            # np.arange() excludes the final position
            if self._mm_per_projection == 0:

                # Axial scan
                self._z_pos = np.zeros( (self._np,), np.float64 )

            else:

                # Helicl scan
                self._z_pos = np.arange( 0, self._mm_per_projection * self._np, self._mm_per_projection, np.float64 )

            self._z_pos = self._z_pos - self._z_pos[ self._np // 2 - 1 ]
            # self._z_pos *= self._flip_z

            self._zo = self._z_offset # z position at the center of the reconstructed volume (mm)

            if self._z_range < 0: # determine the number of slices to reconstruct automatically

                self._nz = int( np.abs( np.round( ( self._mm_per_projection * self._np ) / self._dz ) ) )

                # For an axial scan, we want to reconstruct a minimum number of slices to cover the detector collimation.
                nz_min   = int( np.round( self._detector_collimation / self._dz ) )
                self._nz = np.maximum( self._nz, nz_min )

            else: # Reconstruct a specific z range

                self._nz = int( np.abs( np.round( self._z_range / self._dz ) ) )

        # Default reconstruction mask

            self._rmask = make_default_rmask( self._nx, self._ny, self._nz )

        # Default projection weights

            # Bug fix: 7-30-24
            # self._W = np.ones( (self._np,1) , dtype=np.float64 )
            self._W = np.ones( (1,self._np) , dtype=np.float64 )

            return 0


        def operator_setup( self ):
        # Basic DukeSim set-up for reconstructing single-source data in a 3rd-generation, MDCT geometry with a cylindrical detector.

        # Package reconstruction parameters by precision

            implicit_mask = 1 # (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
            explicit_mask = 0 # (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
            use_affine    = 0 # (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

            FBP               = 1 # (1) Load and use a Fourier filter for analytical reconstruction. Can be overridden when declaring the reconstruction operator.
            use_norm_vol      = 1 # (1) Normalize each voxel by the weighted sum of contributing line integrals.

            if self._recon_type == 'single_source_WFBP' or self._recon_type == 'single_source_iterative':

                # We will reconstruct rebinned data.
                det_type = 2 # (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections

            elif self._recon_type == 'single_source_SART':

                # We will perform algebraic reconstruction with unfiltered backprojection and the original projections.
                det_type = 1 # (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections

            int_params_f = np.array( [ self._nu, self._nv, self._np, self._nx, self._ny, self._nz, FBP, use_affine,
                                       implicit_mask, explicit_mask, det_type, use_norm_vol ], dtype=np.int32  )


            ao    = 0.0 # reconstruction angluar offset (radians)
            scale = 1.0 # rescale the reconstructed volume by this value

            double_params = np.array( [ self._du, self._dv, self._dx, self._dy, self._dz, self._zo, ao,
                                        self._lsd, self._lso, self._uc,  self._vc, self._eta, self._sig, self._phi,
                                        scale, self._db, self._limit_angular_range ], dtype=np.float64 )


        # Initialize the reconstruction operators with the reconstruction parameters.

            # Set integer parameter vector
            self.set_int_params( int_params_f, self._rotdir )

            # Set double parameter vector
            self.set_double_params( double_params )

            # Affine registration
            # Dummy parameters used here for no affine, minimal computation
            Rt_aff = np.array([1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0], np.float32)
            self.set_affine( Rt_aff )

            # Explicit reconstruction mask
            self.set_rmask( self._rmask )

            # GPUs to use for recon. operators
            self.set_GPU_list( gpu_list_in=self._gpu_list )

        # Rebin the projection data prior to reconstruction with WFBP.

            if self._recon_type == 'single_source_WFBP' or self._recon_type == 'single_source_iterative':

                zrot        = self._mm_per_projection / self._ap # z travel in mm / degree
                delta_theta = self._ap                           # degrees / projection
                uc_A        = self._uc                           # central detector element
                nu_A        = self._nu

                self.set_rebin_params( zrot, delta_theta, uc_A, nu_A, row_oversampling_factor=self._r_os )

                self._Yb = self.rebin( Y_A=self._Y, Y_B=0, GPU_idx=int(self._gpu_list[0]) )

                rebinned = True

            elif self._recon_type == 'single_source_SART':

                # Since we are not performing rebinning, we need this flag to be set before calling DD_init.
                self._flags[7] = 1
                self._flags[8] = 1

                rebinned = False

        # Create filter file and geometry file for the rebinned projection data

            # Default FBP filter
            # filter_type: 'ram-lak','shepp-logan', 'cosine', 'hamming', 'hann'
            filt_name = os.path.join( self._outpath, 'ram-lak_%s.txt' )
            self.make_and_set_filter( filt_name, filter_type='ram-lak', f50=1.0, rebinned=rebinned )

            # Geo file
            geofile = os.path.join( self._outpath, 'geoall_test.geo' )
            self.make_and_set_geofile( geofile, self._angles, self._z_pos, self._W, rebinned=rebinned )

            # Initialization on the GPUs and the host
            self.DD_init( rebinned=rebinned )

            return 0


        def parse_recon_file( self, recon_param_file ):

            recon_params = {}

            for line in open(recon_param_file):

                if len(line) <= 1 or line[0] == '#':

                    continue

                s1, s2 = line.split(':', 1)
                s2 = s2.replace(' ', '')
                s2 = s2.replace('\n','')

                recon_params[s1] = s2

            return recon_params


        def convert_to_HU(self, X):

            # 1 / dx => 1 / cm => HU
            return 1000.0 * ( X * ( 10.0 / self._dx ) / self._mu_eff - 1.0 )


        def reconstruction( self ):

            def new_filter( window, rebinned, f50 ):

                # We cannot run DD_init() again until the current recon allocation associated with this object
                # has been cleared.
                self.reset_allocation( reset_flags=False )

                filt_name = os.path.join( self._outpath, window + '_%s.txt' )
                self.make_and_set_filter( filt_name, filter_type=window, f50=f50, rebinned=rebinned )

                self.DD_init( rebinned=rebinned )


            if self._recon_type == 'single_source_WFBP':
            # if self._run_fbp:
            # NOTE: It would be much more efficient to allow a new filter to be read in
            #       when the backprojection operator is called. Currently, we need a
            #       whole new reconstruction object initialization (DD_init()) call
            #       to read in a new filter filter.

                print('Performing analytical reconstruction...')

                # Reconstruct all combinations of filtration windows and 50% modulation frequencies.
                for window in self._filter_types:

                    if window == 'ram-lak':

                        print('Reconstructing with a ram-lak (ramp) filter.')

                        # Use a standard ramp.
                        # Truncating a ramp causes artifacts, so ignore the 50% modulation frequencies.
                        new_filter( window='ram-lak', rebinned=True, f50=None )

                        X_WFBP = self.Rtfb( self._Yb, self._W )

                        X_WFBP = self.convert_to_HU( X_WFBP )

                        recon_name = os.path.join( self._outpath, 'Recon_WFBP_ram-lak.nii' )
                        save_nii( X_WFBP, recon_name, vox_size=[self._dz,self._dy,self._dx] )

                    else:

                        for f50 in self._f50s:

                            print('Reconstructing with a ' + window + ' filter, f50% ' + str(f50) + ' (cycles/mm).')

                            new_filter( window=window, rebinned=True, f50=f50 )

                            X_WFBP = self.Rtfb( self._Yb, self._W )

                            X_WFBP = self.convert_to_HU( X_WFBP )

                            recon_name = os.path.join( self._outpath, 'Recon_WFBP_' + window + '_' + str(f50) + '.nii' )
                            save_nii( X_WFBP, recon_name, vox_size=[self._dz,self._dy,self._dx] )


            elif self._recon_type == 'single_source_SART':

                print('Performing algebraic reconstruction with SART...')

                X0 = np.zeros( (self._nz,self._ny,self._nx), dtype=np.float32 )

                # X_min, resvec, _ = bicgstabl_os_v2( RC=self, Y=np.squeeze(self._Y), x=X0, maxit=6, W=self._W, ell=2)
                X_min, resvec, _ = bicgstabl_os_v2( RC=self, Y=np.squeeze(self._Y), x=X0, maxit=8, W=self._W, ell=2)

                print('Algebraic reconstruction residuals: ' + str(resvec))

                # Bug fix 10-10-24: SART reconstructions were not saved in HU.
                X_min = self.convert_to_HU( X_min )

                recon_name = os.path.join( self._outpath, 'Recon_SART.nii' )
                save_nii( X_min, recon_name, vox_size=[self._dz,self._dy,self._dx] )

            elif self._recon_type == 'single_source_iterative':

            # Initialization with WFBP

                print('Performing initial reconstruction with a ram-lak (ramp) filter...')

                # Use a standard ramp.
                # Truncating a ramp causes artifacts, so ignore the 50% modulation frequencies.
                new_filter( window='ram-lak', rebinned=True, f50=None )

                X_WFBP = self.Rtfb( self._Yb, self._W )

            # Iterative reconstruction

                print('Running iterative reconstruction...')

                # It is not a good idea to change this directly...
                # How to better handle this?
                det_type = 1 # (0) - detector is flat; (1) detector is cylindrical; (2) detector is cylindrical, backproject rebinned projections
                self._int_params[10] = det_type

                new_filter( window='ram-lak', rebinned=False, f50=None )

                # Regularization operator for iterative reconstruction
                attn_water = np.array( [self._mu_eff], np.float32 ) # Attenuation values for water per energy threshold
                At         = [1.0] # Temporal resampling for 4D BF
                e          = 1
                p          = 1
                gpu_list   = self._gpu_list

                # debugging of the iterative reconstruction
                debug_outpath = os.path.join( self._outpath, 'debug' )
                # debug_outpath = None

                # denoise_op = RSKR( self._recon_lib_path, At=At, nvols=e, ntimes=p, mask=True, time_mode=1, weights=attn_water,
                #                    gpu_idx=gpu_list[0], resampling=0.8, multiplier=1.2 )
                denoise_op = RSKR( self._recon_lib_path, At=At, nvols=e, ntimes=p, mask=True, time_mode=1, weights=attn_water,
                                   gpu_idx=gpu_list[0], resampling=1.2, multiplier=1.2 )

                # iterative reconstruction operators
                dummy_init   = [[partial( precomputed_initialization, X0=X_WFBP )]]
                fidelity_ops = DukeSim_SS_SE_fidelity_ops( self )

                X_iter = SplitBregman_IterRecon( initialization_ops=dummy_init, fidelity_updates=fidelity_ops, regularizer=denoise_op, times=p, energies=e,
                                                 attn_water=attn_water, debug_outpath=debug_outpath, mu0=0.005 )( self._Y )

                # Convert to HU before saving the result.
                X_iter = self.convert_to_HU( X_iter )

                recon_name = os.path.join( self._outpath, 'Recon_iterative.nii' )
                save_nii( X_iter, recon_name, vox_size=[self._dz,self._dy,self._dx] )


            # if self._run_iterative:
            #
            #     print('This is a placeholder for running an iterative reconstruction.')
            #
            # if not self._run_fbp and not self._run_iterative:
            #
            #     print('The reconstruction function of this DukeSim_WFBP object has been called; however ' +
            #           'the run_fbp and run_iterative fields of the reconstruction parameter file indicate ' +
            #           'that no reconstruction is to be performed.')

            return 0


    # Regularized, iterative reconstruction with the split Bregman method.
    # [1] Goldstein, T., & Osher, S. (2009).
    #     The split Bregman method for L1-regularized problems.
    #     SIAM journal on imaging sciences, 2(2), 323-343.
    # [2] Gao, H., Yu, H., Osher, S., & Wang, G. (2011).
    #     Multi-energy CT based on a prior rank, intensity and sparsity model (PRISM).
    #     Inverse problems, 27(11), 115012.
    # [3] Clark, D. P., & Badea, C. T. (2023).
    #     MCR toolkit: A GPU‐based toolkit for multi‐channel reconstruction of preclinical and clinical x‐ray CT data.
    #     Medical Physics. Aug;50(8):4775-4796.
    class SplitBregman_IterRecon():
    # To Do: Temporal reconstruction is too slow with sparse weights. The projections need to be sliced per time point.

        def __init__( self, initialization_ops, fidelity_updates, regularizer, times, energies, attn_water, mu0=0.005,
                      debug_outpath=None ):

            self._initialization_ops = initialization_ops
            self._fidelity_updates   = fidelity_updates
            self._regularizer        = regularizer
            self._mu0 = mu0

            self._times    = times
            self._energies = energies
            self._attn_water = attn_water

            assert sum([len(v) for v in initialization_ops]) == times * energies
            assert sum([len(v) for v in fidelity_updates  ]) == times * energies
            assert attn_water.size == energies

            self._attn_water = np.reshape(self._attn_water,(energies,1,1,1,1))

            # Extra path which can be provided to save all intermediate results for debugging purposes.
            self._debug_outpath = debug_outpath


        def __call__( self, Y ):

        # Initialization

            print('Initialization...')

            for s in range(self._energies):

                # Data fidelity updates
                for p in range(self._times):

                    if s == 0 and p == 0:

                        X_ = self._initialization_ops[s][p]()

                        nz, ny, nx = X_.shape

                        X = np.zeros( (self._energies,self._times,nz,ny,nx), np.float32 )

                        X[s,p,...] = X_

                        del X_

                    else:

                        X[s,p,...] = self._initialization_ops[s][p]()

                    print('Done with energy ' + str(s+1) + ', time ' + str(p+1))


            if self._debug_outpath is not None:

                print('Saving initialization results. (debug)')

                save_nii( X, os.path.join( self._debug_outpath, 'X_init.nii' ) )

        # Reconstruction variables

            V = X * 0
            D = X * 0

        # Iteration

            mu       = np.zeros( (self._energies,), np.float32 )
            mu_scale = np.zeros( (self._energies,), np.float32 )

            for Breg_iter in range(1,5):

            # Data fidelity updates
            # This approach to determining the scale parameter may not be ideal.
            # In the prior MATLAB version the scale was measured after the initial round of data fidleity updates.

                print('\nStarting itertaion ' + str(Breg_iter) + '...\n')

                print('Data fidelity updates...')

                for s in range(self._energies):

                    # Data fidelity updates
                    for p in range(self._times):

                        X[s,p,...], residuals, scale = self._fidelity_updates[s][p]( x=X[s,p,...], c=D[s,p,...]-V[s,p,...], mu=mu[s] )

                        print('Iteration ' + str(Breg_iter) + ' residuals ( energy ' + str(s+1) + ', time ' + str(p+1) + ' ): ' + str(residuals))

                        if Breg_iter == 1 and p == 0:

                            mu_scale[s] = scale

            # Calibrate regularization strength
            # To Do: Divide X by the attenuation of water before the MAD calculation?

                if Breg_iter == 1:

                    print('Scaling regularization parameters...')

                    mask = make_default_rmask( nz, ny, nx )

                    # => e,1,1,1,1
                    MAD = masked_noise_measurement_3D( X[:,:1,...] / self._attn_water, mask=mask, stride=1 )

                    MAD = MAD / np.amin(MAD);

                    mu[:self._energies] = self._mu0 * np.squeeze( MAD ) * np.squeeze( mu_scale )

                    print('Regularization parameters by energy: ' + str(mu))

                    del mask

            # Regularization

                # Only regularize if we are going to iterate again.
                if Breg_iter < 4:

                    print('\nRegularization...')

                    D = self._regularizer( X + V )

                    V = X + V - D

            # Save intermediate results for debugging

                if self._debug_outpath is not None:

                    print('Saving iteration ' + str(Breg_iter) + ' results. (debug)')

                    save_nii( X, os.path.join(self._debug_outpath, 'X_iter' + str(Breg_iter) + '.nii'))
                    save_nii( D, os.path.join(self._debug_outpath, 'D_iter' + str(Breg_iter) + '.nii'))
                    save_nii( V, os.path.join(self._debug_outpath, 'V_iter' + str(Breg_iter) + '.nii'))

            return X



'''
Save / Load Reconstruction Objects
'''

if True:

    # Load a saved Recon object's parameters into a dictionary.
    def load_recon_object( recon_params_file ):

        recon_params = np.load( recon_params_file, allow_pickle=True )

        recon_params = recon_params[()]

        return recon_params


    # After loading (and possibly modifying) a Recon object's parameters, use them to initialize
    # and return a new Recon object.
    def initialize_recon_object( recon_params, recon_lib_path=None, outpath=None, gpu_list=None ):
    # recon_lib_path: use this path instead of the saved one
    # outpath: create the reconstruction filter and geometry file at this location instead of the original location
    # gpu_list: override the gpu_list saved in the recon_params file

    # recon_params = { 'recon_lib'    : self._recon_lib_path,
    #                  'int_params'   : int_params,
    #                  'rot_dir'      : self._rot_dir,
    #                  'double_params': double_params,
    #                  'filt_name'    : self._filt_name_original,
    #                  'filt_type'    : self._filt_type,
    #                  'filt_f50'     : self._filt_f50,
    #                  'proj_weights' : self._proj_weights,
    #                  'z_pos'        : self._z_pos,
    #                  'angles'       : self._angles,
    #                  'geofile'      : self._geofile,
    #                  'Rt_aff'       : self._Rt_aff,
    #                  'rmask_in'     : self._rmask_in,
    #                  'gpu_list_in'  : self._gpu_list
    #                }

        # Create a new reconstruction object
        if recon_lib_path is not None: RC = Recon( recon_lib_path )
        else: RC = Recon( recon_params['recon_lib'] )

        # Set integer parameter vector
        RC.set_int_params( recon_params['int_params'], recon_params['rot_dir'] )

        # Set double parameter vector
        RC.set_double_params( recon_params['double_params'] )

        filt_name = recon_params['filt_name']
        geofile   = recon_params['geofile']
        if outpath is not None:
            filt_name = os.path.join( outpath, os.path.basename(filt_name) )
            geofile   = os.path.join( outpath, os.path.basename(geofile) )

        # FBP filter
        # filter_type: 'ram-lak','shepp-logan', 'cosine', 'hamming', 'hann'
        RC.make_and_set_filter( filt_name, filter_type=recon_params['filt_type'], f50=recon_params['filt_f50'] )

        # Geo file
        RC.make_and_set_geofile( geofile, recon_params['angles'], recon_params['z_pos'], recon_params['proj_weights'] )

        # Affine registration
        RC.set_affine( recon_params['Rt_aff'] )

        # Explicit reconstruction mask
        RC.set_rmask( recon_params['rmask_in'] )

        # GPUs to use for recon. operators
        if gpu_list is not None: gpu_list_in = np.array( gpu_list, np.int32 )
        else: gpu_list_in = recon_params['gpu_list_in']

        RC.set_GPU_list( gpu_list_in )

        # Initialization on the GPUs and the host
        RC.DD_init()

        return RC


    # Save the relevant parameters of an existing reconstruction object for future use.
    def save_recon_object( RC, outfile ):

        if not RC.check_recon_initialization:

            print('Cannot save uninitialized reconstruction object (call DD_init()).')

            return -1

        recon_params = RC.get_recon_params()

        np.save( outfile, recon_params )

        return 0


'''
Geometry Calibration
'''

if True:

    class GeoCalibration( ):

        def __init__(self, Y_in, recon_params, deltas, metric_func, ausamp=4, display_results=False):

            # Projection data
            self.Y_in = Y_in
            self.szy  = Y_in.shape

            # Initial search range per parameter
            self.deltas = deltas

            # Binary function which computes a similarity metric
            # Higher values assumed to correspond with greater similarity.
            self.metric_func = metric_func

            # Projection angule undersampling factor
            # Higher undersampling factors: faster speed, potentially lower precision
            self.ausamp = ausamp

            # (Ture) Display intermediate results
            self.display_results = display_results

            # Report cost function progress relative to the initial value
            self.sim_metric0 = None

            # Visualize the best results
            self.lowest_cost = np.inf

            # Figure scales for consistent visualizations
            self.min_Y = None
            self.max_Y = None
            self.min_X = None
            self.max_X = None

            # NOTE: '.copy' only makes a shallow copy
            #       'copy.deepcopy()' is required to make an independent copy which can be modified locally
            recon_params = deepcopy(recon_params)

            # Update the initial reconstruction parameter dictionary and projections for angular undersampling
            # np,nv,nu
            if self.ausamp > 1:

                # NOTE: We >must< trigger a copy operation when undersampling the projections.
                #       Otherwise, the GPU will still see the original projections in memory.
                #       This syntax creates a new numpy view, and does >not< change the data in memory:
                #       self.Y_in = self.Y_in[::ausamp,...]
                self.Y_in                    = np.copy( self.Y_in[::ausamp,...] )

                self.szy                     = (self.Y_in.shape[0],self.szy[1],self.szy[2])
                recon_params['proj_weights'] = recon_params['proj_weights'][::ausamp,...]
                recon_params['z_pos']        = recon_params['z_pos'][::ausamp,...]
                recon_params['angles']       = recon_params['angles'][::ausamp,...]

            int_params    = recon_params['int_params']
            int_params[2] = self.szy[0] # Update the number of projections.
            int_params[6] = 1           # Make sure we are doing analytical reconstruction
            recon_params['int_params'] = int_params

            # W               = recon_params['proj_weights'] * 0
            # W[::ausamp,...] = 1
            # recon_params['proj_weights'] = W

            # Initial reconstruction parameters
            self.recon_params0 = recon_params


        def __call__(self, params):

        # 1) Update reconstruction parameters

            recon_params = self.recon_params0

            double_params       = recon_params['double_params']
            # double_params[7:14] = params
            double_params[9:14] = params
            recon_params['double_params'] = double_params

            RC = initialize_recon_object( recon_params )

        # 2) WFBP + Reprojection

            X  = RC.Rtf( self.Y_in, recon_params['proj_weights'] )
            Y_ = RC.R( X, recon_params['proj_weights'] )

        # 3) Evaluate similarity metric

            sim_metric = self.metric_func( self.Y_in, Y_ )

            if self.sim_metric0 is None:

                self.sim_metric0 = sim_metric

            # Convert a similarity metric where higher values are better, to a cost where lower values are better.
            cost = self.sim_metric0 / sim_metric

        # 4) Visualize best results

            if self.display_results and cost <= self.lowest_cost:

                self.lowest_cost = cost

                samp = np.linspace(0,self.Y_in.shape[0],6).astype(np.uint64)
                temp = imc( [ self.Y_in[samp[1:5],...], Y_[samp[1:5],...] ], [self.szy[1],self.szy[2]*4], show=False )

                if self.szy[1] > self.szy[2]: temp = temp.T

                if self.min_Y is None:

                    self.max_Y = 3 * np.std( temp )
                    self.min_Y = -0.33 * self.max_Y

                plt.figure(num=99)
                plt.imshow( temp, cmap='gray', vmin=self.min_Y, vmax=self.max_Y )
                plt.title('Best Results: ' + str( cost ))
                plt.tick_params( which='both', left=False, bottom=False, labelleft=False, labelbottom=False)
                plt.show(block=False)
                plt.pause(0.3)

                preview_slice = X.shape[0] // 2
                if self.min_X is None:

                    self.max_X = 3 * np.std( X[preview_slice,...] )
                    self.min_X = -0.33 * self.max_X

                plt.figure(num=100)
                plt.imshow( X[preview_slice,...], cmap='gray', vmin=self.min_X, vmax=self.max_X )
                plt.title('Best Results: ' + str( cost ))
                plt.tick_params( which='both', left=False, bottom=False, labelleft=False, labelbottom=False)
                plt.show(block=False)
                plt.pause(0.3)

            return cost


'''
L2 Solvers
'''

if True:

    # Sleijpen, G. L., & Van Gijzen, M. B. (2010).
    # Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
    # SIAM journal on scientific computing, 32(5), 2687-2709.
    # Algorithm 3.1
    def bicgstabl_os(A, b, x, maxit, W, ell):
    # Needs more testing with varying numbers of subsets, varying A operators...

        b = b.flatten()
        x = x.flatten()

        if ( b.dtype != np.float32 or x.dtype != np.float32 ):

            print( 'b and x should be numpy arrays of dtype np.float32.' )
            sys.exit()

        # If there is only one subset, shape should be [1,#].
        snum = W.shape[0]

        # Initial residuals
        r0 = b - A(x, np.sum( W, 0, keepdims=True ) )

        n = x.size;
        r = np.zeros( (ell+1,n), dtype=np.float32 )
        u = np.zeros( (ell+2,n), dtype=np.float32 )

        resvec      = np.nan_to_num( np.zeros( [snum, maxit+1], dtype=np.float32 ) + math.inf )
        resvec[0,0] = np.sqrt( np.sum( r0**2 ) )

        r[0:1,:] = r0
        # u[0:2,:] = np.concatenate( ( r0, A(r0,W[:1,:]) ), axis=1 )
        u[0:2,:] = np.stack( ( r0, A(r0,W[:1,:]) ), axis=0 )

        r0t  = np.copy( r0 )
        xmin = np.copy( x )

        del r0

        for i in range(0,maxit):

            for s in range(0,snum):

                # The Bi-CG step
                for j in range(0,ell):

                    sigma = np.sum( r0t * u[j+1:j+2,:] )
                    alpha = np.sum( r0t * r[j:j+1,:]   ) / sigma

                    x            = x + u[:1,:] * alpha
                    r[0:j+1,:]   = r[0:j+1,:] - u[1:j+2,:] * alpha
                    r[j+1:j+2,:] = A(r[j,:],W[s:s+1,:])

                    beta         = np.sum( r0t * r[j+1:j+2,:] ) / sigma
                    u[0:j+2,:]   = r[0:j+2,:] - u[0:j+2,:] * beta
                    u[j+2:j+3,:] = A(u[j+1,:],W[s:s+1,:])


                # The polynomial step
                # Bug fix, 9-5-24: modified np.eye to maintain single precision
                Q,R   = np.linalg.qr( r[1:,:].T, mode='reduced' )
                gamma = np.linalg.inv( R.T @ R + np.eye(ell,dtype=np.float32) * 1e-6 ) @ ( R.T @ ( Q.T @ r[:1,:].T ) )

                x       = x + gamma.T @ r[0:ell,:]    # x = x + r(:,1:ell) * gamma;
                r[:1,:] = r[:1,:] - gamma.T @ r[1:,:] # r(:,1) = r(:,1) - r(:,2:end) * gamma;

                # u(:,1:2) = [u(:,1) - u(:,2:end-1) * gamma, u(:,2) - u(:,3:end)   * gamma];
                u[0:2,:] = np.concatenate( ( u[0:1,:] - gamma.T @ u[1:-1,:], u[1:2,:] - gamma.T @ u[2:,:] ), axis=0 )

                resvec[s,i+1] = np.linalg.norm( r[:1,:], 'fro' )

                if resvec[s,i+1] == np.amin( resvec ):

                    xmin = np.copy( x )


        resvec[resvec>1e38] = 0

        return xmin, resvec


    # Bug fix: 7-22-24
    # All instances of W should now be formatted as shape (1,np_).
    def bicgstabl_os_v2( RC, Y, x, maxit, W, ell, c=0, mu=0):

        sh_x = x.shape
        n    = x.size
        sh_y = Y.shape

        if len(W.shape) != 2:

            print( 'W argument (projection weights) has an invalid shape. The shape should be (# subsets,np_).' )
            sys.exit()

        if len(Y.shape) != 3:

            print( 'Y argument (projection data) has an invalid shape. The shape should be (np_,nv,nu).' )
            sys.exit()


        def A(X, W):

            Y     = RC.R( X, W )
            X_out = RC.Rt( Y, W )

            if mu > 0:

                X_out = X_out + mu * X

            return X_out


        b = RC.Rt( Y, W ) + mu * c

        if ( b.dtype != np.float32 or x.dtype != np.float32 ):

            print( 'b and x should be numpy arrays of dtype np.float32.' )
            sys.exit()

        if W.shape[0] > W.shape[1]:

            print( 'Input weight shape (' + str(W.shape[0]) + ',' + str(W.shape[1]) + ') may be invalid. The shape should be (# subsets,np_).' )

        # A scale factor we use for the regularization parameters
        # Only valid when mu = 0
        if np.sum(x**2) == 0:
            scale = 0.0
        else:
            scale = np.linalg.norm( b.flatten() ) / np.linalg.norm( x.flatten() )

        # If there is only one subset, shape should be [1,#].
        snum = W.shape[0]

        # Initial residuals
        r0 = b - A(x, np.sum( W, 0, keepdims=True ) )

        r = np.zeros( (ell+1,) + sh_x, dtype=np.float32 )
        u = np.zeros( (ell+2,) + sh_x, dtype=np.float32 )

        resvec      = np.nan_to_num( np.zeros( [snum, maxit+1], dtype=np.float32 ) + math.inf )
        resvec[0,0] = np.sqrt( np.sum( r0.flatten()**2 ) )

        r[0:1,...] = r0
        u[0:2,:]   = np.stack( ( r0, A(r0,W[:1,:]) ), axis=0 )

        r0t  = r0
        xmin = np.copy( x )

        for i in range(0,maxit):

            for s in range(0,snum):

                # The projection and backprojection operators expect arrays.
                x = np.reshape(x,(-1,)+sh_x)
                r = np.reshape(r,(-1,)+sh_x)
                u = np.reshape(u,(-1,)+sh_x)

                # The Bi-CG step
                for j in range(0,ell):

                    sigma = np.sum( r0t * u[j+1:j+2,...] )
                    alpha = np.sum( r0t * r[j:j+1,...]   ) / sigma

                    x              = x + u[:1,...] * alpha
                    r[0:j+1,...]   = r[0:j+1,...] - u[1:j+2,...] * alpha
                    r[j+1:j+2,...] = A(r[j,...],W[s:s+1,:])

                    beta           = np.sum( r0t * r[j+1:j+2,...] ) / sigma
                    u[0:j+2,...]   = r[0:j+2,...] - u[0:j+2,...] * beta
                    u[j+2:j+3,...] = A(u[j+1,...],W[s:s+1,:])


                # The polynomial step
                # QR factorization applies to matrices.
                x = np.reshape(x,(-1,n))
                r = np.reshape(r,(-1,n))
                u = np.reshape(u,(-1,n))

                # The polynomial step
                # Bug fix, 9-5-24: modified np.eye to maintain single precision
                Q,R   = np.linalg.qr( r[1:,:].T, mode='reduced' )
                gamma = np.linalg.inv( R.T @ R + np.eye(ell,dtype=np.float32) * 1e-6 ) @ ( R.T @ ( Q.T @ r[:1,:].T ) )

                x       = x + gamma.T @ r[0:ell,:]
                r[:1,:] = r[:1,:] - gamma.T @ r[1:,:]

                u[0:2,:] = np.concatenate( ( u[0:1,:] - gamma.T @ u[1:-1,:], u[1:2,:] - gamma.T @ u[2:,:] ), axis=0 )

                resvec[s,i+1] = np.linalg.norm( r[:1,:], 'fro' )

                if resvec[s,i+1] == np.amin( resvec ):

                    xmin = np.copy( x ).reshape(sh_x)


        resvec[resvec>1e38] = 0

        return xmin, resvec, scale


    def RtR_op(RC, mu=0):

        def A(X, W):

            Y     = RC.R( X, W )
            X_out = RC.Rt( Y, W )

            if mu > 0:

                X_out = X_out + mu * X

            return X_out

        return A


    def get_subset_weights( W, np_, snum, order='C' ): # order='F'

        # Bit reverse order
        if snum == 1:

            sets = [1]

        elif snum == 2:

            sets = [1,2]

        elif snum == 3:

            sets = [1, 3, 2]

        elif snum == 4:

            sets = [1, 3, 2, 4]

        elif snum == 8:

            sets = [1, 5, 3, 7, 2, 6, 4, 8]

        elif snum == 16:

            sets = [1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16]

        else:

            assert('Specified number of subsets not supported (snum = 1, 2, 3, 4, 8, or 16).')

        Ws = np.zeros([snum,np_],order=order)
        for s in range(0,snum):

            idx = sets[s] - 1

            Ws[s:s+1,idx::snum] = W[:,idx::snum]

        return Ws


'''
Iterative Reconstruction Operators
'''

if True:

    def precomputed_initialization( X0 ):

        return X0


    # All energies share the same geometry.
    # Each time point has its own set of weights.
    def DukeSim_SS_SE_fidelity_ops( RC, maxiter=6, ell=2 ):

        Y = RC._Y
        W = RC._W

        ne = Y.shape[0]
        nt = W.shape[0]

        ops = [[partial( bicgstabl_os_v2, RC=RC, Y=Y, maxit=maxiter, W=W, ell=ell )]]

        return ops


    # Single Source (SS), Single Energy (SE)
    # DukeSim - Cylindrical detector, rebinned projections
    def DukeSim_SS_SE_initialization_ops( RC ):

        Yb = RC._Yb
        W  = RC._W

        ne = 1
        nt = W.shape[0]

        ops = [[partial( RC.Rtfb, Y_b=Yb, W=W )]]

        return ops


    # All energies share the same geometry.
    # Each time point has its own set of weights.
    def PCD_initialization_ops( RC, Y, Wt ):

        ne = Y.shape[0]

        # Bug fix: 7-22-24
        # nt = Wt.shape[1]
        nt = Wt.shape[0]

        ops = [ [0]*nt for i in range(ne)]

        for s in range(ne):

            for p in range(nt):

                # Bug fix: 7-22-24
                # ops[s][p] = partial( RC.Rtf, Y=Y[s,...], W=Wt[:,p:p+1] )
                ops[s][p] = partial( RC.Rtf, Y=Y[s,...], W=Wt[p:p+1,:] )

        return ops


    # All energies share the same geometry.
    # Each time point has its own set of weights.
    def PCD_data_fidelity_ops( RC, Y, Wt, maxiter=6, ell=2 ):

        ne = Y.shape[0]

        # Bug fix: 7-22-24
        # nt = Wt.shape[1]
        nt = Wt.shape[0]

        ops = [ [0]*nt for i in range(ne) ]

        for s in range(ne):

            for p in range(nt):

                # Bug fix: 7-22-24
                # ops[s][p] = partial( bicgstabl_os_v2, RC=RC, Y=Y[s,...], maxit=maxiter, W=Wt[:,p:p+1], ell=ell )
                ops[s][p] = partial( bicgstabl_os_v2, RC=RC, Y=Y[s,...], maxit=maxiter, W=Wt[p:p+1,:], ell=ell )

        return ops


    # One sinogram per time point.
    # All time points share the same geometry, but may have different weights.
    def time_resolved_sinograms_WFBP_initialization_ops( RC, Y, Wt, ne=1 ):

        nt = Y.shape[0]
        assert nt == Wt.shape[0]
        assert ne == 1

        ops = [ [0]*nt for i in range(ne) ]

        for s in range(ne):

            for p in range(nt):

                ops[s][p] = partial( RC.Rtf, Y=Y[p,...], W=Wt[p:p+1,:] )

        return ops


    # One sinogram per time point.
    # All time points share the same geometry, but may have different weights.
    def time_resolved_sinograms_fidelity_ops( RC, Y, Wt, maxiter=6, ell=2, ne=1 ):

        nt = Y.shape[0]
        assert nt == Wt.shape[0]
        assert ne == 1

        ops = [ [0]*nt for i in range(ne) ]

        for s in range(ne):

            for p in range(nt):

                ops[s][p] = partial( bicgstabl_os_v2, RC=RC, Y=Y[p,...], maxit=maxiter, W=Wt[p:p+1,:], ell=ell )

        return ops










