# External Python modules (Python v3.10)
import numpy as np    # (v1.21.5)
import nibabel as nib # (v5.1.0)
import os
import matplotlib.pyplot as plt # (v3.5.1)
import time

from Recon_Utilities import load_nii, save_nii

## To Do: Subclass ndarray from numpy?


class vol_object():


    def __init__(self, name='generic_volume'):

        self.name = name

        # Set when we assign data to this object.
        self.contains_data = False
        self.pix_size_set  = False

        self.raw_data = None

        self.nu_ = None # Number of projection rows
        self.nv_ = None # Number of projection columns
        self.np_ = None # Number of projections

        self.du_ = 1 # Pixel dimension along a row
        self.dv_ = 1 # Pixel dimension along a column


    def set_pixel_size(self, dv, du):
    # Set the projection pixel size in mm.

        if dv <= 0 or du <= 0:

            print( 'Pixel sizes must be > 0 mm.' )
            return -1

        self.dv_ = dv
        self.du_ = du

        self.pix_size_set = True

        return 0


    def import_nifti(self, nii_file, get_px_size=False, load_xyz_format=True, expected_type='float32'):
    # xyz_format: if True, reformat data to zyx after import
    #             if False, data already in zyx format
    # get_px_size: if True, set the detector pixel size based on the nifti header information

        if self.contains_data:

            print('Replacing existing projections\n')

        self.raw_data = load_nii( nii_file, load_xyz_format=load_xyz_format, type=expected_type, return_pix_size=get_px_size )

        if get_px_size:

            self.raw_data, pix_size = self.raw_data

            _, self.dv_, self.du_ = pix_size

            self.pix_size_set = True

        self.contains_data = True

        sh = self.raw_data.shape
        self.np_ = sh[0]
        self.nv_ = sh[1]
        self.nu_ = sh[2]

        return 0


    def export_nifti(self, nii_file, save_xyz_format=True, expected_type='float32'):
    # xyz_format: if True, reformat data to zyx after import
    #             if False, data already in zyx format

        if not self.contains_data:

            print('No projection data to save.\n')
            return -1

        if self.pix_size_set:

            save_nii( self.raw_data, nii_file, save_xyz_format=save_xyz_format,
                      vox_size=[1,self.dv_,self.du_],
                      affine=[[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]] )

        else:

            save_nii( self.raw_data, nii_file, save_xyz_format=save_xyz_format,
                      vox_size=[1,1,1],
                      affine=[[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]] )

        return 0


