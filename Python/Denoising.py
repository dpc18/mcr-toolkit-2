# Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References
#
#  Tomasi, C., & Manduchi, R. (1998, January).
#  Bilateral filtering for gray and color images.
#  In Sixth international conference on computer vision (IEEE Cat. No. 98CH36271) (pp. 839-846). IEEE.
#
#  Takeda, H., Farsiu, S., & Milanfar, P. (2007).
#  Kernel regression for image processing and reconstruction.
#  IEEE Transactions on Image Processing, 16(2), 349-366.
#
#  Clark, D. P., Ghaghada, K., Moding, E. J., Kirsch, D. G., & Badea, C. T. (2013).
#  In vivo characterization of tumor vasculature using iodine and gold nanoparticles and dual energy micro-CT.
#  Physics in Medicine & Biology, 58(6), 1683.
#
#  Kim, K., Ye, J. C., Worstell, W., Ouyang, J., Rakvongthai, Y., El Fakhri, G., & Li, Q. (2014).
#  Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty.
#  IEEE Transactions on Medical Imaging, 34(3), 748-760.
#
#  Clark, D. P., & Badea, C. T. (2017).
#  Hybrid spectral CT reconstruction.
#  PloS one, 12(7), e0180324.

'''
Imports
'''

import ctypes
import numpy as np # (v1.21.5)

from Python.Recon_Utilities import Nelder_Mead, Resamp_Kernel_Approx, eig_SVD, make_default_rmask

from scipy.signal import convolve

''' 
Denoising Methods
'''

if True:

    class BilateralFilter():

        #  Ctypes interface with Python
        # v5: Use spatially adaptive noise estimation.

        # [ Xf, noise_sig ] = jointBF4D( X, nvols, ntimes, A, At, size, radius, multiplier, gpu_idx, return_noise_map, Xr )
        # INPUTS:
        #                  X: (float*)    vectorized data to be filtered; 3D volume x volumes to be jointly filter x time points to extend the filtration domain
        #                 Xr: (float*)    if size(Xr) == nx*ny*nz*nvols, take resampled values from these volumes; otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
        #                 Xf: (float*)    pre-allocated memory for storing the filtered results
        #                 Xn: (float*)    pre-allocated memory for storing the noise map
        #                  A: (float*)    x,y,z factored resampling kernel (expected size: radius x 3)
        #                 At: (float*)    temporal resampling weights (vector with ntimes entries; used to combine resampling results between timepoints; sums to 1)
        #               size: (uint64_t*) volume size in voxels (x,y,z)
        #         multiplier: (float*)    scales filtration strength as this multiple of the estimated noise standard deviation (vector with nvols entries)
        #              nvols: (uint64_t)  number of volumes to jointly filter
        #             ntimes: (uint64_t)  number of time points along which to extend the filtration domain
        #             radius: (int)       filter radius (width = 2*radius + 1)
        #            gpu_idx: (int32)     hardware index pointing to the GPU to be used for BF (defaults to the system default)
        #   return_noise_map: (int32)     (0) do not return noise map, Xn is a placeholder; (1) return noise map in Xn
        # perform_resampling: (int32)     (0) use Xr instead of resampling the data on the GPU; (1) resample on the GPU using A...Xr may be a placeholder
        #
        # OUTPUTS:
        #                 Xf: (single*) vectorized, filtered volumes ( prod(size) x nvols )
        #          noise_sig: (single*) returned if return_noise_map == 1; estimated noise standard deviation ( prod(size) x nvols )

        def __init__(self, recon_lib_path, nvols=1, ntimes=1, At=[1.0], radius=6, resampling=1.2, multiplier=1.2, gpu_idx=None, return_noise_map=False, time_mode=1 ):

            # Record the input parameters for saving / loading of BF objects
            self.init_params = { 'recon_lib_path' : recon_lib_path,
                                 'nvols' : nvols,
                                 'ntimes': ntimes,
                                 'At' : At,
                                 'radius' : radius,
                                 'resampling' : resampling,
                                 'multiplier' : multiplier,
                                 'gpu_idx' : gpu_idx,
                                 'return_noise_map': return_noise_map,
                                 'time_mode' : time_mode }

            self._ready = False

            # (single*) data to be filtered:
            # joint filtration: range kernels multiplied across energy channels
            # 4D filtration: filtration domain extended along the time dimension
            # Valid Cases:
            #                                     -Input Dims-     -Input Sizes-
            #  3D filtration                    : [    z,y,x]   (nvols=1, ntimes=1)
            #  3D, joint filtration             : [  e,z,y,x]   (nvols=e, ntimes=1)
            #  3D filtration; loop over n       : [  n,z,y,x]   (nvols=1, ntimes=1)
            #  3D, joint filtration; loop over n: [e,n,z,y,x]   (nvols=1, ntimes=1)
            #  4D filtration                    : [  t,z,y,x]   (nvols=1, ntimes>1,<=t)
            #  4D, joint filtration             : [e,t,z,y,x]   (nvols=e, ntimes>1,<=t)
            self._X = None
            self._size = None

            # For easy external reference, assign a string with the type of filtration being performed.
            self.filtration_case = None

            # Return denoised data with a shape matching the input shape.
            self._output_shape   = None

            # Flag which tells the GPU that it should perform kernel resampling (1)
            # or if provided, a resampled volume should be used instead (0).
            self._perform_resampling = None

            # (int32) number of volumes to jointly filter
            self._nvols = np.array( nvols, np.uint64 )

            # (int32) number of time points along which to extend the filtration domain
            # assumed odd
            self._ntimes = np.array( ntimes, np.uint64 )

            # (single*) x,y,z factored resampling kernel (expected size: radius x 3)
            self._resampling = resampling
            self._A = None

            # (single*) temporal resampling weights (vector with ntimes entries; used to combine resampling results between timepoints
            # sums to 1
            assert np.allclose( np.sum(At), 1.0 )
            self._At = np.array( At, np.float32 )

            # (int32*) volume size in voxels (x,y,z)
            self._size = None

            # (int32) filter radius (width = 2*radius + 1)
            # Non-standard kernel radius requires modification of the source code.
            self._radius = int(radius)

            # (single*) scales filtration strength as this multiple of the estimated noise standard deviation (vector with nvols entries)
            # Use this public function to avoid repeat code, but suppress error checking until initialization is complete.
            self._multiplier = None
            self.set_reg_params(multiplier, error_check=False)

            # (int32) hardware index pointing to the GPU to be used for BF (defaults to the system default)
            if gpu_idx is None:
                self._gpu_idx = 0
            else:
                self._gpu_idx = int(gpu_idx)

            # (int32) (False) do not return noise map; (True) return noise map (defaults to 0 to reduce memory transfers)
            if return_noise_map:
                self._return_noise_map = 1
            else:
                self._return_noise_map = 0

            # (single*) if size(Xr) == nx*ny*nz*nvols, take resampled values from these volumes;
            # otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
            self._Xr = None

            # Time modes (time_mode):
            # (0) Valid              : given phases [1,2,3] and At = [0.2,0.6,0.2], filters with domain [1,2,3] and returns phase 2
            # (1) Wrap    (cardiac)  : given phases [1,2,3] and At = [0.2,0.6,0.2], filters with domains [3,1,2], [1,2,3], and [2,3,1]
            # (2) Reflect (perfusion): given phases [1,2,3] and At = [0.2,0.6,0.2], filters with domains [2,1,2], [1,2,3], and [2,3,2]
            self._time_mode = time_mode

        # Check to make sure valid parameters have been specified.
        # Prevent denoising operations when parameters are invalid.

            error_status = self._check_params()

            if error_status < 0:

                print( 'Invalid input parameters. (error code ' + str(error_status) + ')' )
                return -1

            else:

                self._ready = True

        # Precompute the factored resampling kernel.

            # Assigns a kernel approximation to self._A
            self._A = self.resampling_kernel( w2=self._resampling, verbose=False ).astype('float32')

        # Set up the compiled function call through ctypes

            # BF( const float *X, const float *Xr, float *Xf, float *Xn, const float *A, const float *At, const uint64_t *size, float *multiplier,
            #     uint64_t nvols, uint64_t ntimes, int radius, int gpu_idx, int return_noise_map, int perform_resampling )

            recon_lib = ctypes.CDLL(recon_lib_path)
            self._BF  = recon_lib.jointBF4D

            self._BF.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32),   # const float *X
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # const float *Xr
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # float *Xf
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # float *Xn
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # const float *A
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # const float *At
                                  np.ctypeslib.ndpointer(dtype=np.ulonglong), # const uint64_t *size
                                  np.ctypeslib.ndpointer(dtype=np.float32),   # float *multiplier,
                                  ctypes.c_uint64,                            # uint64_t nvols
                                  ctypes.c_uint64,                            # uint64_t ntimes
                                  ctypes.c_int,                               # int radius
                                  ctypes.c_int,                               # int gpu_idx
                                  ctypes.c_int,                               # int return_noise_map
                                  ctypes.c_int                              ] # int perform_resampling


        def __call__(self, X, Xr=None):
        # NOTE: X and Xr are treated as constants within this function call.

        # Parse inputs

            if not self._ready:

                print( 'BilateralFilter object has not been correctly initialized. Please create a new object.' )
                return None, None

            # INPUTS:
            #  X : (single*) volumetric data to be filtered; (z,y,x) or (ne,nt,z,y,x)
            #  Xr: (single*) take resampled values from these volumes; otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
            #                valid shapes: (z,y,x), (ne,1,z,y,x), or (ne,nt,z,y,x) with self.ntimes = 1
            self._X  = X
            self._Xr = Xr

        # Make sure the inputs are valid; compare them with the specified filtration parameters

            error_status = self._check_inputs()

            if error_status < 0:

                print( 'Invalid bilateral filter setup. (error code ' + str(error_status) + ')' )

                return -1, None

        # Set-up

            # NOTE: We have to pass the size in backwards to compensate for the GPU's expection of Fortran (column-major) memory order.
            # self._size = np.array(self._X.shape[-3:]).astype('ulonglong')
            # sz_in = self._size[::-1]
            # self._size = np.array(self._X.shape[-3:]).astype('ulonglong')
            self._size = np.array([self._X.shape[-1],self._X.shape[-2],self._X.shape[-3]]).astype('ulonglong')

            if self._Xr is None: Xr_in = np.zeros( (), np.float32 )
            else:                Xr_in = self._Xr

        # 3D or 3D+Energy (dot multiply the range kernel across energy channels)

            if self._ntimes == 1: # Perform a single filtration operation

                if self._return_noise_map == 1: Xn = np.zeros( (self._nvols,) + self._X.shape[-3:], np.float32 )
                else:                           Xn = np.zeros( 0, np.float32 )

                # nvols,z,y,x
                Xf = np.zeros( (self._nvols,) + self._X.shape[-3:], np.float32 )

                self._BF( self._X,                   # const float *X
                          Xr_in,                     # const float *Xr
                          Xf,                        # float *Xf
                          Xn,                        # float *Xn
                          self._A,                   # const float *A
                          self._At,                  # const float *At
                          self._size,                # const uint64_t *size
                          self._multiplier,          # float *multiplier,
                          self._nvols,               # uint64_t nvols
                          self._ntimes,              # uint64_t ntimes
                          self._radius,              # int radius
                          self._gpu_idx,             # int gpu_idx
                          self._return_noise_map,    # int return_noise_map
                          self._perform_resampling ) # int perform_resampling

                # Remove singleton energy dimension
                if self._nvols == 1: Xf = Xf[0,...]
                if self._nvols == 1 and self._return_noise_map == 1: Xn = Xn[0,...]

        # 4D or 4D+Energy (extend the filtration domain along the time dimension)

            if self._ntimes > 1: # Loop over time points

                wt     = self._At.size
                phases = np.arange( -(wt//2), (wt//2)+1 )

            # Queue filtration operations based on the time mode.

                time_domains = []
                for p in range(self._ntimes):

                    time_domain = p + phases

                    # Valid
                    if self._time_mode == 0:

                        if time_domain[0] < 0 or time_domain[-1] >= self._ntimes:

                            continue

                    # Wrap
                    elif self._time_mode == 1:

                        time_domain = time_domain % self._ntimes

                    # Reflect
                    else:

                        time_domain = np.abs(time_domain)
                        idx = time_domain > self._ntimes - 1
                        time_domain[idx] = self._ntimes - time_domain[idx] - 2

                    time_domains.append( time_domain )

            # Apply the filter to each time point

                # nvols,ntimes_out,z,y,x
                ntimes_out = len(time_domains)
                Xf = np.zeros( (self._nvols,ntimes_out) + self._X.shape[-3:], np.float32 )

                if self._return_noise_map == 1: Xn = np.zeros( (self._nvols,ntimes_out) + self._X.shape[-3:], np.float32 )
                else:                           Xn = np.zeros( 0, np.float32 )

                ntimes_in = np.array( wt, np.uint64 )

                for p in range(ntimes_out):

                    # "Fancy" indexing triggers a copy operation in NumPy. No additional copy() operation needed here.
                    X_in   = self._X[ ..., time_domains[p], :, :, : ]

                    Xf_out = np.zeros( (self._nvols,) + self._X.shape[-3:], np.float32 )

                    if self._return_noise_map == 1: Xn_out = np.zeros( (self._nvols,) + self._X.shape[-3:], np.float32 )
                    else:                           Xn_out = np.zeros( 0, np.float32 )


                    # BF code expects wt,e,z,y,x rather than e,wt,z,y,x
                    if self._nvols > 1:
                        # We need to force a copy operation here, otherwise NumPy just does a view
                        # which is not resepected in the compiled code!
                        X_in = np.transpose( X_in, [ 1,0,2,3,4 ] ).copy()

                    self._BF( X_in,                      # const float *X
                              Xr_in,                     # const float *Xr (placeholder)
                              Xf_out,                    # float *Xf
                              Xn_out,                    # float *Xn
                              self._A,                   # const float *A
                              self._At,                  # const float *At
                              self._size,                # const uint64_t *size
                              self._multiplier,          # float *multiplier,
                              self._nvols,               # uint64_t nvols
                              ntimes_in,                 # uint64_t ntimes
                              self._radius,              # int radius
                              self._gpu_idx,             # int gpu_idx
                              self._return_noise_map,    # int return_noise_map
                              self._perform_resampling ) # int perform_resampling

                    Xf[:,p,...] = Xf_out
                    if self._return_noise_map == 1: Xn[:,p,...] = Xn_out


        # Return results

            if self._return_noise_map == 1: Xn = np.reshape(Xn, self._output_shape)

            Xf = np.reshape( Xf, self._output_shape )

            return Xf, Xn


        def set_reg_params(self, new_multiplier, error_check=True):
        # Updates self._multiplier
        # It may be bad practice to allow this to be changed after the object is initialized;
        # however, we need to update the denoising multipliers when setting them semi-automatically in RSKR.

            self._multiplier = np.array( new_multiplier, np.float32 )

            if self._multiplier.size == 1 and self._nvols > 1:

                self._multiplier = np.array( [new_multiplier]*int(self._nvols), np.float32 )

            if error_check:

                error_status = self._check_params()

                if error_status < 0:

                    self._ready = False

                    print( 'Invalid input parameters. (error code ' + str(error_status) + ')' )
                    return -1

                else:

                    self._ready = True

            return 0


        def get_resampling_kernel(self):

            return np.copy( self._A )


        def _check_inputs(self):

        # Check data types

            if ( type(self._X) != np.ndarray ):

                print( 'Numpy ndarray is expected.' )
                return -1

            if ( self._X.dtype != np.float32 ):

                print( 'np.float32 dtype expected.' )
                return -2

            if self._Xr is not None:

                if ( type(self._Xr) != np.ndarray ):

                    print( 'Numpy ndarray is expected.' )
                    return -3

                if ( self._Xr.dtype != np.float32 ):

                    print( 'np.float32 dtype expected.' )
                    return -4


        # Make sure the dimensions of X and the nvols and ntimes variables correspond with a valid denoising case.

            if self._X.ndim == 3 and self._nvols == 1 and self._ntimes == 1:

                #  3D filtration: [z,y,x]  (nvols=1, ntimes=1)
                self.filtration_case = '3D filtration'

            elif ( self._X.ndim == 4 or self._X.ndim == 5 ) and self._nvols == self._X.shape[0] and self._ntimes == 1:

                #  3D, joint filtration: [e,1,z,y,x]  (nvols=e, ntimes=1)
                self.filtration_case = '3D, joint filtration'
                self._X = np.reshape( self._X, (self._nvols,1) + self._X.shape[-3:] )
                self._time_dim = None

            elif ( self._X.ndim == 4 or self._X.ndim == 5 ) and self._nvols == 1 and ( self._ntimes > 1 and self._ntimes <= self._X.shape[-4] ):

                #  4D filtration: [t,z,y,x]  (nvols=1, ntimes>1,<=t)
                self.filtration_case = '4D filtration'
                self._X = np.reshape( self._X, (1,self._ntimes) + self._X.shape[-3:] )

            elif self._X.ndim == 5 and self._nvols > 1 and ( self._ntimes > 1 and self._ntimes <= self._X.shape[-4] ):

                #  4D, joint filtration: [e,t,z,y,x]  (nvols=e, ntimes>1,<=t)
                self.filtration_case = '4D, joint filtration'

            else:

                print( 'The combination of input dimensions, nvols, and ntimes is invalid.' )
                return -5

            self._size = np.array( self._X.shape[-3:], np.int32 )

            if np.any( self._size < ( 2 * self._radius + 1 ) ):

                print( 'Input volume dimensions must be at least 2*radius+1.' )
                return -6

        # Match the input and output shapes as closely as possible

            self._output_shape = list( self._X.shape )

            # The time dimension will be smaller in the output array when self._time_mode == 0 (Valid).
            if self._ntimes > 1 and self._time_mode == 0:

                self._output_shape[-4] = self._output_shape[-4] - 2 * ( self._At.size // 2 )


        # Check the resampled volume, if one is provided.

            if self._Xr is not None:

                # This check has been replaced with the self._perform_resampling flag, which disables resampling when Xr is specified.
                # Without this check, it is possible to accidently resample a strided volume...
                # if self._ntimes > 1:
                #
                #     print( 'The variable ntimes is greater than 1, but temporal denoising does not currently support resampled input volumes (Xr).' )
                #     return -7

                if self._Xr.size != self._X.size:

                    print( 'The size of the resampled volume, Xr, does not match the size of the input filtration volume, X.' )
                    return -8

                # Reshape Xr, if necessary.
                self._Xr = np.reshape( self._Xr, self._X.shape )

                self._perform_resampling = 0

            else:

                self._perform_resampling = 1

            return 0


        def _check_params(self):

            if self._nvols < 1:

                print('Number of energy channels (nvols) must be >= 1.')
                return -1

            if self._multiplier.size != self._nvols:

                print('The denoising multiplier(s) (multiplier) must broadcast to the number of energy channels.')
                return -2

            if np.any( self._multiplier <= 0 ):

                print('GPU index values (gpu_idx) must be >= 0.')
                return -3

            if np.any( self._gpu_idx < 0 ):

                print('GPU index values (gpu_idx) must be >= 0.')
                return -4

            # if self._ntimes < 1 or not (int(self._ntimes) % 2 == 1):
            if self._ntimes < 1: # or not (int(self._ntimes) % 2 == 1):

                # print('The number of time points to include in the filter domain (ntimes) must be at least 1 and must be an odd number.')
                print('The number of time points to include in the filter domain (ntimes) must be at least 1.')
                return -5

            if not self._resampling > 0 or self._resampling > 2.5:

                print('The resampling kernel bandwidth (resampling) must be greater than 0 and <= 2.5.')
                return -6

            if not self._radius == 6:

                print('The source code must be recompiled to support a kernel radius (radius) different than 6.')
                return -7

            if self._At.size % 2 != 1:

                print('Size of temporal resampling weights (At) must be odd.')
                return -8

            if self._At.size > self._ntimes:

                print('Size of temporal resampling weights (At) must be <= ntimes.')
                return -9

            if self._At.size % 2 != 1:

                print('Size of temporal resampling weights (At) must be odd for unambiguous filtration.')
                return -10

            if self._time_mode < 0 or self._time_mode > 2:

                print('Invalid temporal padding mode (time_mode: 0 - Valid; 1 - Wrap; 2 - Reflect).')
                return -11

            return 0


        def resampling_kernel( self, w2, verbose=True ):
        # Approximates the 3D, w1 * Gaussian + w2 * LoG 'classic' resampling kernel, A, from Takeda et al. 2007
        # as a sum of three separable, 1D Gaussian kernels
        # Separable 1D kernels => high computational efficiency
        # Inputs:
        #    w: kernel radius
        #   w2: kernel bandwidth w2 = 2*sig^2
        # Output:
        #    A_: separated, 1D kernels which approximate A when applied in sequence

            w  = self._radius
            w2 = float(w2)

        # Compute the dense, 3D resampling kernel

            q = w * 2

            x1, x2, x3 = np.meshgrid( np.arange( -w, w+1 ), np.arange( -w, w+1 ), np.arange( -w, w+1 ), indexing='ij' )

            # Xx: (2*w+1)**3,10
            Xx = np.concatenate( [ np.ones( [(2*w+1)**3,1] ),
                                   np.reshape(x1,[(2*w+1)**3,1]),
                                   np.reshape(x2,[(2*w+1)**3,1]),
                                   np.reshape(x3,[(2*w+1)**3,1]),
                                   np.reshape(x1**2,[(2*w+1)**3,1]),
                                   np.reshape(x2**2,[(2*w+1)**3,1]),
                                   np.reshape(x3**2,[(2*w+1)**3,1]),
                                   np.reshape(x1*x2,[(2*w+1)**3,1]),
                                   np.reshape(x2*x3,[(2*w+1)**3,1]),
                                   np.reshape(x3*x1,[(2*w+1)**3,1]) ],
                                   axis=1 )

            # tt, W: (2*w+1)**3,1
            tt = np.reshape( x1**2 + x2**2 + x3**2, [(2*w+1)**3,1] )
            W  = np.exp( -(1.0/w2) * tt )


            # Xw: (2*w+1)**3,10 x (2*w+1)**3,1 => (2*w+1)**3,10
            Xw = Xx * W

            # inv( 10,(2*w+1)**3 x (2*w+1)**3,10 + 10,10 ) * 10,(2*w+1)**3 => 10,(2*w+1)**3
            # A: 10,(2*w+1)**3 => (2*w+1)**3,
            A  = np.linalg.inv( Xx.T @ Xw + np.eye( 10 ) * 1e-6 ) @ Xw.T
            A  = A[0,:]
            A /= np.sum( A )

        # Solve for the dense approximation

            sigma_k = np.sqrt( w2 / 2.0 )

            # Create normalized kernels to use as basis function
            # (2*w+1)**3,1
            e1  = np.exp( -tt / ( 2.0 * sigma_k**2) )
            e1 /= np.sum(e1)
            e2 = np.exp( -tt / ( 2.0 * ( sigma_k / np.sqrt(1.0654) )**2) )
            e2 /= np.sum(e2)
            e3 = np.exp( -tt / ( 2.0 * ( sigma_k * np.sqrt(1.0654) )**2) )
            e3 /= np.sum(e3)

            # E: (2*w+1)**3,3
            E = np.concatenate( [e1,e2,e3], axis=1 )

            cost_function = Resamp_Kernel_Approx( A, E )

            params0  = np.array( [0.1, 0.05, 0.05] )
            offsets0 = 2 * params0
            opt_params, _ = Nelder_Mead( params0, offsets0, cost_function, maxiter=300, fun_tol=3e-16, verbose=verbose )

        # Separate the dense Gaussian kernels to 1D kernels

            diam = 2 * w + 1

            # Extract diagonals, rescale
            # G: (2*w+1)**3,3 => 2*w+1,3
            G = E[ diam**2 * np.arange( 0,diam ) + diam * np.arange( 0,diam ) + np.arange( 0,diam ), : ]
            G = G**(1/3)

        # Construct the approximation

            coeffs = np.sign( opt_params ) * ( np.abs( opt_params ) / np.sum(opt_params) )**(1/3)
            A_     = coeffs * G

        # Check the accuracy of the approximation

            if verbose:

                A1 = A_[:,0]
                A2 = A_[:,1]
                A3 = A_[:,2]

                A_approx = A1[ np.newaxis, np.newaxis, : ] * A1[ np.newaxis, :, np.newaxis ] * A1[ :, np.newaxis, np.newaxis ] + \
                           A2[ np.newaxis, np.newaxis, : ] * A2[ np.newaxis, :, np.newaxis ] * A2[ :, np.newaxis, np.newaxis ] + \
                           A3[ np.newaxis, np.newaxis, : ] * A3[ np.newaxis, :, np.newaxis ] * A3[ :, np.newaxis, np.newaxis ]

                percent_error = 100 * np.linalg.norm( A.flatten() - A_approx.flatten() ) / np.linalg.norm( A.flatten() )

                print( 'Kernel approximation error (%): ' + str( percent_error ) )

        # Switch from column-major to row-major order

            A_ = A_.T.copy()

            return A_


        def CPU_resampling(self, X):
        # Ideally, this would always be handled on the GPU, but strided kernels get a bit messy
        # and this is relatively cheap when approximated with 1D kernels.

            if not self._ready:

                print( 'BilateralFilter object has not been correctly initialized. Please create a new object.' )
                return None, None

        # Make sure the inputs are valid; compare them with the specified filtration parameters

            self._X  = X
            self._Xr = None

            error_status = self._check_inputs()

            if error_status < 0:

                print( 'Invalid bilateral filter setup.' )

                return -1

        # Perform resampling on the CPU.

            A1 = self._A[0,:]
            A2 = self._A[1,:]
            A3 = self._A[2,:]

            sh_in = self._X.shape

            d = np.reshape( self._X.copy(), (-1,) + sh_in[-3:] )

            r = self._radius
            d = np.pad( d, ( (0,0), (r,r), (r,r), (r,r) ), 'reflect' )

            d_in = d.copy()
            d    =     convolve( convolve( convolve( d_in, A1[np.newaxis,np.newaxis,np.newaxis,:], mode='valid' ),
                                                           A1[np.newaxis,np.newaxis,:,np.newaxis], mode='valid' ),
                                                           A1[np.newaxis,:,np.newaxis,np.newaxis], mode='valid' )
            d    = d + convolve( convolve( convolve( d_in, A2[np.newaxis,np.newaxis,np.newaxis,:], mode='valid' ),
                                                           A2[np.newaxis,np.newaxis,:,np.newaxis], mode='valid' ),
                                                           A2[np.newaxis,:,np.newaxis,np.newaxis], mode='valid' )
            d    = d + convolve( convolve( convolve( d_in, A3[np.newaxis,np.newaxis,np.newaxis,:], mode='valid' ),
                                                           A3[np.newaxis,np.newaxis,:,np.newaxis], mode='valid' ),
                                                           A3[np.newaxis,:,np.newaxis,np.newaxis], mode='valid' )

            d = np.reshape( d, sh_in )

            return d


    # Rank-Sparse Kernel Regression (RSKR): Make sense of data with something to hide...
    # => For multi-channel data sets (e.g. 3D + Time + Energy) with variable noise levels, it is impractical to manually calibrate denoising parameters.
    #    RSKR estimates and adapts denoising parameters in a semi-automated and systematic fashion.
    # => Multi-Energy CT:
    #     - Perform joint BF on the singular vectors of a variance-weighted SVD
    #     - Relax singular vectors back toward the input to prevent oversmoothing. Less significant singular vectors are smoothed more.
    #     - Data adaptation: regularization strength scales adaptively with local noise level and iteration
    # => Time-Resolved CT:
    #     - Non-convex, patch-based Singular Value Thresholding (pSVT)
    #     - Data adaptation: Gaussian noise with equivalent standard deviation used to calibrate thresholds
    class RSKR( BilateralFilter ):

        # Additional Inputs:
        #   pSVT_operator    : (None) not used
        #                      (pSVT object) calibrated to denoise X; applied in sequence with the BF_operator
        #   mask             : (False) do not use a mask
        #                      (True)  use the implicit reconstruction mask (see Python.Recon_Utilities.make_default_rmask() )
        #                              improves accuracy when estimating the average global noise standard deviation
        #                      ([z,y,x] np.ndarray) binary mask; 1.0 for voxels inside of the reconstruction mask
        #   weights          : (None)            do not use weighting
        #                      ([e,] np.ndarray) multiplicative weights applied per energy prior to the SVD, [e]
        #                                        typically weights[e] = 1 / ( relative_sigma[e] * attn_water[e] ), where relative_sigma[e]
        #                                        is the average noise standard deviation scaled to 1.0 for the energy with the lowest noise
        #   stride           : (1)       denoise within a BF domain of 13**3 voxels
        #                      (odd, >1) dilate the domain size by this factor to affect a larger receptive field, address lower frequency noise
        #   lambda0, exponent: factors which scale denoising strength as a function of each singular vector's significance (E value)
        #                      less significant singular vectors (smaller E values) are denoised more
        #                      BF multiplier = lambda0 * ( E[0,0] / diag(E) ) ** exponent
        #


        def __init__(self, recon_lib_path, nvols=1, ntimes=1, At=[1.0], radius=6, resampling=1.2, multiplier=1.2, gpu_idx=None, return_noise_map=False, time_mode=1,
                           pSVT_operator=None, mask=True, weights=None, stride=1, exponent=0.5 ):

        # BilateralFilter object initialization

            super(RSKR, self).__init__(recon_lib_path, nvols, ntimes, At, radius, resampling, multiplier, gpu_idx, return_noise_map, time_mode)

        # Record input parameters for saving and loading RSKR objects

            self.init_params = { 'recon_lib_path' : recon_lib_path,
                                 'nvols' : nvols,
                                 'ntimes': ntimes,
                                 'At' : At,
                                 'radius' : radius,
                                 'resampling' : resampling,
                                 'multiplier' : multiplier,
                                 'gpu_idx' : gpu_idx,
                                 'return_noise_map': return_noise_map,
                                 'time_mode' : time_mode,
                                 'pSVT_operator': pSVT_operator,
                                 'mask': mask,
                                 'weights': weights,
                                 'stride': stride,
                                 'exponent': exponent }

            self._pSVT_operator = pSVT_operator
            self._mask          = mask
            self._weights       = weights
            self._stride        = stride
            self._exponent      = exponent
            self._multiplier0   = multiplier # We need to keep the original value in case the internal value is changed during a previous call.


        # Inputs:
        #   X     : CT volume data to denoise, [e,t,z,y,x]
        # Outputs:
        #   X_out : denoised version of the input data, [e,t,z,y,x]
        def __call__(self, X):

        # Do not modify the input volume directly

            X = np.copy( X )

        ## Parse input parameters

            # Input shape
            ne, nt, nz, ny, nx = X.shape

            # Check for consistency between the BilateralFilter object and the input volume
            assert self._nvols  == ne
            assert self._ntimes <= nt

         # Create a mask, if necessary

            # Reconstruction mask
            if isinstance(self._mask,bool):

                if self._mask:
                    mask = make_default_rmask( nz, ny, nx )
                else:
                    mask = 1

            elif isinstance(self._mask,np.ndarray):

                assert np.all( self._mask.shape == X.shape[-3:] )

                mask = self._mask

            else:

                print( 'RSKR: Illegal argument for mask input parameter (must be bool or np.ndarray).' )
                return None

            if isinstance(mask,np.ndarray):

                mask = mask.astype(np.float32)

        # If we are using a reconstruction mask, add Gaussian noise outside of the mask to ensure homogeneous local noise estimates.

            MAD = None

            if isinstance(mask,np.ndarray):

                MAD = masked_noise_measurement_3D( X[:,:1,...], mask=mask, stride=1 )

                X = mask * X  + ( 1.0 - mask ) * np.random.normal(loc=0.0, scale=MAD, size=X.shape).astype(np.float32)

        # Energy / SVD weights

            weights = None

            if self._weights is not None and isinstance(self._weights,np.ndarray):

                assert self._weights.size == ne

                weights = np.copy( self._weights )

                weights = np.reshape(weights,[ne,1,1,1,1])

                if MAD is None:

                    # Assume we can use global noise estimates.
                    # Assumes each timepoint has a similar noise level.
                    MAD = masked_noise_measurement_3D( X[:,:1,...] / weights, mask=None, stride=1 )

                rel_MAD = MAD / np.amin(MAD)

                weights *= rel_MAD

                X /= weights

        ## Global, weighted Singular Value Decompostion (SVD)

            X = np.reshape( X, [ne,nt*nz*ny*nx] )

            U, E, Vt = eig_SVD( X, memory_efficient=True, eps=1e-6 )

            Vt = np.reshape( Vt, [ ne,nt,nz,ny,nx ] )

        # Calibrate regularization stength vs. the first singular vector

            mu = self._multiplier0 * ( E[0] / E ) ** self._exponent

            print( 'Regularization strength scaling by singular vector: ' + str(mu) )

            # Use these regularization factors with our BF_operator object.
            self.set_reg_params( mu )

            mu = np.reshape( mu, [ ne, 1, 1, 1, 1 ] )

        # TO DO: Add temporal regularization strength scaling.

            # Avoid executing the code that has not yet been implemented.
            apply_pSVT = False

        # RSKR iteration (split Bregman method)

            # Pre-allocate memory resources before we do heavy computation.
            D  = np.zeros_like( Vt )
            V  = np.zeros_like( Vt )
            Vt0 = np.copy( Vt )

            for iter in range(4):

            # Strided BF

                if self._stride == 1:

                    D, _ = super(RSKR, self).__call__( Vt + V )

                else:

                    D1, _ = super(RSKR, self).__call__( Vt + V )

                    D2    = Vt + V
                    D2    = strided_sampling_3D( D2, stride=self._stride, inverse=False )
                    D2, _ = super(RSKR, self).__call__( D2, Xr=D2 ) # specify Xr to prevent resampling of a strided volume
                    D2    = strided_sampling_3D( D2, stride=self._stride, inverse=True )

                    # Resample after the strided volume is destrided.
                    D2 = self.CPU_resampling( D2 )

                    D = (D1 + D2) / 2.0

                    del D1, D2

            # Apply pSVT along the time dimension.

                if apply_pSVT:

                    # To Do: Add pSVT along the time dimension.
                    abc = 1

            # Fidelity update and cost computation

                V = Vt + V - D

                b = ( Vt0 + mu * ( D - V ) ) / ( 1.0 + mu )

                delta_norm = np.linalg.norm( Vt.flatten() - b.flatten() ) / np.linalg.norm( b.flatten() )
                print( 'Iter ' + str(iter) + ', Convergence criterion: ' + str(delta_norm) )

                Vt = b


        # Output

            Vt = np.reshape( Vt, [ne,nt*nz*ny*nx] )

            X_out = ( U * E[...,None,:] ) @ Vt

            X_out = np.reshape( X_out, [ ne,nt,nz,ny,nx ] )

            if isinstance(mask,np.ndarray): # Remove added noise, if necessary

                X_out *= mask

            if weights is not None and isinstance(weights,np.ndarray):

                X_out *= np.reshape(weights,[ne,1,1,1,1])

            return X_out


    # Rank-Sparse Kernel Regression (RSKR): Make sense of data with something to hide...
    # => For multi-channel data sets (e.g. 3D + Time + Energy) with variable noise levels, it is impractical to manually calibrate denoising parameters.
    #    RSKR estimates and adapts denoising parameters in a semi-automated and systematic fashion.
    # => Multi-Energy CT:
    #     - Perform joint BF on the singular vectors of a variance-weighted SVD
    #     - Relax singular vectors back toward the input to prevent oversmoothing. Less significant singular vectors are smoothed more.
    #     - Data adaptation: regularization strength scales adaptively with local noise level and iteration
    # => Time-Resolved CT:
    #     - Non-convex, patch-based Singular Value Thresholding (pSVT)
    #     - Data adaptation: Gaussian noise with equivalent standard deviation used to calibrate thresholds
    # def RSKR( X, BF_operator, pSVT_operator=None, mask=True, weights=None, stride=1, exponent=0.5, lambda0=1.2 ):
    # # Inputs:
    # #   X                : CT volume data to denoise, [e,t,z,y,x]
    # #   BF_operator      : BilateralFilter object calibrated to denoise X
    # #   pSVT_operator    : (None) not used
    # #                      (pSVT object) calibrated to denoise X; applied in sequence with the BF_operator
    # #   mask             : (False) do not use a mask
    # #                      (True)  use the implicit reconstruction mask (see Python.Recon_Utilities.make_default_rmask() )
    # #                              improves accuracy when estimating the average global noise standard deviation
    # #                      ([z,y,x] np.ndarray) binary mask; 1.0 for voxels inside of the reconstruction mask
    # #   weights          : (None)            do not use weighting
    # #                      ([e,] np.ndarray) multiplicative weights applied per energy prior to the SVD, [e]
    # #                                        typically weights[e] = 1 / ( relative_sigma[e] * attn_water[e] ), where relative_sigma[e]
    # #                                        is the average noise standard deviation scaled to 1.0 for the energy with the lowest noise
    # #   stride           : (1)       denoise within a BF domain of 13**3 voxels
    # #                      (odd, >1) dilate the domain size by this factor to affect a larger receptive field, address lower frequency noise
    # #   lambda0, exponent: factors which scale denoising strength as a function of each singular vector's significance (E value)
    # #                      less significant singular vectors (smaller E values) are denoised more
    # #                      BF multiplier = lambda0 * ( E[0,0] / diag(E) ) ** exponent
    # #
    # # Outputs:
    # #   X_out: denoised version of the input data
    #
    # # Do not modify the input volume directly
    #
    #     X = np.copy( X )
    #
    # ## Parse input parameters
    #
    #     # Input shape
    #     ne, nt, nz, ny, nx = X.shape
    #
    #     # Check which filtration operation(s) the BilateralFilter object will perform.
    #     BF_params = BF_operator.init_params
    #
    #     # Check for consistency between the BilateralFilter object and the input volume
    #     assert BF_params['nvols']  == ne
    #     assert BF_params['ntimes'] <= nt
    #
    #     # Reconstruction mask
    #     if isinstance(mask,bool):
    #
    #         if mask:
    #             mask = make_default_rmask( nz, ny, nx )
    #         else:
    #             mask = 1
    #
    #     elif isinstance(mask,np.ndarray):
    #
    #         assert np.all( mask.shape == X.shape[-3:] )
    #
    #     else:
    #
    #         print( 'RSKR: Illegal argument for use_mask input parameter (must be bool or np.ndarray).' )
    #         return -1
    #
    # # If we are using a reconstruction mask, add Gaussian noise outside of the mask to ensure homogeneous local noise estimates.
    #
    #     MAD = None
    #
    #     if isinstance(mask,np.ndarray):
    #
    #         mask = mask.astype(np.float32)
    #
    #         MAD = masked_noise_measurement_3D( X, mask=mask, stride=stride )
    #
    #         X = mask * X  + ( 1.0 - mask ) + np.random.normal(loc=0.0, scale=MAD, size=None).astype(np.float32)
    #
    # # Energy / SVD weights
    #
    #     if weights is not None and isinstance(weights,np.ndarray):
    #
    #         assert weights.size == ne
    #
    #         weights = np.reshape(weights,[ne,1,1,1,1])
    #
    #         if MAD is None:
    #
    #             # Assume we can use global noise estimates.
    #             # Assumes each timepoint has a similar noise level.
    #             MAD = masked_noise_measurement_3D( X[:,:1,...] / weights, mask=None, stride=stride )
    #
    #         rel_MAD = MAD / np.amin(MAD)
    #
    #         weights *= rel_MAD
    #
    #         X /= weights
    #
    # ## Global, weighted Singular Value Decompostion (SVD)
    #
    #     X = np.reshape( X, [ne,nt*nz*ny*nx] )
    #
    #     U, E, Vt = eig_SVD( X, memory_efficient=True, eps=1e-6 )
    #
    #     Vt = np.reshape( Vt, [ ne,nt,nz,ny,nx ] )
    #
    # # Calibrate regularization stength vs. the first singular vector
    #
    #     mu = lambda0 * ( E[0] / E ) ** exponent
    #
    #     print( 'Regularization strength scaling by singular vector: ' + str(mu) )
    #
    #     # Use these regularization factors with our BF_operator object.
    #     BF_operator.set_reg_params( mu )
    #
    #     mu = np.reshape( mu, [ ne, 1, 1, 1, 1 ] )
    #
    # # TO DO: Add temporal regularization strength scaling.
    #
    #     # Avoid executing the code that has not yet been implemented.
    #     apply_pSVT = False
    #
    # # RSKR iteration (split Bregman method)
    #
    #     # Pre-allocate memory resources before we do heavy computation.
    #     D  = np.zeros_like( Vt )
    #     V  = np.zeros_like( Vt )
    #     Vt0 = np.copy( Vt )
    #
    #     for iter in range(4):
    #
    #     # Strided BF
    #
    #         if stride == 1:
    #
    #             D, _ = BF_operator( Vt + V )
    #
    #         else:
    #
    #             D1, _ = BF_operator( Vt + V )
    #
    #             D2    = Vt + V
    #             D2    = strided_sampling_3D( D2, stride=stride, inverse=False )
    #             D2, _ = BF_operator( D2, Xr=D2 ) # specify Xr to prevent resampling of a strided volume
    #             D2    = strided_sampling_3D( D2, stride=stride, inverse=True )
    #
    #             # Resample after the strided volume is destrided.
    #             D2 = BF_operator.CPU_resampling( D2 )
    #
    #             D = (D1 + D2) / 2.0
    #
    #             del D1, D2
    #
    #     # Apply pSVT along the time dimension.
    #
    #         if apply_pSVT:
    #
    #             # To Do: Add pSVT along the time dimension.
    #             abc = 1
    #
    #     # Fidelity update and cost computation
    #
    #         V = Vt + V - D
    #
    #         b = ( Vt0 + mu * ( D - V ) ) / ( 1.0 + mu )
    #
    #         delta_norm = np.linalg.norm( Vt.flatten() - b.flatten() ) / np.linalg.norm( b.flatten() )
    #         print( 'Iter ' + str(iter) + ', Convergence criterion: ' + str(delta_norm) )
    #
    #         Vt = b
    #
    #
    # # Output
    #
    #     Vt = np.reshape( Vt, [ne,nt*nz*ny*nx] )
    #
    #     X_out = ( U * E[...,None,:] ) @ Vt
    #
    #     X_out = np.reshape( X_out, [ ne,nt,nz,ny,nx ] )
    #
    #     if isinstance(mask,np.ndarray): # Remove added noise, if necessary
    #
    #         X_out *= mask
    #
    #     if weights is not None and isinstance(weights,np.ndarray):
    #
    #         X_out *= np.reshape(weights,[ne,1,1,1,1])
    #
    #     return X_out


''' 
Denoising Methods - Support Functions
'''

if True:

    def strided_sampling_3D( X, stride=3, inverse=False ):
    # Given stride = 3
    # Maps
    # [a1 b1 c1 a2 b2 c2]
    # to
    # [a1 a2 b1 b2 c1 c2]
    # for the last 3 dimensions of X
    # Useful for extending a kernel domain without the computational
    # overhead of using a kernel which is stride times larger in each dimension.
    # When inverse = True, reverses the process.

        sh = X.shape

        nz = sh[-3]
        ny = sh[-2]
        nx = sh[-1]
    # Stencil of indices of where to read voxels from
    # May need to be cropped for some offsets.

        idxs_take0 = np.reshape( np.arange( 0, nz, stride ) * ny * nx, [-1, 1, 1] ) + \
                     np.reshape( np.arange( 0, ny, stride ) * nx     , [ 1,-1, 1] ) + \
                     np.reshape( np.arange( 0, nx, stride )          , [ 1, 1,-1] )


        nz2, ny2, nx2 = idxs_take0.shape[-3:]

    # Stencil of where to put voxels in the output volume

        idxs_put0  = np.reshape( np.arange( 0, nz2 ) * ny * nx, [-1, 1, 1] ) + \
                     np.reshape( np.arange( 0, ny2 ) * nx     , [ 1,-1, 1] ) + \
                     np.reshape( np.arange( 0, nx2 )          , [ 1, 1,-1] )

    # Precompute output locations for each strided sample

        os_put_z = np.zeros( (stride**3,), np.int64 )
        os_put_y = np.zeros( (stride**3,), np.int64 )
        os_put_x = np.zeros( (stride**3,), np.int64 )

        sample = 0

        offset_z = 0
        for z in range(0,stride):
            offset_y = 0
            ndz = ( nz - (z+1) ) // stride + 1
            for y in range(0,stride):
                offset_x = 0
                ndy = ( ny - (y+1) ) // stride + 1
                for x in range(0,stride):

                # Total number of strided samples at this offset for each dimension

                    ndx = ( nx - (x+1) ) // stride + 1

                    os_put_z[sample] = offset_z
                    os_put_y[sample] = offset_y
                    os_put_x[sample] = offset_x

                    offset_x += ndx

                    sample = sample + 1

                offset_y += ndy

            offset_z += ndz


    # Strided sampling

        # Reshape for broadcasting
        X = np.reshape( X, [-1,nz,ny,nx] )

        X_out = np.zeros_like( X )

        for sample in range( stride**3 ):

        # Offset for the indices of the take stencil

            os_take_z = sample // (stride**2)
            os_take_y = ( sample - os_take_z * stride**2 ) // stride
            os_take_x = sample - os_take_z * stride**2 - os_take_y * stride

            idxs_take = os_take_z * ny * nx + os_take_y * nx + os_take_x

        # Total number of strided samples at this offset for each dimension

            ndz = ( nz - (os_take_z+1) ) // stride + 1
            ndy = ( ny - (os_take_y+1) ) // stride + 1
            ndx = ( nx - (os_take_x+1) ) // stride + 1

        # Offset for the indices of the put stencil

            idxs_put = os_put_z[sample] * ny * nx + os_put_y[sample] * nx + os_put_x[sample]

        # Copy data to the output

            for channel in range(X.shape[0]):

                if inverse:

                    np.put( X_out[channel,...], idxs_take0[:ndz,:ndy,:ndx] + idxs_take, np.take( X[channel,...], idxs_put0[:ndz,:ndy,:ndx] + idxs_put ) )

                else:

                    np.put( X_out[channel,...], idxs_put0[:ndz,:ndy,:ndx] + idxs_put, np.take( X[channel,...], idxs_take0[:ndz,:ndy,:ndx] + idxs_take ) )


        X_out = np.reshape(X_out,sh)

        return X_out


    def masked_noise_measurement_3D( X, mask=None, stride=None ):
    # Use the median absolute deviation of finite differences to estimate the noise level.
    # When mask is None, the noise level is estimated globally.
    # When mask is an ndarray with dimensions matching X.shape[-3:], it is used to limit the voxels used for noise estimation.
    # mask = 1 for voxels to use for noise estimation
    # When stride is an integer greater than 1, dilates the high pass filter kernel to estimate the noise level at a coarser scale.
    # NOTE: To save computation, the median of the HP result is assumed to be zero (i.e. not subtracted prior to taking the absolute value).

        if mask is not None:

            assert np.all( X.shape[-3:] == mask.shape )

    # Broadcast to all additional channels

        sh_in    = X.shape
        nz,ny,nx = X.shape[-3:]
        X        = np.reshape(X,[-1,nz,ny,nx])
        ch       = X.shape[0]

    # Noise estmiation kernel (HHH component of the Haar wavelet transform; normalized after convolution)

        if stride is not None and stride > 1:

            HH_weights = np.zeros(shape=(1,stride+1,stride+1,stride+1), dtype='float32')
            for zv in [0,1]:
                for yv in [0,1]:
                    for xv in [0,1]:
                        HH_weights[0,zv*stride,yv*stride,xv*stride] = (2*xv-1) * (2*yv-1) * (2*zv-1)

        else:

            HH_weights = np.zeros(shape=(1,2,2,2), dtype='float32')
            for zv in [0,1]:
                for yv in [0,1]:
                    for xv in [0,1]:
                        HH_weights[0,zv,yv,xv] = (2*xv-1) * (2*yv-1) * (2*zv-1)

    # High pass filter the input data

        HP = convolve( X, HH_weights, mode='valid' )

        # Crop the mask to match HP
        if mask is not None:

            mask = mask[:-1,:-1,:-1]

        sh_valid = HP.shape[-3:]

    # Estimate the noise level in each channel

        HP = HP / np.sqrt(2.0**3) # 3D HHH filter normalization

        HP = np.abs(HP)

        HP   = np.reshape(HP,[-1,sh_valid[0]*sh_valid[1]*sh_valid[2]])

        if mask is not None:

            mask = mask.flatten()
            HP   = HP[:,mask==1]

        MAD = np.median( HP, axis=-1 )

        MAD = np.reshape(MAD,sh_in[:-3] + (1,1,1))

        MAD = MAD / 0.6745 # Normalzation for Gaussian noise assumption

        return MAD

