## Temporary code and hacked code for special cases.
#  Useful code should be tested and added to one of the main files.

if True:

    import ctypes
    import numpy as np # (v1.21.5)
    import time
    import sys
    import os.path
    import math
    # import csv
    import matplotlib # (v3.5.1)
    import matplotlib.pyplot as plt

    from copy import deepcopy

    # from PIL import Image
    import scipy.signal

    from Python.Recon_Utilities import *
    from Python.Denoising import masked_noise_measurement_3D

    from functools import partial

    # Global variables
    PI             = 3.14159265358979
    CRITICAL_PITCH = 0.8 # pitch >= to this value triggers short-scan reconstruction for single-source data

    # Regularized, iterative reconstruction with the split Bregman method.
    # [1] Goldstein, T., & Osher, S. (2009).
    #     The split Bregman method for L1-regularized problems.
    #     SIAM journal on imaging sciences, 2(2), 323-343.
    # [2] Gao, H., Yu, H., Osher, S., & Wang, G. (2011).
    #     Multi-energy CT based on a prior rank, intensity and sparsity model (PRISM).
    #     Inverse problems, 27(11), 115012.
    # [3] Clark, D. P., & Badea, C. T. (2023).
    #     MCR toolkit: A GPU‐based toolkit for multi‐channel reconstruction of preclinical and clinical x‐ray CT data.
    #     Medical Physics. Aug;50(8):4775-4796.
    class SplitBregman_IterRecon_skip_fid_1():
    # To Do: Temporal reconstruction is too slow with sparse weights. The projections need to be sliced per time point.

        # Skip unregularized algebraic reconstruction during Bregman iteration 1 so that the regularizer
        # is applied to FBPs rather than to algebraic reconstruction.
        # This is useful when working with angularly undersampled projections
        # because unregularized algebraic reconstruction causes blurring.

        def __init__( self, initialization_ops, fidelity_updates, regularizer, times, energies, attn_water, mu0=0.005,
                      debug_outpath=None ):

            self._initialization_ops = initialization_ops
            self._fidelity_updates   = fidelity_updates
            self._regularizer        = regularizer
            self._mu0 = mu0

            self._times    = times
            self._energies = energies
            self._attn_water = attn_water

            assert sum([len(v) for v in initialization_ops]) == times * energies
            assert sum([len(v) for v in fidelity_updates  ]) == times * energies
            assert attn_water.size == energies

            self._attn_water = np.reshape(self._attn_water,(energies,1,1,1,1))

            # Extra path which can be provided to save all intermediate results for debugging purposes.
            self._debug_outpath = debug_outpath


        def __call__( self, Y ):

        # Initialization

            print('Initialization...')

            for s in range(self._energies):

                # Data fidelity updates
                for p in range(self._times):

                    if s == 0 and p == 0:

                        X_ = self._initialization_ops[s][p]()

                        nz, ny, nx = X_.shape

                        X = np.zeros( (self._energies,self._times,nz,ny,nx), np.float32 )

                        X[s,p,...] = X_

                        del X_

                    else:

                        X[s,p,...] = self._initialization_ops[s][p]()

                    print('Done with energy ' + str(s+1) + ', time ' + str(p+1))


            if self._debug_outpath is not None:

                print('Saving initialization results. (debug)')

                save_nii( X, os.path.join( self._debug_outpath, 'X_init.nii' ) )

        # Reconstruction variables

            V = X * 0
            D = X * 0

        # Iteration

            mu       = np.zeros( (self._energies,), np.float32 )
            mu_scale = np.zeros( (self._energies,), np.float32 )

            for Breg_iter in range(1,5):

            # Data fidelity updates
            # This approach to determining the scale parameter may not be ideal.
            # In the prior MATLAB version the scale was measured after the initial round of data fidleity updates.

                print('\nStarting itertaion ' + str(Breg_iter) + '...\n')

                print('Data fidelity updates...')

                if Breg_iter == 1: X_init = np.copy( X )

                for s in range(self._energies):

                    # Data fidelity updates
                    for p in range(self._times):

                        X[s,p,...], residuals, scale = self._fidelity_updates[s][p]( x=X[s,p,...], c=D[s,p,...]-V[s,p,...], mu=mu[s] )

                        if Breg_iter == 1: X[s,p,...] = X_init[s,p,...]

                        print('Iteration ' + str(Breg_iter) + ' residuals ( energy ' + str(s+1) + ', time ' + str(p+1) + ' ): ' + str(residuals))

                        if Breg_iter == 1 and p == 0:

                            mu_scale[s] = scale

                if Breg_iter == 1: del X_init

            # Calibrate regularization strength
            # To Do: Divide X by the attenuation of water before the MAD calculation?

                if Breg_iter == 1:

                    print('Scaling regularization parameters...')

                    mask = make_default_rmask( nz, ny, nx )

                    # => e,1,1,1,1
                    MAD = masked_noise_measurement_3D( X[:,:1,...] / self._attn_water, mask=mask, stride=1 )

                    MAD = MAD / np.amin(MAD);

                    mu[:self._energies] = self._mu0 * np.squeeze( MAD ) * np.squeeze( mu_scale )

                    print('Regularization parameters by energy: ' + str(mu))

                    del mask

            # Regularization

                # Only regularize if we are going to iterate again.
                if Breg_iter < 4:

                    print('\nRegularization...')

                    D = self._regularizer( X + V )

                    V = X + V - D

            # Save intermediate results for debugging

                if self._debug_outpath is not None:

                    print('Saving iteration ' + str(Breg_iter) + ' results. (debug)')

                    save_nii( X, os.path.join(self._debug_outpath, 'X_iter' + str(Breg_iter) + '.nii'))
                    save_nii( D, os.path.join(self._debug_outpath, 'D_iter' + str(Breg_iter) + '.nii'))
                    save_nii( V, os.path.join(self._debug_outpath, 'V_iter' + str(Breg_iter) + '.nii'))

            return X