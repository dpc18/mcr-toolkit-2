
# Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import numpy as np # (v1.21.5)
import time
import sys
import os.path
import math
# import csv
import matplotlib # (v3.5.1)
import matplotlib.pyplot as plt

import nibabel as nib # (5.1.0)

# from PIL import Image
from scipy.signal import convolve
from scipy.sparse.linalg import svds
from scipy.fft import rfft2, irfft2
from scipy.ndimage import median_filter
from scipy.optimize import fminbound

import gc

# matplotlib.use('Qt5Agg')

PI = 3.14159265358979


'''
Data Objects
'''

if True:

    # Subclass the np.ndarray class to add properties to projection arrays which guide reconstruction
    # https://numpy.org/doc/stable/user/basics.subclassing.html#simple-example-adding-an-extra-attribute-to-ndarray
    class Projections( np.ndarray ):

        def __new__(cls, input_array, rebinned=False ):
            # Input array is an already formed ndarray instance
            # We first cast to be our class type
            obj = np.asarray(input_array).view(cls)
            # add the new attribute to the created instance
            obj.rebinned = rebinned
            # Finally, we must return the newly created object:
            return obj

        def __array_finalize__(self, obj):
            # ``self`` is a new object resulting from
            # ndarray.__new__(InfoArray, ...), therefore it only has
            # attributes that the ndarray.__new__ constructor gave it -
            # i.e. those of a standard ndarray.
            #
            # We could have got to the ndarray.__new__ call in 3 ways:
            # From an explicit constructor - e.g. InfoArray():
            #    obj is None
            #    (we're in the middle of the InfoArray.__new__
            #    constructor, and self.info will be set when we return to
            #    InfoArray.__new__)
            if obj is None: return
            # From view casting - e.g arr.view(InfoArray):
            #    obj is arr
            #    (type(obj) can be InfoArray)
            # From new-from-template - e.g infoarr[:3]
            #    type(obj) is InfoArray
            #
            # Note that it is here, rather than in the __new__ method,
            # that we set the default value for 'info', because this
            # method sees all creation of default objects - with the
            # InfoArray.__new__ constructor, but also with
            # arr.view(InfoArray).
            self.rebinned = getattr(obj, 'rebinned', False)
            # We do not need to return anything


    #
    # # Base class for CT image volumes ('Volume') and CT projection data ('Projection')
    # class Base_Volume():
    #
    #
    #
    #
    #
    # # Container for numpy arrays.
    # # Designed to hold image volumes, possibly with multiple time and energy channels.
    # class Volume():
    #
    #     def __init__(self):
    #
    #
    #
    #     def set_vol(self, X):
    #
    #         self._vol = X
    #
    #         self.set_size()
    #
    #     def get_vol():
    #
    #     def set_size(self):
    #
    #
    #
    #         self._size = X.shape
    #
    #         self._ndims = len(self._size)
    #
    #         if self._ndims == 3: # z,y,x volume
    #
    #             self._ne =
    #
    #         elif len(X.shape) == 4: # e,z,y,x volume
    #
    #         else:
    #
    #             if
    #
    #
    #
    #
    #
    #
    #
    #         self.temperature = temperature
    #
    #     def to_fahrenheit(self):
    #         return (self.temperature * 1.8) + 32
    #
    #     @property
    #     def size(self):
    #         print("Getting value...")
    #         return self._temperature
    #
    #
    # # Container for numpy arrays.
    # # Designed to hold projections, possibly with multiple energy channels.
    # class Projection():
    #
    #     def __init__(self, Y: np.ndarray, read_only=False, deep_copy=False ):
    #
    #         assert isinstance( Y, np.ndarray )
    #
    #         # When Y may be used outside of this object, specify deep_copy = True
    #         # to avoid passing by reference.
    #         if deep_copy: self._vol = np.copy( Y )
    #         else: self._vol = Y
    #
    #         # The shape should only change if / when Y is replaced.
    #         self._shape = self._vol.shape
    #         self._ndims = len(self._shape)
    #         self._parse_shape()
    #
    #         # When True, issue an error if the user attempts to overwrite self._vol.
    #         self._read_only = read_only
    #
    #         # Flag indicating the projection data is rebinned with cone-parallel rebinning
    #         # for reconstruction with WFBP.
    #         self._rebinned        = False
    #
    #         # Flag indicating the projection data has multiple energy channels, potentially with a separate
    #         # geometry per channel.
    #         # e.g.: dual-source, dual-energy CT data
    #         self._multi_energy     = False
    #
    #         # Flag indicating the projection data has multiple energy channels, each with identical geometry.
    #         # e.g.: photon-counting CT data
    #         self._photon_counting = False
    #
    #
    #     def _parse_shape(self):
    #
    #         # expected dimensions: [np,nv,nu] or [ne,np,nv,nu]
    #         assert self._ndims == 3 or self._ndims == 4
    #
    #         if self._ndims == 3:  self._ne = 1
    #         else: self._ne = self._shape[0]
    #
    #
    #     @property
    #     def raw_data(self):
    #
    #         if self._read_only: return np.copy( self._vol )
    #         else: return self._vol
    #
    #     @raw_data.setter
    #     def raw_data(self):
    #
    #
    #
    #
    #     @property
    #     def rebinned(self):
    #
    #         return self._rebinned
    #
    #     @rebinned.setter
    #     def rebinned(self, flag: bool):
    #
    #         self._rebinned = flag
    #
    #     @property
    #     def multi_energy(self) -> bool:
    #
    #         return self._multi_energy
    #
    #     @multi_energy.setter
    #     def multi_energy(self, flag: bool):
    #
    #         self._multi_energy = flag
    #
    #     @property
    #     def photon_counting(self) -> bool:
    #
    #         return self._photon_counting
    #
    #     @photon_counting.setter
    #     def photon_counting(self, flag: bool):
    #
    #         self._photon_counting = flag


'''
NIfTI Files
'''

if True:

    # NOTE: Given that the Toolkit operators were originally written for data in Fortran memory order while Numpy defaults
    #       to C memory order, we need to reverse the dimensions when saving and loading NIfTI files.
    def load_nii( vol_file, load_xyz_format=True, return_pix_size=False, type='float32' ):

        # We need to use this syntax to read the data directly into memory.
        vol      = nib.load( vol_file )
        pix_size = vol.header['pixdim'][1:4]
        # vol      = vol.get_fdata().astype(type)
        vol      = np.array( vol.dataobj, dtype=type )

        if load_xyz_format:

            dims = np.arange(len(vol.shape))

            # e.g. x,y,z => z,y,x
            vol = np.transpose( vol, dims[::-1] )

            pix_size = pix_size[::-1]

        if return_pix_size:

            return vol, pix_size

        else:

            return vol


    def save_nii( vol, vol_file, save_xyz_format=True, vox_size=[1,1,1], affine=[[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]] ):

        if save_xyz_format:

            # e.g. z,y,x => x,y,z
            dims = np.arange(len(vol.shape))
            vol = np.transpose( vol, dims[::-1] )

            vox_size = vox_size[::-1]

        nii = nib.Nifti1Image( vol, affine )
        nii.header['pixdim'][1:4] = vox_size
        nib.save(nii, vol_file)



'''
Reconstruction Masks
'''

if True:

    # Project a volumetric reconstruction mask along z for efficient masking on the GPU.
    def mask_zrange( mask ):
    # Input: 3D GPU projection / backprojection mask
    # Output: 2D z index mask

        zmask = 0 * mask[0:2,:,:];

        sh = mask.shape

        temp = zmask[0,:,:]

        for i in range( 1, sh[0]+1 ):

            temp[ np.logical_and( temp == 0, mask[i-1,:,:] != 0 ) ] = i;

        zmask[0,:,:] = temp

        temp = zmask[1,:,:]

        for i in range( sh[0], 0, -1 ):

            temp[ np.logical_and( temp == 0, mask[i-1,:,:] != 0 ) ] = i;

        zmask[1,:,:] = temp

        zmask = zmask.astype(np.int32)

        zmask[0,:,:] = np.maximum( zmask[0,:,:] - 1, 0 );

        zmask = zmask.astype(np.uint16)

        # zmask = np.asfortranarray(zmask)

        return zmask


    # Reproduces the implicit mask as an explicit mask
    def make_default_rmask( nz, ny, nx ):

        rmask = np.zeros( (ny,nx), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1 ),
                              np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1), indexing='ij' )

        rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= min(ny/2,nx/2) ] = 1

        rmask = np.repeat( rmask[np.newaxis,:,:], nz, axis=0 )

        rmask = rmask.astype( np.uint16 );

        # rmask = np.asfortranarray(rmask)

        return rmask


    # Make a mask with a specific diameter in the y,x plane
    def make_diam_rmask( nz, ny, nx, diam ):

        rmask = np.zeros( (ny,nx), dtype=np.float32 )

        x1, x2 = np.meshgrid( np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1 ),
                              np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1), indexing='ij' )

        rmask[ np.sqrt( np.add(x1**2, x2**2) ) <= diam/2.0 ] = 1

        rmask = np.repeat( rmask[np.newaxis,:,:], nz, axis=0 )

        rmask = rmask.astype( np.uint16 );

        # rmask = np.asfortranarray(rmask)

        return rmask


    # Make an elliptical mask with a specific x, y axis length
    def make_ellip_rmask( nz, ny, nx, axis_y, axis_x ):

        rmask = np.zeros( (ny,nx), dtype=np.float32 )

        y, x = np.meshgrid( np.arange( 0. - ny / 2., ny - 1. - ny / 2. + 1 ),
                            np.arange( 0. - nx / 2., nx - 1. - nx / 2. + 1 ), indexing='ij' )

        rmask[ ( y**2 / axis_y**2 + x**2 / axis_x**2 ) <= 1.0 ] = 1

        rmask = np.repeat( rmask[np.newaxis,:,:], nz, axis=0 )

        rmask = rmask.astype( np.uint16 );

        # rmask = np.asfortranarray(rmask)

        return rmask


'''
Geometry Files
'''

if True:


    def rotx3(x):

        return np.array( [ [1, 0, 0], [0, np.cos(x), -1. * np.sin(x)], [ 0, np.sin(x), np.cos(x)] ], dtype=np.double )


    def roty3(x):

        return np.array( [ [np.cos(x), 0, np.sin(x)], [0, 1, 0], [-1. * np.sin(x), 0, np.cos(x)] ], dtype=np.double )


    def rotz3(x):

        return np.array( [ [np.cos(x), -1. * np.sin(x), 0], [ np.sin(x), np.cos(x), 0 ], [0, 0, 1] ], dtype=np.double )


    # def make_geo_file( geofile, int_params, double_params, rot_dir, angles, proj_weights, z_pos ):
    #
    #     a2r = PI / 180.
    #     r2a = 180. / PI
    #
    #     # int_params:    [ nu, nv, np, nx, ny, nz, filtration, use_affine, [implicit_mask], [explicit_mask], [cy_detector], [use_norm_vol] ]
    #     # double_params: [ du, dv, dx, dy, dz, zo, ao, lsd, lso, uc, vc, eta, sig, phi, [scale], [db], [limit_angular_range] ]
    #
    #     np_ = int_params[2]
    #
    #     lsd = double_params[7]
    #     lso = double_params[8]
    #     uc  = double_params[9]
    #     vc  = double_params[10]
    #     eta = double_params[11]
    #     sig = double_params[12]
    #     phi = double_params[13]
    #     zz  = double_params[5]
    #     aa  = double_params[6]
    #     du  = double_params[0]
    #     dv  = double_params[1]
    #
    #     geo_lines = np.zeros( ( np_, 21), dtype=np.double )
    #
    #     # j = -1
    #
    #     rot = np.matmul( rotx3(-eta), np.matmul( roty3(-sig), rotz3(-phi) ) )
    #     puv = np.matmul( rot, [[0.],[du],[0.]] )
    #     pvv = np.matmul( rot, [[0.],[0.],[dv]] )
    #
    #
    #     if ( z_pos.size == 1 ): # circular geometry
    #
    #         src = np.array( [[-1.0 * lso], [ 0.0 ]                    , [ 0.0 ]      ] ) - \
    #               np.array( [[ 0.0 ]     , [ 0.0 ]                    , [ 0.0 - zz ] ] ) + \
    #               np.array( [[ 0.0 ]     , [ lsd * np.sin( AMu * db )], [ AMv * dv ] ] )
    #
    #         dtv = np.array( [[ lsd - lso ], [ 0.0 ]                    , [ 0.0 ]      ] ) - \
    #               np.array( [[ 0.0 ]      , [ 0.0 ]                    , [ 0.0 - zz ] ] )
    #
    #         # src = [   [-1.*lso], [ 0. ], [ -zz ] ]
    #         # dtv = [ [ lsd-lso ], [ 0. ], [ -zz ] ]
    #
    #     for i in range(0,np_):
    #
    #         # j = j + 1
    #
    #         if ( z_pos.size > 1 ): # helical geometry
    #
    #             src = np.array( [[-1.0 * lso], [ 0.0 ]                    , [ 0.0 ]           ] ) - \
    #                   np.array( [[ 0.0 ]     , [ 0.0 ]                    , [ z_pos[i] - zz ] ] ) + \
    #                   np.array( [[ 0.0 ]     , [ lsd * np.sin( AMu * db )], [ AMv * dv ]      ] )
    #
    #             dtv = np.array( [[ lsd - lso ], [ 0.0 ]                   , [ 0.0 ]           ] ) - \
    #                   np.array( [[ 0.0 ]      , [ 0.0 ]                   , [ z_pos[i] - zz ] ] )
    #
    #             # src = [   [-1.*lso], [ 0. ], [ z_pos[i]-zz ] ]
    #             # dtv = [ [ lsd-lso ], [ 0. ], [ z_pos[i]-zz ] ]
    #
    #         angle = angles[i] + aa
    #
    #         # prevent singular projection matrices
    #         # if ( np.mod( angle, 45.0 ) == 0 ):
    #
    #         #    angle = angle + 1e-4
    #
    #         rsrc = np.matmul( rotz3( rot_dir * angle * a2r) , src )
    #         rdtv = np.matmul( rotz3( rot_dir * angle * a2r) , dtv )
    #         rpuv = np.matmul( rotz3( rot_dir * angle * a2r) , puv )
    #         rpvv = np.matmul( rotz3( rot_dir * angle * a2r) , pvv )
    #
    #         other_params = np.array([lsd, lso, uc, vc, eta, sig, phi, proj_weights[i,0], angle])
    #         # other_params = np.array([lsd, lso, uc, vc, eta, sig, phi, proj_weights[0,i], angle])
    #         geo_lines[i,:] = np.concatenate( [ rsrc.flatten(),rdtv.flatten(),rpuv.flatten(),rpvv.flatten(),other_params ], axis=0 )
    #
    #     # geo_lines.tofile( geofile )
    #
    #     np.savetxt( geofile, geo_lines, fmt='%1.7e' )
    #
    #     return 0


    def make_geo_file( geofile, int_params, double_params, rot_dir, angles, proj_weights, z_pos, flip_z=1.0, AMu=0.0, AMv=0.0, db=0.0 ):

        a2r = PI / 180.
        r2a = 180. / PI

        # int_params:    [ nu, nv, np, nx, ny, nz, filtration, use_affine, [implicit_mask], [explicit_mask], [cy_detector], [use_norm_vol] ]
        # double_params: [ du, dv, dx, dy, dz, zo, ao, lsd, lso, uc, vc, eta, sig, phi, [scale], [db], [limit_angular_range] ]

        np_ = int_params[2]

        lsd = double_params[7]
        lso = double_params[8]
        uc  = double_params[9]
        vc  = double_params[10]
        eta = double_params[11]
        sig = double_params[12]
        phi = double_params[13]
        zz  = flip_z * double_params[5]
        aa  = double_params[6]
        du  = double_params[0]
        dv  = flip_z * double_params[1]

        z_pos2 = flip_z * z_pos

        geo_lines = np.zeros( ( np_, 21), dtype=np.double )

        # j = -1

        rot = np.matmul( rotx3(-eta), np.matmul( roty3(-sig), rotz3(-phi) ) )
        puv = np.matmul( rot, [[0.],[du],[0.]] )
        pvv = np.matmul( rot, [[0.],[0.],[dv]] )


        if ( z_pos2.size == 1 ): # circular geometry

            src = np.array( [[-1.0 * lso], [ 0.0 ]                    , [ 0.0 ]      ] ) + \
                  np.array( [[ 0.0 ]     , [ 0.0 ]                    , [ 0.0 - zz ] ] ) + \
                  np.array( [[ 0.0 ]     , [ lsd * np.sin( AMu * db )], [ AMv * dv ] ] )

            dtv = np.array( [[ lsd - lso ], [ 0.0 ]                    , [ 0.0 ]      ] ) + \
                  np.array( [[ 0.0 ]      , [ 0.0 ]                    , [ 0.0 - zz ] ] )


        for i in range(0,np_):

            # j = j + 1

            if ( z_pos2.size > 1 ): # helical geometry

                src = np.array( [[-1.0 * lso], [ 0.0 ]                    , [ 0.0 ]           ] ) + \
                      np.array( [[ 0.0 ]     , [ 0.0 ]                    , [ z_pos2[i] - zz ] ] ) + \
                      np.array( [[ 0.0 ]     , [ lsd * np.sin( AMu * db )], [ AMv * dv ]      ] )

                dtv = np.array( [[ lsd - lso ], [ 0.0 ]                   , [ 0.0 ]           ] ) + \
                      np.array( [[ 0.0 ]      , [ 0.0 ]                   , [ z_pos2[i] - zz ] ] )


            angle = angles[i] + aa

            # prevent singular projection matrices
            # if ( np.mod( angle, 45.0 ) == 0 ):

            #    angle = angle + 1e-4

            rsrc = np.matmul( rotz3( rot_dir * angle * a2r) , src )
            rdtv = np.matmul( rotz3( rot_dir * angle * a2r) , dtv )
            rpuv = np.matmul( rotz3( rot_dir * angle * a2r) , puv )
            rpvv = np.matmul( rotz3( rot_dir * angle * a2r) , pvv )

            # Bug fix 7-22-24
            # other_params = np.array([lsd, lso, uc, vc, eta, sig, phi, proj_weights[i,0], angle])
            other_params = np.array([lsd, lso, uc, vc, eta, sig, phi, proj_weights[0,i], angle])

            geo_lines[i,:] = np.concatenate( [ rsrc.flatten(),rdtv.flatten(),rpuv.flatten(),rpvv.flatten(),other_params ], axis=0 )

        # geo_lines.tofile( geofile )

        np.savetxt( geofile, geo_lines, fmt='%1.7e' )

        return 0


'''
Gating
'''



'''
Projection Processing
'''

if True:

    # To Do: Revise projection exposure correction for helical data sets.
    def transform_projections( data, air, dark=None, gap_map=None, fix_proj=None, enforce_non_negativity=False,
                               suppress_zingers=0.20, per_projection=False, select_airraws=None, gap_correction=True ):
    # data: Projection data as a nifti file.
    # air: Air raws for air normalization
    # dark: darkfield images (for EIDs, None for PCDs)
    # gap_map: binary map of detector pixels to interpolate (1.0: interpolate)
    # fix_proj: first-order compensation for under-exposed projections
    # enforce_non_negativity: (True) threshold negative values to zero after computation of the log transform
    # suppress_zingers: allowed deviation in air raw signal as a fraction of the median air raw value (outliers are interpolated)
    #                   Set to None or 0 to disable zinger detection.
    #                   Currently, only the first energy is used for zinger detection (assumed to have the most signal).
    # per_projection: (False) apply the same zinger correction to all projection angles and energies
    #                 (True) fix zingers per projection and energy
    # select_airraws: (disabled) (None) apply the same air normalization to all projections
    #                 (Similarity Metric) select air raws which maximize the similarity metric per projection
    # gap_correction: (False) do not apply gap correction / zinger interpolation, regardless of other options
    #                 (True)  apply gap correction / zinger interpolation, in line with other options

    # read projection data

        Y = load_nii( data )

        szy = Y.shape

        if len(Y.shape) > 3:
            # Y: e,np,nv,nu => (energies,projections,rows,columns)
            includes_energy=True
        else:
            # Y: np,nv,nu => 1,np,nv,nu
            includes_energy=False
            Y = Y[np.newaxis,...]

        nv = Y.shape[-2]
        nu = Y.shape[-1]

    # read air raw data

        I00 = load_nii(air)

        if includes_energy:
            assert len(I00.shape) == 4
            assert I00.shape[0] == Y.shape[0]
        else:
            assert len(I00.shape) == 3
            I00 = I00[np.newaxis,...]

        assert I00.shape[-2] == nv and I00.shape[-1] == nu

        # Reduce noise and the impact of exposure artifacts by taking the median
        # of a number of air raws.
        I0 = np.median( I00, axis=1, keepdims=True )

    # darkfield image

        if dark is not None:
        # shape: nv,nu? => broadcasts to 1,1,nv,nu as needed

            DF = load_nii(dark)

            assert DF.shape[-2] == nv and DF.shape[-1] == nu

            # If we have multiple darkfield images, average them.
            if DF.ndim > 2:

                DF = np.mean( DF, axis=-3, keepdims=True )

        else:

            DF = 0

    # load gap map
    # Has a value of 1 for detector pixels to interpolate;
    # 0 elsewhere.

        # If multi-energy (ne,np,nv,nu), use a different gap map per threshold.
        # If single energy (np,nv,nu), broadcast the map as needed.

        if gap_map is not None:

            gaps = load_nii( gap_map )

            assert gaps.shape[-2] == nv and gaps.shape[-1] == nu

            if len(gaps.shape) == 2: gaps = gaps[np.newaxis,...] # One gap map for all projection angles

            if len(gaps.shape) == 3: gaps = gaps[np.newaxis,...] # One gap map for all energies

        else:

            gaps = np.zeros_like( Y[:1,:1,...] )

    # Exposure correction
    # Should work well for circular scans, but may cause problems with helical scans...

        if fix_proj is not None:

            # e,np,nv,nu => e,np,1,1
            Y_sums = np.sum( Y, axis=(2,3), keepdims=True )

            # minimum MSE rescaling
            # Y     : e,np,nv,nu
            # Y_sums: e,np,1 ,1
            gain = np.median( Y_sums, axis=1, keepdims=True ) / Y_sums[:,fix_proj,:,:]
            Y[:,fix_proj,:,:] = Y[:,fix_proj,:,:] * gain

            del Y_sums, gain

    # Additional correction when there are significant energy and/or projection-specific errors.
    # Not intended for routine use.

        if per_projection:

            # I0     : ne,1,nv,nu
            # med_val: ne,1,1 ,1
            med_val = np.median( I0, axis=[-2,-1], keepdims=True )

            mask = np.greater( Y, med_val * 5.0 )

            for s in range(szy[-4]): # energy

                for proj in range(szy[-3]): # proj

                    if np.any( mask[s,proj,...] ):

                        print('Special correction applied to energy ' + str(s+1) + ', projection ' + str(proj+1))

                        Ym = median_filter(Y[s,proj,...], size=3)

                        Y[s,proj,...] = np.where( mask[s,proj,...], Ym, Y[s,proj,...] )

            del mask


    # Zinger detection

        if suppress_zingers is not None and suppress_zingers > 0:

            assert suppress_zingers < 1.0

            # I0     : ne,1,nv,nu
            # med_val: 1,1,1 ,1
            med_val = np.median( I0[:1,...], axis=[2,3], keepdims=True )

            # Only check the first energy. (assumed to have the most counts)
            mask = np.logical_or( np.greater( I0[:1,...], med_val * ( 1.0 + suppress_zingers ) ),
                                  np.less   ( I0[:1,...], med_val * ( 1.0 - suppress_zingers ) ) )
            gaps = np.logical_or( gaps, mask )


    # Air normalization

        # if select_airraws is not None:
        #
        #     print('Using MI to choose air raws...');
        #
        #     ne  = Y.shape[0]
        #     np_ = Y.shape[1]
        #     na  = I00.shape[-3]
        #
        #     MIs  = np.zeros( (ne,np_,na), np.float32 )
        #     # Idxs = np.zeros( (ne,np_), np.int32 )
        #
        #     avg_radius = 5
        #
        #     for s in range(ne):
        #
        #         print('Processing energy' + str(s+1) + '...')
        #
        #         for p in range(np_):
        #
        #             for a in range(na):
        #
        #                 # MIs[s,p,a] = select_airraws( Y[s,p,:,:], I__ )
        #                 MIs[s,p,a] = -np.nanstd( -np.log(Y[s,p,:,465:480] / I00[s,a,:,465:480]) )
        #
        #             # Find the air raw with the maximum MI
        #             # MIs:  ne,np_,na
        #             # Idxs: ne,np_
        #             Idxs = np.argsort( MIs[s,p,:] )
        #
        #             I0_match = I0_[Idxs[-1],:,:]
        #
        #             num   = np.maximum( Y[s,p,:,:] - DF, 0.0 )
        #             denom = np.maximum(I0_match - DF, 1e-6)
        #             if enforce_non_negativity: num = np.where( num > denom, denom, num )
        #             Y[s,p,...] = -np.log( num / denom )
        #
        #             if p % 20 == 0:
        #
        #                 print('Done with ' + str(p) + ' of ' + str(np_) + '...')
        #
        #
        #     # When there are gaps, some values are probably zero, which turn to inf when we take the -log.
        #     Y = np.where( np.isfinite(Y), Y, 0.0 )
        #
        # else:

        # I0     : ne,1,nv,nu
        # med_val: ne,1,1 ,1

        med_val = np.median(I0 - DF, axis=[2,3], keepdims=True )

        eps = 0.1 * med_val
        num   = np.maximum(Y - DF,0.0)
        denom = np.maximum(I0 - DF,eps)
        if enforce_non_negativity: num = np.where( num > denom, denom, num )
        Y     = -np.log( num / denom )

        # When there are gaps, some values are probably zero, which turn to inf when we take the -log.
        Y = np.where( np.isfinite(Y), Y, 0.0 )

    # convolve and normalize to fill gaps
    # Assumes every gap pixel is within 11 pixels of a known pixel; otherwise, this code will divde by zero.

        if gap_correction:

            # Gaussian with 2 pixel FWHM
            # For large projection sets it will be faster/cheaper to do 2x 1D convolutions vs. a 2D convolution.
            sig = 2.0 / ( 2.0 * np.sqrt( 2 * np.log(2) ) )
            G   = np.exp( -np.arange(-5,6)**2 / ( 2.0 * sig**2 ) )
            G   = G / np.sum(G)
            G1  = np.reshape(G,[1,1,1,-1])
            G2  = np.reshape(G,[1,1,-1,1])

            # Normalization
            # This function doesn't broadcast in the usual way, so we need to match the number of dimensions between each
            #   input.
            # This seems to return very small, negative values, which confuses the issue of checking to make sure every
            #   pixel is within the kernel size of a valid pixel.
            # input.
            # gaps: ne,np,nv,nu
            # G: nv,nu => 1,1,nv,nu
            norm0 = convolve( convolve( 1.0 - gaps.astype('float32'), G1, mode='same' ), G2, mode='same' )

            # Avoid dividing by zero
            if np.amin(norm0) <= 0.0:

                print( 'Default smoothing kernel size (11x11) is too small to fill in missing pixel data!' )
                return -1

            # gaps: 1,1,nv,nu or ne,1,nv,nu
            # Y2: ne,np,nv,nu
            Y2 = np.copy( Y )                                                # Copy the projections
            Y2 = np.where( gaps, 0.0, Y2 )                                   # Set gap values exactly to zero
            Y2 = convolve( convolve( Y2, G1.astype('float32'), mode='same'), # Smooth in neighboring known values
                                     G2.astype('float32'), mode='same')
            Y2 /= norm0.astype('float32')                                    # Normalize
            Y = np.where( gaps, Y2, Y )                                      # Insert low-pass values where there are gaps

        return Y


    def spectral_ring_correction( Y, sinogram_rank=3, summarize_results=False, limit_rank=None, tol=3e-5, return_LP=3.5*15 ):
    # Y                : (ne,np_,nv,nu) projection data on which to perform ring correction
    # sinogram_rank    : maximum rank of itensity oscillations due to ring artifacts along the angle dimension (e.g. 3)
    # summarize_results: when True, show before and after images and residuals following correction
    # limit_rank       : maximum number of spectral components to correct (<= number of energies; 'None' automatically sets limit_rank = ne)
    # tol              : tolerance to meet for convergence of RPCA (NOTE: RPCA reports % error, so convergence is at tolerance / 100 %; 12 iterations minimum )
    # return_LP        : controls a final low-pass filtration operation performed within each projection to prevent image details from bleeding into the correction
    #                    (None) do not perform this step
    #                    (float) do not correct spatial frequencies below 1 cycle / ( return_LP voxels )

        szy = Y.shape
        ne  = szy[0];
        np_ = szy[1];
        nv  = szy[2];
        nu  = szy[3];

        start_time = time.time()

        # Y : ne,np*nv*nu
        # U : ne,ne
        # E : ne,
        # Vt: ne,np*nv*nu
        U, E, Vt = eig_SVD( np.reshape( Y, [ne, -1] ) )

        Vt     = np.reshape( Vt, szy )
        Vt_out = np.zeros_like( Vt )

        # Tolerance for RPCA factorizations.
        # tol = 3e-5

        # Maximum number of iterations for RPCA operations.
        max_iter = 200

    # Fix the average projections

        if limit_rank is None:

            limit_rank = ne

        for c in range(ne):

            if c >= limit_rank:

                Vt_out[c,...] = Vt[c,...]

            else:

            # Extract ?

                print('\nWorking on component ' + str(c) + '...\n')

                chunk = Vt[c,...]

            # Low pass filter

                # Originally, in the MATLAB version, sigma was 8 here...
                krad = 30
                ksigma = 4.0
                G  = np.exp( -np.arange( -krad, krad+1 )**2 / ( 2 * ksigma**3 ) )
                G /= np.sum(G)
                G  = G.astype( Y.dtype )

                # Low pass filter the rows
                # np,nv,nu
                chunkG = np.pad( chunk, ( (0,0), (0,0), ( krad, krad ) ), 'reflect' )
                chunkG = convolve( chunkG, G[np.newaxis,np.newaxis,:], 'valid' )
                chunkG = np.pad( chunkG, ( (0,0), ( krad, krad ), (0,0) ), 'reflect' )
                chunkG = convolve( chunkG, G[np.newaxis, :, np.newaxis], 'valid' )

                diff = chunk - chunkG

                del chunk

            # Rank reduction

                # Limit the rank of XL more to prevent valid sinogram data from being assigned to XL
                # Broadcasts along the first ndim-2 dimensions; however, the convergence criterion is
                # averaged across the broadcast dimensions.
                XL, XS = RPCA( diff, nrows=np_, ncols=nv*nu, iters=max_iter, tol=tol, limit_rank=sinogram_rank ) # , subsample_SVD=10 )

            # Return additional low-frequency components

                if return_LP is not None:

                    # np,nv,nu
                    # XLG = np.pad( XL, ( (0,0), (krad,krad), (0,0) ), 'reflect' )
                    # XLG = convolve( XLG, G[np.newaxis,:,np.newaxis], 'valid' )
                    # XLG = np.pad( XLG, ( (0,0), (0,0), (krad,krad) ), 'reflect' )
                    # XLG = convolve( XLG, G[np.newaxis,np.newaxis,:], 'valid' )

                    # Fourier LP filter
                    FXL = rfft2( XL )
                    ny_ = ( 2 * FXL.shape[-2] ) // return_LP
                    nx_ = ( 2 * FXL.shape[-1] ) // return_LP
                    mask = make_ellip_rmask( nz=1, ny=FXL.shape[-2], nx=2*FXL.shape[-1], axis_y=ny_, axis_x=nx_ )
                    mask = scipy.fft.ifftshift(mask,axes=[-2,-1])[:,:,:FXL.shape[-1]]
                    FXL = FXL * (1.0 - mask)
                    XLG = irfft2( FXL )
                    XLG = XL-XLG

                    del mask, FXL

                else:

                    XLG = 0

            # Report residual for QA purposes

                residual = np.linalg.norm( ( diff-XS-XLG ).flatten() ) / np.linalg.norm( diff.flatten() )

                print('Relative sinogram residual (%): ' + str( 100 * residual ) )

            # Put the deringed result into the output

                Vt_out[c,...] = XS + chunkG + XLG

            del chunkG, XLG, diff, XL, XS

        del Vt

        end_time = time.time()

        print('Total processing time: ' + str( end_time-start_time ) + ' seconds' )

    # Reconstruction output

        # U : ne,ne
        # E : ne,
        # Vt: ne,np,nv,nu
        Y_out = np.reshape( ( U * E[np.newaxis,...] ) @ np.reshape( Vt_out, [ne, -1] ), szy )

        if summarize_results:

            sl = nv // 4

            if ne*np_ > nu:

                # ne,np,nv,nu => ne,np,nu
                imc( [ np.transpose( Y    [:,:,sl,:],[0,2,1]),
                       np.transpose( Y_out[:,:,sl,:],[0,2,1]) ], shape=(ne*nu,np_), gamma=1.0, show=True, outfile=None )

            else:

                # ne,np,nv,nu
                imc( [ Y[:,:,sl,:], Y_out[:,:,sl,:] ], shape=(ne*np_,nu), gamma=1.0, show=True, outfile=None )

        return Y_out


    def spectral_ring_correction_v2( Y, sinogram_rank=3, summarize_results=False, start_FWHM=19, return_FWHM=52 ):
    # Y                : (ne,np_,nv,nu) projection data on which to perform ring correction
    # sinogram_rank    : maximum rank of itensity oscillations due to ring artifacts along the angle dimension (e.g. 3)
    # summarize_results: when True, show before and after images and residuals following correction
    # start_FWHM       : Gaussian FWHM to use for initial low-pass filtering
    # return_FWHM      : Gaussian FWHM to use to suppress details which bleed into the correction

        def FFT_Gaussian_LP_2D( X, FWHM, cross=False ):

            # y, x = np.meshgrid( np.arange( 0. - nv / 2., nv - 1. - nv / 2. + 1 ),
            #                     np.arange( 0. - nu / 2., nu - 1. - nu / 2. + 1 ), indexing='ij' )

            sigma = FWHM / ( 2 * np.sqrt( 2 * np.log(2) ) )

            # Pad nv to reduce wrap-around artifacts
            # np,nv,nu
            X   = np.pad( X, ((0,0),(FWHM,FWHM),(0,0)), mode='reflect' )
            nv_ = nv + 2 * FWHM

            # We need the nu dimension to be even; otherwise rfft2 will crop off a pixel.
            if nu % 2 == 1:
                odd_size=True
                nu_ = nu + 1
                X = np.concatenate( [X,X[...,-1:]], axis=-1 )
            else:
                odd_size=False
                nu_ = nu

            # Works for odd values because -nv//2 rounds down
            # vs. -(nv//2) which doesn't
            y = np.reshape( np.arange( -nv_//2, nv_//2 ), [nv_,  1] )
            x = np.reshape( np.arange( -nu_//2, nu_//2 ), [ 1, nu_] )

            G            = np.exp( -( x**2 + y**2 ) / ( 2 * sigma**2 ) )
            G           /= np.sum(G)
            G            = G.astype( X.dtype )
            FG           = rfft2( G )

            if cross:

                fy = np.sum( ( ( np.arange(nv_) / ( 2 * nv_ ) ) < ( ( (0.2 * FWHM) - 1 ) / ( 2 * nv_ ) ) ).astype(np.int32) )
                fx = np.sum( ( ( np.arange(nu_) / ( 2 * nu_ ) ) < ( ( (0.2 * FWHM) - 1 ) / ( 2 * nu_ ) ) ).astype(np.int32) )

                FG[:fy,...]  = 0
                FG[-fy:,...] = 0
                FG[:,:fx]    = 0

            XG = np.fft.ifftshift( irfft2( FG * rfft2( X ) ), axes=[-2,-1] )

            if odd_size:
                XG = XG[...,:-1]

            XG = XG[...,FWHM:-FWHM,:]

            return XG


        def Gaussian_LP_2D( X, FWHM ):

            ksigma = FWHM / ( 2 * np.sqrt( 2 * np.log(2) ) )
            krad   = 3 * FWHM

            # Originally, in the MATLAB version, sigma was 8 here...
            G  = np.exp( -np.arange( -krad, krad+1 )**2 / ( 2 * ksigma**3 ) )
            G /= np.sum(G)
            G  = G.astype( X.dtype )

            XG = np.pad  ( X, ( (0,0), (krad,krad), (0,0) ), 'reflect' )
            XG = convolve( XG, G[np.newaxis,:,np.newaxis], 'valid' )
            XG = np.pad  ( XG, ( (0,0), (0,0), (krad,krad) ), 'reflect' )
            XG = convolve( XG, G[np.newaxis,np.newaxis,:], 'valid' )

            return XG


        def FFT_cutoff( X, bandwidth=10, cross=True ):
        # bandwidth in voxels / cycle

            # Fourier LP filter
            FXL = rfft2( X )
            ny_ = ( 2 * FXL.shape[-2] ) // bandwidth
            nx_ = ( 2 * FXL.shape[-1] ) // bandwidth
            mask = make_ellip_rmask( nz=1, ny=FXL.shape[-2], nx=2*FXL.shape[-1], axis_y=ny_, axis_x=nx_ )
            mask = np.fft.ifftshift(mask,axes=[-2,-1])[:,:,:FXL.shape[-1]]

            if cross:

                fy = bandwidth // 3 # np.sum( ( ( np.arange(nv_) / ( 2 * nv_ ) ) < ( ( (0.2 * FWHM) - 1 ) / ( 2 * nv_ ) ) ).astype(np.int32) )
                fx = bandwidth // 3  # np.sum( ( ( np.arange(nu_) / ( 2 * nu_ ) ) < ( ( (0.2 * FWHM) - 1 ) / ( 2 * nu_ ) ) ).astype(np.int32) )

                mask[...,:fy,:]  = 0
                mask[...,-fy:,:] = 0
                mask[...,:,:fx]  = 0

                FXL = FXL * (1.0 - mask)

            else:

                FXL = FXL * mask

            X_out = irfft2( FXL )

            return X_out


        # f(x) = exp( - a * x**2 )
        # sigma = 2.0
        # a = 1 / ( 2 * sigma**2 )
        # F(x) = sqrt( pi / a ) * exp( - pi**2 * k**2 / a )

        szy = Y.shape
        ne  = szy[0];
        np_ = szy[1];
        nv  = szy[2];
        nu  = szy[3];

        start_time = time.time()

        if ne == 1:

            # No need to do a real SVD if the number of energies is 1.
            # U : ne,ne
            # E : ne,
            # Vt: ne,np,nv,nu
            Vt = Y
            U  = np.ones( (1,1), np.float32 )
            E  = np.ones( (1,), np.float32 )

        else:

            # Y : ne,np*nv*nu
            # U : ne,ne
            # E : ne,
            # Vt: ne,np*nv*nu
            U, E, Vt = eig_SVD( np.reshape( Y, [ne, -1] ) )
            Vt       = np.reshape( Vt, szy )

        Vt_out = np.zeros_like( Vt )

    # Fix the average projections

        for c in range(ne):

        # Extract ?

            print('\nWorking on component ' + str(c+1) + '...\n')

            chunk = Vt[c,...]

        # Low pass filter

            chunkG = FFT_Gaussian_LP_2D( chunk, FWHM=start_FWHM )
            # chunkG = Gaussian_LP_2D( chunk, FWHM=start_FWHM )

            diff = chunk - chunkG

            del chunk

        # Truncated SVD of the angle dimension

            u,s,vh = svds( np.reshape(diff,[np_,nv*nu]), k=sinogram_rank )
            s = s[np.newaxis,:]
            XL = np.reshape( ( u * s ) @ vh, [np_,nv,nu] )

        # Rank reduction

            # Limit the rank of XL more to prevent valid sinogram data from being assigned to XL
            # Broadcasts along the first ndim-2 dimensions; however, the convergence criterion is
            # averaged across the broadcast dimensions.
            # XL, XS = RPCA( diff, nrows=np_, ncols=nv*nu, iters=max_iter, tol=tol, limit_rank=sinogram_rank )

        # Return additional low-frequency components

            XLG = FFT_Gaussian_LP_2D( XL, FWHM=return_FWHM )
            # XLG = Gaussian_LP_2D( XL, FWHM=return_FWHM )
            # XLG = FFT_Gaussian_LP_2D( XL, FWHM=return_FWHM, cross=True )
            # XLG = FFT_cutoff( XL, bandwidth=10, cross=True )

        # Report residual for QA purposes

            residual = 1.0 - np.linalg.norm( ( diff - (XL - XLG) ).flatten() ) / np.linalg.norm( diff.flatten() )

            print('Relative sinogram residual (%): ' + str( 100 * residual ) )

        # Put the deringed result into the output

            Vt_out[c,...] = diff - ( XL - XLG ) + chunkG

        del Vt, chunkG, XLG, diff, XL

        end_time = time.time()

        print('Total processing time: ' + str( end_time-start_time ) + ' seconds' )

    # Reconstruction output

        # U : ne,ne
        # E : ne,
        # Vt: ne,np,nv,nu
        Y_out = np.reshape( ( U * E[np.newaxis,...] ) @ np.reshape( Vt_out, [ne, -1] ), szy )

        if summarize_results:

            sl = nv // 4

            if ne*np_ > nu:

                # ne,np,nv,nu => ne,np,nu
                imc( [ np.transpose( Y    [:,:,sl,:],[0,2,1]),
                       np.transpose( Y_out[:,:,sl,:],[0,2,1]) ], shape=(ne*nu,np_), gamma=1.0, show=True, outfile=None )

            else:

                # ne,np,nv,nu
                imc( [ Y[:,:,sl,:], Y_out[:,:,sl,:] ], shape=(ne*np_,nu), gamma=1.0, show=True, outfile=None )

        return Y_out


''' 
Frequency filters for analytical reconstruction
'''

if True:


    def make_FBP_filter( name, du, nu, window_type, cutoff ):
    # Make frequnecy domain filter
    # A. C. Kak and Malcolm Slaney, Principles of Computerized Tomographic Imaging, IEEE Press, 1988.

        # Number of detector elements along a (padded) row, must be a power of 2
        assert np.round(np.log2(nu)) == np.log2(nu)

        # Chapter 3, Eq. 61
        filter = get_half_ramp(nu,du)

        filter = apply_window( filter, window_type, cutoff, nu )

        filter = filter.astype( np.float32 )

        # NOTE: np.savetxt does not encode in the expected way
        filter.tofile(name)

        return filter


    def make_FBP_filter_v2( name, du, nu, window_type, f50 ):
    # Make frequnecy domain filter
    # A. C. Kak and Malcolm Slaney, Principles of Computerized Tomographic Imaging, IEEE Press, 1988.
    #
    # Inputs:
    # 1) name: complete file name (*.txt) and path where the filter will be saved
    # 2) du: detector pixel size along the u axis
    # 3) nu: number of detector pixels (rounded up to the next highest power of 2)
    # 4) window_type: function to use for ramp filter windowing
    # 5) f50: frequency in cycles / mm at which the window function reaches 50%
    #
    # Outputs:
    # filter: frequency filter from 0 to 0.5 cycles / pixel

        # Number of detector elements along a (padded) row, must be a power of 2
        assert np.round(np.log2(nu)) == np.log2(nu)

        # Chapter 3, Eq. 61
        filter = get_half_ramp(nu,du)

        if window_type != 'ram-lak':

            f = np.linspace( 0, nu//2, nu//2 + 1 ) / ( nu * du )
            fmax  = f[-1]
            fmax0 = f[-1]

            # Check if the target frequency for 50% modulation (' num2str(f50) ' cycles/mm) exceeds 2 x Nyquist frequency (' num2str(2*fmax) ' cycles/mm).'
            if f50 > 2 * fmax:

                print('Target frequency for 50% modulation (' + str(f50) + ' cycles/mm) exceeds 2 x Nyquist frequency (' + str(2*fmax) + ' cycles/mm).')

                assert f50 <= 2 * fmax

            # Use a higher maximum frequency in case f50 is beyond Nyquist.
            fmax = 1.0 / du
            f    = np.linspace( 0, nu, nu + 1 ) / ( nu * du )

            # Convert f50 from a spatial frequency to an index
            f_   = np.abs( f - f50 )
            # f50  = np.argmax( f_ == np.amin(f_) ) # [0] # f50  = find( f_ == min(f_), 1, 'first');
            f50 = np.where( f_ == np.amin(f_) )[0]

            filter = apply_window_v2( filter, window_type, f50, fmax0, fmax, nu, f )

        filter = filter.astype( np.float32 )

        # NOTE: np.savetxt does not encode in the expected way
        filter.tofile(name)

        return filter


    def get_half_ramp(nu, du):

        filter = np.zeros((nu,))
        N = np.arange(-(nu//2),(nu//2))

        filter[nu//2] = 1.0 / ( 4.0 * du**2 )

        idx = np.mod( N, 2 ).astype('bool')

        filter[idx] = -1.0 / ( PI * N[idx] * du )**2

        filter = du * np.real( np.fft.fft( np.fft.fftshift( filter ) ) )

        filter = filter[0:(nu//2+1)]

        return filter


    def apply_window( filter, window, cutoff, nu ):
    # https://en.wikipedia.org/wiki/Window_function

        w = np.linspace(0,PI,num=nu//2+1)

        if window == 'shepp-logan':

            P = np.sin( w[1:] / (2*cutoff) ) * (2.0*cutoff) / w[1:]

        elif window == 'cosine':

            P = np.cos( w[1:] / (2.0*cutoff) )

        elif window == 'hamming':

            P = 0.53836 + 0.46164 * np.cos( w[1:] / cutoff )

        elif window == 'hann':

            P = ( 1.0 + np.cos( w[1:] / cutoff ) ) / 2.0

        else: # Otherwise, assume Ram-Lak

            P = 1

        filter[1:] *= P

        filter[(np.ceil(cutoff*nu/2) + 1).astype(np.int32):] = 0

        return filter


    def custom_recon_windows( omega, modt, freq, ftype ):
    # omega : spatial frequencies of control points
    # modt  : modulation of control points
    # freq  : spatial frequencies sampled by the discrete Fourier transform
    # ftype : 'sharp'
    #           mt0 = mt1 = mtf2
    #           mt3 >= mt4
    #           mt4 >= 0
    #         'smooth'
    #           mt0 = 1.0
    #           mt1 < 1.0
    #           mt2 = 0.0
    #         'enhancing'
    #           mt0  = 1.0
    #           mt1 >= 1.0
    #           mt2 >= 1.0
    #           mt3  = 1.0
    #           mt4  < mt3
    #           mt5  < mt4

        window = np.zeros( (len(freq),), np.float32 )

        if ftype == 'sharp' or ftype == 'Sharp':

            assert len(omega) == 5
            assert len(modt)  == 5

            assert modt[0] == 1.0 and modt[1] == 1.0 and modt[2] == 1.0
            assert modt[3] >= modt[4]

            # flat part
            window[freq <= omega[2]] = 1.0
            window[freq >  omega[4]] = 0.0

            # curve part - 3rd order
            A = np.array( [ [ omega[2]**3   , omega[2]**2 , omega[2]**1, 1 ],
                            [ omega[3]**3   , omega[3]**2 , omega[3]**1, 1 ],
                            [ omega[4]**3   , omega[4]**2 , omega[4]**1, 1 ],
                            [3 * omega[2]**2, 2 * omega[2], 1          , 0 ]] )
            b = np.array( [[1.0],[modt[3]],[modt[4]],[0.0]] )

            # curve part - 4th order
            # A = np.array( [ [omega[2]**4    , omega[2]**3    , omega[2]**2 , omega[2]**1, 1 ],
            #                 [omega[3]**4    , omega[3]**3    , omega[3]**2 , omega[3]**1, 1 ],
            #                 [omega[4]**4    , omega[4]**3    , omega[4]**2 , omega[4]**1, 1 ],
            #                 [4 * omega[4]**3, 3 * omega[4]**2, 2 * omega[4],           1, 0 ],
            #                 [4 * omega[2]**3, 3 * omega[2]**2, 2 * omega[2],           1, 0 ]] )
            #                 # [omega[3]*6 ,       2    ,           0,       0 ]] )
            # b = np.array( [[1.0],[modt[3]],[modt[4]],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            interp = x[0]*freq**3 + x[1]*freq**2 + x[2]*freq**1 + x[3]
            # interp = x[0]*freq**4 +x[1]*freq**3 + x[2]*freq**2 + x[3]*freq**1 + x[4]

            idxs = np.logical_and(freq > omega[2],freq <= omega[4])
            window[idxs] = interp[idxs]

            window[window < 0  ] = 0.0
            window[window > 1.0] = 1.0

        elif ftype == 'smooth' or ftype == 'Smooth':

            assert len(omega) == 3
            assert len(modt)  == 3

            assert modt[0] == 1.0
            assert modt[1] < modt[0] and modt[1] >= 0.0
            assert modt[2] == 0.0

            # curve part - 4th order
            A = np.array( [ [omega[0]**4    , omega[0]**3    , omega[0]**2 , omega[0]**1, 1 ],
                            [omega[1]**4    , omega[1]**3    , omega[1]**2 , omega[1]**1, 1 ],
                            [omega[2]**4    , omega[2]**3    , omega[2]**2 , omega[2]**1, 1 ],
                            [4 * omega[0]**3, 3 * omega[0]**2, 2 * omega[0],           1, 0 ],
                            [4 * omega[2]**3, 3 * omega[2]**2, 2 * omega[2],           1, 0 ]] )
                            # [omega[3]*6 ,       2    ,           0,       0 ]] )
            b = np.array( [[1.0],[modt[1]],[0.0],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            window = x[0]*freq**4 +x[1]*freq**3 + x[2]*freq**2 + x[3]*freq**1 + x[4]

            # Clamp values outside of the valid range of the polynomial fit
            window[ freq > omega[2] ] = 0.0

            window[window < 0  ] = 0.0
            window[window > 1.0] = 1.0

        elif ftype == 'enhance' or ftype == 'Enhance':

            assert len(omega) == 6
            assert len(modt)  == 6

            assert modt[0] == 1.0
            assert modt[1] >= 1.0
            assert modt[2] >= 1.0
            assert modt[3] == 1.0
            assert modt[4] < modt[3]
            assert modt[5] < modt[4]

            dofs = 8

            A = np.array( [ [ omega[0]**i for i in range(dofs) ],
                            [ omega[1]**i for i in range(dofs) ],
                            [ omega[2]**i for i in range(dofs) ],
                            [ omega[3]**i for i in range(dofs) ],
                            [ omega[4]**i for i in range(dofs) ],
                            [ omega[5]**i for i in range(dofs) ],
                            [0] + [ i * omega[0]**(i-1) for i in range(1,dofs) ],
                            [0] + [ i * omega[5]**(i-1) for i in range(1,dofs) ] ] )

            b = np.array( [[1.0],[modt[1]],[modt[2]],[1.0],[modt[4]],[modt[5]],[0.0],[0.0]] )

            x = np.linalg.pinv(A) @ b

            window = [ x[i]*freq**i for i in range(dofs) ]
            window = np.sum( np.stack(window,axis=0), axis=0)

            # Clamp values outside of the valid range of the polynomial fit
            window[ freq > omega[5] ] = modt[5]

            # idxs = np.logical_and(freq >= omega[0], freq <= omega[1])
            # window[idxs] = 1.0
            # idxs = np.logical_and(freq >= omega[0], freq <= omega[3])
            # window[idxs] = np.maximum(window[idxs],1.0)
            window[window < 0] = 0.0

        return window


    def custom_recon_windows_v2( params, modt, freq, ftype ):
    # v2: Use exponential functions to avoid ugly polynomials.
    # params: Gaussian parameters
    # modt  : Gaussian magnitudes
    # freq  : spatial frequencies sampled by the discrete Fourier transform
    # ftype : 'sharp', 'smooth', 'enhancing'

        window = np.zeros( (len(freq),), np.float32 )

        if ftype == 'sharp' or ftype == 'Sharp':

            window[ freq <= params[0] ] = 1.0

            sigma = 2 * params[1] / ( 2.0 * np.sqrt( 2.0 * np.log( 2 ) ) )

            window = np.maximum( window, np.exp( -( freq - params[0] )**2 / ( 2 * sigma**2 ) ) )

        elif ftype == 'smooth' or ftype == 'Smooth':

            sigma = 2 * params[0] / ( 2.0 * np.sqrt( 2.0 * np.log( 2 ) ) )

            window = np.exp( -freq**2 / ( 2 * sigma**2 ) )

        elif ftype == 'enhancing' or ftype == 'Enhancing':

            sigma1 = 2 * params[0] / ( 2.0 * np.sqrt( 2.0 * np.log( 2 ) ) )
            sigma2 = 2 * params[1] / ( 2.0 * np.sqrt( 2.0 * np.log( 2 ) ) )

            G1 =  modt[0] * np.exp( -( freq - params[2] )**2 / ( 2 * sigma1**2 ) )
            G2 =            np.exp( -( freq - params[3] )**2 / ( 2 * sigma2**2 ) )

            window = G1 - G2 + 1

            # Find where the second derivative is zero and clamp all later values.
            cond1  = window[1:-1] - window[:-2]  <  0
            cond2  = window[2:]   - window[1:-1] >= 0
            idx    = np.where( np.logical_and( cond1, cond2 ) )[0]
            if len(idx) > 0:
                idx = idx[0]
                window[idx:] = window[idx]

        return window


    def apply_window_v2( filter, window, f50, fmax0, fmax, nu, feval ):
    # https://en.wikipedia.org/wiki/Window_function

        w = np.linspace(0,2*PI,nu+1)

        if window == 'shepp-logan':

            def cfun( c50 ):

                return np.abs( np.sin( w[f50] / (2*c50) ) * (2.0*c50) / w[f50] - 0.5 )

            c50 = fminbound(cfun, 0.0, fmax)

            P = np.sin( w[1:] / (2.0*c50) ) * (2.0*c50) / w[1:]

        elif window == 'cosine':

            c50 = w[f50] / ( 2.0 * np.arccos( 0.5 ) )

            P = np.cos( np.minimum( w[1:] / (2.0*c50), PI / 2.0 ) )

        elif window == 'hamming':

            c50 = w[f50] / np.arccos( ( 0.5 - 0.53836 ) / 0.46164 )

            P = 0.53836 + 0.46164 * np.cos( w[1:] / c50 )

        elif window == 'hann':

            c50 = w[f50] / np.arccos( 0.0 )

            P = ( 1.0 + np.cos( w[1:] / c50 ) ) / 2.0

        elif window == 'ram-lak':

            P = w[1:] * 0.0 + 1.0

        elif window == 'smooth':

            fc = feval[f50[0]]

            modt   = [ ]
            params = [ fc ]

            P = custom_recon_windows_v2( params, modt, feval, ftype='smooth' )

        elif window == 'sharp':

            fc = feval[f50[0]]

            # params = [ fc - 0.15, 0.15 ]
            params = [ fc - 0.20, 0.27 ]
            modt   = []

            P = custom_recon_windows_v2( params, modt, feval, ftype='sharp' )

        elif window == 'enhancing':

            fc = feval[f50[0]]

            params = [0.1,0.2,fmax0*0.25,fc+0.2]
            modt   = [0.4]

            P = custom_recon_windows_v2( params, modt, feval, ftype='enhancing' )

        else:

            print('FBP apodization window ' + window + ' not recognized.')

            assert window == 'ram-lak'

        P = P[1:(nu//2 + 1)]

        # Find where the second derivative is zero and clamp all later values.
        cond1  = P[1:-1] - P[:-2]  <  0
        cond2  = P[2:]   - P[1:-1] >= 0
        idx     = np.where( np.logical_and( cond1, cond2 ) )[0]
        if len(idx) > 0:
            idx = idx[0]
            P[idx:] = P[idx]

        # idx = np.where( P[1:] - P[:-1] <= 0.0 )[0][0]
        # idx = np.where( P[:-1] - P[1:] <= 0.0 )[0][-1]
        # P[idx:] = P[idx]

        P[P < 0.0] = 0.0

        filter[1:] = filter[1:] * P

        return filter



''' 
Geometry Calibration
'''

if True:

    # Nelder-Mead optimization alogirthm.
    # Lagarias, J. C., Reeds, J. A., Wright, M. H., & Wright, P. E. (1998).
    # Convergence properties of the Nelder--Mead simplex method in low dimensions.
    # SIAM Journal on optimization, 9(1), 112-147.
    def Nelder_Mead( params0, offsets0, eval_fun, maxiter=50, fun_tol=1e-4, verbose=True ):
    # --Inputs--
    # params0 : initial values for n parameters
    # offsets0: offset from each of n parameters implying the expected viable deviation from params0 to opt_params
    # eval_fun: function handle which returns the cost value for a given set of n parameters
    # maxiter : maximum number of Nelder-Mead iterations allowed
    # fun_tol : exit early if the best and worst function values are within this tolerance

    # --Outputs--
    # opt_params: parameters which yield the lowest function value found
    # opt_fval  : lowest function value found
    #
    #     opt_params = 0
    #     opt_fval = 0
    #
    #     return opt_params, opt_fval

    # optimization parameters

        rho   = 1
        chi   = 2
        gamma = 0.5
        sigma = 0.5

    # Initial simplex

        n = params0.size

        # n + 1 vertices, n params
        offsets = np.diag( offsets0 )
        offsets = np.concatenate( [ offsets[:1,:]*0, offsets ], axis=0 )
        offsets = offsets.astype(np.float64)

        # n + 1 vertices, n params
        params = np.ones( ( n+1, 1 ), np.float64 ) * np.reshape( params0, (1,n) )

        params = params + offsets

    # n + 1 function values

        fvals = np.zeros( (n+1,), np.float64 )

    # Initialization: compute the function values for the initial simplex,
    #                 params0 and params0 + one offset at a time

        if verbose: print('\nInitialization...')
        for i in range(0,n+1):

            if verbose: print('Evaluating parameters: ' + str( params[i,:] ))

            fvals[i] = eval_fun( params[i,:] )

            if verbose: print('Function value: ' + str( fvals[i] ))

    # Optimization

        for k in range( 0, maxiter ):

            if verbose: print('\nIteration ' + str(k) + '...')

        # 1. Order: sort parameter vertices by function values

            # NOTE: Sorting after each iteration may result in tie-break inconsistencies vs. those described in the referenced paper.

            # sort by increasing function values
            idx = np.argsort( fvals )

            params = params[idx,:]
            fvals  = fvals[idx]

        # Check for convergence

            if np.abs( fvals[n] - fvals[0] ) < fun_tol:

                if verbose: print('\nConverged on iteration ' + str(k) + ' to within tolerance ' + str(fun_tol) + '.\n')

                opt_params = params[0,:]
                opt_fval   = fvals[0]

                return opt_params, opt_fval

        # 2. Reflect

            x_ = np.mean( params[:n,:], axis=0 )

            x_r = (1.0 + rho) * x_ - rho * params[n,:]

            if verbose: print('Evaluating parameters: ' + str(x_r))

            f_r = eval_fun( x_r )

            if verbose: print('Function value: ' + str(f_r) + ' (best: ' + str(fvals[0]) + ')')

            if ( f_r < fvals[n-1] ) and ( f_r >= fvals[0] ):

                if verbose: print('Reflect step for iteration ' + str(k) + '.')

                fvals[n]    = f_r
                params[n,:] = x_r

                continue

        # 3. Expand

            if f_r < fvals[0]:

                x_e = ( 1.0 + rho * chi ) * x_ - rho * chi * params[n,:]

                if verbose: print('Evaluating parameters: ' + str(x_e))

                f_e = eval_fun( x_e )

                if verbose: print('Function value: ' + str(f_e) + ' (best: ' + str(fvals[0]) + ')')

                if ( f_e < f_r ):

                    f_out = f_e
                    x_out = x_e

                else:

                    f_out = f_r
                    x_out = x_r

                if verbose: print('Expand step for iteration ' + str(k) + '.')

                fvals[n]    = f_out;
                params[n,:] = x_out;

                continue

        # 4. Contract

            if f_r >= fvals[n-1]:

            # 4.a Outside contraction

                if (f_r < fvals[n]):

                    x_c = (1.0 + rho * gamma) * x_ - rho * gamma * params[n,:]

                    if verbose: print('Evaluating parameters: ' + str(x_c))

                    f_c = eval_fun( x_c )

                    if verbose: print('Function value: ' + str(f_c) + ' (best: ' + str(fvals[0]) + ')')

                    if f_c <= f_r:

                        if verbose: print('Contract outside step for iteration ' + str(k) + '.');

                        fvals[n]    = f_c
                        params[n,:] = x_c

                        continue


            # 4.b Inside contraction

                if ( f_r >= fvals[n] ):

                    x_cc = (1.0 - gamma) * x_ + gamma * params[n,:]

                    if verbose: print('Evaluating parameters: ' + str(x_cc))

                    f_cc = eval_fun( x_cc )

                    if verbose: print('Function value: ' + str(f_cc) + ' (best: ' + str(fvals[0]) + ')')

                    if f_cc < fvals[n]:

                        if verbose: print('Contract inside step for iteration ' + str(k) + '.')

                        fvals[n]    = f_cc
                        params[n,:] = x_cc

                        continue


        # 5. Shrink

            if verbose: print('\nPerforming a shrink step for iteration ' + str(k) + '.')

            for i in range(1,n+1):

                v_i = params[0,:] + sigma * ( params[i,:] - params[0,:] )

                if verbose: print('Evaluating parameters: ' + str(v_i))

                f_i = eval_fun( v_i )

                if verbose: print('Function value: ' + str(f_i) + ' (best: ' + str(fvals[0]) + ')')

                fvals[i]    = f_i;
                params[i,:] = v_i;

        # If we hit the maximum number of iterations, return the parameters with the lowest function value.

        if verbose: print('\nMaximum number of iterations reached (' + str(maxiter) + ' iterations).')

        # sort by increasing function values
        idx = np.argsort( fvals )

        params = params[idx,:]
        fvals  = fvals[idx]

        opt_params = params[0,:]
        opt_fval   = fvals[0]

        return opt_params, opt_fval


    # Manage the inputs and outputs into the MI() function in the reconstruction library.
    # Call the MI() function using ctypes.
    class MutualInformation():

        def __init__(self, recon_lib_path, nbins1=512, nbins2=512):

            self._minval = None
            self._maxval = None

            self._imlen  = np.zeros( (1,), dtype=np.uint64 ) # np.ndarray.size, total number of array elements

            self._nbins1    = np.zeros( (1,), dtype=np.uint64 )
            self._nbins1[0] = nbins1

            self._nbins2    = np.zeros( (1,), dtype=np.uint64 )
            self._nbins2[0] = nbins2

        # Set up the compiled function call through ctypes

            recon_lib = ctypes.CDLL(recon_lib_path)
            self._MI  = recon_lib.MI

            # MI(float *imgA, float *imgB, uint64_t img_len, uint64_t bin_x, uint64_t bin_y)
            self._MI.argtypes = [ np.ctypeslib.ndpointer(dtype=np.float32), np.ctypeslib.ndpointer(dtype=np.float32),
                                  ctypes.c_uint64, ctypes.c_uint64, ctypes.c_uint64 ]

            self._MI.restype = ctypes.c_float


        def __call__(self, X1, X2):

            # Check the implicit assumptions
            assert type(X1) == np.ndarray
            assert type(X2) == np.ndarray
            assert X1.dtype == np.float32
            assert X2.dtype == np.float32
            assert X1.size  == X2.size

            self._imlen[0] = X1.size

            # Set the bounds on the histogram when the first input is provided.
            if self._minval is None: self.set_histogram_limits(X1,X2)

            # The histogram function expects input values between 0 and 1, inclusive.

            X1 = self._normalize(X1)
            X2 = self._normalize(X2)

            MI_val = self._MI( X1, X2, self._imlen[0], self._nbins1[0], self._nbins2[0] )

            return MI_val


        def set_histogram_limits(self, X1, X2):
        # Sets the bounds on computed histograms based on sample inputs.

            limit = np.max( np.abs(X1.flatten() - X2.flatten()) )

            assert limit > 0.0

            self._minval = -1.1 * limit
            self._maxval =  1.1 * limit


        def _normalize(self, X):

            X = ( X - self._minval )/( self._maxval - self._minval )

            # This has been folded into the compiled code for efficiency.
            # X[X < 0.0] = 0.0
            # X[X > 1.0] = 1.0

            return X


''' 
Material decomposition
'''

if True:

    def mat_decomp_LS( X, M ):
    # Approach: Compute the decomposition for all subsets of materials and choose the answer with all non-negative values
    #           and the smallest L2 error norm.
    # Inputs:
    #   X: (e,t,z,y,x)
    #   M: (m,e) material sensitivity matrix
    #
    # Outputs:
    #   C: (m,t,z,y,x)

        m = M.shape[0]

        sh_X = X.shape
        sh_C = (m,) + sh_X[1:]
        X = np.reshape(X,[-1,X.size//sh_X[0]])

        C = np.linalg.pinv( M.T ) @ X

        return np.reshape(C,sh_C)


    def mat_decomp_LS_non_neg( X, M ):
    # Approach: Compute the decomposition for all subsets of materials and choose the answer with all non-negative values
    #           and the smallest L2 error norm.
    # Inputs:
    #   X: (e,t,z,y,x)
    #   M: (m,e) material sensitivity matrix
    #
    # Outputs:
    #   C: (m,t,z,y,x)

        # https://stackoverflow.com/questions/1482308/how-to-get-all-subsets-of-a-set-powerset
        # Powerset: all unique subsets of the set s
        def powerset(s):
            x = len(s)
            masks = [1 << i for i in range(x)]
            for i in range(1 << x):
                yield [ss for mask, ss in zip(masks, s) if i & mask]

        assert X.shape[0] == M.shape[1]

        sh_X = X.shape
        m    = M.shape[0]
        e    = X.shape[0]
        n    = X.size//sh_X[0]
        sh_C = (m,) + sh_X[1:]
        X    = np.reshape(X,(e,n))

        C_out    = np.zeros( (m,n), np.float32 )
        C_minres = np.nan_to_num( np.zeros( (n,), dtype=np.float32 ) + math.inf )

        sets = list( powerset( np.arange(0,m).tolist() ) )

        # Enforce non-negativity.
        for i in range( len(sets) ):

            if len(sets[i]) > 0:

                # m_ = len(sets[i])
                M_ = M[ sets[i], : ]

                # C_: (m_,e) @ (e,n) => (m_,n)
                C_ = np.linalg.pinv( M_.T ) @ X

                # res: (e,m_) @ (m_,n) - (e,n) => (e,n) => (n,)
                res = np.sum( (M_.T @ C_ - X)**2, axis=0 )

                # (n,) < (n,), (n,) > 0
                condition = np.logical_and( res < C_minres, np.amin( C_, axis=0 ) > 0.0 )

                # (m_,n) = (m_,n), (m_,n)
                C_out[ sets[i], : ] = np.where( condition, C_, C_out[ sets[i], : ] )

                # (n,) = (n,), (n,)
                C_minres = np.where( condition, res, C_minres )

        return np.reshape(C_out,sh_C)


''' 
Other Support Functions
'''

if True:

    class Resamp_Kernel_Approx():
    # Compute the mean-squared-error between a 3D convolution kernel, A,
    # and its separable approximation.

        def __init__(self, A, E ):

            # Kernel to fit
            # (2*w+1)**3,
            self.A = A

            # Kernel fitting basis functions
            # (2*w+1)**3,3
            self.E = E

        def __call__(self, coeffs):

            # coeffs: 3,

            # sum(3, x (2*w+1)**3,3) => (2*w+1)**3,
            profile = np.sum(coeffs * self.E,1) / np.sum(coeffs)

            MSE = np.mean( (self.A - profile)**2 )

            return MSE


    # Displays a reference image (e.g. an input image, FBP reconstruction)
    # followed by pairs of output images (e.g. denoised, iterative reconstruction)
    # and their absolute residuals relative to the reference image.
    def imc( img_list, shape, gamma=1.0, show=True, outfile=None, title=False, skip=0, stddevs=[3,3], fig_num=0, abs_res=True ):

        # We can only display 2D images
        assert len(shape) == 2

        all_images = []
        for c in range(len(img_list)):
            # if c == 0:
            if c <= skip:
                all_images.append( np.reshape( img_list[c], shape ) )
            else:
                all_images.append( np.reshape( img_list[c], shape ) )

                if abs_res:

                    all_images.append( np.reshape( np.abs(img_list[0]-img_list[c]), shape ) )

                else:

                    all_images.append( np.reshape( img_list[0]-img_list[c], shape ) )

        # if img_list[0].shape[0] > img_list[0].shape[1]:
        if img_list[0].shape[-1] > img_list[0].shape[-2]:

            all_images = np.concatenate( all_images, axis=-2 )

        else:

            all_images = np.concatenate( all_images, axis=-1 )

        if all_images.shape[0] > all_images.shape[1]:

            all_images = all_images.T

        all_images = all_images**gamma

        if show:

            mval = np.mean( all_images.flatten() )
            std  = np.std( all_images.flatten() )

            plt.figure(num=fig_num)
            plt.imshow( all_images, cmap='gray', vmin=mval-stddevs[0]*std, vmax=mval+stddevs[1]*std, aspect='equal' )
            if title is not None: plt.title( title )
            plt.show()

        if outfile is not None:

            if not show:
                mval = np.mean( all_images.flatten() )
                std  = np.std( all_images.flatten() )

            plt.imsave(outfile, all_images, cmap='gray', vmin=mval-stddevs[0]*std, vmax=mval+stddevs[1]*std )

        return all_images


    def AtB(A, B, chunk=512**3):
    # compute A'*B with broadcasting over the first N-2 dimensions
    # accuracy of single(double(A)'*double(B)) at a fraction of the memory usage, but with longer execution time

        # A: ...a,b
        # B: ...a,c
        # M: ...b,c

        inshape = A.shape[-2]
        outd1   = A.shape[-1]
        outd2   = B.shape[-1]

        # Assume A and B match for all broadcast dimensions
        shape0 = A.shape
        out_shape = shape0[:-2] + (outd1,outd2)
        v = np.zeros(shape0[:-2],np.float64)
        M = np.zeros(out_shape,np.float32)

        for i in range(outd1):
            for j in range(outd2):
                v *= 0.0
                for c in range(0,inshape,chunk):
                    v += np.sum( A[...,c:min(c+chunk,inshape),i].astype('float64') * B[...,c:min(c+chunk,inshape),j].astype('float64'), axis=-1 )
                M[...,i,j] = v

        M = M.astype('float32')

        return M


    def ABt(A, B, chunk=512**3):
    # compute A*B' with broadcasting over the first N-2 dimensions
    # accuracy of single(double(A)'*double(B)) at a fraction of the memory usage, but with longer execution time

        # A: ...a,b
        # B: ...c,b
        # M: ...a,c

        inshape = A.shape[-1]
        outd1   = A.shape[-2]
        outd2   = B.shape[-2]

        # Assume A and B match for all broadcast dimensions
        shape0 = A.shape
        out_shape = shape0[:-2] + (outd1,outd2)
        v = np.zeros(shape0[:-2],np.float64)
        M = np.zeros(out_shape,np.float32)

        for i in range(outd1):
            for j in range(outd2):
                v *= 0.0
                for c in range(0,inshape,chunk):
                    v += np.sum( A[...,i,c:min(c+chunk,inshape)].astype('float64') * B[...,j,c:min(c+chunk,inshape)].astype('float64'), axis=-1 )
                M[...,i,j] = v

        M = M.astype('float32')

        return M


    # Faster and more accurate SVD for long/tall and skinny matrices when using single precision to conserve memory.
    # Otherwise, single precision SVDs of large matrices can introduce significant numerical errors.
    # Supports broadcasting along the first ndim-2 dimensions.
    # Update: It looks like np.linalg.svd converts data to double precision when called with single precision data.
    #         The results are similarly accurate, at much greater memory usage using np.linalg.svd.
    def eig_SVD( X, memory_efficient=True, eps=1e-6 ):
    # X: matrix to compute SVD from (likely single precision)
    # memory_efficient: if True, transiently convert vectors to double precision during inner product calculations
    #                   Reduces memory usage at the expense of additional computation time.
    #                   if False, perform double precision calculations directly
    # eps: eliminate singular vectors, values below this tolerance value

        # We need to multiply out the larger dimension.
        if X.shape[-2] < X.shape[-1]: # X @ X'
            case = 1
            order = '...ab,...cb->...ac'
        else: # X' @ X
            case = 2
            order = '...ab,...ac->...bc'

        if not memory_efficient:

            X = X.astype('float64')

            eig_vals, eig_vecs = np.linalg.eig( np.einsum( order, X, X ) )

        else:

            if case == 1:

                eig_vals, eig_vecs = np.linalg.eig( ABt( X, X ) )

            else:

                eig_vals, eig_vecs = np.linalg.eig( AtB( X, X ) )

        # Make sure the eigenvalues are never negative due to precision errors
        eig_vals = np.maximum( eig_vals, 0 )

        eig_vals = np.sqrt(eig_vals)

        # We need to sort the eigenvalues from largest to smallest to match the SVD
        idx = np.argsort( eig_vals, axis=-1 )
        idx = idx[...,::-1]

        eig_vals = np.take_along_axis(eig_vals, idx, axis=-1)
        eig_vecs = np.take_along_axis(eig_vecs, idx[...,None,:], axis=-1)

        # Issue a warning if a rank-defficient matrix is encountered.
        if np.min( eig_vals ) < eps:

            print('Rank defficient matrix encountered (singular values < ' + str(eps) + '). SVD may be inaccurate.')


        if case == 1: # X * X'

            U  = eig_vecs
            E  = eig_vals
            Vt = ( np.swapaxes( U, -2, -1 ) @ X ) / E[...,:,None]

        else: # X' * X

            Vt = eig_vecs
            E  = eig_vals
            U  = ( X @ np.swapaxes( Vt, -2, -1 ) ) / E[...,None,:]

        return U, E, Vt


    # Candès, E. J., Li, X., Ma, Y., & Wright, J. (2011).
    # Robust principal component analysis?.
    # Journal of the ACM (JACM), 58(3), 11.
    # Supports broadcasting along the first ndim-2 dimensions; however, for convergence, the error norm will
    # be averaged over all broadcast dimensions.
    # When limit_rank is not None, limits the rank of XL to be at most the specified integer value.
    def RPCA( X, nrows, ncols, iters, tol=1e-5, limit_rank=None, use_double=False ): # , subsample_dim=None, subsample_factor=None ):

        sh_in = X.shape

        X = np.copy(X)
        X = np.reshape(X,[-1,nrows,ncols])

        if use_double:
            type = X.dtype
            X    = X.astype('float64')

        XL = X
        XS = X * 0
        f  = X * 0

        N       = np.maximum(nrows,ncols)
        # mu      = (nrows*ncols) / ( 4.0 * np.linalg.norm( X.flatten(), ord=1 ) )
        mu      = (nrows*ncols) / ( 4.0 * np.sum( np.abs(X), axis=(-2,-1), keepdims=True ) )
        lamb    = 1.0 / np.sqrt( N )
        threshL = 1.0 / mu
        threshS = lamb / mu

        norm_X = np.linalg.norm( X.flatten() )

        fid = tol + 1
        i = 0

        while i < 12 or ( i < iters and fid > tol ):

            i += 1

        # Update XL

            XL = X - XS + f / mu

            if limit_rank is not None:

                # It looks feasible to do an SVD on fewer projections and / or on the GPU; however,
                # it doesn't seem to provide a speed benefit without an svds algorithm implemented
                # on the GPU.

                # if subsample_dim is not None:
                #
                #     assert subsample_dim == 1 or subsample_dim == 2
                #     assert subsample_factor is not None
                #
                #     if subsample_dim == 1:
                #
                #         # u,s,vh = zip(*[svds( XL[i,0::subsample_factor,:], k=limit_rank ) for i in range(XL.shape[0])])
                #         # u,s,vh = np.stack(u,0), np.stack(s,0), np.stack(vh,0)
                #
                #         s, u, vh = tf.linalg.svd( XL[:,0::subsample_factor,:], full_matrices=False, compute_uv=True )
                #         s  = s.numpy()[:,:limit_rank]
                #         # vh = np.transpose( vh.numpy(), [0, 1, 2] )[:,:limit_rank,:]
                #         vh = vh.numpy()[:,:,:limit_rank]
                #         u  = u.numpy()[:,:,:limit_rank]
                #
                #         s = subsample_factor * s
                #         u = ( XL @ vh ) / s[:,np.newaxis,:]
                #         vh = np.transpose( vh, [0,2,1] )
                #
                #     else:
                #
                #         # u,s,vh = zip(*[svds( XL[i,:,0::subsample_factor], k=limit_rank ) for i in range(XL.shape[0])])
                #         # u,s,vh = np.stack(u,0), np.stack(s,0), np.stack(vh,0)
                #
                #         s, u, vh = tf.linalg.svd( XL[:,:,0::subsample_factor], full_matrices=False, compute_uv=True )
                #         s  = s.numpy()[:,:limit_rank]
                #         # vh = np.transpose( vh.numpy(), [0, 1, 2] )
                #         u  = u.numpy()[:,:,:limit_rank]
                #
                #         s  = subsample_factor * s
                #         vh = ( np.transpose( u, [0,2,1] ) @ XL ) / s[:,:,np.newaxis]
                #
                # else:

                # Scipy's svds() doesn't broadcast...
                u,s,vh = zip(*[svds( XL[i,...], k=limit_rank ) for i in range(XL.shape[0])])
                u,s,vh = np.stack(u,0), np.stack(s,0), np.stack(vh,0)

            else:

                u, s, vh = np.linalg.svd(XL, full_matrices=False, compute_uv=True)

            s = s[...,np.newaxis,:]
            s = np.maximum( s - threshL, 0.0 )

            XL = ( u * s ) @ vh

            del u, s, vh

        # Update XS

            XS = X - XL + f / mu

            XS = np.sign( XS ) * np.maximum( np.abs(XS) - threshS, 0.0 )

        # Residuals

            f = f + mu * ( X - XL - XS )

            fid = np.linalg.norm( ( X - XL - XS ).flatten() ) / norm_X
            print('iter ' + str( i ) + ', fidelity error (%): ' + str(100 * fid) )

            # Try to address an apparent memory leak...
            gc.collect()


        XL = np.reshape(XL,sh_in)
        XS = np.reshape(XS,sh_in)

        if use_double:
            XL = XL.astype(type)
            XS = XS.astype(type)

        return XL, XS
