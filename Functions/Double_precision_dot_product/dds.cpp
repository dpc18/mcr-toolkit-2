// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "mex.h"
#include <math.h>

// find maximum of two numbers
#ifndef max
#define max( a, b ) ( ((a) > (b)) ? (a) : (b) )
#endif

// find minimum of two numbers
#ifndef min
#define min( a, b ) ( ((a) < (b)) ? (a) : (b) )
#endif

// ***     ***
// *** MEX ***
// ***     ***

// Improve the precision of a single-precision dot product calculation
// by multiplying and summing elements in double precision.
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    
    if (nrhs != 2)
        mexErrMsgTxt("Two input arguments required! (c = dds(a,b)");
    
    // if (nlhs != 1)
    //    mexErrMsgTxt("One output argument required! (c = dds(a,b)");
    
    if( !mxIsSingle(prhs[0]) || !mxIsSingle(prhs[1]) )
        mexErrMsgTxt("Arguments must be single precision");
    
    float* a = (float*) mxGetData(prhs[0]);
    float* b = (float*) mxGetData(prhs[1]);
    const size_t *a_size = mxGetDimensions(prhs[0]);
    const size_t *b_size = mxGetDimensions(prhs[1]);
    
    if ((a_size[0] != b_size[0]) || (a_size[1] != b_size[1])) {
     
        mexErrMsgTxt("Dimensions of vectors a and b must match.");
        
    }
    
    double summation = 0;
    
    for (size_t i = 0; i < max(a_size[0],a_size[1]); i++) {
     
        summation += ((double) a[i])*((double) b[i]);
        
    }
    
    const mwSize siz[2] = {1,1};
    plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
    float* c = (float*) mxGetData(plhs[0]);
    
    c[0] = (float) summation;
    
}