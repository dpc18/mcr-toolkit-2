
# Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References

# Long, Y., Fessler, J. A., & Balter, J. M. (2010).
# 3D forward and back-projection for X-ray CT using separable footprints.
# IEEE transactions on medical imaging, 29(11), 1839-1850.
#
# Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
# Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
# Physics in Medicine & Biology, 49(11), 2209.
#
# Van der Vorst, H. A. (1992).
# Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
# SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
#
# Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
# Cone-beam reprojection using projection-matrices.
# IEEE transactions on medical imaging, 22(10), 1202-1214.
#
# Sleijpen, G. L., & Van Gijzen, M. B. (2010).
# Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
# SIAM journal on scientific computing, 32(5), 2687-2709.
# Algorithm 3.1

##########
# BUG: Fix a potential bug related to the multi-channel weight vectors (e.g. Wt).
#      When the weight vectors are computed in Python, they must have dimensions (nt,np_).
#      A memory order transformation may be required for the weights imported from the *.mat file
#      in this script...
#      At the very least, the dimensions must be flipped to (nt,np_) after the file is loaded in.
##########


if True:
    
    import os

    # MCR Toolkit
    import sys
    toolkit_path  = '/home/dpc/Documents/MCR_Toolkit_2/'
    sys.path.append(toolkit_path + 'Python/')
    from Recon_Operators import *
    recon_lib_path = os.path.join(toolkit_path, 'Operators/DD_operators_v25.so')
    proj_weights = os.path.join(toolkit_path, 'Test_Data/Wt.mat')

    from Recon_Utilities import *

    from Python.Denoising import RSKR

    # External Python modules (Python v3.10)
    import numpy as np    # (v1.21.5)
    import nibabel as nib # (v5.1.0)
    import matplotlib.pyplot as plt # (v3.5.1)
    import time

    from scipy.io import loadmat

    # GPU(s) to use for reconstruction
    gpu_list = np.array( [0], np.int32 ) # array of GPU indices to use with the reconstruction operators, starting from 0

    print('Starting reconstruction script using ' + str(gpu_list.size) + ' GPU(s).' )


## 1) In vivo mouse data, ApoE KO model
# % METR iterative reconstruction example
# % Energy thresholds: 25, 34, 40, 55 keV
# % Source spectrum: 80 kVp tungsten
if True:

    # Results path
    outpath = '/data/Python_Recon/Results/'
    debug_outpath = '/data/Python_Recon/Debug_Results/'

    # Data set path
    # Reference CT data sets can be found here: https://doi.org/10.7924/r45m6cs1f.
    dpath_base = '/media/justify/Paper_Data/Med_Phys_2022_Recon_Toolkit_Release/MCR_Toolkit_Datasets/' # sample path
    dpath = os.path.join(dpath_base,'Mouse_Cardiac_ApoE_KO_180417_5')

    # Make our output directory, if it does not already exist
    if not os.path.isdir(outpath): os.mkdir(outpath)

    # Unique name for results
    recon_name = '180416-5_v2'

    # Raw projections, 4 energy thresholds
    data = os.path.join(dpath,'Y.nii.gz')

    # Air raws, 4 energy thresholds
    air = os.path.join(dpath,'I0.nii.gz')

    # Mask of bad, missing pixels to interpolate on all projections
    gap_map = os.path.join(dpath,'gap_map.nii.gz')

    # Physiological gating signals
    gating = os.path.join(dpath,'18_04_17_100516.csv')

    # Resave projection data after log normalization, gap interpolation, and ring artifact correction
    # File size: ~20 GB
    projection_data = os.path.join(outpath,'Yc.nii')

    # numpy archive of geometry parameters
    # either pre-calibrated or saved after geometry calibration
    geo_params = os.path.join(outpath,'geo_params.npy')

    # Save a copy of the reconstruction operator parameters for geometry calibration, easy reuse.
    ref_RC = os.path.join(outpath,'recon_params.npy')


## 2) Preprocess projections - air normalization, log transform, gain correction, gap interpolation, ring correction

    if not os.path.exists( projection_data ):

        print('Projection pre-processing...')

    # Projection pre-processing: air normalization, log transform, exposure correction

        # Projections to exposure correct
        # simple rescaling to reduce underexposure artifacts present in the first few projections of some acquisitions
        fix_proj = np.arange(0,25)

        enforce_non_negativity = False # False: don't enforce non-negativity; True: negative line integrals are set to zero

        Y = transform_projections( data=data, air=air, gap_map=gap_map, fix_proj=fix_proj, enforce_non_negativity=enforce_non_negativity, per_projection=True )

        save_nii( Y, os.path.join(outpath,'Y_log.nii') )

    # Spectral ring artifact correction

        # Rationale: Defects in PCDs can yield gain artifacts in reconstructed images (ring artifacts).
        #            These defects can vary slowly over time and tend to be correlated across energy bins.
        #            Apply the SVD accross PCD projection energies to eliminate redundancy and to yield zero
        #            mean in lower order components.
        #            Use RPCA to factor slowly varying gain (ring) artifacts from more rapidly varying sinogram data.

        # Approach:
        # (1) SVD decomposition of projections accross the energy dimension
        # (2) Remove rings from each component using RPCA
        #     (the first component has low spatial frequencies making it harder to deal
        #      with; the lower order components may be dominated by noise,
        #      depending on the exposure per projection, making RPCA ill-conditioned)
        # (3) Reverse the SVD decomposition of projections accross the energy dimension

        # RPCA factorization is more effective with helical projection data than with circular projections.
        # Works best on data where negative values have not been truncated to zero.
        # Computation time depends on how fast RPCA converges

        # sinogram_rank     = 1;          # Number of degrees of freedom for describing gain variations over the projection acquisition time
        # summarize_results = True;       # (True) display a quality assurance image after ring reduction

        # Y                : (ne,np_,nv,nu) projection data on which to perform ring correction
        # sinogram_rank    : maximum rank of itensity oscillations due to ring artifacts along the angle dimension (e.g. 3)
        # summarize_results: when True, show before and after images and residuals following correction
        # limit_rank       : maximum number of spectral components to correct (<= number of energies; 'None' automatically sets limit_rank = ne)
        # tol              : tolerance to meet for convergence of RPCA (NOTE: RPCA reports % error, so convergence is at tolerance / 100 %; 12 iterations minimum )
        # return_LP        : controls a final low-pass filtration operation performed within each projection to prevent image details from bleeding into the correction
        #                    (None) do not perform this step
        #                    (float) do not correct spatial frequencies below 1 cycle / ( return_LP voxels )
        # Y = spectral_ring_correction( Y, sinogram_rank, summarize_results )

        # Faster alternative which uses the truncated SVD
        # Y                : (ne,np_,nv,nu) projection data on which to perform ring correction
        # sinogram_rank    : maximum rank of itensity oscillations due to ring artifacts along the angle dimension (e.g. 3)
        # summarize_results: when True, show before and after images and residuals following correction
        # start_FWHM       : Gaussian FWHM to use for initial low-pass filtering
        # return_FWHM      : Gaussian FWHM to use to suppress details which bleed into the correction
        # Y = spectral_ring_correction_v2( Y, sinogram_rank=3, summarize_results=True, start_FWHM=19, return_FWHM=35 )
        # Y = spectral_ring_correction_v2( Y, sinogram_rank=3, summarize_results=True, start_FWHM=19, return_FWHM=52 ) # turntable EID system
        Y = spectral_ring_correction_v2( Y, sinogram_rank=1, summarize_results=True, start_FWHM=4, return_FWHM=4 ) # Dectris PCD data
        # Y = spectral_ring_correction_v2( Y, sinogram_rank=5, summarize_results=True, start_FWHM=30, return_FWHM=30 )

        # Enforce non-negativity on deringed line integrals.
        Y[Y < 0] = 0

        save_nii(Y,projection_data)

    else:

        print('Reloading previously processed projections...')

        Y = load_nii(projection_data)


## 3) Specify data acquisition parameters

    sdd = 831.1812 # source-detector distance (mm)
    sod = 679.8042 # source-object distance (mm)

    mag = sdd/sod # geometric magnification

    np_    = 9000                                                 # projection number
    ap     = (360*3 - 10)/np_                                     # angular increment (degrees)
    angles = np.linspace( 0, 360*3-10-ap, np_, dtype=np.float64 ) # projection angles
    rotdir = -1                                                   # Rotation direction (+/- 1)

    # Detector pixel size (mm)
    du = 0.15
    dv = 0.15

    # Detector pixel count
    nu = 515
    nv = 257

    # Reconstructed voxel size (mm)
    dx = du/mag
    dy = du/mag
    dz = du/mag

    # Reconstructed voxel counts
    nx = 360
    ny = 360
    nz = 400 # 360 # 256

    e = 4   # number of energy thresholds
    p = 10  # number of time points to reconstruct

    # preview slice for iterative reconstruction
    slice_num = round(nz/2)

    # Reconstruct data with an angular offset (radians)
    ao = 0;

    # pitch
    rotation            = 3*360-10                           # degrees
    rotations           = rotation / 360
    travel              = 12.5                               # mm
    travel_per_rotation = travel / rotations
    detector            = 40                                 # detector height, mm
    collimation         = detector / mag                     # ~z coverage of the object, mm
    pitch               = travel_per_rotation / collimation

    # fan angle (assuming the source is well centered on the detector)
    fan_angle = 2 * np.arctan2( ( nu * du / 2 ), sdd ) * 180 / np.pi  # degrees

    # z offset
    mm_per_proj = travel/np_
    zo          = mm_per_proj * np.arange(0,np_) # relative z position of the source-object plane per projection
    zo_         = (np_//2)*mm_per_proj           # central plane of the reconstructed volume


# 4) Geometric parameters: previously optimized parameters or an initial guess

    geo_all    = os.path.join( outpath,'geo_all.geo' )

    # If we have calibrated the geometry previously, use the previous settings.
    if os.path.exists( geo_params ):

        print('Reusing a previous geometry calibration...')

        # params = [optim_params zo_ ao] in MATLAB
        params = np.load( geo_params, allow_pickle=True )

    # Optimize the geometry parameters from an initial guess
    else:

        print('Starting with a default geometry calibration...')

        # [sdd sod uc vc eta sig phi]
        params = np.array( [sdd, sod, 270, 125, 0.0, 0.0, 0.0, zo_, ao], np.float64 )

# 5) Reconstruction parameters

    print('Setting reconstruction parameters...')

    # Reconstruction flags
    FBP = 1                    # (1) FBP, (0) simple backprojection
    simple_back_projection = 0
    implicit_mask = 1          # (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
    explicit_mask = 0          # (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask; (2) use volumetric reconstruction mask
    use_affine = 0             # (0) - affine transform is not used (even if one is provided); (1) - affine transform is used

    scale = 1 # Backprojection scaling

    # Explicit mask - image domain reconstruction mask
    # Only used when explicit_mask = 1
    rmask = make_default_rmask( nx, ny, nz ) # reproduces the implicit reconstruction mask (a cylinder) as an explicit mask

    # control the use of divergent rays with cos^2 weighting (no computation benefit)
    # see: Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
    #      Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
    #      Physics in Medicine & Biology, 49(11), 2209.
    short_scan_range    = collimation * pitch * ( 180 + fan_angle ) / 360;
    limit_angular_range = short_scan_range / collimation;

    # (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection
    use_norm_vol        = 0  # implicit normalization during algebraic reconstruction
    use_norm_vol_WFBP   = 1  # explicit normalization during WFBP analytical reconstruction

    # The projection/backprojection operators can incorporate an affine transform.
    # This transform can be used to e.g. register data from different imaging chains
    # without an extra data interpolation step.
    # Dummy parameters used here for no affine, minimal computation
    # NOTE: When the affine causes a volume change (i.e. it is non-rigid), the resultant FBP will not be properly normalized.
    #       Also, inversion of the z axis is not supported.
    Rt_aff = np.array([1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0], np.float32)

    # Detector type
    # (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
    cy_detector =  0
    db          = -1 # fan angle increment (radians) (only used for cylindrical detectors)

    # Parameter vectors

    # Required parameters
    int_params    = np.array( [nu, nv, np_, nx, ny, nz, simple_back_projection, use_affine], dtype=np.int32  )
    int_params_f  = np.array( [nu, nv, np_, nx, ny, nz,                    FBP, use_affine], dtype=np.int32  )
    double_params = np.concatenate( [np.array( [du, dv, dx, dy, dz, zo_, ao], dtype=np.double ), params[:7]], axis=0 )

    # Add optional parameters
    # int_params    = np.concatenate( (   int_params, np.array( [implicit_mask, explicit_mask, cy_detector, use_norm_vol], dtype=np.int32 ) ), axis=0 )
    int_params_f  = np.concatenate( ( int_params_f, np.array( [implicit_mask, explicit_mask, cy_detector, use_norm_vol_WFBP], dtype=np.int32 ) ), axis=0 )
    double_params = np.concatenate( (double_params, np.array( [scale, db, limit_angular_range], dtype=np.double ) ), axis=0 )

    # Analytical reconstruction filter file
    filt_name = os.path.join(outpath,'Ram-Lak_%s.txt')

    # Source-detector geometry file
    geofile   = os.path.join(outpath,'geoall_test.geo')

    # Projection weights
    # Assigning 1.0 to all weights will reconstruct the time average
    # Fixed 9-24-24: Revised to respect the standardized order of projection weights (nt,np_)
    # W = np.ones( (np_,1) , dtype=np.double )
    W = np.ones( (1,np_) , dtype=np.double )

# 6) Set up reconstruction object

    print('Creating initial reconstruction operator...')

    # Create a new reconstruction object
    RC = Recon(recon_lib_path)

    # Set integer parameter vector
    RC.set_int_params( int_params_f, rotdir )

    # Set double parameter vector
    RC.set_double_params( double_params )

    # FBP filter
    # filter_type: 'ram-lak','shepp-logan', 'cosine', 'hamming', 'hann'
    RC.make_and_set_filter( filt_name, filter_type='ram-lak', f50=1.0 )

    # Geo file
    RC.make_and_set_geofile( geofile, angles, zo, W )

    # Affine registration
    RC.set_affine( Rt_aff )

    # Explicit reconstruction mask
    RC.set_rmask( rmask )

    # GPUs to use for recon. operators
    RC.set_GPU_list( gpu_list )

    # Initialization on the GPUs and the host
    RC.DD_init()

    # Save the reconstruction object for geometry calibration and future use
    save_recon_object( RC, ref_RC )
    recon_params = RC.get_recon_params()

# 7) Assess geometry with analytical reconstruction (time averaged)

    # X = np.zeros( (e,nz,ny,nx), np.float32 )
    # # Y = np.reshape( Y, [ e, np_*nv*nu ] )
    #
    # for s in range(e):
    #
    #     X[s,...] = RC.Rtf( Y[s,...], W )
    #
    # save_nii( X, os.path.join(outpath,'X_WFBP.nii') )

# 8) Refine the geometry if needed

    # Steps:
    #   a) Create a geometry file using candidate parameters, params
    #   b) Perform reconstruction with WFBP
    #   c) Reproject the reconstruction
    #   d) Compare the reprojections with the original projections using mutual information (MI)
    #   e) Update the geometry parameters to improve the MI using the Nelder-Mead optimization algorithm
    # NOTE: Absolute references are needed to calibrate the source-detector and source-object distances. They cannot be optimized with this method.

    # opt_params: array([ 2.73673546e+02,  1.29942347e+02, -3.98152458e-02,  3.10013636e-02, -7.41856986e-03])
    # opt_fval: 0.9776454431656599

    # Do geometry calibration if we do not have a precalibrated
    # 'geo_params.npz' file included in the output directory.
    if not os.path.exists( geo_params ):

        print('Performing geometry calibration...')

        # Clear reconstruction allocations, since we need to declare a new one each time we change the geometry.
        del RC

        # Core geometry parameters: [sdd, sod, uc , vc , eta  , sig  , phi  ]
        # Units:                    [ mm,  mm, pix, pix, rad  , rad  , rad  ]
        # initial_params = params[:7] # get these from the recon_params dictionary
        # deltas         = np.array([    0,   0,   5,   5, 0.001, 0.001, 0.001], np.float64) # generous guess as to how close the optimal solution is to the initial guess

        # Core geometry parameters: [uc , vc , eta  , sig  , phi  ]
        # Units:                    [pix, pix, rad  , rad  , rad  ]
        initial_params = params[2:7] # get these from the recon_params dictionary
        deltas         = np.array([    5,   5, 0.001, 0.001, 0.001], np.float64) # generous guess as to how close the optimal solution is to the initial guess

        sim_metric           = MutualInformation(recon_lib_path)
        geo_cal_function     = GeoCalibration( Y[0,...], recon_params, deltas, sim_metric, ausamp=max(np_//500,1), display_results=True )
        opt_params, opt_fval = Nelder_Mead( initial_params, deltas, geo_cal_function, maxiter=100, fun_tol=1e-5 )

        params = np.concatenate( [[sdd, sod],opt_params,[zo_, ao]] ).astype(np.float64)
        np.save( geo_params, params )

        recon_params['double_params'][9:14] = opt_params
        RC = initialize_recon_object( recon_params )

    else:

        print('Previously calibrated geometric parameters are in use.')


    # Analytical reconstruction using newly calibrated geometry
    print('Demonstrating geometry calibration with time-averaged analytical reconstruction...')
    X = np.zeros( (e,nz,ny,nx), np.float32 )
    # Y = np.reshape( Y, [ e, np_*nv*nu ] )
    tstart = time.time()
    for s in range(e):

        X[s,...] = RC.Rtf( Y[s,...], W )

    tend = time.time()
    print('Total time for analytical reconstruction: ' + str( tend-tstart ) + 's')

    save_nii( X, os.path.join(outpath,'X_WFBP_' + recon_name + '.nii') )


# 9) Cardiac gating

    print('Loading prior temporal weights.')

    # To Do: perform intrinsic gating as with newer data sets...
    # Fixed 9-24-24: Revised to respect the standardized order of projection weights (nt,np_)
    Wt = loadmat(proj_weights)
    Wt = Wt['Wt']
    Wt = np.transpose(Wt,[1,0])

# 10) Set up operators for iterative reconstruction

    print('Setting up for iterative reconstruction.')

    # RSKR denoising algorithm
    # Inputs:
    #      X  : Image domain data (dimensions: t,e,z,y,x)
    # Outputs:
    #   X_out : Denoised image data (dimensions: t,e,z,y,x)

    attn_water = np.array( [0.002767,0.002592,0.002496,0.002415], np.float32 ) # Attenuation values for water per energy threshold
    At = [0.2,0.6,0.2] # Temporal resampling for 4D BF

    denoise_op = RSKR( recon_lib_path, At=At, nvols=e, ntimes=p, mask=True, time_mode=1, weights=attn_water, gpu_idx=gpu_list[0] )

    # Time, energy initialization operators (WFBP)
    analytical_init = PCD_initialization_ops( RC, Y, Wt )

    # Time, energy data fidelity update operators (BiCGSTAB(l) with unfiltered backprojection)
    fidelity_ops = PCD_data_fidelity_ops( RC, Y, Wt, maxiter=6, ell=2 )


# 11) Perform iterative reconstruction

    # 7.7 hours on Artax
    # To Do: improve support for sparse weights.

    print('\nPerforming iterative reconstruction...\n')

    # X = np.tile( X[:,np.newaxis,...], (1,p,1,1,1) )
    # X_iter = denoise_op( X )

    tstart = time.time()

    X_iter = SplitBregman_IterRecon( initialization_ops=analytical_init, fidelity_updates=fidelity_ops, regularizer=denoise_op, times=p, energies=e,
                                     attn_water=attn_water, debug_outpath=debug_outpath )( Y )

    tend = time.time()

    print('Total time for iterative reconstruction: ' + str(tend - tstart) + 's')

    save_nii( X_iter, os.path.join(outpath,'X_iter_' + recon_name + '.nii') )

# 12) Material decomposition

    print('\nPerforming material decomposition...\n')

    # Inputs:
    #   X_iter: (e,t,z,y,x)
    #        M: (m,e) material sensitivity matrix
    M = np.array([[0.00280918, 0.00262516, 0.00254458, 0.00247173], # water
                  [0.00500953, 0.00472432, 0.00430647, 0.00370879], # 12 mg/mL iodine
                  [0.00333010, 0.00301627, 0.00287222, 0.00270698]  #  5 mg/mL gold
                 ], np.float32)

    # Subtract water from mixtures
    M[1:, :] = M[1:, :] - M[:1, :]

    # Normalize by concentration
    M[1,:]   = M[1,:] / 12
    M[2,:]   = M[2,:] / 5

    # Normalized condition number: 1.7232441 / 0.0640904 = 26.89
    # scale = np.sqrt(np.sum(M ** 2, axis=1, keepdims=True))
    # M = M / scale

    # Simple least-squares solution
    # C_LS = mat_decomp_LS(X_iter, M)
    # save_nii(C_LS, os.path.join(outpath, 'C_LS.nii'))

    # Non-negative least-squares solution
    C_noneg = mat_decomp_LS_non_neg(X_iter, M)
    save_nii(C_noneg, os.path.join(outpath, 'C_noneg.nii'))

# End of script.

    print('Reconstruction of ' + recon_name + ' complete.')



