
# Copyright (C) 2023, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
# Original Author: Darin Clark, PhD
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# References

# Long, Y., Fessler, J. A., & Balter, J. M. (2010).
# 3D forward and back-projection for X-ray CT using separable footprints.
# IEEE transactions on medical imaging, 29(11), 1839-1850.
#
# Stierstorfer, K., Rauscher, A., Boese, J., Bruder, H., Schaller, S., & Flohr, T. (2004).
# Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch.
# Physics in Medicine & Biology, 49(11), 2209.
#
# Van der Vorst, H. A. (1992).
# Bi-CGSTAB: A fast and smoothly converging variant of Bi-CG for the solution of nonsymmetric linear systems.
# SIAM Journal on scientific and Statistical Computing, 13(2), 631-644.
#
# Galigekere, R. R., Wiesent, K., & Holdsworth, D. W. (2003).
# Cone-beam reprojection using projection-matrices.
# IEEE transactions on medical imaging, 22(10), 1202-1214.
#
# Sleijpen, G. L., & Van Gijzen, M. B. (2010).
# Exploiting BiCGstab (ℓ) strategies to induce dimension reduction.
# SIAM journal on scientific computing, 32(5), 2687-2709.
# Algorithm 3.1


# MCR Toolkit
import sys
toolkit_path  = '/home/dpc/Documents/MCR_Toolkit_2/'
sys.path.append(toolkit_path + 'Python/')
from DD_Recon import *
recon_lib_path = toolkit_path + 'DD_operators_v25/DD_operators_v25.so'

# External Python modules (Python v3.10)
import numpy as np    # (v1.21.5)
import nibabel as nib # (v5.1.0)
import os
import matplotlib.pyplot as plt # (v3.5.1)
import time

# perform_alg_recon: True, perform algebraic reconstruction starting from 0s; False, do not perform algebraic reconstruction
# Note: Linear algebra with Numpy seems to be quite slow compared with Matlab. (~3x slower)
#       Numpy could be converting back and forth between C and Fortran memory order.
perform_alg_recon = True


# Load phantom data

if True:

    volname = os.path.join(toolkit_path,'Test_Data','SL3D_ph_new_sph0.nii.gz')
    X = load_nii( volname, type='float32' )

# System and data acquisition parameters
if True:

    # Volume dimensions
    nz  = X.shape[0]
    ny  = X.shape[1] # MUST be divisible by 8
    nx  = X.shape[2] # MUST be divisible by 8

    # Projection dimensions
    nu  = 800
    nv  = 544

    lsd = 800 # source-detector distance, mm
    lso = 700 # source-object distance, mm

    ap  = 1 # degrees of rotation per projection
    np_ = 360//ap # total number of projections

    du  = 0.044 # mm
    dv  = 0.044 # mm
    dx  = 0.088 # mm
    dy  = 0.088 # mm
    dz  = 0.088 # mm

    uc = 380 # detector elements, intersection of central ray along detector rows
    vc = 270 # detector elements, intersection of central ray along detector columns

    # detector plane rotations (radians)
    eta = 0
    sig = 0
    phi = 0

    # rotation angle (degrees)
    angles = np.linspace(0,np_,np_+1)
    angles = angles[:-1]
    rotdir = 1 # invert the rotation direction when set to -1

     # z offset of the central ray from the center of the reconstructed volume, per projection (mm)
    zo  = np.zeros( [np_,], dtype=np.double )

    # offset of the reconstructed volume from z = 0 (mm)
    zo_ = 0

    # Angular offset for the reconstructed volume relative to the projection angle (degrees)
    ao = 0

    gpu_list = np.array( [0], np.int32 ) # array of GPU indices to use with the reconstruction operators, starting from 0

# Reconstruction parameters
if True:

    # Helical CT

        limit_angular_range = 0  # (0) do not use; (>0) ignore rays which diverge by more than this absolute distance (in mm) from the central ray at the detector
        scale = 1                # scalar volume multiplier

        # cylindrical detector
        cy_detector = 0          # (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, cone-parallel rebinning; (3) flat panel, cone-parallel rebinning; (4) array of parallel beams (synchrotron geometry)
        db = 0.067864 * PI / 180 # fan angle increment (radians) (used for cylindrical detector only)

    # Additional flags

        FBP = 1                    # (1) FBP, (0) simple backprojection
        simple_back_projection = 0
        implicit_mask = 1          # (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
        explicit_mask = 0          # (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask; (2) use volumetric reconstruction mask
        use_affine = 0             # (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
        use_norm_vol = 0           # (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection

        # NOTE: When the affine transform causes a volume change (i.e. it is non-rigid), the resultant FBP will not be properly normalized.
        #       Also, inversion of the z axis is not supported.
        Rt_aff = np.array( [1,0,0,0,1,0,0,0,1,0,0,0], dtype=np.float32 ) # Identity transform

        # Explicit mask - image domain reconstruction mask
        # Only used when explicit_mask = 1
        # rmask = make_default_rmask( nx, ny, nz ) # reproduces the implicit reconstruction mask (a cylinder) as an explicit mask
        rmask = make_default_rmask( nz, ny, nx ) # reproduces the implicit reconstruction mask (a cylinder) as an explicit mask

        # Parameter vectors

        # int_params
        # int_params_f = int32([nu nv np nx ny nz FBP                    cy_detector implicit_mask explicit_mask use_affine use_norm_vol]);
        # double_params = [du dv dx dy dz zo ao lsd lso uc vc eta sig phi scale db limit_angular_range];

        # Required parameters
        # int_params    = np.array( [nu, nv, np_, nx, ny, nz, simple_back_projection, use_affine], dtype=np.int32  )
        int_params_f  = np.array( [nu, nv, np_, nx, ny, nz,                    FBP, use_affine], dtype=np.int32  )
        double_params = np.array( [du, dv, dx, dy, dz, zo_, ao, lsd, lso, uc, vc, eta, sig, phi], dtype=np.double )

        # Add optional parameters
        # int_params    = np.concatenate( (   int_params, np.array( [implicit_mask, explicit_mask, cy_detector, use_norm_vol], dtype=np.int32 ) ), axis=0 )
        int_params_f  = np.concatenate( ( int_params_f, np.array( [implicit_mask, explicit_mask, cy_detector, use_norm_vol], dtype=np.int32 ) ), axis=0 )
        double_params = np.concatenate( (double_params, np.array( [scale, db, limit_angular_range], dtype=np.double ) ), axis=0 )

        # Analytical reconstruction filter file
        filt_name = os.path.join(toolkit_path,'Test_Data','Ram-Lak_%s.txt')

        # Source-detector geometry file
        geofile   = os.path.join(toolkit_path,'Test_Data','geoall_test.geo')

        # Projection weights
        W = np.ones( (1,int(np_)) , dtype=np.double ) # , order='F' )

# Set up reconstruction object
if True:

    # Create a new reconstruction object
    RC = Recon(recon_lib_path)

    # Set integer parameter vector
    RC.set_int_params( int_params_f, rotdir )

    # Set double parameter vector
    RC.set_double_params( double_params )

    # FBP filter
    # filter_type: 'ram-lak','shepp-logan', 'cosine', 'hamming', 'hann'
    RC.make_and_set_filter( filt_name, filter_type='ram-lak', cutoff=1.0 ) # detector_type=cy_detector,

    # Geo file
    RC.make_and_set_geofile( geofile, angles, zo, W )

    # Affine registration
    RC.set_affine( Rt_aff )

    # Explicit reconstruction mask
    RC.set_rmask( rmask )

    # GPUs to use for recon. operators
    RC.set_GPU_list( gpu_list )

    # Initialization on the GPUs and the host
    RC.DD_init()

# Create projections
if True:

    Y = RC.R(X, W)

    plt.imshow( np.transpose(np.reshape(Y[np.round(np.linspace(0,np_-1,5)).astype('int32'),:,:],[5*nv,nu]), [1,0]), cmap='gray' )
    plt.draw()
    figManager = plt.get_current_fig_manager()
    # figManager.window.showMaximized()
    figManager.resize(*figManager.window.maxsize())
    plt.pause(2.0)
    plt.close()

# Perform analytical reconstruction
if True:

    t0 = time.time()
    Xf = RC.Rtf(Y, W)
    print('Total time (seconds): ' + str(time.time() - t0))

    RMSE = np.sqrt( np.mean( (X-Xf)**2 ) )

    print('Analytical reconstruction RMSE (expected ~0.020128): ' + str(RMSE))

    X_img  = np.reshape(X [np.round(np.linspace(0,nz-1,5)).astype('int32'),:,:],[5*ny,nx])
    Xf_img = np.reshape(Xf[np.round(np.linspace(0,nz-1,5)).astype('int32'),:,:],[5*ny,nx])

    x_fig = np.concatenate( (X_img,Xf_img,np.abs(X_img-Xf_img)), axis=1 )

    fig = plt.imshow( x_fig, cmap='gray' )
    plt.draw()
    plt.title('Original (column 1), Analytical Reconstruction (column 2), Absolute Residuals (column 3)')
    figManager = plt.get_current_fig_manager()
    # figManager.window.showMaximized()
    figManager.resize(*figManager.window.maxsize())
    plt.pause(3.0)
    plt.close()

# Perform algebraic reconstruction
# Solve for x: 0.5 * (R(x)-y)**2_W = 0.5 * (R(x)-y)' * W * (R(x)-y)
if perform_alg_recon:

    # Returns a function which computes Rt(R(x,w),w) + mu*x
    A  = RtR_op(RC, mu=0) # , mat_ord='F')

    # Constant
    b  = RC.Rt(Y, W)

    # Initial guess
    x0 = b*0

    ell   = 2 # Residual updates per iteration
    maxit = 3 # Iterations
    snum  = 3 # (1) Do not use subsets (>1) Divide projections into subsets

    # Divide projection weights W, into subsets
    Ws = get_subset_weights( W, np_, snum )

    # Perform algebraic reconstruction
    t0 = time.time()
    X_alg, resvec = bicgstabl_os(A, b, x0, maxit, Ws, ell)
    print('Total time (seconds): ' + str(time.time() - t0))

    # Summarize results
    print('Residuals per subset (rows), iteration (columns)...')
    print(str(resvec))

    X_alg = np.reshape(X_alg,[nz,ny,nx])

    RMSE = np.sqrt( np.mean( (X-X_alg)**2 ) )

    print('Algebraic reconstruction RMSE (expected ~0.024054): ' + str(RMSE))

    X_img     = np.reshape(X    [np.round(np.linspace(0,nz-1,5)).astype('int32'),:,:],[5*ny,nx])
    X_alg_img = np.reshape(X_alg[np.round(np.linspace(0,nz-1,5)).astype('int32'),:,:],[5*ny,nx])

    x_fig = np.concatenate( (X_img,X_alg_img,np.abs(X_img-X_alg_img)), axis=1 )

    fig = plt.imshow( x_fig, cmap='gray' )
    plt.draw()
    plt.title('Original (column 1), Algebraic Reconstruction (column 1), Absolute Residuals (column 3)')
    figManager = plt.get_current_fig_manager()
    # figManager.window.showMaximized()
    figManager.resize(*figManager.window.maxsize())
    plt.pause(3.0)
    plt.close()






