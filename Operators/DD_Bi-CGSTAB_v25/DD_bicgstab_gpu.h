
#ifndef BICGSTAB_GPU
#define BICGSTAB_GPU

    // Data structure definitions and required C headers
    #include "DD_v24_data_structures.h"
    
    // Environment variables
    #include "DD_v24_macros.h"

    // Global reference to reconstruction allocation / parameters
    recon_parameters *rp;
    
    // Allow the projection weights and weight sum to be redirected for use with ordered subsets
    int    perform_filtration; //
    double np_eff_sub0;         // Sum of weights for a subset.
    double *weights_sub0;       // Projection weights for a subset.
    double np_sub0;             // Assumed number of projections which contribute to each voxel. May be violated when performing helical reconstruction.

    // new data required for bicgstab
    int use_Q;
    int *subsets;
    int subsets_len;
    float eps;
    float mu_TV;
    float *b, *x0, *Q;
    float mu;
    size_t iter;
    
    // outputs
    float *xmin, *resvec;
    
    // Call CUDA code
    extern void bicgstab_cuda( );

#endif // BICGSTAB_GPU