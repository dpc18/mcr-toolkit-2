
// External headers

#include "cuda_runtime.h"
#include "mex.h"
#include "cufft.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>

// Custom headers

#include "DD_bicgstab_gpu.h"

// Function headers

int IsPrime(unsigned int number);
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]); // mex interface
void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len); // check the validity of argument lists based on their length

// ***     ***
// *** MEX ***
// ***     ***

// Solve A(x) - b = 0 for x
// A(x) := Rt(Q(R(x))) + mu*x
// b := Rt(Q(Y)) + mu*(d-v)
// [xmin, resvec] = DD_bicgstab_gpu( b, recon_alloc, use_filtration, proj_weights, x0, mu, iter, Q, subsets )
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    
    // Make sure we have the expected number of inputs / outputs
    
        if (nrhs != 9)
            mexErrMsgTxt("Nine input arguments expected! ( [xmin, resvec] = DD_bicgstab_gpu( b, recon_alloc, use_filtration, proj_weights, x0, mu, iter, Q, subsets ) )");

        if (nlhs != 2)
            mexErrMsgTxt("Two output arguments required! ( [xmin, resvec] = DD_bicgstab_gpu( b, recon_alloc, use_filtration, proj_weights, x0, mu, iter, Q, subsets ) )");
    
    // Check precision of inputs
        
        // Check the precision of the inputs
        if (!mxIsClass(prhs[0],"single")){ // b
            mexErrMsgTxt("First input argument (b) must be single precision.");  
        }

        if (!mxIsClass(prhs[1],"uint64")){ // recon_parameters structure
            mexErrMsgTxt("Second input argument (reconstruction parameter allocation) must be uint64.");  
        }
        
        if (!mxIsClass(prhs[2],"int32")){
            mexErrMsgTxt("Third input argument (filtration flag) must be int32.");  
        }
        
        if (!mxIsClass(prhs[3],"double")){
            mexErrMsgTxt("Fourth input argument (projection weights) must be double precision.");  
        }
        
        if (!mxIsClass(prhs[4],"single")){ // x0
            mexErrMsgTxt("Fifth input argument (initial guess, x0) must be single precision.");  
        }
        
        if (!mxIsClass(prhs[5],"single")){ // mu
            mexErrMsgTxt("Sixth input argument (regularization parameter, mu) must be single precision.");  
        }
        
        if (!mxIsClass(prhs[6],"int32")){ // iter
            mexErrMsgTxt("Seventh input argument (iteration number) must be int32.");  
        }
        
        if (!mxIsClass(prhs[7],"single")){ // Q
            mexErrMsgTxt("Eighth input argument (weighted least squares weights, Q) must be single precision.");  
        }

        if (!mxIsClass(prhs[8],"int32")){ // subsets
            mexErrMsgTxt("Ninth input argument (# of subsets) must be int32.");  
        }
        
    // Import values / pointers
        
        eps = 1e-8;
        
        // reconstruction allocations / parameters (convert from integer back to a pointer to a parameter object)
        rp = (recon_parameters *) *((uint64_t *)mxGetData(prhs[1]));
        
        // Make sure we have not cleared the allocation associated with the provided pointer
        if ( rp == NULL ) {
         
            mexErrMsgTxt("Passed reconstruction parameter allocation appears to have been cleared. Please reinitialize the reconstruction problem.");  
            
        }
        
        // target output
        b = (float*) mxGetData(prhs[0]);
        check_len(prhs, 1, 0, rp->nxyz);
        
        // Filtration override

            // Check to make sure the filtration flag has a valid value

                perform_filtration = (int) mxGetScalar(prhs[2]);

                if ( perform_filtration != 0 && perform_filtration != 1 ) {

                    mexErrMsgTxt("Third input argument (filtration flag) must be zero or one.");

                }

            // Check to make sure a filter has been specified and allocated during initialization

                if ( rp->filtration == 0 && perform_filtration == 1 ) {

                    mexErrMsgTxt("Cannot perform filtration. No filter was allocated during initialization.");

                }
                
        // Projection weights
            
            // check to make sure the new weights have the right size
                
                check_len(prhs, 4, 3, rp->np);
            
            // Assign the Matlab memory to the internal (redirected) weights pointer. We could overwrite the existing weights vector;
            // however, we choose to default back to the initial weights during future calls, since overrided weights will likely change
            // between calls.
            
                weights_sub0 = (double*) mxGetData(prhs[3]);

        // Read in the initial guess
        x0 = (float*) mxGetPr(prhs[4]);
        check_len(prhs, 5, 4, rp->nxyz);
        
        // regularization parameter
        mu = *((float*) mxGetPr(prhs[5]));
        
        // iteration number
        iter = (size_t) *((int *) mxGetPr(prhs[6]));
        
        // Weighted least squares weights
        Q = (float *) mxGetPr(prhs[7]);
        
            // check length
            const size_t *Qsz = mxGetDimensions(prhs[7]);
            mwSize Q_dims = mxGetNumberOfDimensions(prhs[7]);
            size_t Qcounter = 1;
            for (int i = 0; i < Q_dims; i++) {

                Qcounter *= Qsz[i];

            }
            
            if ( Qcounter == 1 ) {
             
                // Save memory by not allocating Q.
                use_Q = 0;
                
            }
            else {
             
                // Use Q.
                use_Q = 1;
                
                check_len(prhs, 8, 7, rp->nuvp);
                
            }
            
        // Read in subsets parameters
        subsets = (int*) mxGetPr(prhs[8]);
        
            // check length
            const size_t *subsetssz = mxGetDimensions(prhs[8]);
            mwSize subsets_dims = mxGetNumberOfDimensions(prhs[8]);
            size_t subsetscounter = 1;
            for (int i = 0; i < subsets_dims; i++) {

                subsetscounter *= subsetssz[i];

            }
            
            if ( subsetscounter == 0) {

                mexErrMsgTxt("At least one 'subset' is required..");

            }
            
            if ( subsetscounter != 1 && !IsPrime(subsetscounter) ) {
                
                mexErrMsgTxt("Number of subsets must be 1 or a prime number.");
                
            }
            
            subsets_len = subsetscounter;

            if ( ( (float) rp->np ) / ( (float) subsets_len ) < 30.0f) {

                mexErrMsgTxt("For stability, at least 30 projections are required per subset.");

            }

            // check to make sure every subset is covered

            subsetscounter = 0;

            for (int i = 0; i < subsets_len; i++) {

                for (int j = 1; j <= subsets_len; j++) {

                    if (j == subsets[i]) {

                        subsetscounter++;
                        break; // do not allow repeats

                    }

                }

            }

            if (subsetscounter != subsets_len) {

                mexErrMsgTxt("Every subset must be covered once, and only once. Also, subsets must range from 1 to length(subsets).");

            }

            
        // Sanity checks
            
            if (iter == 0) {
                
                mexErrMsgTxt("Input iter (sixth argument) must be at least 1.");
                
            }
            
            if (mu < 0.0f) {
                
                mexErrMsgTxt("Input mu (fifth argument) must be greater than or equal to 0.");
                
            }
            
    // Allocate outputs
            
        const mwSize siz[2] = {rp->nxyz,1};
        plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
        xmin = (float*) mxGetData(plhs[0]);
        
        const mwSize siz2[2] = {1,subsets_len*iter+1};
        plhs[1] = mxCreateNumericArray(2,siz2,mxSINGLE_CLASS,mxREAL);
        resvec = (float*) mxGetData(plhs[1]);
    
    // Call bicgstab solver
        
        bicgstab_cuda();
    
}

// check the validity of argument lists based on their length
void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len)
{

    char err_string[200];

    if ( mxGetNumberOfElements(prhs[idx]) != len ) {

        sprintf(err_string, "Input argument %i does not have expected length of %lu.\n", arg_num, len);

        mexErrMsgTxt(err_string);

    }

}

// int isPowerOfTwo (unsigned int x)
// {
//   return ((x != 0) && !(x & (x - 1)));
// }

int IsPrime(unsigned int number) {
    if (number <= 1) return 0; // zero and one are not prime
    unsigned int i;
    for (i=2; i*i<=number; i++) {
        if (number % i == 0) return 0;
    }
    return 1;
}












