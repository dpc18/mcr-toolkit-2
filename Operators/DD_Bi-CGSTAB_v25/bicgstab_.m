function [x, resvec] = bicgstab_(A,b,x,maxit)
% Modifed version of Matlab's bicgstab implementation.
% Avoid storing x0, xmin, ph, and sh to save RAM. Write over x in place, if possible.
% http://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method

    resvec = zeros(maxit+1,1);

    % n2b = norm(b);
    
    % 1) residual
    r = b - A(x);
    resvec(1) = norm(r);
    
    % 2) previous iteration init
    rt = r;
   
    % 3) rate parameters
    rho = 1;
    omega = 1;
    alpha = 1;
    
    % store the minimum result
    xmin = x;

    for ii = 1:maxit
        
        rho1 = rho;
        
        % 1)
        rho = rt' * r;
        
        if ii == 1
            p = r;
        else
            % 2)
            beta = (rho/rho1)*(alpha/omega);
            % 3) 
            p = r + beta * (p - omega * v);
        end

        % 4)
        v = A(p);
        
        % 5)
        alpha = rho / (rt' * v);
        
        % 6)
        s = r - alpha * v;
        
        % 7)
        t = A(s);
        
        % 8)
        omega = (t' * s) / (t' * t);
        
        % 9)
        x = x + alpha*p + omega*s;
        
        % 11)
        r = s - omega * t;
        
        resvec(ii+1) = norm(r);
        
        if (resvec(ii+1) == min(resvec))
           
            xmin = x;
            
        end

    end                                % for ii = 1 : maxit
    
    x = xmin;

end