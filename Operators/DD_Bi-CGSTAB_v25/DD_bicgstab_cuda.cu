

// External headers

#include "cuda_runtime.h"
#include "mex.h"
#include "cufft.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include "omp.h"

// Custom headers

#include "error_chk.h"
#include "DD_v24_macros.h"                 // Environment variables and length checker function
#include "DD_v24_data_structures.h"        // Data structure definitions
#include "DD_v24_GPU_support_functions.h"  // Extra GPU functions
#include "linear_algebra_cuda.h"           // Linear algebra kernels
#include "DD_bicgstab_cuda.h"

// variables which must be assigned for projection / backprojection operations
double np_eff_sub;   // Sum of weights for a subset.
double *weights_sub; // Projection weights for a subset.
double np_sub;       // Assumed number of projections which contribute to each voxel. May be violated when performing helical reconstruction.
extern recon_parameters *rp;

#include "DD_v24_backproject.h"
#include "DD_v24_project.h"

// size_t check_counter = 0;

// debugging
// float *reporter;
// unsigned int reporter_idx;

// ***           ***
// *** FUNCTIONS ***
// ***           ***

__global__ void set_res(float *current_res, const float *resvec, int subsets_len_subsamp, int last_idx);
__global__ void set_subsample_flag(const float *current_res, const float *old_res, int *subsample);
void eval_A(float *vol_in, float *vol_out); // , cudaStream_t *streams);
void l2_norm(float *av, float *res);
void make_subsets(int subsample);
void init_bicgstab();
void bicgstab_cuda();
void DD_project_cuda( float *Y, float *X, int output_on_GPU );
void DD_backproject_cuda( float *X, float *Y, int output_on_GPU );

// ***                  ***
// *** GLOBAL VARIABLES ***
// ***                  ***

// newly imported weights
extern double *weights_sub0;

// intermediate variables for working with subsets
double *subset_weights; // projection weighting vectors for each subset
double *np_effs;        // weighted projection count normalization factor for each subset
double *np_subs;      

// Filtered backprojection
extern int perform_filtration;

// External reference to reconstruction allocation / parameters
extern recon_parameters *rp;

// GPU to use for BiCGSTAB calculations other than projection/backprojection
int master_GPUid = rp->GPU_indices[0];

// subset subsampling flag
int *subsample;
int *subsample_;
float *current_res_;
float *old_res_;
int resvec_offset;
int last_update;

// inputs; directly allocated on / copied to GPU
float *b_, *x0_, *xmin_; // , *mom_; // copies of X
float *Q_; // copies of Y
float *resvec_;

// Intermediate managed memory arrays
float *y_m; // copies of Y
float *r_m, *rt_m, *x_m, *p_m, *v_m, *s_m, *t_m; // copies of X

// Device scalars
float *rho_, *rho1_, *omega_, *alpha_, *beta_, *mu_;

dim3 scalar_grid = dim3(1,1,1);
dim3 scalar_block = dim3(1,1,1);

// ***        ***
// *** DEVICE ***
// ***        ***

__global__ void set_res(float *current_res, const float *resvec, int subsets_len_subsamp, int last_idx) {
    
    current_res[0] = 0;
    
    for (int i = 0; i < subsets_len_subsamp; i++) {
     
        current_res[0] += resvec[last_idx - 1 - i];
        
    }
    
    current_res[0] /= subsets_len_subsamp;
    
}

__global__ void set_subsample_flag(const float *current_res, const float *old_res, int *subsample) {
 
    
    if (current_res[0] <= old_res[0]) {
        
        subsample[0] = 0;
        
    }
    else {
        
        subsample[0] = 1;
        
    }
    
    
}

// ***      ***
// *** HOST ***
// ***      ***

// void report(int flag, float *val) {
//     
//     cudaMemcpy(reporter, val, sizeof(float), cudaMemcpyDeviceToHost);
//  
//     mexPrintf("Read %f at flag %i \n", reporter[0], flag);
//     
// }

// evaluate Rt(Q(R(x))) + mu*x
void eval_A(float *vol_in, float *vol_out) {
    
    // 1) R(x) (1 at the end is specify GPU / managed output)
    DD_project_cuda( y_m, vol_in, 1 );
    cudaSetDevice(master_GPUid);
    
    // 2) Q(R(x))
    if (use_Q == 1) {
        
        av_dot_bv<<<vec_grid_y, vec_block>>>(y_m, Q_, y_m); // (av, bv, output var)
    
    }
    
    // 3) Rt(Q(R(x))) (1 at the end is specify GPU / managed output)
    DD_backproject_cuda( vol_out, y_m, 1 );
    cudaSetDevice(master_GPUid);
    
    // 4) Rt(Q(R(x))) + mu*x
    av_plus_bs_dot_cv<<<vec_grid, vec_block>>>(vol_out, mu_, vol_in, vol_out); // (av, bs, cv, output var)
    
}

void l2_norm(float *av, float *res) { // (input vector, output scalar)
    
    // done internally now
    // cudaMemset(res, 0, sizeof(float));
    
    // squared_sum<<<vec_grid, vec_block, shared_mem_sz>>>(av, res);
    // squared_sum(av, res);
    squared_sum(av, res, rp->nxyz);
    
    scalar_sqrt<<<scalar_grid,scalar_block>>>(res, res); // (input, output)
    
}

void make_subsets(int subsample) {

    if (subsample != 1) {
     
        mexErrMsgTxt("Subset subsampling is not compatible with prime numbers of subsets...");
        
    }

    memset(subset_weights, 0, rp->np*subsets_len*sizeof(double));
    memset(np_effs, 0, subsets_len*sizeof(double));
    memset(np_subs, 0, subsets_len*sizeof(double));
    
    for (int i = 0; i < subsets_len / subsample; i++) {
        
        for (int k = i*subsample; k < i*subsample+subsample; k++) { // Assign multiple subsets to a single new subset, when subsample > 1.
            
            for (int j = subsets[k]-1; j < rp->np; j += subsets_len) { // minus one to avoid skipping the first projection at index 0
                 
                subset_weights[i*rp->np + j]  = weights_sub0[j];
                np_effs[i]               += weights_sub0[j];
                
                if ( weights_sub0[j] > 0 ) {
                 
                    np_subs[i]++;
                    
                }
                    
            }

        }
        
    }
    
}

// Set up GPU variables
// Used unified memory?
void init_bicgstab() {
    
        // reporter_idx = (nz/2)*nx*ny + (ny/2)*nx + nx/2;
        // reporter = (float *) malloc( sizeof(float) );
    
    // padded sizes
    
        size_t vol_p = ((rp->nxyz+TBP_x2-1)/TBP_x2)*TBP_x2;
        size_t projs_p = ((rp->nuvp+TBP_x2-1)/TBP_x2)*TBP_x2;
        
        size_t vol_size_p = vol_p*sizeof(float);
        size_t projs_size_p = projs_p*sizeof(float);
    
    // grid size for vector operations on X
    
        vec_grid = dim3(vol_p/TBP_x2,1,1);
        vec_grid_y = dim3(projs_p/TBP_x2,1,1);
    
    // 1) port input variables to GPU
    // input X: b, x0, xmin
    // input Y: Q
    // input scalar: mu, iter
    // input, other: resvec (length(iter + 1))
        
        cudaMallocManaged( (void **) &b_, vol_size_p, cudaMemAttachGlobal);
        cudaMallocManaged( (void **) &x0_, vol_size_p, cudaMemAttachGlobal);
        cudaMallocManaged( (void **) &xmin_, vol_size_p, cudaMemAttachGlobal);

        //cudaMalloc( (void **) &b_, vol_size_p);
        //cudaMalloc( (void **) &x0_, vol_size_p);
        //cudaMalloc( (void **) &xmin_, vol_size_p);
        
        // cudaMalloc( (void **) &mom_, vol_size_p);
        
        // make sure padded voxels are zero
        cudaMemset(b_, 0, vol_size_p);
        cudaMemset(x0_, 0, vol_size_p);
        cudaMemset(xmin_, 0, vol_size_p);
        // cudaMemset(mom_, 0, vol_size_p);

        cudaMemcpy(b_, b, rp->vol_size, cudaMemcpyDefault);
        cudaMemcpy(x0_, x0, rp->vol_size, cudaMemcpyDefault);
        cudaMemcpy(xmin_, x0_, rp->vol_size, cudaMemcpyDefault);
        
        if (use_Q == 1) {
            
            // cudaMalloc( (void **) &Q_, projs_size_p);
            
            cudaMallocManaged( (void **) &Q_, projs_size_p, cudaMemAttachGlobal);
            cudaMemset( Q_, 0, projs_size_p);
            cudaMemcpy(Q_, Q, rp->projs_size, cudaMemcpyHostToDevice);
            
        }

        // cudaMalloc( (void **) &resvec_, (subsets_len*iter+1)*sizeof(float));
        
        cudaMallocManaged( (void **) &resvec_, (subsets_len*iter+1)*sizeof(float), cudaMemAttachGlobal);
        cudaMemset(resvec_, 0, (subsets_len*iter+1)*sizeof(float));
 
    // 2) Allocate unified variables for oversubscription of gpu memory (when needed)
    // Y: y
    // X: r, rt, x, p, v, s, t
    // scalar: rho, rho1, omega, alpha, beta
        
        // cudaMalloc( (void **) &y_m, projs_size_p); // temp variable
        cudaMallocManaged( (void **) &y_m, projs_size_p, cudaMemAttachGlobal);
        cudaMemset( y_m, 0, projs_size_p);

        // cudaMalloc( (void **) &r_m, vol_size_p);
        cudaMallocManaged( (void **) &r_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( r_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &rt_m, vol_size_p);
        cudaMallocManaged( (void **) &rt_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( rt_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &x_m, vol_size_p); // temp variable
        cudaMallocManaged( (void **) &x_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( x_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &p_m, vol_size_p);
        cudaMallocManaged((void **) &p_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( p_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &v_m, vol_size_p);
        cudaMallocManaged( (void **) &v_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( v_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &s_m, vol_size_p);
        cudaMallocManaged( (void **) &s_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( s_m, 0, vol_size_p);
        
        // cudaMalloc( (void **) &t_m, vol_size_p);
        cudaMallocManaged( (void **) &t_m, vol_size_p, cudaMemAttachGlobal);
        cudaMemset( t_m, 0, vol_size_p);
        
    // regularization factor
        
        // cudaMalloc( (void **) &mu_, sizeof(float));
        cudaMallocManaged( (void **) &mu_, sizeof(float), cudaMemAttachGlobal);
        cudaMemcpy( mu_, &mu, sizeof(float), cudaMemcpyHostToDevice);
        
    // scalars
        
        subsample = (int *) malloc( sizeof(int) );
        
        cudaMalloc( (void **) &subsample_, sizeof(int));
        cudaMalloc( (void **) &old_res_, sizeof(float));
        cudaMalloc( (void **) &current_res_, sizeof(float));
        
        cudaMalloc( (void **) &rho_, sizeof(float));
        cudaMalloc( (void **) &rho1_, sizeof(float));
        cudaMalloc( (void **) &omega_, sizeof(float));
        cudaMalloc( (void **) &alpha_, sizeof(float));
        cudaMalloc( (void **) &beta_, sizeof(float));
        
    // Check for GPU allocation errors, since it is easy to run out of RAM...
        
        check(0);
        
    // subsets
         
        // allocate subset weights
        subset_weights = (double *) malloc( rp->np*subsets_len*sizeof(double) );
        np_effs        = (double *) malloc( subsets_len*sizeof(double) );
        np_subs        = (double *) malloc( subsets_len*sizeof(double) );

        // Assign subsets based on subsets vector
        // Pass a factor of 1 to initally use all subsets.
        make_subsets(1);
        
}

void bicgstab_cuda() {
    
    // Set device to use for BiCGSTAB calculations
    
        cudaSetDevice(master_GPUid);
    
    // initialize bicgstab variables
    
        init_bicgstab();
        
    // initialize linear algebra variables
        
        // initialize_linear_algebra();
        dp_inner_product_init(rp->nxyz);
        
    // Subsets init
        
        int subsets_len_subsamp = subsets_len;
        resvec_offset = 0;
        last_update = 0;
        
    // intermediate variables
    
        float *temp1_, *temp2_, *rescurrent_;

        cudaMalloc( (void **) &temp1_, sizeof(float));
        cudaMalloc( (void **) &temp2_, sizeof(float));
        cudaMalloc( (void **) &rescurrent_, sizeof(float));
    
    // 1) r = b - A(x);
    // resvec(1) = norm(r);
    // use the full weights vector
        
        //eval_A(x0_, x_m); // , streams); // (vol in, vol out)
        
//         // report(1, x_m+reporter_idx);
//         
//         av_sub_bv<<<vec_grid, vec_block>>>(b_, x_m, r_m); // (av, bv, output var)
//         
//         // report(2, r_m+reporter_idx);
//         
//         l2_norm( r_m, resvec_); // (input vector, output scalar)
//         
//         // report(3, resvec_);
//         
//         scalar_equals<<<scalar_grid,scalar_block>>>(rescurrent_, resvec_); // a = b
//     
//     // 2) previous iteration init
//     // rt = r;
//         
//         av_equals_bv<<<vec_grid, vec_block>>>(rt_m, r_m); // (av, bv)
//     
//     // 3) rate parameters
//     // rho = 1, omega = 1, alpha = 1
//         
//         set_as<<<scalar_grid,scalar_block>>>(rho_, 1.0f);
//         set_as<<<scalar_grid,scalar_block>>>(omega_, 1.0f);
//         set_as<<<scalar_grid,scalar_block>>>(alpha_, 1.0f);
    
    // store the minimum result
    // xmin = x;
        
        // already copied in init_bicgstab()
    
//     for (unsigned int l = 0; l < iter; l++) { // for each iteration of bicgstab
//         
//         for (int sub = 0; sub < subsets_len_subsamp; sub++) { // for each subset
//             
//             // redirect to current subset
//                 
//                 // variable used by the projection / backprojection operators
//                 weights_sub = &subset_weights[sub*rp->np]; 
//                 np_eff_sub  = np_effs[sub];
//                 np_sub      = np_subs[sub];
//                 
//             // rho1 = rho;
// 
//                 // check(1);
// 
//                 scalar_equals<<<scalar_grid,scalar_block>>>(rho1_, rho_); // a = b
// 
//             // 1) rho = rt' * r;
// 
//                 // set to zero internally now
//                 // cudaMemset(rho_, 0, sizeof(float));
// 
//                 // inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(rt_m, r_m, rho_);
//                 inner_prod(rt_m, r_m, rho_, rp->nxyz);
//                 
//                 // report(4, rho_);
// 
//             // if ii == 1
//             //    p = r;
//             // else
//             //    % 2)
//             //    beta = (rho/rho1)*(alpha/omega);
//             //    % 3) 
//             //    p = r + beta * (p - omega * v);
//             // end
// 
//                 // check(2);
// 
//                 if (l == 0 && sub == 0) {
// 
//                     // p = r;
// 
//                     av_equals_bv<<<vec_grid, vec_block>>>(p_m, r_m); // (av, bv)
// 
//                 }
//                 else {
// 
//                     // 2) beta = (rho/rho1)*(alpha/omega);
// 
//                         scalar_divide<<<scalar_grid,scalar_block>>>(rho_, rho1_, temp1_); // (scalar 1, scalar 2, output)
// 
//                         scalar_divide<<<scalar_grid,scalar_block>>>(alpha_, omega_, temp2_); // (scalar 1, scalar 2, output)
// 
//                         scalar_multiply<<<scalar_grid,scalar_block>>>(temp1_, temp2_, beta_); // (scalar 1, scalar 2, output)
// 
//                     // 3) p = r + beta * (p - omega * v);
// 
//                         av_minus_bs_dot_cv<<<vec_grid, vec_block>>>(p_m, omega_, v_m, x_m); // (av, bs, cv, out var)
// 
//                         av_plus_bs_dot_cv<<<vec_grid, vec_block>>>(r_m, beta_, x_m, p_m); // (av, bs, cv, out var)
// 
//                 }
// 
//                 // check(3);
// 
//             // 4) v = A(p)
// 
//                 eval_A(p_m, v_m); // , streams); // (vol in, vol out)
//                 
//                 // report(5, v_m + reporter_idx);
// 
//                 // check(4);
// 
//             // 5) alpha = rho / (rt' * v);
// 
//                 // set to zero internally now
//                 // cudaMemset(temp1_, 0, sizeof(float));
// 
//                 // inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(rt_m, v_m, temp1_); // (av, bv, out var)
//                 inner_prod(rt_m, v_m, temp1_, rp->nxyz); // (av, bv, out var)
//                 
//                 // report(6, temp1_);
// 
//                 scalar_divide<<<scalar_grid,scalar_block>>>(rho_, temp1_, alpha_); // (scalar 1, scalar 2, output)
//                 
//                 // report(7, alpha_);
// 
//                 // check(5);
// 
//             // 6) s = r - alpha * v;
// 
//                 av_minus_bs_dot_cv<<<vec_grid, vec_block>>>(r_m, alpha_, v_m, s_m); // (av, bs, cv, out var)
//                 
//                 // report(8, s_m + reporter_idx);
// 
//                 // check(6);
//                 
//             // 7) t = A(s);
// 
//                 eval_A(s_m, t_m); //, streams); // (vol in, vol out)
//                 
//                 // report(9, t_m + reporter_idx);
// 
//                 // check(7);
// 
//             // 8) omega = (t' * s) / (t' * t);
// 
//                 // done internally now
//                 // cudaMemset(temp1_, 0, sizeof(float));
//                 // cudaMemset(temp2_, 0, sizeof(float));
// 
//                 // inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(t_m, s_m, temp1_); // (av, bv, out var)
//                 inner_prod(t_m, s_m, temp1_, rp->nxyz);
//                 
//                 // report(10, temp1_);
// 
//                 // inner_prod<<<vec_grid,vec_block,shared_mem_sz>>>(t_m, t_m, temp2_); // (av, bv, out var)
//                 inner_prod(t_m, t_m, temp2_, rp->nxyz);
//                 
//                 // report(11, temp2_);
// 
//                 scalar_divide<<<scalar_grid,scalar_block>>>(temp1_, temp2_, omega_); // (scalar 1, scalar 2, output)
//                 
//                 // report(12, omega_);
// 
//                 // check(8);
// 
//             // 9) x = x + alpha*p + omega*s;
//                 
//                 av_plus_bs_dot_cv<<<vec_grid, vec_block>>>(x0_, alpha_, p_m, x0_); // (av, bs, cv, out var)
//                 
//                 // report(13, x0_ + reporter_idx);
// 
//                 av_plus_bs_dot_cv<<<vec_grid, vec_block>>>(x0_, omega_, s_m, x0_); // (av, bs, cv, out var)
//                 
//                 // report(14, x0_ + reporter_idx);
// 
//                 // check(9);
// 
//             // 11) r = s - omega * t;
// 
//                 av_minus_bs_dot_cv<<<vec_grid, vec_block>>>(s_m, omega_, t_m, r_m); // (av, bs, cv, out var)
//                 
//                 // report(15, r_m + reporter_idx);
// 
//                 // check(10);
// 
//             // resvec(ii+1) = norm(r);
// 
//                 l2_norm( r_m, resvec_ + l*subsets_len + sub + resvec_offset + 1); // (input vector, output scalar)
//                 
//                 // report(16, resvec_ + l*subsets_len + sub + resvec_offset + 1);
// 
//                 // check(11);
// 
//             // if (resvec(ii+1) == min(resvec))
// 
//             //    xmin = x;
// 
//             // end
// 
//                 // if scalar a < scalar b, cv = dv
//                 scalar_a_less_b_vector_equals<<<vec_grid, vec_block>>>(resvec_ + l*subsets_len + sub + resvec_offset + 1, rescurrent_, xmin_, x0_ ); // (as, bs, cv, dv )
// 
//                 // if scalar a < scalar b, scalar c = scalar d
//                 scalar_a_less_b_scalar_equals<<<scalar_grid, scalar_block>>>(resvec_ + l*subsets_len + sub + resvec_offset + 1, rescurrent_, rescurrent_, resvec_ + l*subsets_len + sub + resvec_offset + 1); // (as, bs, cs, ds )
// 
//                 // check(12);
//                 
//         }
// 
//     }
    
    // x = xmin;
        
    cudaMemcpy(xmin, xmin_, rp->vol_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(resvec, resvec_, (subsets_len*iter+1)*sizeof(float), cudaMemcpyDeviceToHost);
    
}







// Distribute projections over GPUs and GPU streams. Each GPU stream is assigned its own CPU thread using OpenMP.
void DD_backproject_cuda( float *X, float *Y, int output_on_GPU ) {
    
    // Check for outstanding CUDA errors
    
        check(0);
        
    // for each GPU and stream
    
        // declare private variables
        int tid;
        Private_GPU_Data *GPUpri;
        Public_GPU_Data  *GPUpub;
        int GPUid, GPUnum, c, q, p, cnt, cnt_host;
        int vol_start, vol_end;
        int proj_start, proj_end;
        int p_size, z_size;
        size_t os;

        // make sure we will cover the entire volume
        int vol_increment = ( rp->nz + NUMSTREAMS * rp->nDevices - 1) / ( NUMSTREAMS * rp->nDevices ); // round up w/ integer truncation
        
    // Perform projection filtration
    // Do this in its own loop, since all streams use all projections later.

        if (rp->filtration && perform_filtration) {
            
            // projections per stream (round up w/ integer truncation)
            // op repeated on each GPU, so all filtered projections are available later
            int proj_increment = ( rp->np + NUMSTREAMS - 1 ) / ( NUMSTREAMS );
            
            // private(i)
            #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, proj_start, proj_end, q, p, os ) default( shared ) num_threads(THREAD_COUNT)
            {
                
                // Which CPU thread are we running on?
                
                    tid = omp_get_thread_num();
                    
                // Since we need to have a static number of threads, leave threads that we are not using unoccupied.
         
                    if ( tid < NUMSTREAMS*rp->nDevices ) {
    
                        // which GPU are we working on?

                            GPUnum = tid / NUMSTREAMS; // integer truncation

                            GPUid = rp->GPU_indices[ GPUnum ];

                            cudaSetDevice( GPUid );

                        // Direct private pointers to the correct data structures within rp

                            GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                            GPUpub = &(rp->GPU_pub[GPUnum]);
                            
                        // Which projections are we responsible for filtering?
                        
                            proj_start = (tid % NUMSTREAMS) * proj_increment;
                            proj_end   = min( (tid % NUMSTREAMS + 1) * proj_increment, (int) rp->np);
                            
                        // Copy projections to managed memory
                            
                            for ( p = proj_start; p < proj_end; p++ ) {

                                if ( weights_sub[p] == 0 ) continue;

                                os = p*rp->nuv;

                                cudaMemcpyAsync( &(GPUpub[0].proj_[os]), &(Y[os]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );

                            }
                            
                        // Perform filtration
                        for ( p = proj_start; p < proj_end; p++) {
                                
                            // no point in filtering projections we will not use...
                            if ( weights_sub[p] == 0 ) continue;

                            os = p*rp->nuv;

                            // copy projection to complex 
                            realtocomplex<<<rp->pgrid_fft, rp->pblock, 0, GPUpri[0].mystream>>>( &(GPUpub[0].proj_[os]), GPUpri[0].fftproj_, rp->nu, rp->nu_fft);

                            // fft of projection
                            cufftExecC2C(GPUpri[0].fftplan, GPUpri[0].fftproj_, GPUpri[0].fftproj_, CUFFT_FORWARD);

                            // multiply fft by filter
                            filterprojection<<<rp->pgrid_fft, rp->pblock, 0, GPUpri[0].mystream>>>( GPUpri[0].fftproj_, GPUpub[0].filt_, rp->nu_fft);

                            // ifft
                            cufftExecC2C(GPUpri[0].ifftplan, GPUpri[0].fftproj_, GPUpri[0].fftproj_, CUFFT_INVERSE);

                            // copy complex to projection
                            complextoreal<<<rp->pgrid, rp->pblock, 0, GPUpri[0].mystream>>>(GPUpri[0].fftproj_, &(GPUpub[0].proj_[os]), rp->nu, rp->nu_fft);

                        }

                    }
            
            }
            
            // Check for outstanding CUDA errors
    
                check(1);
            
            // Synchronize memory before we access all projections with each stream
            for ( int i = 0; i < rp->nDevices; i++ ) {

                GPUid = rp->GPU_indices[ i ];

                cudaSetDevice( GPUid );
                
                cudaDeviceSynchronize();
                
            }
            
        }
        
    // Copy data to each GPU, if we did not filter the data
        
        if ( !(rp->filtration) || !(perform_filtration) ) {

            // Do this in its own pragma to avoid difficult thread/stream synchronization issues.
            #pragma omp parallel private( tid, GPUpub, GPUid, GPUnum, p, os ) default( shared ) num_threads(THREAD_COUNT)
            {

                // Which CPU thread are we running on?

                    tid = omp_get_thread_num();
                    
                // Perform the copy only once per GPU

                    if ( ( tid < NUMSTREAMS*rp->nDevices ) && ( ( tid % NUMSTREAMS ) == 0 ) ) {

                        // Which GPU are we working on?

                            GPUnum = tid / NUMSTREAMS; // integer truncation
                            GPUid = rp->GPU_indices[ GPUnum ];
                            cudaSetDevice( GPUid );

                        // Direct private pointers to the correct data structures within rp

                            GPUpub = &(rp->GPU_pub[GPUnum]);

                        // Copy data once per GPU
                            
                            for ( p = 0; p < rp->np; p++ ) {

                                if ( weights_sub[p] == 0 ) continue;

                                os = p*rp->nuv;

                                cudaMemcpy( &(GPUpub[0].proj_[os]), &(Y[os]), rp->proj_size, cudaMemcpyDefault );

                            }

                    }

            }
            
        }

    // Perform backprojection
        
        #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, c, vol_start, vol_end, q, p, z_size, p_size, cnt, cnt_host ) default( shared ) num_threads(THREAD_COUNT)
        {

            // Which CPU thread are we running on?

                tid = omp_get_thread_num();

            // Since we need to have a static number of threads, leave threads that we are not used unoccupied.
            if ( tid < NUMSTREAMS*rp->nDevices ) {

                // which GPU are we working on?

                    GPUnum = tid / NUMSTREAMS; // integer truncation

                    GPUid = rp->GPU_indices[ GPUnum ];

                    cudaSetDevice( GPUid );

                // Direct private pointers to the correct data structures within rp

                    GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                    GPUpub = &(rp->GPU_pub[GPUnum]);
                    
                // Which z slices is this stream / thread responsible for?

                    vol_start = tid * vol_increment;
                    vol_end   = min( (tid + 1) * vol_increment, (int) rp->nz ); // Make sure we do not go out of bounds!

                // Create events to let the host know when we are done processing each chunk of volume data.
                    
                    cudaEvent_t *done_events;
                    if ( output_on_GPU == 0 ) {
                    
                        done_events = (cudaEvent_t *) malloc( ( ( vol_end - vol_start + VOL_CHUNK - 1 ) / VOL_CHUNK ) * sizeof(cudaEvent_t) );
                        cnt = 0;

                    }

                // Backproject host data in chunks

                    // loop over volume chunks
                    for ( c = vol_start; c < vol_end; c += VOL_CHUNK ) {
                        
                        z_size = min( vol_end - c, VOL_CHUNK );

                        cudaMemsetAsync( &(GPUpub[0].vol_[c*rp->nxy]), 0, z_size * rp->slice_size, GPUpri[0].mystream);
                        
                        if ( rp->use_norm_vol == 1 ) {
                         
                            cudaMemsetAsync( &(GPUpub[0].norm_vol_[c*rp->nxy]), 0, z_size * rp->slice_size, GPUpri[0].mystream );
                            
                        }

                        // NOTE: The header has already been removed from the rmask_ volume.
                        // This could be set up to only copy mask slices within the valid z range; however, it is not clear how helpful this would be
                        // since this is in the outer loop and is uint16 precision.
                        if ( rp->explicit_mask == 2 ) { // x,y,z mask w/ z range
                                
                            cudaMemPrefetchAsync( &(GPUpub[0].rmask_[c*rp->nxy]), z_size*rp->nxy*sizeof(unsigned short), GPUid, GPUpri[0].mystream );
                            
                        }
                        
                        for ( q = 0; q < rp->np; q += PROJ_CHUNK ) {
                            
                            p_size = min( rp->np - q, PROJ_CHUNK );

                            // Backproject projection chunk into our volume data
                            DD_backproject_cuda( &GPUpub[0], &GPUpri[0], q, min( q + PROJ_CHUNK, rp->np), c, z_size, GPUpri[0].mystream, perform_filtration );

                        }
                        
                        // perform count normalization
                        if ( rp->use_norm_vol == 1 ) {
                            
                            volume_normalization<<<rp->vgrid, rp->vblock, 0, GPUpri[0].mystream>>>( &(GPUpub[0].vol_[c*rp->nxy]), &(GPUpub[0].norm_vol_[c*rp->nxy]), make_int3( rp->nx, rp->ny, z_size ), np_sub ); 
                    
                        }
                        
                        // v24: copy out volume data chunk to...
                        //      pinned memory, if the output is on the host
                        //      GPU memory, if the output is on the GPU or managed

                        if ( output_on_GPU == 0 ) { // output on the host

                            cudaMemcpyAsync( &(rp->Pinned_Buffer[c*rp->nxy]), &(GPUpub[0].vol_[c*rp->nxy]), z_size*rp->slice_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            
                            // v24: Record an event we can use to copy data out of the pinned memory on the host.
                            cudaEventCreateWithFlags( &done_events[cnt], cudaEventDisableTiming );
                            cudaEventRecord( done_events[cnt], GPUpri[0].mystream );
                            cnt++;

                        }
                        else { // output on the GPU or managed

                            cudaMemcpyAsync( &(X[c*rp->nxy]), &(GPUpub[0].vol_[c*rp->nxy]), z_size*rp->slice_size, cudaMemcpyDefault, GPUpri[0].mystream );

                        }

                    }
                    
                    // If the output is on the host, copy out of pinned memory as soon as possible.
                    if ( output_on_GPU == 0 ) {

                        for ( cnt_host = 0; cnt_host < cnt; cnt_host++ ) {

                            cudaEventSynchronize( done_events[cnt_host] );

                            c = vol_start + VOL_CHUNK*cnt_host;
                            
                            memcpy( &(X[c*rp->nxy]), &(rp->Pinned_Buffer[c*rp->nxy]), min( vol_end - c, VOL_CHUNK ) * rp->slice_size );

                        }

                        free(done_events);

                    }

            }

        } // implicit synchronization between threads
        
        // Check for outstanding CUDA errors
    
            check(2);
            
        // Make sure all devices are done
            
            for ( int i = 0; i < rp->nDevices; i++ ) {

                GPUid = rp->GPU_indices[ i ];

                cudaSetDevice( GPUid );
                
                cudaDeviceSynchronize();
                
            }

}

// Distribute projections over GPUs and GPU streams. Each GPU stream is assigned its own CPU thread using OpenMP.
void DD_project_cuda( float *Y, float *X, int output_on_GPU ) {
    
    // Check for any outstanding CUDA errors
    
        check(0);
        
    // for each GPU and stream
    
        // declare private variables
        int tid;
        Private_GPU_Data *GPUpri;
        Public_GPU_Data  *GPUpub;
        int GPUid, GPUnum, c, p, q, cnt, cnt_host;
        int proj_start, proj_end;
        int z_size, p_size;

        // make sure we will cover all projections
        // Cannot assume this is evenly divisible by a certain number! (e.g. subsets, prime numbers of projections, weighted temporal problems, ...)
        int proj_increment = ( rp->np + NUMSTREAMS * rp->nDevices - 1) / ( NUMSTREAMS * rp->nDevices ); // round up w/ integer truncation
        
    // Copy data to each GPU
    // Do this in its own pragma to avoid difficult thread/stream synchronization issues.
    #pragma omp parallel private( tid, GPUpub, GPUid, GPUnum ) default( shared ) num_threads(THREAD_COUNT)
    {

        // Which CPU thread are we running on?

            tid = omp_get_thread_num();
            
        if ( ( tid < NUMSTREAMS*rp->nDevices ) && ( ( tid % NUMSTREAMS ) == 0 ) ) {
            
                // which GPU are we working on?
            
                GPUnum = tid / NUMSTREAMS; // integer truncation
                GPUid = rp->GPU_indices[ GPUnum ];
                cudaSetDevice( GPUid );
                
            // Direct private pointers to the correct data structures within rp

                GPUpub = &(rp->GPU_pub[GPUnum]);
                
            // Copy data once per GPU
         
                cudaMemcpy( GPUpub[0].vol_, X, rp->vol_size, cudaMemcpyDefault );
            
        }

    }
        
    #pragma omp parallel private( tid, GPUpri, GPUpub, GPUid, GPUnum, proj_start, proj_end, z_size, p_size, c, p, q, cnt, cnt_host ) default( shared ) num_threads(THREAD_COUNT)
    {
        
        // Which CPU thread are we running on?

            tid = omp_get_thread_num();
        
        // Since we need to have a static number of threads, leave threads that we are not used unoccupied.
        if ( tid < NUMSTREAMS*rp->nDevices ) {
            
            // which GPU are we working on?
            
                GPUnum = tid / NUMSTREAMS; // integer truncation
                GPUid = rp->GPU_indices[ GPUnum ];
                cudaSetDevice( GPUid );
                
            // Direct private pointers to the correct data structures within rp
            
                GPUpri = &(rp->GPU_pri[GPUnum*NUMSTREAMS + (tid % NUMSTREAMS)]);
                GPUpub = &(rp->GPU_pub[GPUnum]);

            // Which projections is this stream / thread responsible for?
                
                proj_start = tid * proj_increment;
                proj_end   = min( (tid + 1) * proj_increment, (int) rp->np ); // Make sure we do not go out of bounds!
                
            // Allocate events to control data flow on the host
                
                cudaEvent_t *done_events;
                if ( output_on_GPU == 0 ) {
                    
                    done_events = (cudaEvent_t *) malloc( ( ( proj_end - proj_start + PROJ_CHUNK - 1 ) / PROJ_CHUNK ) * sizeof(cudaEvent_t) );
                    cnt = 0;

                }

            // Project host data in chunks
                
                for ( q = proj_start; q < proj_end; q += PROJ_CHUNK ) {
                    
                    p_size = min( PROJ_CHUNK, proj_end - q);
                    
                    // v24: Make sure all non-zero weighted projections begin at zero
                    for ( p = q; p < (p_size + q); p++) {
                        
                        if ( weights_sub[p] == 0 ) continue;
                     
                        cudaMemsetAsync( &(GPUpub[0].proj_[p*rp->nuv]), 0, rp->proj_size, GPUpri[0].mystream);
                        
                    }

                    // loop over volume chunks
                    for ( c = 0; c < rp->nz ; c += VOL_CHUNK ) {
                        
                        z_size = min( VOL_CHUNK, rp->nz - c );

                        // Project chunk and add to projection data
                        DD_project_cuda( &GPUpub[0], &GPUpri[0], q, min( q + PROJ_CHUNK, proj_end), c, z_size, GPUpri[0].mystream );

                    }
                    
                    // v24: copy out all non-zero weighted projections to...
                    //      pinned memory, if the output is on the host
                    //      GPU memory, if the output is on the GPU or managed
                    for ( p = q; p < (p_size + q); p++) {
                     
                        if ( weights_sub[p] == 0 ) continue;
                        
                        if ( output_on_GPU == 0 ) { // output on the host
                            
                            cudaMemcpyAsync( &(rp->Pinned_Buffer[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            // cudaMemPrefetchAsync( &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaCpuDeviceId, GPUpri[0].mystream );
                            
                        }
                        else { // output on the GPU
                            
                            cudaMemcpyAsync( &(Y[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size, cudaMemcpyDefault, GPUpri[0].mystream );
                            
                        }
                        
                    }
                    
                    if ( output_on_GPU == 0 ) {
                    
                        // v24: Record an event we can use to copy data out of the pinned memory on the host.
                        cudaEventCreateWithFlags( &done_events[cnt], cudaEventDisableTiming );
                        cudaEventRecord( done_events[cnt], GPUpri[0].mystream );
                        cnt++;
                        
                    }
                        
                }
                
                // If the output is on the host, copy out of pinned memory as soon as possible.
                if ( output_on_GPU == 0 ) {
                
                    for ( cnt_host = 0; cnt_host < cnt; cnt_host++ ) {
                        
                        cudaEventSynchronize( done_events[cnt_host] );

                        q = proj_start + PROJ_CHUNK*cnt_host;

                        p_size = min( PROJ_CHUNK, proj_end - q );

                        for ( p = q; p < (q + p_size); p++ ) {
                            
                            if ( weights_sub[p] == 0 ) continue;
                            
                            memcpy( &(Y[p*rp->nuv]), &(rp->Pinned_Buffer[p*rp->nuv]), rp->proj_size );
                            // memcpy( &(Y[p*rp->nuv]), &(GPUpub[0].proj_[p*rp->nuv]), rp->proj_size );

                        }

                    }
                    
                    free(done_events);
                
                }

        }
        
    }
    
    // Check for any outstanding CUDA errors
    
        check(1);
        
    // Make sure all GPUs are done before returning
            
        for ( int i = 0; i < rp->nDevices; i++ ) {

            GPUid = rp->GPU_indices[ i ];

            cudaSetDevice( GPUid );

            cudaDeviceSynchronize();

        }

}


// test code
// void bicgstab_cuda() {
//     
//     // initialize projection / backprojection operators
//     initialize();
//     
//     init_bicgstab();
//     
//     // allocate streams
// 
//     cudaStream_t streams[NUMSTREAMS];
//     for (int s = 0; s < NUMSTREAMS; s++) {
// 
//         if(cudaStreamCreate(&streams[s]) != 0) {
// 
//             mexErrMsgTxt("Stream allocation error!");
// 
//         }
// 
//     }
//     
//     if (filtration) {
// 
//         for (int s = 0; s < NUMSTREAMS; s++) {
// 
//             cufftPlan1d(&fftplan[s],nu_fft,CUFFT_C2C,nv);
//             cufftPlan1d(&ifftplan[s],nu_fft,CUFFT_C2C,nv);
// 
//         }
// 
//     }
//     
//     // read b
//     float *b_;
//     cudaMalloc( (void **) &b_, vol_size);
//     cudaMemcpy(b_, b, vol_size, cudaMemcpyHostToDevice);
//     
//     // read Q
//     float *Q_;
//     cudaMalloc( (void **) &Q_, projs_size);
//     // cudaMemset( Q_, 0, projs_size);
//     // cudaMemcpy(Q_, Q, projs_size, cudaMemcpyHostToDevice);
//     
//     // try projection
//     DD_project_cuda(b_, Q_, streams, 1); // (float *vol_, float *projs_)
//     
//     // try (filtered) backprojection
//     DD_backproject_cuda(Q_, b_, streams); // (float *projs_, float *vol_)
//     
// //     // try projection again
// //     DD_project_cuda(b_, Q_); // (float *vol_, float *projs_)
// //     
// //     // try (filtered) backprojection again
// //     DD_backproject_cuda(Q_, b_); // (float *projs_, float *vol_)
//     
//     // copy results back to host
//     cudaMemcpy(xmin, b_, vol_size, cudaMemcpyDeviceToHost);
//     
// }

