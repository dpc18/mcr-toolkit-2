
#ifndef DD_BICGSTAB_CUDA
#define DD_BICGSTAB_CUDA

    extern float *b;
    extern float *x0;
    extern float mu;
    extern size_t iter;

    // outputs
    extern float *xmin;
    extern float *resvec;
    
    // Q
    extern int use_Q;
    extern float *Q;
    
    // small constant
    extern float eps;
    
    // subsets
    extern int *subsets;
    extern int subsets_len;
    

#endif // DD_BICGSTAB_CUDA