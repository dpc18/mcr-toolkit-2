// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __pSVT_CUDA__
#define __pSVT_CUDA__

__host__ __device__ float pythag(float a, float b);
__global__ void  pSVT( float *X, float *Xf, int* count, int *om, float *threshs, float nu, int3 sz, int e, int dsum, float multiplier );
__global__ void  normalize( float *Xf, int* count, int3 sz, int e);
void pSVT_cuda(float **Xcs, float **Xfcs, int c, int *szs, int e, float *threshs, int w, float nu, float multiplier);

#endif // __pSVT_CUDA__