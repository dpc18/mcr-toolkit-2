// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "mex.h"
#include <cuda_runtime.h>
#include "pSVT.h"

// Mex Interface with Matlab

// Inputs: X (data), c (sub-band num), szs (sub-band sizes), e (col. num), threshs (singular value thresholds),
//         w (window radius), nu (Huber / sub-convex exponential scaling factor)
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
 
    // Check for the appropriate number of arguments
    if (nrhs != 8) mexErrMsgTxt("Invalid number of input arguments! (8 expected)");
    
    // Check the precision of the inputs
    if (!mxIsClass(prhs[0],"single")){ // X
        mexErrMsgTxt("First input argument (data) must be single precision.");  
    }
    
    if (!mxIsClass(prhs[1],"int32")){ // c
        mexErrMsgTxt("Second input argument (number of sub-bands) must be int32.");  
    }
    
    if (!mxIsClass(prhs[2],"int32")){ // szs
        mexErrMsgTxt("Third input argument (sub-band sizes) must be int32.");  
    }
    
    if (!mxIsClass(prhs[3],"int32")){ // e
        mexErrMsgTxt("Fourth input argument (number of energies, timepoints, etc.) must be int32.");   
    }
    
    if (!mxIsClass(prhs[4],"single")){ // threshs
        mexErrMsgTxt("Fifth input argument (singular value thresholds) must be single precision.");   
    }
    
    if (!mxIsClass(prhs[5],"int32")){ // w
        mexErrMsgTxt("Sixth input argument (window radius) must be int32.");
    }
    
    if (!mxIsClass(prhs[6],"single")){ // nu
        mexErrMsgTxt("Seventh input argument (exponential scaling factor) must be single precision.");
    }
    
    if (!mxIsClass(prhs[7],"single")){ // multiplier
        mexErrMsgTxt("Eighth input argument (threshold multiplier) must be single precision.");
    }
    
    // Get the size for the output size and dimensions
    const mwSize *out_size = mxGetDimensions(prhs[0]);
    mwSize out_dims = mxGetNumberOfDimensions(prhs[0]);
    
    // Parse inputs (X, nvols, ntimes, A, At, s, w, m)
    float* X = (float*) mxGetData(prhs[0]);
    int c = *((int*) mxGetData(prhs[1]));
    int* szs = (int*) mxGetData(prhs[2]);
    int e = *((int*) mxGetData(prhs[3]));
    float* threshs = (float*) mxGetData(prhs[4]);
    int w = *((int*) mxGetData(prhs[5]));
    float nu = *((float*) mxGetData(prhs[6]));
    float multiplier = *((float*) mxGetData(prhs[7]));
    
    // Check to make sure the patch size will not exceed the allowable number of threads per block.
    // w = 6 results in 925 threads / block (w = 7+ goes over the 1024 limit)
    // This can probably be exceeded when there is only one thread per block.
    if ( w > 6 ) {
     
        mexErrMsgTxt("Maximum allowable patch radius exceeded (sixth input argument, w <= 6). Note this is a sanity check more than a hard limit.");
        
    }
    
    // Check to make sure we have at least two columns to work with (since we will be doing the SVD across columns)
    if ( e < 2 ) {
     
        mexErrMsgTxt("At least two columns are required to perform a SVD (col. num >= 2).");
        
    }
    
    // Check to make sure the number of energies specified matches the number of columns in X
    const mwSize *X_size = mxGetDimensions(prhs[0]);
    if ( ((int) X_size[1]) != e) {
     
        mexErrMsgTxt("Number of columns in the input data (first input argument) does not match the specified number of columns (fourth input argument).");
        
    }
    
    // Check to make sure a reasonable number of columns has been specified. More columns may require additional debugging.
    if ( e > 12 ) {
     
        mexErrMsgTxt("Cannot work with more than 12 columns. Note this is a sanity check more than a hard limit.");
        
    }
    
    // Check to makes sure the threshs dimension is # columns by # sub-bands (i.e. e thresholds for each of c sub-bands are required)
    const mwSize *threshs_size = mxGetDimensions(prhs[4]);
    if ( (((int) threshs_size[0]) != e) || (((int) threshs_size[1]) != c) ) {
     
        mexErrMsgTxt("Thresholds must be specified in a matrix with size # of columns (e, fourth input argument) by # of sub-bands (c, second input argument).");
        
    }
    
    // Check to make sure the size of the szs vector equals number of # sub-bands * 3
    const mwSize *szs_size = mxGetDimensions(prhs[2]);
    if ( ((int) szs_size[1]) != 3*c ) {
     
        mexErrMsgTxt("The sub-band sizes vector must be a row vector with (number of sub-bands)*3 elements.");
        
    }
    
    // Check to make sure the number of elements in each column matches the specified sizes
    mwSize tot_szs = 0;
    for (int i = 0; i < c; i++) {
     
        tot_szs += szs[3*i]*szs[3*i+1]*szs[3*i+2];
        
    }
    
    if ( ((int) X_size[0]) != ((int) tot_szs) ) {
     
        mexErrMsgTxt("The data (first input argument) must be arranged in column vectors and have a total number of elements matching the specified sizes.");
        
    }
    
    // Allocate output (same size as input)
    plhs[0] = mxCreateNumericArray(out_dims,out_size,mxSINGLE_CLASS,mxREAL);
    float* Xf = (float*) mxGetData(plhs[0]);
    
    // Create an array of offsets to the beginning of each sub-band and column within X and Xf
    float **Xcs = (float**) malloc(c * e * sizeof(float*));
    float **Xfcs = (float**) malloc(c * e * sizeof(float*));
    tot_szs = 0;
    int j = 0;
    for (int i = 0; i < c*e; i++) {
     
        Xcs[i] = X + tot_szs;
        Xfcs[i] = Xf + tot_szs;
        j = i % c;
        tot_szs += szs[3*j]*szs[3*j+1]*szs[3*j+2];
        
    }
    
    // Run pSVT on the GPU
    pSVT_cuda(Xcs, Xfcs, c, szs, e, threshs, w, nu, multiplier);
    
    // Make sure the device is done executing CUDA code
    cudaDeviceSynchronize();
    
    // Free memory allocated by malloc
    free(Xcs);
    free(Xfcs);
    
    // Run a final error check
    // This may return errors from other sources...
    // Return flag = 1 if an error occured
    // flag = 0 if an error did not occur
    cudaError_t error = cudaGetLastError();
    plhs[1] = mxCreateLogicalScalar(error != cudaSuccess);

    if (error != cudaSuccess) {
        
        mexPrintf("CUDA ERROR: %s\n",cudaGetErrorString(error));
        
    }
    
    // Reset the device to free memory and to allow CUDA profiling
    // Also to clear errors
    // cudaDeviceReset();
    
}