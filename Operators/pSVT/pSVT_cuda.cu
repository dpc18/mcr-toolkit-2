// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cuda_runtime.h>
#include "mex.h"
#include "pSVT_cuda.h"
#include <math.h>
// #include <cuda.h>
// #include <thrust/sort.h>
// #include <thrust/execution_policy.h>

//***           ***
//*** Constants ***
//***           ***

// Constants
// __device__ const int MAX_COLS = 12; // maximum number of columns

//***                  ***
//*** Inline functions ***
//***                  ***

// Compute linear index for linear memory reading and writing
// __device__ size_t lindx(int3 coord, int3 vsize) {return vsize.x*vsize.y*coord.z + vsize.x*coord.y + coord.x;};
__device__ unsigned int lindx(int3 coord, int3 vsize) {return vsize.x*vsize.y*coord.z + vsize.x*coord.y + coord.x;};

// retrieve data from a 2D array stored in 1D memory
// assumes indices start from 1
// m is the # of rows
// __forceinline__ __device__ float id(float * mat, int idx1, int idx2, int m) {
//     
//     return mat[(idx1-1)*m + (idx2 - 1)];
//     
// }

//***                     ***
//*** CUDA Kernels - pSVT ***
//***                     ***

// "Computes (a^2 + b^2)^(1/2) without destructive underflow or overflow."
__host__ __device__ float pythag(float a, float b) {
    
    return sqrtf(a*a+b*b);
 
//     float absa, absb;
//     
//     absa = fabsf(a);
//     absb = fabsf(b);
//     
//     if (absa > absb) {
//         
//         return absa*sqrt(1.0+(absb/absa)*(absb/absa));
//         
//     }
//     else {
//         
//         return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+(absa/absb)*(absa/absb)));
//         
//     }
    
}

// TO DO: Fix normalization at edges? Should return exact values when no threshold is specified.
// TO DO: Sort singular values if applying a different threshold to each singular value...?!

// TO DO: Stagger atomic add operations?
// TO DO: Add back in the error message for non-convergence after 30 iterations?
// TO DO: Check if initialization to 0 is needed in shared memory...
// Does adding multiple values to the same voxel via mirroring handle the normalization issue at the edges?
// SVD code adapted from:
// "Numerical Recipes in C: The Art of Scientific Computing" (Second Edition).
// William H. Press, Saul A. Teukolsky, William T. Vetterling, Brian P. Flannery
// CAMBRIDGE UNIVERSITY PRESS 1992
__global__ void  pSVT( float *X, float *Xf, int* count, int *om, float *threshs, float nu, int3 sz, int e, int dsum, float multiplier ) {
 
    // int ix = blockIdx.x*blockDim.x + threadIdx.x;
    // int iy = blockIdx.y*blockDim.y + threadIdx.y;
    
    int ix = blockIdx.x;
    int iy = blockIdx.y;
    int iz = blockIdx.z;
    
    // Not needed given one kernel call per voxel...
    // Do nothing if we have gone out of bounds.
    // if ( ix >= sz.x || iy >= sz.y) return;
    
    int k, k2, j, i;
    int3 cpos;
    unsigned int o = sz.x*sz.y*sz.z;
    
    // SVD variables
    extern __shared__ float pdata[]; // dynamically allocated shared memory for patch data
    
    int m = dsum; // blockDim.x; // rows
    int n = e; // columns
    
    // parse shared memory for each matrix / vector
    float *a = pdata;
    float *w = a + m*n;
    float *v = w + n;
    float *rv1 = v + n*n;
    
    unsigned int offset_X; // , offset_p;
    
    int flag, its, jj, l, nm;
    float anorm, c, f, g, h, s, scale, x, y, z;
    float w0;
    
    // for (int iz = 0; iz < sz.z; iz++) {
    
        for (int tid = 0; tid < dsum; tid++) {

            // Read offsets from precomputed Cartesian offsets within the radial domain.
            i = om[3*tid];
            j = om[3*tid+1];
            k2 = om[3*tid+2];
            cpos = make_int3(ix+i,iy+j,iz+k2);

            // Mirror as needed.
            if (cpos.x < 0) cpos.x = -cpos.x;
            if (cpos.x > sz.x-1) cpos.x = sz.x - 1 - i;

            if (cpos.y < 0) cpos.y = -cpos.y;
            if (cpos.y > sz.y-1) cpos.y = sz.y - 1 - j;

            if (cpos.z < 0) cpos.z = -cpos.z;
            if (cpos.z > sz.z-1) cpos.z = sz.z - 1 - k2;

            offset_X = lindx(cpos, sz);

            // Copy data for each column
            for (l = 0; l < e; l++) {

                a[tid*n + l] = X[l*o + offset_X];

            }

        }

        g = scale = anorm = 0.0;

        for ( i = 1; i <= n; i++) { // for each column

            l = i + 1;

            rv1[i-1] = scale*g; // rv1[i] = scale*g;

            g = s = scale = 0.0;

            if (i <= m) {

                for (k = i; k <= m; k++) scale += fabsf(a[(k-1)*n + (i - 1)]); // fabs(a[k][i]);

                if (scale) {

                    for (k = i; k <= m; k++) {

                        a[(k-1)*n + (i - 1)] /= scale; // a[k][i] /= scale;

                        s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (i - 1)]; // a[k][i]*a[k][i];

                    }

                    f = a[(i-1)*n + (i - 1)]; // a[i][i];

                    g = -copysignf(sqrtf(s),f); // -SIGN(sqrt(s),f);

                    h = f*g - s;

                    a[(i-1)*n + (i - 1)] = f - g; // a[i][i] = f-g;

                    for ( j = l; j <= n; j++ ) {

                        for (s = 0.0, k = i; k <= m; k++) s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (j - 1)]; // a[k][i]*a[k][j];

                        f = s / h;

                        for (k = i; k <= m; k++) a[(k-1)*n + (j - 1)] += f*a[(k-1)*n + (i - 1)]; // a[k][j] += f*a[k][i];

                    }

                    for (k = i; k <= m; k++) a[(k-1)*n + (i - 1)] *= scale; // a[k][i] *= scale;

                }
            }

            w[i-1] = scale * g; // w[i] = scale * g;

            g = s = scale = 0.0;

            if (i <= m && i != n) {

                for (k = l; k <= n; k++) scale += fabsf(a[(i-1)*n + (k - 1)]); // scale += fabs(a[i][k]);

                if (scale) {

                    for (k = l; k <= n; k++) {

                        a[(i-1)*n + (k - 1)] /= scale; // a[i][k] /= scale;
                        s += a[(i-1)*n + (k - 1)]*a[(i-1)*n + (k - 1)]; // a[i][k]*a[i][k];

                    }

                    f = a[(i-1)*n + (l - 1)]; // a[i][l];

                    g = -copysignf(sqrtf(s),f); // -SIGN(sqrt(s),f);

                    h = f*g-s;

                    a[(i-1)*n + (l - 1)] = f - g; // a[i][l] = f-g;

                for (k = l; k <= n; k++) rv1[k-1] = a[(i-1)*n + (k - 1)] / h; // rv1[k] = a[i][k] / h;

                for (j = l; j <= m; j++) {

                    for (s = 0.0, k = l; k <= n; k++) s += a[(j-1)*n + (k - 1)]*a[(i-1)*n + (k - 1)]; // s += a[j][k]*a[i][k];

                    for (k = l; k <= n; k++) a[(j-1)*n + (k - 1)] += s*rv1[k-1]; // a[j][k] += s*rv1[k];

                }

                for (k = l; k <= n; k++) a[(i-1)*n + (k - 1)] *= scale; // a[i][k] *= scale;

                }
            }

            // anorm = FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
            anorm = fmaxf(anorm,fabsf(w[i-1])+fabsf(rv1[i-1]));

        }

        // 2.2) Accumulation of right-hand transformations.

        for (i = n; i >= 1; i--) {

            if (i < n) {

                if (g) {

                    for (j = l; j <= n; j++) // Double division to avoid possible underflow.
                        v[(j-1)*n + (i - 1)] = (a[(i-1)*n + (j - 1)] / a[(i-1)*n + (l - 1)]) / g; // v[j][i]=(a[i][j]/a[i][l])/g;

                    for (j = l; j <= n; j++) {

                        for (s = 0.0, k = l; k <= n; k++) s += a[(i-1)*n + (k - 1)]*v[(k-1)*n + (j - 1)]; // a[i][k]*v[k][j];

                        for (k = l; k <= n; k++) v[(k-1)*n + (j - 1)] += s*v[(k-1)*n + (i - 1)]; // v[k][j] += s*v[k][i];

                    }
                }

                for ( j = l; j <= n; j++) v[(i-1)*n + (j - 1)] = v[(j-1)*n + (i - 1)] = 0.0; // v[i][j]=v[j][i]=0.0;
            }

            v[(i-1)*n + (i - 1)] = 1.0; // v[i][i]=1.0;

            g = rv1[i-1]; // rv1[i];

            l = i;

        }

        // 2.3) Accumulation of left-hand transformations.

        for (i = n; i >= 1; i--) { // assume m >= n // for (i = IMIN(m,n); i >= 1; i--)

            l = i + 1;

            g = w[i-1]; // w[i];

            for (j = l; j <= n; j++) a[(i-1)*n + (j - 1)] = 0.0; // a[i][j]=0.0;

            if (g) {

                g = 1.0 / g;

                for (j = l; j <= n; j++) {

                    for (s = 0.0, k = l; k <= m; k++) s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (j - 1)]; // a[k][i]*a[k][j];

                    f = ( s / a[(i-1)*n + (i - 1)])*g; // (s/a[i][i])*g;

                    for (k = i; k <= m; k++) a[(k-1)*n + (j - 1)] += f*a[(k-1)*n + (i - 1)]; // a[k][j] += f*a[k][i];

                }

                for (j = i; j <= m; j++) a[(j-1)*n + (i - 1)] *= g; // a[j][i] *= g;

            }
            else {

                for (j = i; j <= m; j++) a[(j-1)*n + (i - 1)] = 0.0; // a[j][i]=0.0;

            }

            ++a[(i-1)*n + (i - 1)]; // ++a[i][i];

        }

        // 2.4) Diagonalization of the bidiagonal form: Loop over singular values and over allowed iterations.

        for (k = n; k >= 1; k--) {

            for (its = 1; its <= 30; its++) {

                flag = 1;

                for (l = k; l >= 1; l--) { // Test for splitting.

                    nm = l - 1; // Note that rv1[1] is always zero.

                    if ( (float) (fabsf(rv1[l-1])+anorm) == anorm) { // ((float)(fabs(rv1[l])+anorm) == anorm)

                        flag = 0;

                        break;

                    }

                    if ( (float) (fabsf(w[nm-1]) + anorm) == anorm) break; // ( (float) (fabs(w[nm])+anorm) == anorm) break;

                }

                if (flag) {

                    c = 0.0; // Cancellation of rv1[l], if l > 1.

                    s = 1.0;

                    for ( i = l; i <= k; i++) {

                        f = s*rv1[i-1]; // s*rv1[i];

                        rv1[i-1] = c * rv1[i-1]; // rv1[i] = c*rv1[i];

                        if ((float)(fabsf(f)+anorm) == anorm) break; // ((float)(fabs(f)+anorm) == anorm) break;

                        g = w[i-1]; // w[i];

                        h = pythag(f,g);

                        w[i-1] = h; // w[i]=h;

                        h = 1.0/h;

                        c = g*h;

                        s = -f*h;

                        for (j = 1; j <= m; j++) {

                            y = a[(j-1)*n + (nm - 1)]; // y = a[j][nm];

                            z = a[(j-1)*n + (i - 1)]; // a[j][i];

                            a[(j-1)*n + (nm - 1)] = y*c+z*s; // a[j][nm] = y*c+z*s;

                            a[(j-1)*n + (i - 1)] = z*c-y*s; // a[j][i] = z*c-y*s;

                        }

                    }

                }

                z = w[k - 1]; // w[k];

                if (l == k) { // Convergence.

                    if (z < 0.0) { // Singular value is made nonnegative.

                        w[k-1] = -z; // w[k] = -z;

                        for ( j = 1; j <= n; j++) v[(j-1)*n + (k - 1)] = -v[(j-1)*n + (k - 1)]; // v[j][k] = -v[j][k];

                    }

                    break;
                }

                // if (its == 30) nrerror("no convergence in 30 svdcmp iterations");

                x = w[l-1]; // x = w[l]; // Shift from bottom 2-by-2 minor.

                nm = k-1;

                y = w[nm-1]; // y = w[nm];

                g = rv1[nm-1]; // g = rv1[nm];

                h = rv1[k-1]; // h = rv1[k];

                f = ((y-z)*(y+z)+(g-h)*(g+h)) / (2.0*h*y);

                g = pythag(f,1.0);

                f = ( (x-z) * (x+z) + h*( ( y / (f + copysignf(g , f) ) ) - h ) ) / x; // f = ((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;

                c = s = 1.0; 

                // Next QR transformation:
                for (j = l; j <= nm; j++) {

                    i = j+1;

                    g = rv1[i-1]; // rv1[i];

                    y = w[i-1]; // w[i];

                    h = s * g;

                    g = c * g;

                    z = pythag(f,h);

                    rv1[j-1] = z; // rv1[j] = z;

                    c = f / z;

                    s = h / z;

                    f = x * c + g * s;

                    g = g * c - x * s;

                    h = y * s;

                    y *= c;

                    for (jj = 1; jj <= n; jj++) {

                        x = v[(jj - 1)*n + (j - 1)]; // v[jj][j];

                        z = v[(jj - 1)*n + (i - 1)]; // v[jj][i];

                        v[(jj - 1)*n + (j - 1)] = x * c + z * s; // v[jj][j] = x * c + z * s;

                        v[(jj - 1)*n + (i - 1)] = z * c - x * s; // v[jj][i] = z * c - x * s;

                    }

                    z = pythag(f,h);

                    w[j-1] = z; // w[j] = z; 

                    // Rotation can be arbitrary if z = 0.
                    if (z) {

                        z = 1.0 / z;

                        c = f * z;

                        s = h * z;

                    }

                    f = c * g + s * y;

                    x = c * y - s * g;

                    for (jj = 1; jj <= m; jj++) {

                        y = a[(jj-1)*n + (j - 1)]; // a[jj][j];

                        z = a[(jj-1)*n + (i - 1)]; // a[jj][i];

                        a[(jj-1)*n + (j - 1)] = y * c + z * s; // a[jj][j] = y * c + z * s;

                        a[(jj-1)*n + (i - 1)] = z * c - y * s; // a[jj][i] = z * c - y * s;

                    }

                }

                rv1[l-1] = 0.0; // rv1[l] = 0.0;

                rv1[k-1] = f; // rv1[k] = f;

                w[k-1] = x; // w[k] = x;

            }

        }

        // Use insertion sort...

        // mexPrintf("E, raw: %f %f %f \n",w[0],w[1],w[2]);

        for (i = 0; i < e; i++) rv1[i] = i;

        for (i = 1; i < e; i++) {

            f = w[i]; // values
            z = rv1[i]; // keys

            for (j = i; j >= 1 && f > w[j-1]; j--) {

                w[j] = w[j-1];
                rv1[j] = rv1[j-1];

            }

            w[j] = f;
            rv1[j] = z;

        }

        // mexPrintf("E, sorted: %f %f %f \n",w[0],w[1],w[2]);

        // __syncthreads(); // finish sorting before continuing

        // 4) Apply thresholds to singular values.
        // a = U
        // v = V
        // keys = diag(E)

        // using the first n threads
        w0 = w[0];
        for (int tid = 0; tid < e; tid++) {

            // "Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty"
            // Kim et al. 2015. IEEE Trans. Med. Imaging
            // Huber_shrink = @(u,e,v) u*diag(max(e-threshs'.*(e.^(nu-1)),0))*v';
            // (sub-)convex soft thresholding of non-negative singular values
            if (w[tid] > 0) {

                // w is sorted from greatest to smallest to match threshs
                // w[tid] = fmaxf( w[tid] - multiplier*threshs[tid]*powf( w[tid], nu - 1 ), 0);

                // use normalization for consistency
                // this reduces thresholding around noise in empty patches...
                w[tid] = fmaxf( w[tid] - multiplier*threshs[tid]*powf( w[tid] / w0, nu - 1 ), 0);
                
            }
            else {

                w[tid] = 0;

            }

        }

        // mexPrintf("E, thresholded: %f %f %f \n",w[0],w[1],w[2]);

        // put the thresholded singular values back in the original order for reconstructing the output

        for (i = 1; i < e; i++) {

            f = w[i]; // values
            z = rv1[i]; // keys

            for (j = i; j >= 1 && z < rv1[j-1]; j--) {

                w[j] = w[j-1];
                rv1[j] = rv1[j-1];

            }

            w[j] = f;
            rv1[j] = z;

        }

        // mexPrintf("E, thresholded + unsorted: %f %f %f \n",w[0],w[1],w[2]);

        // 5) Reconstruct patch data from SVD.
        // NOTE: V must be transposed. "pdata" and "a" point to the same memory.
        // NOTE: This would be much more efficient to work on columns of U but would require more shared memory?
        // NOTE: These memory accesses are not coalesced...!
        // pdata = a*diag(w)*v' = U*E*V'

        // Compute E*V' = V*E', store in V 
//         for (int tid = 0; tid < e; tid++) {
// 
//             for (i = 0; i < e; i++) {
// 
//                 v[tid*e + i] *= w[i];
// 
//             }
// 
//         }

        // mexPrintf("V*E: %f %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]);

        // compute U*V'

        for (i = 0; i < m; i++) { // for each row of U

            for (int tid = 0; tid < e; tid++) {

                rv1[tid] = 0;

            }

            for (int tid = 0; tid < e; tid++) {

                for ( k = 0; k < n; k++) { // for each column of U

                    rv1[tid] += a[i*n+k]*v[tid*n+k]*w[k];

                }

            }

            for (int tid = 0; tid < e; tid++) {

                a[i*n+tid] = rv1[tid];

            }

        }

        unsigned int t;
        for (int tid = 0; tid < dsum; tid++) {

            // Read offsets from precomputed Cartesian offsets within the radial domain.
            i = om[3*tid];
            j = om[3*tid+1];
            k2 = om[3*tid+2];
            cpos = make_int3(ix+i,iy+j,iz+k2);

            // Mirror as needed.
            if (cpos.x < 0) cpos.x = -cpos.x;
            if (cpos.x > sz.x-1) cpos.x = sz.x - 1 - i;

            if (cpos.y < 0) cpos.y = -cpos.y;
            if (cpos.y > sz.y-1) cpos.y = sz.y - 1 - j;

            if (cpos.z < 0) cpos.z = -cpos.z;
            if (cpos.z > sz.z-1) cpos.z = sz.z - 1 - k2;

            offset_X = lindx(cpos, sz);

            // Copy data for each column
            for (j = 0; j < e; j++) {

                f = a[tid*n + j];
                t = j*o + offset_X;

                if (f == f) { // nan-check

                    atomicAdd( Xf + t, f);

                }
                else { // copy input to minimize normalization errors and inconsistencies

                    atomicAdd( Xf + t, X[t]);

                }

            }

            atomicAdd( count + offset_X, 1);

        }
        
    // }
    
}

__global__ void  normalize( float *Xf, int* count, int3 sz, int e) {
 
    int ix = blockIdx.x;
    int iy = blockIdx.y;
    int iz = blockIdx.z;
    
    unsigned int o = sz.x*sz.y*sz.z;
    unsigned int idx;
    int c;
    
    //for (int iz = 0; iz < sz.z; iz++) {
        
        idx = iz*sz.x*sz.y + iy*sz.x + ix;
        c = count[idx];
        
        for (int j = 0; j < e; j++) {
         
            Xf[j*o+idx] /= c;
            
        }
        
    //}
    
}

void test_svd2() {
 
    // 2.1) Householder reduction to bidiagonal form.
    
    int m = 4;
    int n = 3;
    
    float a[] = {1.227699395777360,-0.391302644675630,-0.165225771582130,-0.334069188977606,3.459790364192850,1.178394184371760,0.311267973938569,-0.371622940772182,0.446110968982439,0.936741475984751,0.558341546702355,1.164753508512238};
    float b[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    float v[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    float w[] = {0.0,0.0,0.0};
    float rv1[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    
    float threshs[] = {0.4,0.3,0.2};
    
    float nu = 0.7;
    
    int e = n;
    
    int k, j, i;
    int flag, its, jj, l, nm;
    float anorm, c, f, g, h, s, scale, x, y, z;

    g = scale = anorm = 0.0;

    for ( i = 1; i <= n; i++) { // for each column

        l = i + 1;

        rv1[i-1] = scale*g; // rv1[i] = scale*g;

        g = s = scale = 0.0;

        if (i <= m) {

            for (k = i; k <= m; k++) scale += fabsf(a[(k-1)*n + (i - 1)]); // fabs(a[k][i]);

            if (scale) {

                for (k = i; k <= m; k++) {

                    a[(k-1)*n + (i - 1)] /= scale; // a[k][i] /= scale;

                    s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (i - 1)]; // a[k][i]*a[k][i];

                }

                f = a[(i-1)*n + (i - 1)]; // a[i][i];

                g = -copysignf(sqrtf(s),f); // -SIGN(sqrt(s),f);

                h = f*g - s;

                a[(i-1)*n + (i - 1)] = f - g; // a[i][i] = f-g;

                for ( j = l; j <= n; j++ ) {

                    for (s = 0.0, k = i; k <= m; k++) s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (j - 1)]; // a[k][i]*a[k][j];

                    f = s / h;

                    for (k = i; k <= m; k++) a[(k-1)*n + (j - 1)] += f*a[(k-1)*n + (i - 1)]; // a[k][j] += f*a[k][i];

                }

                for (k = i; k <= m; k++) a[(k-1)*n + (i - 1)] *= scale; // a[k][i] *= scale;

            }
        }

        w[i-1] = scale * g; // w[i] = scale * g;

        g = s = scale = 0.0;

        if (i <= m && i != n) {

            for (k = l; k <= n; k++) scale += fabsf(a[(i-1)*n + (k - 1)]); // scale += fabs(a[i][k]);

            if (scale) {

                for (k = l; k <= n; k++) {

                    a[(i-1)*n + (k - 1)] /= scale; // a[i][k] /= scale;
                    s += a[(i-1)*n + (k - 1)]*a[(i-1)*n + (k - 1)]; // a[i][k]*a[i][k];

                }

                f = a[(i-1)*n + (l - 1)]; // a[i][l];

                g = -copysignf(sqrtf(s),f); // -SIGN(sqrt(s),f);

                h = f*g-s;

                a[(i-1)*n + (l - 1)] = f - g; // a[i][l] = f-g;

            for (k = l; k <= n; k++) rv1[k-1] = a[(i-1)*n + (k - 1)] / h; // rv1[k] = a[i][k] / h;

            for (j = l; j <= m; j++) {

                for (s = 0.0, k = l; k <= n; k++) s += a[(j-1)*n + (k - 1)]*a[(i-1)*n + (k - 1)]; // s += a[j][k]*a[i][k];

                for (k = l; k <= n; k++) a[(j-1)*n + (k - 1)] += s*rv1[k-1]; // a[j][k] += s*rv1[k];

            }

            for (k = l; k <= n; k++) a[(i-1)*n + (k - 1)] *= scale; // a[i][k] *= scale;

            }
        }

        // anorm = FMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
        anorm = fmaxf(anorm,fabsf(w[i-1])+fabsf(rv1[i-1]));

    }

    // 2.2) Accumulation of right-hand transformations.

    for (i = n; i >= 1; i--) {

        if (i < n) {

            if (g) {

                for (j = l; j <= n; j++) // Double division to avoid possible underflow.
                    v[(j-1)*n + (i - 1)] = (a[(i-1)*n + (j - 1)] / a[(i-1)*n + (l - 1)]) / g; // v[j][i]=(a[i][j]/a[i][l])/g;

                for (j = l; j <= n; j++) {

                    for (s = 0.0, k = l; k <= n; k++) s += a[(i-1)*n + (k - 1)]*v[(k-1)*n + (j - 1)]; // a[i][k]*v[k][j];

                    for (k = l; k <= n; k++) v[(k-1)*n + (j - 1)] += s*v[(k-1)*n + (i - 1)]; // v[k][j] += s*v[k][i];

                }
            }

            for ( j = l; j <= n; j++) v[(i-1)*n + (j - 1)] = v[(j-1)*n + (i - 1)] = 0.0; // v[i][j]=v[j][i]=0.0;
        }

        v[(i-1)*n + (i - 1)] = 1.0; // v[i][i]=1.0;

        g = rv1[i-1]; // rv1[i];

        l = i;

    }

    // 2.3) Accumulation of left-hand transformations.

    for (i = n; i >= 1; i--) { // assume m >= n // for (i = IMIN(m,n); i >= 1; i--)

        l = i + 1;

        g = w[i-1]; // w[i];

        for (j = l; j <= n; j++) a[(i-1)*n + (j - 1)] = 0.0; // a[i][j]=0.0;

        if (g) {

            g = 1.0 / g;

            for (j = l; j <= n; j++) {

                for (s = 0.0, k = l; k <= m; k++) s += a[(k-1)*n + (i - 1)]*a[(k-1)*n + (j - 1)]; // a[k][i]*a[k][j];

                f = ( s / a[(i-1)*n + (i - 1)])*g; // (s/a[i][i])*g;

                for (k = i; k <= m; k++) a[(k-1)*n + (j - 1)] += f*a[(k-1)*n + (i - 1)]; // a[k][j] += f*a[k][i];

            }

            for (j = i; j <= m; j++) a[(j-1)*n + (i - 1)] *= g; // a[j][i] *= g;

        }
        else {

            for (j = i; j <= m; j++) a[(j-1)*n + (i - 1)] = 0.0; // a[j][i]=0.0;

        }

        ++a[(i-1)*n + (i - 1)]; // ++a[i][i];

    }

    // 2.4) Diagonalization of the bidiagonal form: Loop over singular values and over allowed iterations.

    for (k = n; k >= 1; k--) {

        for (its = 1; its <= 30; its++) {

            flag = 1;

            for (l = k; l >= 1; l--) { // Test for splitting.

                nm = l - 1; // Note that rv1[1] is always zero.

                if ( (float) (fabsf(rv1[l-1])+anorm) == anorm) { // ((float)(fabs(rv1[l])+anorm) == anorm)

                    flag = 0;

                    break;

                }

                if ( (float) (fabsf(w[nm-1]) + anorm) == anorm) break; // ( (float) (fabs(w[nm])+anorm) == anorm) break;

            }

            if (flag) {

                c = 0.0; // Cancellation of rv1[l], if l > 1.

                s = 1.0;

                for ( i = l; i <= k; i++) {

                    f = s*rv1[i-1]; // s*rv1[i];

                    rv1[i-1] = c * rv1[i-1]; // rv1[i] = c*rv1[i];

                    if ((float)(fabsf(f)+anorm) == anorm) break; // ((float)(fabs(f)+anorm) == anorm) break;

                    g = w[i-1]; // w[i];

                    h = pythag(f,g);

                    w[i-1] = h; // w[i]=h;

                    h = 1.0/h;

                    c = g*h;

                    s = -f*h;

                    for (j = 1; j <= m; j++) {

                        y = a[(j-1)*n + (nm - 1)]; // y = a[j][nm];

                        z = a[(j-1)*n + (i - 1)]; // a[j][i];

                        a[(j-1)*n + (nm - 1)] = y*c+z*s; // a[j][nm] = y*c+z*s;

                        a[(j-1)*n + (i - 1)] = z*c-y*s; // a[j][i] = z*c-y*s;

                    }

                }

            }

            z = w[k - 1]; // w[k];

            if (l == k) { // Convergence.

                if (z < 0.0) { // Singular value is made nonnegative.

                    w[k-1] = -z; // w[k] = -z;

                    for ( j = 1; j <= n; j++) v[(j-1)*n + (k - 1)] = -v[(j-1)*n + (k - 1)]; // v[j][k] = -v[j][k];

                }

                break;
            }

            // if (its == 30) nrerror("no convergence in 30 svdcmp iterations");

            x = w[l-1]; // x = w[l]; // Shift from bottom 2-by-2 minor.

            nm = k-1;

            y = w[nm-1]; // y = w[nm];

            g = rv1[nm-1]; // g = rv1[nm];

            h = rv1[k-1]; // h = rv1[k];

            f = ((y-z)*(y+z)+(g-h)*(g+h)) / (2.0*h*y);

            g = pythag(f,1.0);

            f = ( (x-z) * (x+z) + h*( ( y / (f + copysignf(g , f) ) ) - h ) ) / x; // f = ((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;

            c = s = 1.0; 

            // Next QR transformation:
            for (j = l; j <= nm; j++) {

                i = j+1;

                g = rv1[i-1]; // rv1[i];

                y = w[i-1]; // w[i];

                h = s * g;

                g = c * g;

                z = pythag(f,h);

                rv1[j-1] = z; // rv1[j] = z;

                c = f / z;

                s = h / z;

                f = x * c + g * s;

                g = g * c - x * s;

                h = y * s;

                y *= c;

                for (jj = 1; jj <= n; jj++) {

                    x = v[(jj - 1)*n + (j - 1)]; // v[jj][j];

                    z = v[(jj - 1)*n + (i - 1)]; // v[jj][i];

                    v[(jj - 1)*n + (j - 1)] = x * c + z * s; // v[jj][j] = x * c + z * s;

                    v[(jj - 1)*n + (i - 1)] = z * c - x * s; // v[jj][i] = z * c - x * s;

                }

                z = pythag(f,h);

                w[j-1] = z; // w[j] = z; 

                // Rotation can be arbitrary if z = 0.
                if (z) {

                    z = 1.0 / z;

                    c = f * z;

                    s = h * z;

                }

                f = c * g + s * y;

                x = c * y - s * g;

                for (jj = 1; jj <= m; jj++) {

                    y = a[(jj-1)*n + (j - 1)]; // a[jj][j];

                    z = a[(jj-1)*n + (i - 1)]; // a[jj][i];

                    a[(jj-1)*n + (j - 1)] = y * c + z * s; // a[jj][j] = y * c + z * s;

                    a[(jj-1)*n + (i - 1)] = z * c - y * s; // a[jj][i] = z * c - y * s;

                }

            }

            rv1[l-1] = 0.0; // rv1[l] = 0.0;

            rv1[k-1] = f; // rv1[k] = f;

            w[k-1] = x; // w[k] = x;

        }

    }
    // free_vector(rv1,1,n);

    // __syncthreads(); // finish computing the SVD before continuting

    // 3) Sort singular values for use with variable thresholds

    // Use thrust

    // reuse rv1 as the keys for the sort
    // for (i = 0; i < e; i++) {

        // rv1[i] = i;

    // }

    // thrust::sort_by_key(thrust::seq, rv1, rv1+e, w);

    // Use insertion sort...
    
    mexPrintf("E, raw: %f %f %f \n",w[0],w[1],w[2]);

    for (i = 0; i < e; i++) rv1[i] = i;

    for (i = 1; i < e; i++) {

        f = w[i]; // values
        z = rv1[i]; // keys

        for (j = i; j >= 1 && f > w[j-1]; j--) {

            w[j] = w[j-1];
            rv1[j] = rv1[j-1];

        }

        w[j] = f;
        rv1[j] = z;

    }
    
    mexPrintf("E, sorted: %f %f %f \n",w[0],w[1],w[2]);

    // __syncthreads(); // finish sorting before continuing

    // 4) Apply thresholds to singular values.
    // a = U
    // v = V
    // keys = diag(E)

    // using the first n threads
    for (int tid = 0; tid < e; tid++) {

        // "Sparse-view spectral CT reconstruction using spectral patch-based low-rank penalty"
        // Kim et al. 2015. IEEE Trans. Med. Imaging
        // Huber_shrink = @(u,e,v) u*diag(max(e-threshs'.*(e.^(nu-1)),0))*v';
        // (sub-)convex soft thresholding of non-negative singular values
        if (w[tid] > 0) {

            // w is sorted from greatest to smallest to match threshs
            w[tid] = fmaxf( w[tid] - threshs[tid]*powf( w[tid], nu - 1 ), 0);

        }
        else {

            w[tid] = 0;

        }
        
    }
    
    mexPrintf("E, thresholded: %f %f %f \n",w[0],w[1],w[2]);

    // put the thresholded singular values back in the original order for reconstructing the output

    for (i = 1; i < e; i++) {

        f = w[i]; // values
        z = rv1[i]; // keys

        for (j = i; j >= 1 && z < rv1[j-1]; j--) {

            w[j] = w[j-1];
            rv1[j] = rv1[j-1];

        }

        w[j] = f;
        rv1[j] = z;

    }
    
    mexPrintf("E, thresholded + unsorted: %f %f %f \n",w[0],w[1],w[2]);

    // 5) Reconstruct patch data from SVD.
    // NOTE: V must be transposed. "pdata" and "a" point to the same memory.
    // NOTE: This would be much more efficient to work on columns of U but would require more shared memory?
    // NOTE: These memory accesses are not coalesced...!
    // pdata = a*diag(w)*v' = U*E*V'

    // Compute E*V' = V*E', store in V 
    for (int tid = 0; tid < e; tid++) {
    
        for (i = 0; i < e; i++) {

            v[tid*e + i] *= w[i];

        }
    
    }
    
    mexPrintf("V*E: %f %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]);
    
    // compute U*V'
    
    for (i = 0; i < m; i++) { // for each row of U

        for (int tid = 0; tid < e; tid++) {

            b[tid] = 0;

        }

        for (int tid = 0; tid < e; tid++) {

            for ( k = 0; k < n; k++) { // for each column of U

                b[tid] += a[i*n+k]*v[tid*n+k];

            }

        }

        for (int tid = 0; tid < e; tid++) {

            a[i*n+tid] = b[tid];

        }

    }
    
    mexPrintf("Final X: %f %f %f %f %f %f %f %f %f %f %f %f \n",a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11]);
    mexPrintf("Final E: %f %f %f \n",w[0],w[1],w[2]);
    mexPrintf("Final V: %f %f %f %f %f %f %f %f %f \n",v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8]);
    
}

// call patch-based singular value thresholding (pSVT)
// Xcs: pointers to the beginning of each sub-band and column within the input data
// Xfcs: pointers to the beginning of each sub-band and column within the output data
// c: number of sub-bands in each column
// szs: volume size of each sub-band
// e: number of columns
// threshs: singular value thresholds
// w: patch radius
// nu: (sub-)convex exponential scaling parameter
// TO DO: Use multiple kernel calls to avoid overlapping atomic memory operations?
// TO DO: Dynamically allocate more L1 vs. more shared memory based on patch size and e? (i.e. balance ILP against shared memory requirements)
// TO DO: Do the normalization counting within the kernel to save time / memory?
void pSVT_cuda(float **Xcs, float **Xfcs, int c, int *szs, int e, float *threshs, int w, float nu, float multiplier) {
    
    // 1) Make sure the hardware can allocate enough shared memory to perform the SVD on each patch. (48 kB shared memory configuration assumed)
    //    Titan X (Maxwell): 48 kB shared memory / block max, 64 kB total shared memory / SM
    int i, j, k;
    int dsum = 0;
    for (i = -w; i <= w; i++) { // compute the number of voxels inside the patch
        for (j = -w; j <= w; j++) {
            for (k = -w; k <= w; k++) {

                if (sqrtf((float) (i*i + j*j + k*k)) > 1.01*w) continue;

                dsum++;

            }
        }
    }
    
    // check to make sure the assumption of # of rows >= # of columns holds
    if (dsum < e) {
        
        mexErrMsgTxt("Assumption that number of rows in the input data >= number of columns has been violated.");
        
    }
    
    if (dsum > 1024) {
        
        mexErrMsgTxt("The number of non-zero radial domain elements exceeds 1024. This is a sanity check rather than a hard limit.");
        
    }
    
    // dim3 bsize = dim3(dsum,1,1); // 1 thread per voxel inside the patch radius
    dim3 bsize = dim3(1,1,1); // task-level parallelism => make sure we can juggle as many concurrent blocks as possible since each one has a lot of work to do
    dim3 gsize;
    
    unsigned int V_bytes = e*e*sizeof(float);
    unsigned int E_bytes = e*sizeof(float);
    unsigned int U_bytes = dsum*e*sizeof(float);
    unsigned int rv1_bytes = e*sizeof(float);
    
    unsigned int B_per_block = V_bytes + E_bytes + U_bytes + rv1_bytes;
    float kB_per_block = B_per_block / 1024.0f;
    float blocks_per_SM = 48 / kB_per_block;
    
    // Note: This is put in as a safe guard in case the hardwired maximum number of columns (originally 12) is ever increased.
    // With w = 6 and e = 12, this limit is not reached (~44 kB used).
    if (blocks_per_SM <= 1.0) {
     
        mexErrMsgTxt("Combination of patch radius and # of columns exceeds hardware limits (single precision, 48 kB shared memory / block limit assumed).");
        
    }
    
    cudaDeviceSetCacheConfig(cudaFuncCachePreferShared); // make sure 48 kB is available
    
    // Create a map of Cartesian offsets within the radial domain to facilitate thread <=> global memory mappings.
    unsigned int count = 0;
    int *offset_map = (int *) malloc( 3*dsum*sizeof(int) );
    for (i = -w; i <= w; i++) { // compute the number of voxels inside the patch
        for (j = -w; j <= w; j++) {
            for (k = -w; k <= w; k++) {

                if (sqrtf((float) (i*i + j*j + k*k)) > 1.01*w) continue;

                offset_map[count] = i;
                offset_map[count+1] = j;
                offset_map[count+2] = k;

                count += 3;

            }
        }
    }
    
    int *offset_map_;
    cudaMalloc( (void **) &offset_map_, 3*dsum*sizeof(int));
    cudaMemcpy(offset_map_, offset_map, 3*dsum*sizeof(int), cudaMemcpyHostToDevice);
    
    // 2) Find the max sub-band size in voxels to minimize GPU memory allocation.
    
    unsigned int max_size = 0;
    unsigned int sz;
    for (i = 0; i < c; i ++) {
        
        sz = szs[i*3]*szs[i*3+1]*szs[i*3+2];
     
        if ( sz > max_size ) {
            
            max_size = sz;
            
        }
        
    }
    
    // Also compare the minimum dimension size with the kernel radius to prevent illegal memory access.
    unsigned int min_dim = max_size;
    for (i = 0; i < c; i ++) {
        
        if ( szs[i*3] < min_dim ) min_dim = szs[i*3];
        if ( szs[i*3+1] < min_dim ) min_dim = szs[i*3+1];
        if ( szs[i*3+2] < min_dim ) min_dim = szs[i*3+2];
        
    }
    
    if ( min_dim < (2*w+1) ) {
        
        mexErrMsgTxt("Minimum volume dimension must be >= kernel size (2*w+1).");
        
    }
    
    float *X_, *Xf_, *threshs_;
    int *count_;
    cudaMalloc( (void **) &X_, e*max_size*sizeof(float));
    cudaMalloc( (void **) &Xf_, e*max_size*sizeof(float));
    cudaMalloc( (void **) &threshs_, e*sizeof(float));
    cudaMalloc( (void **) &count_, max_size*sizeof(int));
    
    int3 sz_;
    unsigned int n;
    
    for (i = 0; i < c; i++) { // for each sub-band, perform pSVT
        
        // 3) Copy current sub-band to the GPU
        sz_ = make_int3(szs[i*3],szs[i*3+1],szs[i*3+2]);
        n = szs[i*3]*szs[i*3+1]*szs[i*3+2];
        
        for (j = 0; j < e; j++) {
         
            cudaMemcpy(X_+n*j, Xcs[c*j + i], n*sizeof(float), cudaMemcpyHostToDevice);
            
        }
        
        // since we are going to add to these vectors, make sure they are zero to start with
        cudaMemset(Xf_,0.0,e*max_size*sizeof(float));
        cudaMemset(count_,0,max_size*sizeof(int));
        
        // 4) Set the appropirate grid size. Each x, y voxel gets a separate call.
        // Use ILP along z to minimize overhead and to improve L1 cache hits.
        // Limited benefit given 48 kB shared memory vs. 16 kB L1...
        gsize = dim3(sz_.x,sz_.y,sz_.z);
        
        // 5) Extract appropriate thresholds.
        cudaMemcpy(threshs_, threshs+i*e, e*sizeof(float), cudaMemcpyHostToDevice);
        
        // 6) Perform patch-based singular value thresholding
        
        pSVT<<<gsize, bsize, B_per_block>>>( X_, Xf_, count_, offset_map_, threshs_, nu, sz_, e, dsum, multiplier );
        
        // 7) intensity normalization to fix edges
        normalize<<<gsize, bsize>>>( Xf_, count_, sz_, e);
        
        // test_svd2();
        
        // 8) Copy thresholded sub-band back to host.
        for (j = 0; j < e; j++) {
         
            cudaMemcpy(Xfcs[c*j + i], Xf_+n*j, n*sizeof(float), cudaMemcpyDeviceToHost);
            
        }
        
    }
    
    // 9) Free malloc'd variables.
    free(offset_map);
    
    // Free cuda arrays
    cudaFree(X_);
    cudaFree(Xf_);
    cudaFree(threshs_);
    cudaFree(count_);
    cudaFree(offset_map_);
    
}
