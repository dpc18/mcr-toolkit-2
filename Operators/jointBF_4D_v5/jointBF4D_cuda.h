// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef __JOINTBF4D_H__
#define __JOINTBF4D_H__

// Constants

    __device__ const int MAX_NVOLS = 10;       // maximum number of volumes which can be jointly filtered
    __device__ const int KERNEL_DIAMETER = 13; // currently, 2*w+1 must equal this expected kernel diameter (i.e. w = 6)
    const int B_SIZE_2D = 16;                  // 2D slice operations
    const int B_SIZE_3D = 8;                   // 3D buffer operations
    const int B_SIZE_3D_2D = 8;                // Expensive 3D operations which output a 2D slice (e.g. BF)
    __device__ const float EPS = 1e-12;        // prevent division by 0

//Inline functions

    // Compute linear index for linear memory reading and writing
    __device__ uint64_t lindx(int3 coord, int3 vsize) {return vsize.x*vsize.y*coord.z + vsize.x*coord.y + coord.x;};
    
// Function headers
    
    __global__ void jointBF_4D( const float *X, const float *N, float *S, const float *m,
                            unsigned int nvols, unsigned int ntimes, int3 vsize, int w, int z_slice);
    
    __global__ void conv3D_separable( float *Slice, const float *X, const float *A, const int dim,
                                      const int3 vsize, const int w, const int3 ksize, const unsigned int z_slice );
    
    __global__ void weightedpluscopy2D(float* out, float* in, float weight, const int3 vsize);
    
    __global__ void grid_MAD_2D_32(const float *slice_2D_in, float *slice_2D_out, float norm_val, int offset, int szx, int szy);
    
    void jointBF4D_cuda(const float **X, const float **Xr, float **Xf, float **Xn,
                        const float *A, const float *At, const float* m, 
                        uint64_t sx, uint64_t sy, uint64_t sz,
                        int w, uint64_t nvols, uint64_t ntimes,
                        int return_noise_map, int perform_resampling);
 

#endif // __JOINTBF4D_H__
