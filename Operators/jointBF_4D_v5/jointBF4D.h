// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef JOINTBF4D
#define JOINTBF4D

// Global Variables //

    uint64_t sx, sy, sz;
    
    const float **Xpts, **Xrpts;
    float **Xfpts, **Xnpts;
    
// Function Declarations

    void validate_GPU( int gpu_idx ); // Make sure the requested GPU is valid before setting the active device
    
// External Functions //

    extern void jointBF4D_cuda(const float **X, const float **Xr, float **Xf, float **Xn,
                               const float *A, const float *At, const float* m, 
                               uint64_t sx, uint64_t sy, uint64_t sz,
                               int w, uint64_t nvols, uint64_t ntimes,
                               int return_noise_map, int perform_resampling);

#endif // JOINTBF4D
