// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// #include "mex.h"
#include <cuda_runtime.h>

#include <stdint.h>

#include "error_chk.h"
#include "jointBF4D.h"

//  Ctypes interface with Python
// v5: Use spatially adaptive noise estimation.

// [ Xf, noise_sig ] = jointBF4D( X, nvols, ntimes, A, At, size, radius, multiplier, gpu_idx, return_noise_map, Xr )
// INPUTS:
//                  X: (float*)    vectorized data to be filtered; 3D volume x volumes to be jointly filter x time points to extend the filtration domain
//                 Xr: (float*)    if size(Xr) == nx*ny*nz*nvols, take resampled values from these volumes; otherwise, compute resampled values with the A, At kernels (defaults to computing resampled values)
//                 Xf: (float*)    pre-allocated memory for storing the filtered results
//                 Xn: (float*)    pre-allocated memory for storing the noise map
//                  A: (float*)    x,y,z factored resampling kernel (expected size: radius x 3)
//                 At: (float*)    temporal resampling weights (vector with ntimes entries; used to combine resampling results between timepoints; sums to 1)
//               size: (uint64_t*) volume size in voxels (x,y,z)
//         multiplier: (float*)    scales filtration strength as this multiple of the estimated noise standard deviation (vector with nvols entries)
//              nvols: (uint64_t)  number of volumes to jointly filter
//             ntimes: (uint64_t)  number of time points along which to extend the filtration domain
//             radius: (int)       filter radius (width = 2*radius + 1)
//            gpu_idx: (int32)     hardware index pointing to the GPU to be used for BF (defaults to the system default)
//   return_noise_map: (int32)     (0) do not return noise map, Xn is a placeholder; (1) return noise map in Xn
// perform_resampling: (int32)     (0) use Xr instead of resampling the data on the GPU; (1) resample on the GPU using A...Xr may be a placeholder
//
// OUTPUTS:
//                 Xf: (single*) vectorized, filtered volumes ( prod(size) x nvols )
//          noise_sig: (single*) returned if return_noise_map == 1; estimated noise standard deviation ( prod(size) x nvols )

extern "C" { // prevent name mangling to make it easy to call this function from a compiled library

    void jointBF4D( const float *X, const float *Xr, float *Xf, float *Xn, const float *A, const float *At, const uint64_t *size, const float *multiplier,
                    uint64_t nvols, uint64_t ntimes, int radius, int gpu_idx, int return_noise_map, int perform_resampling ) {

        sx = size[0];
        sy = size[1];
        sz = size[2];

    // Create arrays of pointers to the beginning of each volume within X and Xf

        Xpts  = (const float**) malloc(ntimes * nvols * sizeof(float*));
        Xfpts = (float**) malloc(nvols * sizeof(float*));
        Xnpts = (float**) malloc(nvols * sizeof(float*));
        Xrpts = (const float**) malloc(nvols * sizeof(float*));

        for (uint64_t i = 0; i < nvols*ntimes; i++) {

            Xpts[i]  = X + i*sx*sy*sz;

        }

        for (uint64_t i = 0; i < nvols; i++) {

            Xfpts[i]                                = Xf + i*sx*sy*sz;
            if ( return_noise_map == 1 )   Xnpts[i] = Xn + i*sx*sy*sz;
            if ( perform_resampling == 0 ) Xrpts[i] = Xr + i*sx*sy*sz;

        }

    // GPU to use for bilateral filtration

        validate_GPU( gpu_idx );

        cudaSetDevice( gpu_idx );

    // Run joint BF on the GPU

        jointBF4D_cuda(Xpts, Xrpts, Xfpts, Xnpts, A, At, multiplier, sx, sy, sz, radius, nvols, ntimes, return_noise_map, perform_resampling);

    // Free memory allocated by malloc

        free(Xpts);
        free(Xfpts);
        free(Xnpts);
        free(Xrpts);

    // Check for outstanding CUDA errors

        check(99);

    // Make sure the device is done executing CUDA code

        cudaDeviceSynchronize();


    }

}

// Make sure the requested GPU is valid before setting the active device

void validate_GPU( int gpu_idx ) {

    // Make sure at least one GPU is visible

        int nDevices_found;

        cudaGetDeviceCount(&nDevices_found);

        if (nDevices_found == 0) {

            ERR("No GPUs found.");

        }

    // Make sure the requested GPU index is legal

        if ( gpu_idx < 0 || gpu_idx > nDevices_found-1 ) {

            ERR("Specified GPU does not exist or is not visible. (NOTE: GPU indexing starts from 0.)");

        }

}



