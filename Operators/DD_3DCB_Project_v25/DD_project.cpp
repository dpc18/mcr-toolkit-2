// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// External headers

    #include "cuda_runtime.h"

    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "cufft.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>
    // #include <inttypes.h>

// Custom headers

    #include "DD_project.h"

// Functions

    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]);              // mex interface
    void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len); // check the validity of argument lists based on their length
    #endif
    
    extern "C" { // Prevent name mangling
    
        void DD_project( float *X, float *Y, uint64_t rp_in, double *weights_in );
    
    }
    
// Functions

// Mex interface layer - Only use mex functions / checks if we are compiling for Matlab
    
    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
        
        // Make sure we have the expected number of inputs / outputs
    
            if (nrhs != 2 && nrhs != 3)
                mexErrMsgTxt("Two [or three] input arguments required! ( Y = DD_project_init(X, recon_alloc, [projection weights]) )");

            if (nlhs != 1)
                mexErrMsgTxt("One output argument required! ( Y = DD_project_init(X, recon_alloc, [projection weights]) )");

        // Check precision of inputs

            // Check the precision of the inputs
            if (!mxIsClass(prhs[0],"single")){ // X
                mexErrMsgTxt("First input argument (volume X) must be single precision.");  
            }

            if (!mxIsClass(prhs[1],"uint64")){ // recon_parameters structure
                mexErrMsgTxt("Second input argument (reconstruction parameter allocation) must be uint64.");  
            }

        // Import values / pointers

            // data volume
            float *X = (float*) mxGetData(prhs[0]);

            // reconstruction allocations / parameters (convert from integer back to a pointer to a parameter object)
            uint64_t rp_in = (uint64_t) mxGetData(prhs[1]);
            recon_parameters *rp0 = (recon_parameters *) *( (uint64_t *) rp_in );
            
            // recon_parameters *rp0 = (recon_parameters *) rp_in;
            
        // Check for inconsistencies / errors

            // Make sure we have not cleared the allocation associated with the provided pointer
            if ( rp0 == NULL ) {

                mexErrMsgTxt("Passed reconstruction parameter allocation appears to have been cleared. Please reinitialize the reconstruction problem.");  

            }

            // Check to make sure the input dimensions match the input data to avoid illegal memory reads/writes.
            check_len(prhs, 1, 0, rp0->nxyz); // const mxArray* prhs, const int arg_num, const int idx, const size_t len

        // If we are using newly assigned weights...
            
            double *weights_in;
  
            if ( nrhs == 3 ) { // we are using newly assigned weights, redirect as needed

                // Check to make sure the new weights have the right precision

                    if (!mxIsClass(prhs[2],"double")){

                        mexErrMsgTxt("Third input argument (projection weights) must be double precision.");  

                    }

                // check to make sure the new weights have the right size

                    check_len(prhs, 3, 2, rp0->np);

                // Assign the Matlab memory to the internal (redirected) weights pointer. We could overwrite the existing weights vector;
                // however, we choose to default back to the initial weights during future calls, since overrided weights will likely change
                // between calls.

                    weights_in = (double*) mxGetData(prhs[2]);

            }
            else { // we are using the initially allocated weights
                
                weights_in = rp0->weights;

            }

        // Allocate output

            const mwSize siz[2] = {rp0->nuvp,1};
            plhs[0] = mxCreateNumericArray(2,siz,mxSINGLE_CLASS,mxREAL);
            float *Y = (float*) mxGetData(plhs[0]);
            
        // Call projection operator
            
            DD_project( X, Y, rp_in, weights_in );
        
    }
    #endif
    
// C / Python (ctypes) interface
// Returns pointer (as an unsigned 64-bit integer) to new recon_params data structure
    
    void DD_project( float *X, float *Y, uint64_t rp_in, double *weights_in ) {
        
        // Debugging
        
        // for (int q = 0; q < 100; q++) {
        // 
        //     printf("X[%i]: %f\n", q, X[q]);
        // 
        // }
        // 
        // for (int q = 0; q < 100; q++) {
        // 
        //     printf("Y[%i]: %f\n", q, Y[q]);
        // 
        // }
        // 
        // printf("recon_alloc: %" PRIu64 "\n", rp_in);
        // 
        // for (int q = 0; q < 100; q++) {
        // 
        //     printf("W[%i]: %f\n", q, weights_in[q]);
        // 
        // }
        
        // Translate inputs to global variables
        
            weights_sub = weights_in;
            
        // reconstruction allocations / parameters (convert from integer back to a pointer to a parameter object)
        
            #ifdef MEX_COMPILE_FLAG
            rp = (recon_parameters *) *( (uint64_t *) rp_in );
            #else
            rp = (recon_parameters *) rp_in;
            #endif
            
            // printf("%p\n", (void *) &rp);
            // printf("%p\n", (void *) &rp);
            
        // Check for inconsistencies / errors
        
            // Make sure we have not cleared the allocation associated with the provided pointer
            // Does this work as intended given the data type conversion?
            if ( rp == NULL ) {

                ERR("Passed reconstruction parameter allocation appears to have been cleared. Please reinitialize the reconstruction problem.");

            }
        
        // Compute the new np_eff for proper normalization.

            np_eff_sub = 0;

            for (int i = 0; i < rp->np; i++) {

                if (weights_sub[i] != 0) {

                    np_eff_sub += weights_sub[i];

                }

            }
     
        // Call projection, output on host ( 0 )
        
            DD_project_cuda( Y, X, 0 );
        
    }

// check the validity of argument lists based on their length
    
    #ifdef MEX_COMPILE_FLAG
    void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len)
    {

        char err_string[200];

        if ( mxGetNumberOfElements(prhs[idx]) != len ) {

            sprintf(err_string, "Input argument %i does not have expected length of %lu.\n", arg_num, len);

            mexErrMsgTxt(err_string);

        }

    }
    #endif