// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_PROJECT
#define DD_PROJECT

    // Mex / C compatible error messages
    #include "error_chk.h"

    // Data structure definitions and required C headers
    #include "DD_v25_data_structures.h"
    
    // Environment variables
    #include "DD_v25_macros.h"

    // Global reference to reconstruction allocation / parameters
    recon_parameters *rp;
    
    // Allow the projection weights and weight sum to be redirected for use with ordered subsets
    double np_eff_sub;
    double *weights_sub;
    
    // External link to compiled CUDA code
    extern void DD_project_cuda( float *Y, float *X, int output_on_GPU );

#endif // DD_PROJECT