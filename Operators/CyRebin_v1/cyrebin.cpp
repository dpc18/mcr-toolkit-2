// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// References

// 1) Approximate, analytical reconstruction of helical data acquired with a cylindrical detector.
//    "Weighted FBP—a simple approximate 3D FBP algorithm for multislice spiral CT with good dose usage for arbitrary pitch"
//     Karl Stierstorfer, Annabella Rauscher, Jan Boese, Herbert Bruder, Stefan Schaller and Thomas Flohr
//     Phys. Med. Biol. 49 (2004) 2209–2218
// 2)  "Image reconstruction and image quality evaluation for a dual source CT scanner"
//     T. G. Flohr, H. Bruder, K. Stierstorfer, M. Petersilka, B. Schmidt, C. H. McCollough
//     Medical Physics, 35(12), 5882-5897.

// External headers

    #include "cuda_runtime.h"

    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "cufft.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>

// Custom headers

    #include "cyrebin.h"

// Function headers

    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]); // mex interface
    void check_len(const mxArray* prhs[], int arg_num, int idx, uint64_t len); // check the validity of argument lists based on their length
    #endif
    
    extern "C" {

        void rebin( const float *Y_A, const float *Y_B, float *Y_b, int *int_params, double *double_params,
                    float *proj_time, float *proj_time_out, int GPU_idx, int det_type );
    
    }

// Functions

// Mex interface layer - Only use mex functions / checks if we are compiling for Matlab
// int_params    = [ nu_A, nu_B, nv, np, r_os, rotdir ];
// double_params = [ Rf, zrot, delta_theta, delta_beta, dv, uc_A, uc_B, dx / r_os, theta_B, scale_A_B, mag ];
// Y_b = cyrebin( Y_A, Y_B, int_params, double_params, GPU_idx, det_type );
// If nu_B = 0, only chain A is rebinned (output Y_b).
// If nu_B > 0, only chain B is rebinned (output Y_b). Y_b is completed with Y_A.
// Y_B must cover a smaller FoV than Y_A (i.e. nu_A > nu_B, du/dv are equal between chains).
// size(Y_A) = [ nu_A, nv, nz ]
// size(Y_B) = [ nu_B, nv, nz ]
// size(Y_b) = [ nu_A * r_os, nv, nz ]
    
    #ifdef MEX_COMPILE_FLAG
    
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {

        // Make sure we have the expected number of inputs / outputs
        
            if (nrhs != 6 && nrhs != 7)
                mexErrMsgTxt("Six or seven input arguments expected! ( [ Y_b, W_t ] = cyrebin( Y_A, Y_B, int_params, double_params, GPU_idx, det_type, [proj_time] ) )");
            
            if (nlhs != 1 && nlhs != 2)
                mexErrMsgTxt("One or two output arguments required! ( [ Y_b, W_t ] = cyrebin( Y_A, Y_B, int_params, double_params, GPU_idx, det_type, [proj_time] ) )");
        
        // Check precision of inputs
        
            // Check the precision of the inputs
            if (!mxIsClass(prhs[0],"single")){ // Y_A
                mexErrMsgTxt("First input argument (projection data Y_A) must be single precision.");  
            }
            
            if (!mxIsClass(prhs[1],"single")){ // Y_B
                mexErrMsgTxt("Second input argument (projection data Y_B) must be single precision.");  
            }
            
            if (!mxIsClass(prhs[2],"int32")){ // int_params
                mexErrMsgTxt("Third input argument (int_params) must be int32 precision.");  
            }

            if (!mxIsClass(prhs[3],"double")){ // double_params
                mexErrMsgTxt("Fourth input argument (double_params) must be double precision.");  
            }
            
            if (!mxIsClass(prhs[4],"int32")){ // GPU_idx
                mexErrMsgTxt("Fifth input argument (GPU_idx) must be int32 precision.");  
            }
            
            if (!mxIsClass(prhs[5],"int32")){ // det_type
                mexErrMsgTxt("Sixth input argument (det_type) must be int32 precision.");  
            }

            if (nrhs > 6) {

                if (!mxIsClass(prhs[6],"single")){ // proj_time
                    mexErrMsgTxt("Seventh input argument (proj_time) must be single precision.");
                }

            }
            
        // Int parameters

            int *int_params = (int*) mxGetPr(prhs[2]);
            int int_params_len = mxGetNumberOfElements(prhs[2]);
            
            // check that the int_params vector is the right length
            check_len(prhs, 3, 2, 6); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len
            
            // Input size parameters
            uint64_t nu_A = int_params[0];
            uint64_t nu_B = int_params[1];
            uint64_t nv   = int_params[2];
            uint64_t np   = int_params[3];
            uint64_t r_os = int_params[4];
            
        // Check assumptions...
            
            if ( nu_A <= nu_B ) {
                
                mexErrMsgTxt("Chain B row length (int_params[1]) expected to be smaller than chain A row length (int_params[0]).");
                
            }
            
            if ( r_os == 0 ) {
                
                mexErrMsgTxt("Resampling factor (r_os, int_params[4]) expected to be >= 1.");
                
            }
            
        // Import values / pointers

            // Projections
            float *Y_A = (float*) mxGetData(prhs[0]);
            check_len(prhs, 1, 0, nu_A*nv*np); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len
            
            float *Y_B;
            
            if ( nu_B > 0 ) {
             
                Y_B = (float*) mxGetData(prhs[1]);
                
                check_len(prhs, 2, 1, nu_B*nv*np); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len
                
            }

            float *proj_time;
            if (nrhs > 6) {

                proj_time = (float*) mxGetData(prhs[6]);

                check_len(prhs, 7, 6, np); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len

            }
            
        // Detector type

            int det_type = (int) mxGetScalar(prhs[5]);
            
            if ( det_type < 0 || det_type > 2 ) {
             
                mexErrMsgTxt("det_type (input 6) expected to be 0 ( flat, cone-beam ), 1 ( cylindrical ), or 2 ( cylindrical, temporal ).");
                
            }
            
            if ( det_type == 0 && nu_B > 0 ) {
             
                mexErrMsgTxt("det_type = 0 (input 6) and nu_B > 0 ( int_params index 2 ): dual-source rebinning for flat, cone-beam geometry not currently supported.");
                
            }

            if ( det_type == 2 && nrhs <= 6 ) {

                mexErrMsgTxt("Seventh input argument (proj_time) required when det_type = 2 (cylindrical, temporal rebinning).");

            }

        // Double parameters

            double *double_params = (double*) mxGetPr(prhs[3]);
            int double_params_len = mxGetNumberOfElements(prhs[3]);
            
            if ( det_type == 1 ) {
                
                // check that the double_params vector is the right length
                // check_len(prhs, 4, 3, 11); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len
                check_len(prhs, 4, 3, 12);
            
            }
            else if (det_type == 2 ) {

                // check that the double_params vector is the right length
                check_len(prhs, 4, 3, 11); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len

            }
            else {
             
                // check that the double_params vector is the right length
                check_len(prhs, 4, 3, 12); // const mxArray* prhs, const int arg_num, const int idx, const uint64_t len
                
            }
            
        // GPU index
            
            int GPU_idx = *((int*) mxGetPr(prhs[4]));
            
            if ( GPU_idx < 0 ) {
             
                mexErrMsgTxt("GPU_idx (input 5) expected to be >=0.");
                
            }
            
        // Allocate output

            const mwSize siz_b[2] = {r_os*nu_A*nv*np,1};
            
            plhs[0] = mxCreateNumericArray(2, siz_b, mxSINGLE_CLASS, mxREAL);
            float *Y_b = (float*) mxGetData(plhs[0]);

            float *proj_time_out;
            if (det_type == 2) {

                const mwSize siz_tout[2] = {r_os*nu_A*nv*np,1};
                plhs[1] = mxCreateNumericArray(2, siz_b, mxSINGLE_CLASS, mxREAL);
                proj_time_out = (float*) mxGetData(plhs[1]);

            }
            
        // Perform rebinning
            
            rebin( Y_A, Y_B, Y_b, int_params, double_params, proj_time, proj_time_out, GPU_idx, det_type );
            
    }

    #endif

// C / Python (ctypes) interface

    void rebin( const float *Y_A, const float *Y_B, float *Y_b, int *int_params, double *double_params, float *proj_time, float *proj_time_out, int GPU_idx, int det_type ) {
            
        // Call rebinning operation
        cy_rebin( Y_A, Y_B, Y_b, int_params, double_params, proj_time, proj_time_out, GPU_idx, det_type );
        
    }

// Check the validity of argument lists based on their length

    #ifdef MEX_COMPILE_FLAG
    void check_len(const mxArray* prhs[], int arg_num, int idx, uint64_t len)
    {

        char err_string[200];

        if ( mxGetNumberOfElements(prhs[idx]) != len ) {

            sprintf(err_string, "Input argument %i does not have expected length of %lu.\n", arg_num, len);

            mexErrMsgTxt(err_string);

        }

    }
    #endif
