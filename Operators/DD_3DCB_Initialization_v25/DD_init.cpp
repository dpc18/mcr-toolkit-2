// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// External headers

    #include "cuda_runtime.h"

    // If we are compiling for matlab...
    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "cufft.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>

// Custom headers

    #include "error_chk.h"              // check for CUDA errors, choose between MATLAB and C error printing

    #include "DD_v25_data_structures.h" // Data structures: Private_GPU_Data, Public_GPU_Data, recon_parameters
    #include "DD_v25_global_vars.h"     // Reconstruction variables to be imported / processed and copied into the recon_parameters structure
    #include "DD_v25_macros.h"          // Environment (constant) variables
    #include "DD_v25_host_init.h"       // Host initialization; imports / initializes geometry and other dervied variables
    #include "DD_v25_multi_GPU_init.h"  // Managing Private_GPU_Data and Public_GPU_Data data structures for multi-GPU computing

    static recon_parameters *recon_params[MAX_RECONS]; // preallocation of reconstruction parameters and buffers
    static unsigned int param_counter = 0;             // keep track of the number of outstanding recon_parameters to clear them
    
// Functions

    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]);                 // mex interface
    void check_len2(const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len); // check the validity of parameter vectors based on length
    #endif
    
    extern "C" { // prevent name mangling
    
        // non-mex interface
        uint64_t DD_init( int *int_params, int int_params_len, double *double_params, int double_params_len, char *filt_name, char *geo_name,
                          float *aff, unsigned short* rmask, int *GPU_indices, int GPU_counter, float *proj_weight_mask_in );

        void clear();                                                                                 // deallocate outstanding recon_parameters
        void reset_devices();                                                                         // break glass in case of emergencies
    
    }
    
// Mex interface layer - Only use mex functions / checks if we are compiling for Matlab
    
    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
     
        // Make sure we have the expected number of inputs / outputs

            // if (nrhs != 8)
            if (nrhs != 7)
                mexErrMsgTxt("Seven input arguments required! ( recon_data = DD_init(int_params, double_params, filt_name, geo_name, Rt_aff, rmask, GPUs)");

            if (nlhs != 1)
                mexErrMsgTxt("One output argument required! ( recon_data = DD_init(int_params, double_params, filt_name, geo_name, Rt_aff, rmask, GPUs)");

        // Check precision of inputs

            // Check the precision of the inputs
            if (!mxIsClass(prhs[0],"int32")){ // int_params
                mexErrMsgTxt("First input argument (integer parameters) must be int32.");  
            }

            if (!mxIsClass(prhs[1],"double")){ // double_params
                mexErrMsgTxt("Second input argument (double parameters) must be double precision.");  
            }

            if (!mxIsClass(prhs[2],"char")){ // filt_name
                mexErrMsgTxt("Third input argument (FDK filter file) must be char.");  
            }

            if (!mxIsClass(prhs[3],"char")){ // geo_name
                mexErrMsgTxt("Fourth input argument (geometry file) must be char.");  
            }

            if (!mxIsClass(prhs[4],"single")){ // aff
                mexErrMsgTxt("Fifth input argument (image domain affine transform) must be single precision.");  
            }

            if (!mxIsClass(prhs[5],"uint16")){ // rmask
                mexErrMsgTxt("Sixth input argument (reconstruction mask) must be uint16 precision.");  
            }

            if (!mxIsClass(prhs[6],"int32")){ // GPUs
                mexErrMsgTxt("Seventh input argument (indices of GPUs to use) must be int32 precision.");  
            }
            
            if (nrhs > 7) { // optional 8th argument - currently unused
            
                if (!mxIsClass(prhs[7],"single")){ // proj_weight_mask
                    mexErrMsgTxt("Eighth input argument (projection weight mask) must be single precision.");  
                }
            
            }
            
        // Int parameters

            int *int_params = (int*) mxGetPr(prhs[0]);
            int int_params_len = mxGetNumberOfElements(prhs[0]);

            // check that the int_params vector is the right length
            check_len2(prhs, 1, 0, 8, 12); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len
            
            // removed use_proj_weight_mask argument
            // check_len2(prhs, 1, 0, 8, 13); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len
            
        // Double parameters

            double *double_params = (double*) mxGetPr(prhs[1]);
            int double_params_len = mxGetNumberOfElements(prhs[1]);

            // check that the double_params vector is the right length
            check_len2(prhs, 2, 1, 14, 17); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len
            
        // Read in FDK file path / name
            
            // filt_name = (char *) malloc( 250*sizeof(char) );
            char *filt_name_mat = mxArrayToString(prhs[2]);

        // Read in geometry file path/name
            
            //geo_name = (char *) malloc( 250*sizeof(char) );
            char *geo_name_mat = mxArrayToString(prhs[3]);

        // Read in spatial domain affine transform
    
            // required argument, so we (perhaps unsafely) assume this index not change
            int use_affine_mat = int_params[7]; // (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
            float * aff_mat;
            
            if (use_affine_mat == 1) {

                aff_mat = (float*) mxGetPr(prhs[4]);

                // check length
                check_len2(prhs, 5, 4, 12, 12); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len

            }
            
        // Read in explicit reconstruction mask
            
            unsigned short* rmask_mat = (unsigned short*) mxGetPr(prhs[5]);

            // explicit_mask == 0 // for any value other than 1 or 2, don't use an explicit mask

            // x,y with z range (good for boxes)
            if (explicit_mask == 1) {
                
                // required/checked arguments, so we (perhaps unsafely) assume these indices will not change
                nx = (size_t)   int_params[3]; // number of voxels in x dimension
                ny = (size_t)   int_params[4]; // number of voxels in y dimension

                // rmask_mat = (unsigned short*) mxGetPr(prhs[5]);

                // check to make sure size matches 2*nx*ny
                check_len2(prhs, 6, 5, 2*nx*ny, 2*nx*ny); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len

            }
            else if (explicit_mask == 2) { // x,y,z mask with z range (good for arbitrary shapes, but slower)
                
                // required/checked arguments, so we (perhaps unsafely) assume these indices will not change
                nx = (size_t)   int_params[3]; // number of voxels in x dimension
                ny = (size_t)   int_params[4]; // number of voxels in y dimension
                nz = (size_t)   int_params[5]; // number of voxels in z dimension

                // rmask_mat = (unsigned short*) mxGetPr(prhs[5]);

                // check to make sure size matches the volume dimensions
                check_len2(prhs, 6, 5, nx*ny*nz + 2*nx*ny, nx*ny*nz + 2*nx*ny); // const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len

            }

            // v24: Removed following the move to managed memory. It may make sense to add this back and test it at some point.
            // find the min and max values within the mask along z to reduce memory copy operations during masked projection operations
            // size_t min_mask = 0;
            // size_t max_mask = nz;
            // if (explicit_mask == 1 || explicit_mask == 2) {
            // 
            //     min_mask = nz;
            //     max_mask = 0;
            // 
            //     size_t temp_min, temp_max;
            // 
            //     for (size_t sub = 0; sub < nx*ny; sub++) {
            // 
            //         temp_min = rmask[sub];
            //         temp_max = rmask[sub + nx*ny];
            // 
            //         if ( temp_max != 0 ) { // ignore indices completely outside of the mask
            // 
            //             if ( temp_min < min_mask ) // find minimum inside of the mask
            //                 min_mask = temp_min;
            // 
            //             if ( temp_max > max_mask ) // find minimum inside of the mask
            //                 max_mask = temp_max; 
            // 
            //         }
            // 
            //     }
            // 
            // }
            
        // Read in GPU indices to use for computation

            int *GPU_indices_in = (int*) mxGetPr(prhs[6]);
            
            int GPU_counter_in = mxGetNumberOfElements(prhs[6]);
            
        // Read projection weight mask
        // This feature has been removed.
            
            float *proj_weight_mask_in;
            int use_proj_weight_mask0 = 0;
            
            
            // if ( int_params_len < 13 ) {
            // 
            //     use_proj_weight_mask0 = 0;
            // 
            // }
            // else {
            // 
            //     use_proj_weight_mask0 = int_params[12];
            // 
            //     if ( use_proj_weight_mask0 < 0 || use_proj_weight_mask0 > 1) {
            // 
            //         mexErrMsgTxt("Projection weight mask flag expected to be 0 (no mask) or 1 (use mask).");
            // 
            //     }
            // 
            // }
            // 
            // if ( use_proj_weight_mask0 == 1 ) {
            // 
            //     proj_weight_mask_in = (float*) mxGetPr(prhs[7]);
            // 
            // }
            
        // Call shared code
            
            uint64_t out_ptr = DD_init( int_params, int_params_len, double_params, double_params_len, filt_name_mat, geo_name_mat, aff_mat, 
                                        rmask_mat, GPU_indices_in, GPU_counter_in, proj_weight_mask_in );
            
        // Allocate output (pointer to our new reconstruction parameter object cast as an unsigned 64-bit integer)

            // assign pointer to the output
            plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
            uint64_t *mex_ptr = (uint64_t *) mxGetPr(plhs[0]);
            *(mex_ptr) = out_ptr;
        
        // Free dynamically allocated mex variables (e.g. mxArrayToString outputs)
            
            // mxFree(geo_name);
            // mxFree(filt_name);

        // Call the clear() function when Matlab is done with this mex function to ensure that all associated host and device memory is freed.

            mexAtExit( clear );
        
    }
    #endif
    
// C / Python (ctypes) interface
// Returns pointer (as an unsigned 64-bit integer) to new recon_params data structure
    
    uint64_t DD_init( int *int_params, int int_params_len, double *double_params, int double_params_len, char *filt_name_in, char *geo_name_in, float *aff_in,
                      unsigned short* rmask_in, int *GPU_indices_in, int GPU_counter, float *proj_weight_mask_in ) {
        
        // Debugging
        
        // printf("\n");
        // 
        // for (int q = 0; q < 12; q++) {
        // 
        //     printf("int_params %i: %i\n", q, int_params[q]);
        // 
        // }
        // 
        // printf("int_params_len %i\n", int_params_len);
        // 
        // for (int q = 0; q < 17; q++) {
        // 
        //     printf("double_params %i: %f\n", q, double_params[q]);
        // 
        // }
        // 
        // printf("double_params_len %i\n", double_params_len);
        // 
        // printf("filt_name_in: %s\n", filt_name_in);
        // 
        // printf("geo_name_in: %s\n", geo_name_in);
        // 
        // for (int q = 0; q < 12; q++) {
        // 
        //     printf("aff_in %i: %f\n", q, aff_in[q]);
        // 
        // }
        // 
        // printf("rmask_in[0]: %hu\n", rmask_in[0]);
        // 
        // for (int q = 0; q < GPU_counter; q++) {
        // 
        //     printf("GPU_list %i: %i\n", q, GPU_indices_in[q]);
        // 
        // }
        // 
        // printf("GPU counter: %i\n",GPU_counter);

        // Copy input variables to corresponding global variables
        // NOTE: These shallow copies should not be used following initialization.
        // All of these variables are deep copied into the reconstruction parameter structure.
        
            filt_name        = filt_name_in;
            geo_name         = geo_name_in;
            aff              = aff_in;
            rmask            = rmask_in;
            GPU_indices      = GPU_indices_in;
            proj_weight_mask = proj_weight_mask_in;
        
        // Unpack integer parameters to global variables
        
            // Required parameters
            nu =            int_params[0]; // number of pixel columns on detector
            nv =            int_params[1]; // number of pixel rows on detector
            np = (size_t)   int_params[2]; // number of projections
            nx = (size_t)   int_params[3]; // number of voxels in x dimension
            ny = (size_t)   int_params[4]; // number of voxels in y dimension
            nz = (size_t)   int_params[5]; // number of voxels in z dimension
            filtration =    int_params[6]; // (1) FBP with specified filter file, (0) simple backprojection 
            use_affine =    int_params[7]; // (0) - affine transform is not used (even if one is provided); (1) - affine transform is used
            
            // Optional parameters
            if (int_params_len > 8) {
                implicit_mask = int_params[8];  // (0) - do not use implicit reconstruction mask; (1) use implicit reconstruction mask
            }
            else {
                implicit_mask = 1;
            }

            if (int_params_len > 9) {
                explicit_mask = int_params[9];  // (0) - do not use explicit reconstruction mask; (1) use explicit reconstruction mask
            }
            else {
                explicit_mask = 0;
            }

            if (int_params_len > 10) {
                cy_detector = int_params[10];  // (0) - detector is flat; (1) detector is cylindrical; (2) cylindrical, rebinned; (3) flat, rebinned; (4) parallel beam
            }
            else {
                cy_detector = 0;
            }

            if (int_params_len > 11) {
                use_norm_vol = int_params[11]; // (0) - do not allocate / use count normalization volume, (1) allocate / use count normalization volume during backprojection
            }
            else {
                use_norm_vol = 0;
            }
            
            // Feature removed
            // if (int_params_len > 12) {
            //     use_proj_weight_mask = int_params[12]; // % (0) do not use a single precision mask to assign backprojection weights per projection pixel; (1) use a backprojection weight mask (e.g. a Tam-Danielson Window)
            // }
            // else {
            //     use_proj_weight_mask = 0;
            // }
            use_proj_weight_mask = 0;
            
        // Check to make sure the integer flags have legal values

            if (filtration != 0 && filtration != 1) {

                ERR("Filtration flag expected to be 0 or 1.");

            }

            if (cy_detector != 0 && cy_detector != 1 && cy_detector != 2 && cy_detector != 3 && cy_detector != 4 && cy_detector != 5) {

                ERR("Cylindrical detector flag expected to be 0 (flat), 1 (cylindrical), 2 (cylindrical, rebinned), 3 (flat rebinned), 4 (parallel beam), 5 (cylindrical, rebinned, temporal).");

            }

            if (implicit_mask != 0 && implicit_mask != 1) {

                ERR("Implicit reconstruction mask flag expected to be 0 or 1.");

            }

            if (explicit_mask != 0 && explicit_mask != 1 && explicit_mask != 2) {

                ERR("Explicit reconstruction mask flag expected to be 0 (no mask), 1 (2*nx*ny mask), or 2 (2*nx*ny + nx*ny*nz mask).");

            }

            if (use_affine != 0 && use_affine != 1) {

                ERR("Use affine flag expected to be 0 or 1.");

            }

            if (use_norm_vol != 0 && use_norm_vol != 1) {

                ERR("Use normalization volume flag expected to be 0 or 1.");

            }
            
            if (use_proj_weight_mask != 0 && use_proj_weight_mask != 1) {

                ERR("Use projection weight mask flag expected to be 0 or 1.");

            }
            
        // Pad the nu dimension to the next power of 2 for FFT operations

            nu_fft =  1 << (int) ceil(log((float) nu)/log(2.0f)); // nu_fft = 1*2^k

        // Unpack double parameters to global variables

            // Required parameters
            du =  double_params[0];  // horizontal pixel pitch (mm)
            dv =  double_params[1];  // vertical pixel pitch (mm)
            dx =  double_params[2];  // voxel size in x dimension
            dy =  double_params[3];  // voxel size in y dimension
            dz =  double_params[4];  // voxel size in z dimension
            zo =  double_params[5];  // initial translation offset on z-axis
            ao =  double_params[6];  // initial angle offset
            lsd = double_params[7];  // length from source to detector (mm)
            lso = double_params[8];  // length from source to object (mm)
            uc =  double_params[9];  // detector center pixel u value
            vc =  double_params[10]; // detector center pixel v value
            eta = double_params[11]; // angle of detector alignment around x-axis
            sig = double_params[12]; // angle of detector alignment around y-axis
            phi = double_params[13]; // angle of detector alignment around z-axis
                
            // Optional parameters
            if (double_params_len > 14) {
                scale = double_params[14];  // scalar volume multiplier
            }
            else {
                scale = 1;
            }

            if (double_params_len > 15) {
                db = double_params[15];  // fan angle increment for cylindrical detector
            }
            else {
                db = 1; // Don't set this to zero, in case we are using a cy detector and forgot to specify this.
            }

            if (double_params_len > 16) {
                limit_angular_range = double_params[16];  // if 0, do not use; if > 0, ignore rays which diverge by more than this absolute distance (in mm) from the central ray at the detector
            }
            else {
                limit_angular_range = 0;
            }
            
        // Check for inconsistencies / errors
                
            // Make sure limit_angular_range is positive, since the absolute distance is used
                
                limit_angular_range = fabs(limit_angular_range);

            // Check to make sure our GPU indices are legal

                // size_t GPU_counter = mxGetNumberOfElements(prhs[6]);

                if ( GPU_counter <= 0 ) {

                    // mexErrMsgTxt("At least one GPU must be specified for use.");
                    
                    ERR("At least one GPU must be specified for use.");

                }

                nDevices = GPU_counter;

            // Check to make sure our GPUs are compatible

                validate_GPUs();

            // Check GPU size parameters 

                // v25: Replace the divisibility by 8 requirement with index checks within GPU kernels.
                // With this change projection padding will no longer be required.
                // This change is made to prevent reprojection inconsistencies when iteratively reconstructing helical data.
                
                // Prevent errors when the grid size does not exactly cover the projections.
                // if( nu % 8 != 0 || nv % 8 != 0) {
                // 
                //     // mexErrMsgTxt("The u and v dimensions of the projection data must be multiples of 8.");
                //     ERR("The u and v dimensions of the projection data must be multiples of 8.");
                // 
                // }

                // Prevent errors when the grid size does not exactly cover the x and y dimensions of the reconstructed volume.
                if( nx % 8 != 0 || ny % 8 != 0) {

                    // mexErrMsgTxt("The x and y dimensions of the volume must be multiples of 8.");
                    ERR("The x and y dimensions of the volume must be multiples of 8.");

                }

            // Check to make sure we can open our geometry and filter text files

                FILE *fp;

                if( ( fp = fopen(geo_name,"r") ) == NULL ) {

                    // mexErrMsgTxt("Cannot open geometry file!");
                    ERR("Cannot open geometry file!");

                }

                fclose(fp);

                if( ( fp = fopen(filt_name,"r") ) == NULL ) {

                    // mexErrMsgTxt("Cannot open FDK filter file!");
                    ERR("Cannot open FDK filter file!");

                }

                fclose(fp);

        // Compute geometric parameters

            initialize_host();
            
        // Allocate a new reconstruction parameter object with the specified parameters.
        // Do this after all of the host allocations to minimize the risk of errors during initialization of the structure.
            
            char err_string[200];

            if (param_counter >= MAX_RECONS) {

                sprintf(err_string,"Hard-wired limit of reconstruction parameter objects reached (current value %d).",MAX_RECONS);

                // mexErrMsgTxt(err_string);
                ERR(err_string);

            }

            recon_params[param_counter] = (recon_parameters *) malloc( sizeof(recon_parameters) );
            
        // Fill the new parameter object

            recon_parameters *rp = recon_params[param_counter];

            // scalars
            rp->nDevices = nDevices;
            rp->cy_detector = cy_detector;
            rp->implicit_mask = implicit_mask;
            rp->explicit_mask = explicit_mask;
            rp->use_affine = use_affine;
            rp->db = db;
            rp->nx = nx;
            rp->ny = ny;
            rp->nz = nz;
            rp->du = du;
            rp->dv = dv;
            rp->dx = dx;
            rp->dy = dy;
            rp->dz = dz;
            rp->zo = zo;
            rp->ao = ao;
            rp->lsd = lsd;
            rp->lso = lso;
            rp->uc = uc;
            rp->vc = vc;
            rp->eta = eta;
            rp->sig = sig;
            rp->phi = phi;
            rp->np = np;
            rp->nu = nu;
            rp->nv = nv;
            rp->nu_fft = nu_fft;
            rp->scale = scale;
            rp->filtration = filtration;
            rp->np_eff = np_eff;
            // rp->min_mask = min_mask;
            // rp->max_mask = max_mask;
            rp->limit_angular_range = limit_angular_range;
            rp->use_norm_vol = use_norm_vol;

            // sizes (number of bytes)
            rp->projs_size = projs_size; // projection stack
            rp->proj_size = proj_size; // projection
            rp->vol_size = vol_size; // volume
            rp->slice_size = slice_size; // slice
            rp->filt_size = filt_size; // filter 
            rp->pms_size = pms_size; // projection matrices for all projections
            rp->vecs_size = vecs_size; // vectors for all projections
            rp->fftproj_size = fftproj_size; // complex fft of projection
            rp->params_size = params_size; // parameter for all projections
            rp->proj3_size = proj3_size; // (x,y,z) for each pixel in projection
            rp->uvs_size = uvs_size; // 1 integer for each projection
            rp->fftslice_size = fftslice_size; // complex fft of padded slice

            // system geometry
            rp->nx2 = nx2; // nx/2
            rp->ny2 = ny2; // ny/2
            rp->nz2 = nz2; // nz/2
            rp->nuv = nuv; // total number of pixels on detector
            rp->nuvp = nuvp; // total number of pixels in projection stack
            rp->nxy = nxy; // total number of voxels in slice
            rp->nxyz = nxyz; // total number of voxels in volume
            rp->n3xyz = n3xyz; // {nx,ny,nz}
            rp->n3xyz1 = n3xyz1; // {nx-1,ny-1,nz-1}
            rp->n3xyz2 = n3xyz2; // {nx/2,ny/2,nz/2}
            rp->d3xyz = d3xyz; // {dx,dy,dz}

            rp->pblock = pblock; // block size for projection operations
            rp->pgrid = pgrid; // grid size for projection operations
            rp->pgrid_fft = pgrid_fft; // grid size for padded FFT operations
            rp->vblock = vblock; // block size for volume operations
            rp->vgrid = vgrid; // grid size for volume operations

            rp->ox = ox; // volume x offset
            rp->oy = oy; // volume y offset
            rp->oz = oz; // volume z offset
            rp->r = r; // radius of sphere containing volume
            rp->dstep = dstep; // size of voxel step for reprojection
            rp->nstep = nstep; // number of steps along diameter
            rp->nstepvox = nstepvox; // number of steps along diameter in terms of voxels
            
            rp->use_proj_weight_mask = use_proj_weight_mask; // flag for backprojection weights by projection pixel

            // host arrays - make deep copies to dissociate this data from Matlab variables which may be cleared or reused?
            rp->GPU_indices = (int *) malloc( nDevices*sizeof(int) );
            memcpy( rp->GPU_indices, GPU_indices, nDevices*sizeof(int) );

            rp->geo_name = (char *) malloc( strlen(geo_name)*sizeof(char) );
            memcpy( rp->geo_name, geo_name, strlen(geo_name)*sizeof(char) );

            rp->filt_name = (char *) malloc( strlen(filt_name)*sizeof(char) );
            memcpy( rp->filt_name, filt_name, strlen(filt_name)*sizeof(char) );

            if ( use_affine == 1 ) {

                rp->aff = (float *) malloc( 12*sizeof(float) );
                memcpy( rp->aff, aff, 12*sizeof(float) );

            }
            else {

                rp->aff = (float *) malloc( sizeof(float) );

            }

            // derived arrays - make shallow copies
            rp->filt = filt; // filter for backprojection
            rp->lsds = lsds; // array of lsd for all projections
            rp->lsos = lsos; // array of lso for all projections
            rp->ucs = ucs; // array of uc for all projections
            rp->vcs = vcs; // array of vc for all projections
            rp->etas = etas; // array of eta for all projections
            rp->sigs = sigs; // array of sig for all projections
            rp->phis = phis; // array of phi for all projections
            rp->weights = weights; // array of weights to apply to projections before performing backprojection, also used elsewhere
            rp->srcs = srcs; // array of source vectors for all projections
            rp->dtvs = dtvs; // array of detector center vectors for all projections
            rp->puvs = puvs; // array of u pixel vectors for all projections
            rp->pvvs = pvvs; // array of v pixel vectors for all projections
            rp->pms = pms; // projection matrices for all projections
            rp->angles = angles; // projection angle (including any offset) to decide which DD operator (xz vs. yz) to use
            rp->pmis = pmis; // projection matrices for all projections in voxel coordinate system
            
        // Check for outstanding CUDA errors before proceeding
            
            check(0);

        // Allocate pinned Host buffers. This potentially uses a lot of extra memory but enables fast,
        // asynchronous data transfers back to the host.

            cudaHostAlloc( (void**) &(rp->Pinned_Buffer), max( vol_size, projs_size ), cudaHostRegisterPortable );

        // Allocate GPU resources

            // check to make sure there are no outstanding CUDA errors before proceeding with device allocations
            
                check(10);

            // Allocate private GPU variables (1 per device and stream)
            // Do this first in case we run into stream allocation errors
                
                rp->GPU_pri = (Private_GPU_Data *) malloc( nDevices*NUMSTREAMS*sizeof(Private_GPU_Data) );

                for (int j = 0; j < nDevices; j++) { // devices

                    cudaSetDevice( GPU_indices[j] );

                    for (int s = 0; s < NUMSTREAMS; s++) { // streams

                        initialize_GPU_private( &(rp->GPU_pri[j*NUMSTREAMS + s]), PROJ_CHUNK, GPU_indices[j] );

                        // check for cuda allocation errors
                        check(0 + j*NUMSTREAMS + s + 1);

                    }

                }

            // Allocate public GPU variables (1 per device)
                
                rp->GPU_pub = (Public_GPU_Data *) malloc( nDevices*sizeof(Public_GPU_Data) );

                for (int j = 0; j < nDevices; j++) { // devices

                    cudaSetDevice( GPU_indices[j] );

                    initialize_GPU_public( &(rp->GPU_pub[j])  );

                    // check for cuda allocation errors
                    check(0 + nDevices*NUMSTREAMS + 1 + j);

                }
                
        // Final check for CUDA errors
            
            check(20);

        // Increment our static, global counter so we can free all allocated data later.

            param_counter++;
            
        // Free allocated arrays
            
            // free(err_string); 
            
        // Return a reference to this new reconstruction parameter object as an unsigned 64-bit integer 
            
            return (uint64_t) recon_params[param_counter - 1];
            
    }

// Clear stored variables using mexAtExit() (i.e. when "clear mex" or "clear DD_init" are called from Matlab),
// or by explicitly calling the clear() function.
    
    void clear()
    {

        recon_parameters *rp;

        Private_GPU_Data *GPUpr;
        Public_GPU_Data  *GPUpu;

        // for each allocated set of reconstruction parameters
        for (unsigned int i = 0; i < param_counter; i++) {

            rp = recon_params[i];

            // free host parameter arrays
            free( rp->aff );
            free( rp->geo_name );
            free( rp->filt_name );
            free( rp->filt );
            free( rp->lsds );
            free( rp->lsos );
            free( rp->ucs );
            free( rp->vcs );
            free( rp->etas );
            free( rp->sigs );
            free( rp->phis );
            free( rp->weights );
            free( rp->srcs );
            free( rp->dtvs );
            free( rp->puvs );
            free( rp->pvvs );
            free( rp->pms );
            free( rp->angles );
            free( rp->pmis );

            // free pinned memory
            cudaFreeHost( rp->Pinned_Buffer );

            // free public GPU arrays
            GPUpu = rp->GPU_pub;
            for (int j = 0; j < rp->nDevices; j++) {

                cudaSetDevice( rp->GPU_indices[j] );

                cudaFree( GPUpu[j].proj_weights_d );
                
                cudaFree( GPUpu[j].aff_ );
                cudaFree( GPUpu[j].pm_ );
                cudaFree( GPUpu[j].filt_ );
                cudaFree( GPUpu[j].rmask_header_ );
                
                // v24: moved to public memory
                cudaFree( GPUpu[j].vol_ );
                cudaFree( GPUpu[j].proj_ );
                cudaFree( GPUpu[j].rmask_ );
                cudaFree( GPUpu[j].norm_vol_ );
                
                // v25
                cudaFree( GPUpu[j].proj_weight_mask_ );

            }

            free( rp->GPU_pub );

            // free private GPU arrays
            for (int j = 0; j < rp->nDevices; j++) { // device

                cudaSetDevice( rp->GPU_indices[j] );

                for (int s = 0; s < NUMSTREAMS; s++) { // stream

                    GPUpr = &(rp->GPU_pri[j*NUMSTREAMS + s]);

                    cudaFree( GPUpr->projs_temp_ );
                    cudaFree( GPUpr->fftproj_ );

                    cufftDestroy( GPUpr->fftplan );
                    cufftDestroy( GPUpr->ifftplan );

                    cudaStreamDestroy( GPUpr->mystream );

                }

            }

            free( rp->GPU_pri );
            free( rp->GPU_indices );
            free( rp );

            // Set the pointer to NULL to prevent trying to use it after it has been cleared
            recon_params[i] = NULL;

        }
        
        // reset param_counter so that we can create new recon_params, if needed
        param_counter = 0;

    }

// Utility function for resetting the GPU(s) if / when something goes wrong.
// NOTE: This will immediately invalidate all GPU memory allocations, including outstanding recon_parameter allocations.
    
    void reset_devices() {
     
        // Number of CUDA devices
        int devCount;
        cudaGetDeviceCount(&devCount);
        
        int i;
        
        for (i = 0; i < devCount; i++) {
            
            cudaSetDevice(i);
            cudaDeviceReset();
            
        }
                
        printf("\nAll %d devices reset...\n", i);
        
    }
    
// check the validity of argument lists based on their length
    
    #ifdef MEX_COMPILE_FLAG
    void check_len2(const mxArray* prhs[], int arg_num, int idx, size_t min_len, size_t max_len)
    {
    
        char err_string[200];
        
        size_t actual_len = mxGetNumberOfElements(prhs[idx]);
    
        if ( actual_len < min_len || actual_len > max_len ) {
    
            sprintf(err_string, "Input argument %i does not have expected length (between %lu and %lu).\n", arg_num, min_len, max_len);
    
            mexErrMsgTxt(err_string);
    
        }
        
        // free(err_string);
    
    }
    #endif
    
