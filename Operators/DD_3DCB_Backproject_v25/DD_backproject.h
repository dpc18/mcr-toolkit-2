// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef DD_BACKPROJECT_INIT
#define DD_BACKPROJECT_INIT

    // Mex / C compatible error messages
    #include "error_chk.h"

    // Data structure definitions and required C headers
    #include "DD_v25_data_structures.h"
    
    // Environment variables
    #include "DD_v25_macros.h"

    // Global reference to reconstruction allocation / parameters
    recon_parameters *rp;
    
    // Allow the projection weights and weight sum to be redirected for use with ordered subsets
    int    perform_filtration; // 
    double np_eff_sub;         // Sum of weights for a subset.
    double *weights_sub;       // Projection weights for a subset.
    double np_sub;             // Assumed number of projections which contribute to each voxel. May be violated when performing helical reconstruction.
    
    // External link to compiled CUDA code
    extern void DD_backproject_cuda( float *X, float *Y, int output_on_GPU );

#endif // DD_BACKPROJECT_INIT
