// // Copyright (C) 2021, Quantitative Imaging and Analysis Lab, Duke University (https://sites.duke.edu/qial/)
// // Original Author: Darin Clark, PhD
// // 
// // This program is free software: you can redistribute it and/or modify
// // it under the terms of the GNU General Public License as published by
// // the Free Software Foundation, either version 3 of the License, or
// // (at your option) any later version.
// // 
// // This program is distributed in the hope that it will be useful,
// // but WITHOUT ANY WARRANTY; without even the implied warranty of
// // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// // GNU General Public License for more details.
// //  
// // You should have received a copy of the GNU General Public License
// // along with this program.  If not, see <https://www.gnu.org/licenses/>.

// External headers

    #include "cuda_runtime.h"

    #ifdef MEX_COMPILE_FLAG
    #include "mex.h"
    #endif

    #include "cufft.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #include <stdint.h>

// Custom headers

    #include "DD_backproject.h"

// Function headers

    #ifdef MEX_COMPILE_FLAG
    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]); // mex interface
    void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len); // check the validity of argument lists based on their length
    #endif
    
    extern "C" { 

        void DD_backproject( float *Y, float *X, uint64_t rp_in, int perform_filtration_in, double *weights_in );
    
    }

// Functions

// Mex interface layer - Only use mex functions / checks if we are compiling for Matlab
    
    #ifdef MEX_COMPILE_FLAG

    void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {

        // Make sure we have the expected number of inputs / outputs
    
            if (nrhs < 2 || nrhs > 4)
                mexErrMsgTxt("Between two and four input arguments expected! ( X = DD_backproject_init(Y, recon_alloc, [use_filtration], [projection weights]) )");

            if (nlhs != 1)
                mexErrMsgTxt("One output argument required! ( X = DD_backproject_init(Y, recon_alloc, [use_filtration], [projection weights]) )");
        
        // Check precision of inputs
        
            // Check the precision of the inputs
            if (!mxIsClass(prhs[0],"single")){ // Y
                mexErrMsgTxt("First input argument (projection data Y) must be single precision.");  
            }

            if (!mxIsClass(prhs[1],"uint64")){ // recon_parameters structure
                mexErrMsgTxt("Second input argument (reconstruction parameter allocation) must be uint64.");  
            }
            
        // Import values / pointers

            // projections
            float *Y = (float*) mxGetData(prhs[0]);
            
            // recon parameter as unsigned 64-bit int
            uint64_t rp_in = (uint64_t) mxGetData(prhs[1]);
            recon_parameters *rp0 = (recon_parameters *) *( (uint64_t *) rp_in );
            // recon_parameters *rp0 = (recon_parameters *) rp_in;
            
        // Check for inconsistencies / errors
        
            // Make sure we have not cleared the allocation associated with the provided pointer
            // Does this work as intended given the data type conversion?
            if ( rp0 == NULL ) {

                mexErrMsgTxt("Passed reconstruction parameter allocation appears to have been cleared. Please reinitialize the reconstruction problem.");

            }
            
            // Check to make sure the input dimensions match the input data to avoid illegal memory reads/writes.
            check_len(prhs, 1, 0, rp0->nuvp); // const mxArray* prhs, const int arg_num, const int idx, const size_t len
            
        // Allocate output
            
            const mwSize siz[2] = {rp0->nxyz,1};
            plhs[0] = mxCreateNumericArray(2, siz, mxSINGLE_CLASS, mxREAL);
            float *X = (float*) mxGetData(plhs[0]);
            
        // Filtration override
            
            int perform_filtration_in;
        
            if ( nrhs >= 3 ) {

                // Check to make sure the filtration flag has the right precision

                    if (!mxIsClass(prhs[2],"int32")){

                        mexErrMsgTxt("Third input argument (filtration flag) must be int32.");  

                    }

                // Check to make sure the filtration flag has a valid value

                    perform_filtration_in = (int) mxGetScalar(prhs[2]);

                    if ( perform_filtration_in != 0 && perform_filtration_in != 1 ) {

                        mexErrMsgTxt("Third input argument (filtration flag) must be zero or one.");

                    }

                // Check to make sure a filter has been specified and allocated during initialization

                    if ( rp0->filtration == 0 && perform_filtration_in == 1 ) {

                        mexErrMsgTxt("Cannot perform filtration. No filter was allocated during initialization.");

                    }

            }
            
        // If we are using newly assigned weights...
            
            if ( nrhs == 4 && rp0->cy_detector == 5 ) {
             
                mexErrMsgTxt("Currently, weights (phases from 0-1) must be assigned from the geometry files when doing cylindrical reconstruction with rebinned data and temporal weighting (cy_detector == 5).");
                
            }
            
            double *weights_in;
  
            if ( nrhs == 4 ) { // we are using newly assigned weights, redirect as needed

                // Check to make sure the new weights have the right precision

                    if (!mxIsClass(prhs[3],"double")){

                        mexErrMsgTxt("Fourth input argument (projection weights) must be double precision.");  

                    }

                // check to make sure the new weights have the right size

                    check_len(prhs, 4, 3, rp0->np);

                // Assign the Matlab memory to the internal (redirected) weights pointer. We could overwrite the existing weights vector;
                // however, we choose to default back to the initial weights during future calls, since overrided weights will likely change
                // between calls.

                    weights_in = (double*) mxGetData(prhs[3]);

            }
            else { // we are using the initially allocated weights
                
                weights_in = rp0->weights;

            }
            
        // Perform backprojection
            
            DD_backproject( Y, X, rp_in, perform_filtration_in, weights_in );
            
    }

    #endif

// C / Python (ctypes) interface
// Returns pointer (as an unsigned 64-bit integer) to new recon_params data structure

    void DD_backproject( float *Y, float *X, uint64_t rp_in, int perform_filtration_in, double *weights_in ) {
        
        // Translate inputs to global variables
        
            perform_filtration = perform_filtration_in;
            weights_sub = weights_in;
            
        // reconstruction allocations / parameters (convert from integer back to a pointer to a parameter object)
        
            #ifdef MEX_COMPILE_FLAG
            rp = (recon_parameters *) *( (uint64_t *) rp_in );
            #else
            rp = (recon_parameters *) rp_in;
            #endif
            
        // Check for inconsistencies / errors
        
            // Make sure we have not cleared the allocation associated with the provided pointer
            // Does this work as intended given the data type conversion?
            if ( rp == NULL ) {

                ERR("Passed reconstruction parameter allocation appears to have been cleared. Please reinitialize the reconstruction problem.");

            }
        
        // Compute the new np_eff for proper normalization.

            np_eff_sub = 0;
            np_sub = 0;

            for (int i = 0; i < rp->np; i++) {

                if (weights_sub[i] != 0) {

                    np_eff_sub += weights_sub[i];

                    // We only need this when working with a normalization volume...
                    // This probably isn't needed any more.
                    // No longer used in GPU_support_functions.h=>volume_normalization().
                    // if ( rp->use_norm_vol == 1 ) {

                    //    np_sub += 1;

                    // }

                }

            }
            
        // Call backprojection
        
            DD_backproject_cuda( X, Y, 0 );
        
    }

// Check the validity of argument lists based on their length

    #ifdef MEX_COMPILE_FLAG
    void check_len(const mxArray* prhs[], const int arg_num, const int idx, const size_t len)
    {

        char err_string[200];

        if ( mxGetNumberOfElements(prhs[idx]) != len ) {

            sprintf(err_string, "Input argument %i does not have expected length of %lu.\n", arg_num, len);

            mexErrMsgTxt(err_string);

        }

    }
    #endif
