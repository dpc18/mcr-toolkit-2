# TO DO: Add explicit header dependencies.
# Until then, make with the "--always-make" option to override date checks.
# Assumes the code is compiled from within the 'Operators' sub-directory

# Inputs
vXX        = v25 # operator version number
cy_vX      = v1  # CyRebin version number
BF_vX      = v5  # Bilateral filter (BF) version number

CC         = /usr/bin/gcc
CUDA_LIBS  = -lcudart -lcufft -lcublas -lgomp
CUDA_LIBS2 = -L/usr/lib/x86_64-linux-gnu/   
CUDA_I     = -I/usr/include/x86_64-linux-gnu/
CU_FLAGS   = -ccbin $(CC) --compiler-options="-fPIC" -arch=sm_61 
CUSTOM_I   = -I../Include/
CFLAGS     = -fPIC -Wall -Wextra -O2 -g
LDFLAGS    = -shared

# Append version numbers
INIT  := $(addsuffix $(vXX)  , DD_3DCB_Initialization_)
BACK  := $(addsuffix $(vXX)  , DD_3DCB_Backproject_)
PROJ  := $(addsuffix $(vXX)  , DD_3DCB_Project_)
REBIN := $(addsuffix $(cy_vX), CyRebin_)
BF    := $(addsuffix $(BF_vX), jointBF_4D_)

# Program entry points
INIT_CPP  := $(addsuffix /DD_init.cpp,$(INIT))
BACK_CPP  := $(addsuffix /DD_backproject.cpp,$(BACK))
PROJ_CPP  := $(addsuffix /DD_project.cpp,$(PROJ))
REBIN_CPP := $(addsuffix /cyrebin.cpp,$(REBIN))
MI_CPP    := MI/MI.cpp
BF_CPP    := $(addsuffix /jointBF4D.cpp,$(BF))

# Program entry points - C object code
INIT_OBJ     := $(addsuffix /DD_init.o,$(INIT))
BACK_OBJ     := $(addsuffix /DD_backproject.o,$(BACK))
PROJ_OBJ     := $(addsuffix /DD_project.o,$(PROJ))
REBIN_OBJ    := $(addsuffix /cyrebin.o,$(REBIN))
MI_OBJ       := MI/MI.o
BF_OBJ       := $(addsuffix /jointBF4D.o,$(BF))

# CUDA kernels
BACK_CU  := $(addsuffix /DD_backproject_cuda.cu,$(BACK))
PROJ_CU  := $(addsuffix /DD_project_cuda.cu,$(PROJ))
REBIN_CU := $(addsuffix /cyrebin_cuda.cu,$(REBIN))
BF_CU    := $(addsuffix /jointBF4D_cuda.cu,$(BF))
BF_H     := $(addsuffix /jointBF4D_cuda.h,$(BF))

# CUDA kernel object code
BACK_OBJ_CU  := $(addsuffix /DD_backproject_cuda.o,$(BACK))
PROJ_OBJ_CU  := $(addsuffix /DD_project_cuda.o,$(PROJ))
REBIN_OBJ_CU := $(addsuffix /cyrebin_cuda.o,$(REBIN))
BF_OBJ_CU    := $(addsuffix /jointBF4D_cuda.o,$(BF))

# Output dynamic library
OUT := $(addprefix $(addprefix DD_operators_,$(vXX)),.so)


# Output Python reconstruction operator library
# NOTE: Must incorporate the host object file and the CUDA object file for each GPU operator!

$(OUT)      : $(INIT_OBJ) $(BACK_OBJ) $(PROJ_OBJ) $(BF_OBJ) $(REBIN_OBJ) $(BACK_OBJ_CU) $(PROJ_OBJ_CU) $(REBIN_OBJ_CU) $(BF_OBJ_CU) $(MI_OBJ)
	$(CC) ${LDFLAGS} -z muldefs -o $@ $^ $(CFLAGS) $(CUSTOM_I) $(CUDA_I) $(CUDA_LIBS) $(CUDA_LIBS2) -lstdc++


# Intermediate files (add a C interface to each CUDA function)

$(INIT_OBJ)  : $(INIT_CPP)
	$(CC) -c -o $@ $^ $(CFLAGS) $(CUDA_I) $(CUSTOM_I) $(CUDA_LIBS) $(CUDA_LIBS2)

$(BACK_OBJ)  : $(BACK_CPP)  $(BACK_OBJ_CU)
	$(CC) -c -o $@ $^ $(CFLAGS) $(CUDA_I) $(CUSTOM_I) $(CUDA_LIBS) $(CUDA_LIBS2)

$(PROJ_OBJ)  : $(PROJ_CPP)  $(PROJ_OBJ_CU)
	$(CC) -c -o $@ $^ $(CFLAGS) $(CUDA_I) $(CUSTOM_I) $(CUDA_LIBS) $(CUDA_LIBS2)

$(REBIN_OBJ) : $(REBIN_CPP) $(REBIN_OBJ_CU)
	$(CC) -c -o $@ $^ $(CFLAGS) $(CUDA_I) $(CUSTOM_I) $(CUDA_LIBS) $(CUDA_LIBS2)
	
$(BF_OBJ)    : $(BF_CPP)    $(BF_OBJ_CU)
	$(CC) -c -o $@ $^ $(CFLAGS) $(CUDA_I) $(CUSTOM_I) $(CUDA_LIBS) $(CUDA_LIBS2)

	
# Support functions: Mutual Information
$(MI_OBJ) : $(MI_CPP)
	$(CC) -c -o $@ $^ $(CFLAGS)


# CUDA files => object code

$(BACK_OBJ_CU) : $(BACK_CU)
	nvcc -ftz=true --use_fast_math -Xptxas -dlcm=ca -c -o $@ $^ $(CU_FLAGS) $(CUDA_LIBS) $(CUDA_I) $(CUSTOM_I) --compiler-options -fopenmp --ptxas-options=-v

$(PROJ_OBJ_CU) : $(PROJ_CU)
	nvcc -ftz=true --use_fast_math -Xptxas -dlcm=ca -c -o $@ $^ $(CU_FLAGS) $(CUDA_LIBS) $(CUDA_I) $(CUSTOM_I) --compiler-options -fopenmp --ptxas-options=-v

$(REBIN_OBJ_CU): $(REBIN_CU)
	nvcc -ftz=true --use_fast_math -Xptxas -dlcm=ca -c -o $@ $^ $(CU_FLAGS) $(CUDA_LIBS) $(CUDA_I) $(CUSTOM_I) --compiler-options -fopenmp --ptxas-options=-v
	
$(BF_OBJ_CU)   : $(BF_CU)
	nvcc -ftz=true --use_fast_math -Xptxas -dlcm=ca -c -o $@ $^ $(CU_FLAGS) $(CUDA_LIBS) $(CUDA_I) $(CUSTOM_I) --compiler-options -fopenmp --ptxas-options=-v

# Clean
